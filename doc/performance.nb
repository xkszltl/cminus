(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     14869,        376]
NotebookOptionsPosition[     14537,        359]
NotebookOutlinePosition[     14893,        375]
CellTagsIndexPosition[     14850,        372]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"f", "[", "A_", "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "}"}], ",", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"i", "-", "1"}], ",", 
         RowBox[{
          FractionBox[
           RowBox[{"A", "[", 
            RowBox[{"[", "i", "]"}], "]"}], 
           RowBox[{"A", "[", 
            RowBox[{"[", "1", "]"}], "]"}]], "*", "100"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", 
         RowBox[{"Length", "[", "A", "]"}]}], "}"}]}], "]"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   SubscriptBox["A", "queens"], "=", 
   RowBox[{"f", "[", 
    RowBox[{"{", 
     RowBox[{
     "1174341", ",", "847399", ",", "837810", ",", "837810", ",", "815466", 
      ",", "815466"}], "}"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   SubscriptBox["A", "treap"], "=", 
   RowBox[{"f", "[", 
    RowBox[{"{", 
     RowBox[{
     "7319871", ",", "6542396", ",", "6538394", ",", "6534392", ",", 
      "6526390", ",", "5776130"}], "}"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   SubscriptBox["A", "maxflow"], "=", 
   RowBox[{"f", "[", 
    RowBox[{"{", 
     RowBox[{
     "16955301", ",", "11934961", ",", "11934961", ",", "11934959", ",", 
      "11096063", ",", "11096063"}], "}"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   SubscriptBox["A", "loop"], "=", 
   RowBox[{"f", "[", 
    RowBox[{"{", 
     RowBox[{
     "5129740", ",", "2479006", ",", "2345926", ",", "2341606", ",", 
      "2284180", ",", "2284180"}], "}"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   SubscriptBox["A", "qsort"], "=", 
   RowBox[{"f", "[", 
    RowBox[{"{", 
     RowBox[{
     "3896736", ",", "2304873", ",", "2304873", ",", "2294873", ",", 
      "2149421", ",", "2149421"}], "}"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"ListLinePlot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     SubscriptBox["A", "queens"], ",", 
     SubscriptBox["A", "treap"], ",", 
     SubscriptBox["A", "maxflow"], ",", 
     SubscriptBox["A", "loop"], ",", 
     SubscriptBox["A", "qsort"]}], "}"}], ",", 
   RowBox[{"PlotLegends", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
     "\"\<N-Queen\>\"", ",", "\"\<Treap\>\"", ",", "\"\<Maxflow\>\"", ",", 
      "\"\<Nesting Loops\>\"", ",", "\"\<Qsort\>\""}], "}"}]}], ",", 
   RowBox[{"PlotLabel", "\[Rule]", 
    RowBox[{"Style", "[", 
     RowBox[{"\"\<Performance\>\"", ",", "Bold"}], "]"}]}], ",", 
   RowBox[{"AxesOrigin", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0"}], "}"}]}], ",", 
   RowBox[{"AxesLabel", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{",", "\"\<(%)\>\""}], "}"}]}], ",", 
   RowBox[{"PlotStyle", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"Orange", ",", "Thick"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"Red", ",", "Thick"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"Darker", "[", "Green", "]"}], ",", "Thick"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"Blue", ",", "Thick"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"Purple", ",", "Thick"}], "}"}]}], "}"}]}]}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Export", "[", 
   RowBox[{"\"\<Pictures/performance.eps\>\"", ",", "%"}], "]"}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.610248854771995*^9, 3.610249050300908*^9}, {
  3.6102490968569098`*^9, 3.610249102397429*^9}, {3.610281938488556*^9, 
  3.610281990095044*^9}, {3.610282730485239*^9, 3.610283083821542*^9}, {
  3.6102831223003263`*^9, 3.61028323816672*^9}, {3.610283689122609*^9, 
  3.610283797640038*^9}, {3.6102838317307177`*^9, 3.610283840634451*^9}, {
  3.610283936748826*^9, 3.610283964700758*^9}, {3.610284013500078*^9, 
  3.610284075136353*^9}, {3.610284111622349*^9, 3.610284391840968*^9}, {
  3.610287878777388*^9, 3.6102879661907043`*^9}}],

Cell[BoxData[
 TemplateBox[{GraphicsBox[{{{}, {{}, {}, {
        RGBColor[1, 0.5, 0], 
        Thickness[Large], 
        LineBox[{{0., 100.}, {1., 72.15953458152275}, {2., 
         71.34299151609285}, {3., 71.34299151609285}, {4., 
         69.44030737239014}, {5., 69.44030737239014}}]}, {
        RGBColor[1, 0, 0], 
        Thickness[Large], 
        LineBox[{{0., 100.}, {1., 89.3785696496564}, {2., 89.3238965550076}, {
         3., 89.2692234603588}, {4., 89.15990459394708}, {5., 
         78.91027041323542}}]}, {
        RGBColor[0, 
         NCache[
          Rational[2, 3], 0.6666666666666666], 0], 
        Thickness[Large], 
        LineBox[{{0., 100.}, {1., 70.3907350273522}, {2., 70.3907350273522}, {
         3., 70.39072323163121}, {4., 65.44303165128122}, {5., 
         65.44303165128122}}]}, {
        RGBColor[0, 0, 1], 
        Thickness[Large], 
        LineBox[{{0., 100.}, {1., 48.32615298241237}, {2., 
         45.73186945147317}, {3., 45.64765465696117}, {4., 
         44.52818271491343}, {5., 44.52818271491343}}]}, {
        RGBColor[0.5, 0, 0.5], 
        Thickness[Large], 
        LineBox[{{0., 100.}, {1., 59.14881069695252}, {2., 
         59.14881069695252}, {3., 58.892185665131024`}, {4., 
         55.159523252280884`}, {5., 55.159523252280884`}}]}}, {}}, {}}, 
    AspectRatio -> NCache[GoldenRatio^(-1), 0.6180339887498948], Axes -> True,
     AxesLabel -> {"", 
      FormBox["\"(%)\"", TraditionalForm]}, AxesOrigin -> {0, 0}, 
    Method -> {}, PlotLabel -> FormBox[
      StyleBox["\"Performance\"", Bold, StripOnInput -> False], 
      TraditionalForm], PlotRange -> {{0, 5.}, {0, 100.}}, PlotRangeClipping -> 
    True, PlotRangePadding -> {{0.1, 0.1}, {2., 2.}}],
   TemplateBox[{
    "\"N-Queen\"", "\"Treap\"", "\"Maxflow\"", "\"Nesting Loops\"", 
     "\"Qsort\""}, "LineLegend", DisplayFunction -> (StyleBox[
      StyleBox[
       PaneBox[
        TagBox[
         GridBox[{{
            TagBox[
             GridBox[{{
                GraphicsBox[{{
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[1, 0.5, 0], 
                    Thickness[Large]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[1, 0.5, 0], 
                    Thickness[Large]], {}}}, AspectRatio -> Full, 
                 ImageSize -> {20, 10}, PlotRangePadding -> None, 
                 ImagePadding -> 1, 
                 BaselinePosition -> (Scaled[0.1] -> Baseline)], #}, {
                GraphicsBox[{{
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[1, 0, 0], 
                    Thickness[Large]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[1, 0, 0], 
                    Thickness[Large]], {}}}, AspectRatio -> Full, 
                 ImageSize -> {20, 10}, PlotRangePadding -> None, 
                 ImagePadding -> 1, 
                 BaselinePosition -> (Scaled[0.1] -> Baseline)], #2}, {
                GraphicsBox[{{
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[0, 
                    NCache[
                    Rational[2, 3], 0.6666666666666666], 0], 
                    Thickness[Large]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[0, 
                    NCache[
                    Rational[2, 3], 0.6666666666666666], 0], 
                    Thickness[Large]], {}}}, AspectRatio -> Full, 
                 ImageSize -> {20, 10}, PlotRangePadding -> None, 
                 ImagePadding -> 1, 
                 BaselinePosition -> (Scaled[0.1] -> Baseline)], #3}, {
                GraphicsBox[{{
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[0, 0, 1], 
                    Thickness[Large]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[0, 0, 1], 
                    Thickness[Large]], {}}}, AspectRatio -> Full, 
                 ImageSize -> {20, 10}, PlotRangePadding -> None, 
                 ImagePadding -> 1, 
                 BaselinePosition -> (Scaled[0.1] -> Baseline)], #4}, {
                GraphicsBox[{{
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[0.5, 0, 0.5], 
                    Thickness[Large]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                   Directive[
                    EdgeForm[{
                    Opacity[0.3], 
                    GrayLevel[0]}], 
                    RGBColor[0.5, 0, 0.5], 
                    Thickness[Large]], {}}}, AspectRatio -> Full, 
                 ImageSize -> {20, 10}, PlotRangePadding -> None, 
                 ImagePadding -> 1, 
                 BaselinePosition -> (Scaled[0.1] -> Baseline)], #5}}, 
              GridBoxAlignment -> {
               "Columns" -> {Center, Left}, "Rows" -> {{Baseline}}}, 
              AutoDelete -> False, 
              GridBoxDividers -> {
               "Columns" -> {{False}}, "Rows" -> {{False}}}, 
              GridBoxItemSize -> {"Columns" -> {{All}}, "Rows" -> {{All}}}, 
              GridBoxSpacings -> {"Columns" -> {{0.5}}, "Rows" -> {{0.8}}}], 
             "Grid"]}}, 
          GridBoxAlignment -> {"Columns" -> {{Left}}, "Rows" -> {{Top}}}, 
          AutoDelete -> False, 
          GridBoxItemSize -> {
           "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
          GridBoxSpacings -> {"Columns" -> {{1}}, "Rows" -> {{0}}}], "Grid"], 
        Alignment -> Left, AppearanceElements -> None, 
        ImageMargins -> {{5, 5}, {5, 5}}, ImageSizeAction -> "ResizeToFit"], 
       LineIndent -> 0, StripOnInput -> False], {FontFamily -> "Times"}, 
      Background -> Automatic, StripOnInput -> False]& ), Editable -> True, 
    InterpretationFunction :> (RowBox[{"LineLegend", "[", 
       RowBox[{
         RowBox[{"{", 
           RowBox[{
             RowBox[{"Directive", "[", 
               RowBox[{
                 RowBox[{"RGBColor", "[", 
                   RowBox[{"1", ",", "0.5`", ",", "0"}], "]"}], ",", 
                 RowBox[{"Thickness", "[", "Large", "]"}]}], "]"}], ",", 
             RowBox[{"Directive", "[", 
               RowBox[{
                 RowBox[{"RGBColor", "[", 
                   RowBox[{"1", ",", "0", ",", "0"}], "]"}], ",", 
                 RowBox[{"Thickness", "[", "Large", "]"}]}], "]"}], ",", 
             RowBox[{"Directive", "[", 
               RowBox[{
                 RowBox[{"RGBColor", "[", 
                   RowBox[{"0", ",", 
                    FractionBox["2", "3"], ",", "0"}], "]"}], ",", 
                 RowBox[{"Thickness", "[", "Large", "]"}]}], "]"}], ",", 
             RowBox[{"Directive", "[", 
               RowBox[{
                 RowBox[{"RGBColor", "[", 
                   RowBox[{"0", ",", "0", ",", "1"}], "]"}], ",", 
                 RowBox[{"Thickness", "[", "Large", "]"}]}], "]"}], ",", 
             RowBox[{"Directive", "[", 
               RowBox[{
                 RowBox[{"RGBColor", "[", 
                   RowBox[{"0.5`", ",", "0", ",", "0.5`"}], "]"}], ",", 
                 RowBox[{"Thickness", "[", "Large", "]"}]}], "]"}]}], "}"}], 
         ",", 
         RowBox[{"{", 
           RowBox[{#, ",", #2, ",", #3, ",", #4, ",", #5}], "}"}], ",", 
         RowBox[{"LegendLayout", "\[Rule]", "\"Column\""}], ",", 
         RowBox[{"LegendMarkers", "\[Rule]", "False"}]}], "]"}]& )]},
  "Legended",
  DisplayFunction->(GridBox[{{
      TagBox[
       ItemBox[
        PaneBox[
         TagBox[#, "SkipImageSizeLevel"], Alignment -> {Center, Baseline}, 
         BaselinePosition -> Baseline], DefaultBaseStyle -> "Labeled"], 
       "SkipImageSizeLevel"], 
      ItemBox[#2, DefaultBaseStyle -> "LabeledLabel"]}}, 
    GridBoxAlignment -> {"Columns" -> {{Center}}, "Rows" -> {{Center}}}, 
    AutoDelete -> False, GridBoxItemSize -> Automatic, 
    BaselinePosition -> {1, 1}]& ),
  Editable->True,
  InterpretationFunction->(RowBox[{"Legended", "[", 
     RowBox[{#, ",", 
       RowBox[{"Placed", "[", 
         RowBox[{#2, ",", "After"}], "]"}]}], "]"}]& )]], "Output",
 CellChangeTimes->{
  3.6102489547506657`*^9, 3.61024900893125*^9, 3.610249051300324*^9, {
   3.610249098614304*^9, 3.610249102898134*^9}, {3.610281945370743*^9, 
   3.610281948171719*^9}, 3.6102828714178267`*^9, {3.610282961281663*^9, 
   3.610283084343628*^9}, {3.610283127832583*^9, 3.610283145993082*^9}, {
   3.610283179657961*^9, 3.6102832385772057`*^9}, {3.6102837378699913`*^9, 
   3.61028379806913*^9}, {3.6102838359530087`*^9, 3.610283841136386*^9}, {
   3.610283941225168*^9, 3.610283951271957*^9}, 3.6102840780272818`*^9, {
   3.6102841525929747`*^9, 3.610284194074274*^9}, 3.610284225546853*^9, {
   3.610284263604189*^9, 3.610284281156695*^9}, {3.610284324536813*^9, 
   3.6102843923061333`*^9}, {3.610287896526894*^9, 3.610287966644452*^9}}]
}, Open  ]]
},
WindowSize->{1440, 770},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 20, \
2012)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 4121, 119, 222, "Input"],
Cell[4703, 143, 9818, 213, 296, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
