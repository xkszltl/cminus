\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Features for User}{7}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Features for Developer}{7}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Installation}{8}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Syntactic Analysis}{9}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Lexer and Parser}{9}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Symbols}{9}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Type}{9}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Variable}{10}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}Jump}{10}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Environment}{10}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Time Complexity}{10}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}ANTLR}{11}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Storage}{13}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Alignment}{13}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Static Global Memory}{13}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Function}{13}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Various Arguments}{13}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Stack Frame}{13}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Intermediate Representation}{15}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Automatic Error Fixing}{15}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Fork System}{16}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Comment}{16}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Optimization}{19}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Function Inline}{19}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Constant Propagation}{20}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Control Flow Analysis}{20}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Unreachable Code Elimination}{20}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}Liveness Analysis}{21}{section.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.6}Useless Move Instruction Elimination}{21}{section.5.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.7}Dead Code Elimination}{21}{section.5.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.8}Strength Reduction}{22}{section.5.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.9}Common Temporary Variable}{22}{section.5.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.10}Structure of Loop}{22}{section.5.10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Register Allocation}{23}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Graph Coloring}{23}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Interval graph}{23}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Alias}{23}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}Network Flow}{23}{section.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.5}Regenerate Intermediate Code}{24}{section.6.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Linking}{25}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Useless Label Elimination}{25}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Program Entrance}{25}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Translation}{27}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Addressing}{27}{section.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}Static Data}{27}{section.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}Comment}{27}{section.8.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.4}Peephole Optimization}{28}{section.8.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {9}Standard Library}{29}{chapter.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9.1}Basic Functions}{29}{section.9.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9.2}I/O Optimization}{30}{section.9.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.2.1}Intermediate Representation Version}{30}{subsection.9.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.2.2}Reduce I/O Call}{30}{subsection.9.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {10}Performance}{31}{chapter.10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10.1}Testing Method}{31}{section.10.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10.2}Result}{31}{section.10.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {11}Compile Error}{33}{chapter.11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {11.1}Useful Information}{33}{section.11.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {11.2}Recovery}{34}{section.11.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {12}Developing and Debuging}{35}{chapter.12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {12.1}Control Panel}{35}{section.12.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {12.2}Debug Mode}{35}{section.12.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {13}Command Line}{37}{chapter.13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {13.1}Options}{37}{section.13.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\leavevmode {\color {ocre}Index}}{41}{section*.2}
\contentsfinish 
