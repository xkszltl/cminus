DIR = $(shell pwd)
SRC_DIR = $(DIR)/src
LIB_DIR = $(DIR)/lib
TARGET_DIR = $(DIR)/bin
DOC_DIR = $(DIR)/doc

LIB = \
	$(LIB_DIR)/antlr-4.2.2-complete.jar \

.PHONY: all
all: 
	@make -C $(SRC_DIR)/

.PHONY: install
install: $(TARGET_DIR)/cminus-tree
	@echo "ln -sf $$\0(TARGET_DIR)/cminus-tree /usr/bin/cminus-tree"
	@ln -sf $(TARGET_DIR)/cminus-tree /usr/bin/cminus-tree
	@echo "ln -sf $$\0(TARGET_DIR)/c-- /usr/bin/c--"
	@ln -sf $(TARGET_DIR)/c-- /usr/bin/c--

.PHONY: uninstall
uninstall:
	@$(RM) /usr/bin/cminus-tree
	@$(RM) /usr/bin/c--

.PHONY: clean
clean:
	@make --no-print-directory -C $(SRC_DIR)/ clean

.PHONY: docs
docs:
	@make -C $(DOC_DIR)/
	@echo "cp $$\0(DOC_DIR)/main.pdf $$\0(DIR)/manual.pdf"
	@cp $(DOC_DIR)/main.pdf $(DIR)/Manual.pdf
