import java.io.*;
import java.util.*;
import java.math.*;

public class ConstIntSymbol extends ConstSymbol
{
    public int val;

    public ConstIntSymbol()
    {
        super(IntSymbol.instance);
        val = 0;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
    }

    public ConstIntSymbol(boolean _val)
    {
        super(IntSymbol.instance);
        val = _val ? 1 : 0;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
    }

    public ConstIntSymbol(char _val)
    {
        super(IntSymbol.instance);
        val = (int)_val;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
    }

    public ConstIntSymbol(int _val)
    {
        super(IntSymbol.instance);
        val = _val;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
    }

    public ConstIntSymbol(ConstSymbol symbol)
    {
        super(IntSymbol.instance);
        if (symbol instanceof ConstCharSymbol)
            val = (int)((ConstCharSymbol)symbol).val;
        else if (symbol instanceof ConstIntSymbol)
            val = ((ConstIntSymbol)symbol).val;
        else if (symbol instanceof ConstStringSymbol)
            val = (int)(((ConstIntSymbol)(((ConstStringSymbol)symbol).addr)).val);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+" from "+symbol.getClass().toString()+Debug.STYLE_DEFAULT);
    }

    public String toString()
    {
        return ((Integer)val).toString();
    }

    public String typeInfo()
    {
        return "int";
    }
}