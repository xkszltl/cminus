import java.io.*;
import java.util.*;
import java.math.*;

public class ConstCharSymbol extends ConstSymbol
{
    public char val;

    public ConstCharSymbol()
    {
        super(CharSymbol.instance);
        val = 0;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
    }

    public ConstCharSymbol(boolean _val)
    {
        super(CharSymbol.instance);
        val = _val ? '\1' : '\0';
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
    }

    public ConstCharSymbol(char _val)
    {
        super(CharSymbol.instance);
        val = _val;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
    }

    public ConstCharSymbol(int _val)
    {
        super(CharSymbol.instance);
        val = (char)_val;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
    }

    public ConstCharSymbol(ConstSymbol symbol)
    {
        super(CharSymbol.instance);
        if (symbol instanceof ConstCharSymbol)
            val = ((ConstCharSymbol)symbol).val;
        else if (symbol instanceof ConstIntSymbol)
            val = (char)((ConstIntSymbol)symbol).val;
        else if (symbol instanceof ConstStringSymbol)
            val = (char)(((ConstIntSymbol)(((ConstStringSymbol)symbol).addr)).val);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+" from "+symbol.getClass().toString()+Debug.STYLE_DEFAULT);
    }

    public String toString()
    {
        return ((Integer)(int)val).toString();
    }

    public String typeInfo()
    {
        return "char";
    }
}