import java.io.*;
import java.util.*;
import java.math.*;

public class ObjectSymbol extends Symbol
{
    public class DataBlock
    {
        int val;

        public DataBlock()
        {
            super();
            val = 0;
        }

        public DataBlock(int _val)
        {
            super();
            val = _val;
        }
    }

    private static int[] mask = {0xFFFFFF00, 0xFFFF00FF, 0xFF00FFFF, 0x00FFFFFF};
    public static List<DataBlock> staticData = new ArrayList<DataBlock>();
    public static List<Set<ObjectSymbol>> frameSymbolList = new ArrayList<Set<ObjectSymbol>>();
    public static List<ConstIntSymbol> frameAllocator = new ArrayList<ConstIntSymbol>();
    public static int framePointer = 0;
    static
    {
        frameSymbolList.add(new HashSet<ObjectSymbol>());
        frameSymbolList.add(new HashSet<ObjectSymbol>());
        frameAllocator.add(new ConstIntSymbol(Arch.startAddr));
        frameAllocator.add(new ConstIntSymbol(Arch.startAddr));
    }

    public Symbol type = null;
    public int sizeof = 0;
    public boolean lvalue = false;
    public int frame;
    public ObjectSymbol addr = null;
    public boolean restrict = false;
    public boolean spill = true;

    public ObjectSymbol(boolean _restrict, Symbol symbol)
    {
        id = ++idCount;
        if (symbol instanceof ObjectSymbol)
            type = ((ObjectSymbol)symbol).type;
        else
            type = symbol;
        sizeof = type.sizeof;
        frame = getFrame();
        addr = new ConstIntSymbol(allocate());
        restrict = _restrict;
        if (Debug.ENABLE)
            if (! (this instanceof ConstSymbol))
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct("+IRGenerator.getAddr(this)+") "+(lvalue ? "Lvalue " : "Rvalue ")+toString()+" -> "+typeInfo()+Debug.STYLE_DEFAULT);
    }

    public ObjectSymbol(boolean _restrict, Symbol symbol, boolean _lvalue)
    {
        id = ++idCount;
        if (symbol instanceof ObjectSymbol)
            type = ((ObjectSymbol)symbol).type;
        else
            type = symbol;
        sizeof = type.sizeof;
        lvalue = _lvalue && ! (type instanceof ArraySymbol);
        frame = getFrame();
        addr = new ConstIntSymbol(allocate());
        restrict = _restrict;
        if (Debug.ENABLE)
            if (! (this instanceof ConstSymbol))
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct("+IRGenerator.getAddr(this)+") "+(lvalue ? "Lvalue " : "Rvalue ")+toString()+" -> "+typeInfo()+Debug.STYLE_DEFAULT);
    }

    public ObjectSymbol(boolean _restrict, Symbol symbol, boolean _lvalue, int _frame)
    {
        id = ++idCount;
        if (symbol instanceof ObjectSymbol)
            type = ((ObjectSymbol)symbol).type;
        else
            type = symbol;
        sizeof = type.sizeof;
        lvalue = _lvalue && ! (type instanceof ArraySymbol);
        frame = _frame;
        addr = new ConstIntSymbol(allocate());
        restrict = _restrict;
        if (Debug.ENABLE)
            if (! (this instanceof ConstSymbol))
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct("+IRGenerator.getAddr(this)+") "+(lvalue ? "Lvalue " : "Rvalue ")+toString()+" -> "+typeInfo()+Debug.STYLE_DEFAULT);
    }

    public ObjectSymbol(boolean _restrict, Symbol symbol, boolean _lvalue, ObjectSymbol _addr)
    {
        id = ++idCount;
        if (symbol instanceof ObjectSymbol)
            type = ((ObjectSymbol)symbol).type;
        else
            type = symbol;
        sizeof = type.sizeof;
        lvalue = _lvalue && ! (type instanceof ArraySymbol);
        frame = getFrame();
        addr = _addr;
        frameSymbolList.get(frame).add(this);
        restrict = _restrict;
        if (Debug.ENABLE)
            if (! (this instanceof ConstSymbol))
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct("+IRGenerator.getAddr(this)+") "+(lvalue ? "Lvalue " : "Rvalue ")+toString()+" -> "+typeInfo()+Debug.STYLE_DEFAULT);
    }

    public ObjectSymbol(boolean _restrict, Symbol symbol, boolean _lvalue, int _frame, ObjectSymbol _addr)
    {
        id = ++idCount;
        if (symbol instanceof ObjectSymbol)
            type = ((ObjectSymbol)symbol).type;
        else
            type = symbol;
        sizeof = type.sizeof;
        lvalue = _lvalue && ! (type instanceof ArraySymbol);
        frame = _frame;
        addr = _addr;
        frameSymbolList.get(frame).add(this);
        restrict = _restrict;
        if (Debug.ENABLE)
            if (! (this instanceof ConstSymbol))
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct("+IRGenerator.getAddr(this)+") "+(lvalue ? "Lvalue " : "Rvalue ")+toString()+" -> "+typeInfo()+Debug.STYLE_DEFAULT);
    }

    public boolean equals(ObjectSymbol b)
    {
        return this == b;
    }

    public ObjectSymbol modify(Symbol _type)
    {
        type = _type;
        sizeof = type.sizeof;
        return this;
    }

    public String typeInfo()
    {
        return type.typeInfo();
    }

    public int allocate()
    {
        frameSymbolList.get(frame).add(this);
        ConstIntSymbol addr = frameAllocator.get(frame);
        addr.val += (4-addr.val % 4) % 4;
        addr.val += sizeof;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Allocate "+sizeof+" byte(s) on frame #"+frame+"(0x"+Integer.toHexString(addr.val-sizeof)+" -> 0x"+Integer.toHexString(addr.val)+")"+Debug.STYLE_DEFAULT);
        if (frame == 0)
            for (; staticData.size()-1 < (addr.val-Arch.startAddr-1) >> 2; staticData.add(new DataBlock(0)));
        return addr.val-sizeof;
    }

    public ConstSymbol read()
    {
        int i = (((ConstIntSymbol)addr).val-Arch.startAddr) >> 2;
        int j = (((ConstIntSymbol)addr).val-Arch.startAddr) & 3;
        if (sizeof == 1)
            return new ConstCharSymbol((char)((staticData.get(i).val >> (j*8)) & 0x000000FF));
        if (j == 0)
            return new ConstIntSymbol(staticData.get(i).val);
        int mask0 = 0xFFFFFFFF;
        int mask1 = 0xFFFFFFFF;
        for (int k = j; k < 4; mask0 &= mask[k++]);
        for (int k = 0; k < j; mask1 &= mask[k++]);
        return new ConstIntSymbol(((staticData.get(i).val >> (j*8)) & (mask1)) ^ ((staticData.get(i+1).val << (32-j*8)) & mask0));
    }

    public int coverByte(int offset, int data)
    {
        int i = (offset-Arch.startAddr) >> 2;
        int j = (offset-Arch.startAddr) & 3;
        staticData.get(i).val &= mask[j];
        staticData.get(i).val ^= ~mask[j] & (data << (j*8));
        return offset+1;
    }

    public int coverWord(int offset, int data)
    {
        int i = (offset-Arch.startAddr) >> 2;
        int j = (offset-Arch.startAddr) & 3;
        if (j == 0)
            staticData.get(i).val = data;
        else
        {
            int mask0 = 0xFFFFFFFF;
            int mask1 = 0xFFFFFFFF;
            for (int k = j; k < 4; mask0 &= mask[k++]);
            for (int k = 0; k < j; mask1 &= mask[k++]);
            staticData.get(i).val &= mask0;
            staticData.get(i).val ^= ~mask0 & (data << (j*8));
            staticData.get(i+1).val &= mask1;
            staticData.get(i+1).val ^= ~mask1 & (data >> (32-j*8));
        }
        return offset+4;
    }

    public int cover(int offset, ConstSymbol obj)
    {
        if (obj instanceof ConstCharSymbol)
        {
            if (Debug.ENABLE)
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Need to cover 1 byte at 0x"+Integer.toHexString(offset)+Debug.STYLE_DEFAULT);
            return coverByte(offset, (int)((ConstCharSymbol)obj).val);
        }
        if (obj instanceof ConstIntSymbol)
        {
            if (Debug.ENABLE)
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Need to cover 1 word at 0x"+Integer.toHexString(offset)+Debug.STYLE_DEFAULT);
            return coverWord(offset, (int)((ConstIntSymbol)obj).val);
        }
        if (obj instanceof ConstStringSymbol)
        {
            if (Debug.ENABLE)
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Need to cover "+(((ConstStringSymbol)obj).val.length()+1)+" byte(s) at 0x"+Integer.toHexString(offset)+Debug.STYLE_DEFAULT);
            for (int i = 0; i < ((ConstStringSymbol)obj).val.length(); ++i)
                offset = coverByte(offset, (int)((ConstStringSymbol)obj).val.charAt(i));
            offset = coverByte(offset, '\0');
        }
        return offset;
    }

    public int init(Object list, int offset, String comment) throws CompileException
    {
        if (list instanceof List)
        {
            if (((List<Object>)list).size() == 0)
                if ((type instanceof CharSymbol) || (type instanceof IntSymbol) || (type instanceof PointerSymbol))
                    throw new CompileException("empty scalar initializer");
            if ((type instanceof CharSymbol) || (type instanceof IntSymbol) || (type instanceof PointerSymbol))
            {
                if (((List<Object>)list).get(0) instanceof List)
                    return init(((List<Object>)list).get(0), offset, comment);
                if (frame == 0)
                {
                    if (! (((List<Object>)list).get(0) instanceof ConstSymbol))
                        throw new CompileException("initializer element is not constant");
                    return cover(offset, (ConstSymbol)((List<Object>)list).get(0));
                }
                assign(this, (ObjectSymbol)(((List<Object>)list).get(0)), comment);
                return offset+sizeof;
            }
            if (type instanceof ArraySymbol)
            {
                if (((ArraySymbol)type).dim.get(0) == -1)
                {
                    type = new ArraySymbol(((ArraySymbol)type).get(), ((List<Object>)list).size());
                    sizeof = type.sizeof;
                    addr = new ConstIntSymbol(allocate());
                }
                for (int i = 0; i < ((List<Object>)list).size() && i < ((ArraySymbol)type).dim.get(0); ++i)
                    offset = subscript(new ConstIntSymbol(i), comment).init(((List<Object>)list).get(i), offset, comment);
                return offset;
            }
            if (type instanceof Aggregate)
            {
                for (int i = 0; i < ((List<Object>)list).size() && i < ((Aggregate)type).getMembersList().size(); ++i)
                    get(((Aggregate)type).getMembersList().get(i), comment).init(((List<Object>)list).get(i), offset, comment);
                return offset;
            }
        }
        if (type instanceof ArraySymbol)
            throw new CompileException("invalid initializer");
        if (frame == 0)
        {
            if (! (list instanceof ConstSymbol))
                throw new CompileException("initializer element is not constant");
            if (list instanceof ConstStringSymbol)
                return cover(offset, (ConstIntSymbol)((ConstStringSymbol)list).addr);
            return cover(offset, (ConstSymbol)list);
        }
        assign(this, (ObjectSymbol)list, comment);
        return offset+sizeof;
    }

    public ObjectSymbol subscript(ObjectSymbol index, String comment) throws CompileException
    {
        if (! (type instanceof Addressable))
            throw new CompileException("subscripted value '"+typeInfo()+"' is neither array nor pointer nor vector");
        if (! (index.type instanceof CharSymbol) && ! (index.type instanceof IntSymbol))
            throw new CompileException("array subscript is not an integer");
        Symbol subType = ((Addressable)type).get();
        if (type instanceof PointerSymbol)
            return new ObjectSymbol(false, subType, true, 0, plus(this, index, comment));
        ObjectSymbol offset = plus(addr, mul(index, new ConstIntSymbol(subType.sizeof), comment), comment);
        ObjectSymbol realAddr = offset;
        if (frame != 0)
        {
            if (offset instanceof ConstSymbol)
                realAddr = new ObjectSymbol(true, IntSymbol.instance);
            IRGenerator.add("ADD", realAddr, "SP", offset, comment);
        }
        return new ObjectSymbol(false, subType, true, 0, realAddr);
    }

    public ObjectSymbol ref(String comment) throws CompileException
    {
        if (! lvalue)
            throw new CompileException("lvalue required as unary '&' operand");
        restrict = false;
        if (frame == 0)
            return addr;
        ObjectSymbol ret = new ObjectSymbol(true, new PointerSymbol(type));
        IRGenerator.add("ADD", ret, "SP", addr, comment);
        return ret;
    }

    public ObjectSymbol deref(String comment) throws CompileException
    {
        if (! (type instanceof Addressable))
            throw new CompileException("invalid type argument of '*' (have '"+typeInfo()+"')");
        if (type instanceof PointerSymbol)
            return new ObjectSymbol(false, ((Addressable)type).get(), true, 0, this);
        return new ObjectSymbol(false, ((Addressable)type).get(), true, frame, addr);
    }

    public ObjectSymbol get(String id, String comment) throws CompileException
    {
        if (! (type instanceof Aggregate))
            throw new CompileException("request for member '"+id+"' in something not a structure or union");
        if (frame == 0)
            return new ObjectSymbol(false, ((Aggregate)type).getType(id), lvalue, 0, plus(addr, ((Aggregate)type).get(id), comment));
        else
        {
            ObjectSymbol realAddr = new ObjectSymbol(true, IntSymbol.instance);
            IRGenerator.add("ADD", realAddr, "SP", addr, comment);
            return new ObjectSymbol(false, ((Aggregate)type).getType(id), lvalue, 0, plus(realAddr, ((Aggregate)type).get(id), comment));
        }
    }

    public ObjectSymbol arrow(String id, String comment) throws CompileException 
    {
        if (! (type instanceof Addressable))
            throw new CompileException("invalid type argument of '->' (have '"+typeInfo()+"')");
        if (! (((Addressable)type).get() instanceof Aggregate))
            throw new CompileException("invalid type argument of '->' (have '"+typeInfo()+"')");
        Aggregate tmpType = (Aggregate)(((Addressable)type).get());
        return new ObjectSymbol(false, tmpType.getType(id), true, 0, plusVoid(this, tmpType.get(id), comment));
    }

    public ObjectSymbol call(List<ObjectSymbol> args, String comment) throws CompileException
    {
        if ((type instanceof PointerSymbol) && (((PointerSymbol)type).type instanceof FuncSymbol))
            return ((FuncSymbol)(((PointerSymbol)type).type)).call(args, comment);
        if (type instanceof FuncSymbol)
            return ((FuncSymbol)type).call(args, comment);
        throw new CompileException("called object is not a function or function pointer");
    }

    public static int pushFrame(FuncSymbol func)
    {
        IRGenerator.addBind(func);
        framePointer = frameAllocator.size();
        frameSymbolList.add(new HashSet<ObjectSymbol>());
        frameAllocator.add(new ConstIntSymbol(0));
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Push frame pointer #"+framePointer+Debug.STYLE_DEFAULT);
        return framePointer;
    }

    public static void popFrame()
    {
        IRGenerator.removeBind();
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Pop frame pointer #"+framePointer+"(Frame size = "+frameAllocator.get(framePointer)+")"+Debug.STYLE_DEFAULT);
        framePointer = 0;
    }

    public static int getFrame()
    {
        return framePointer;
    }

    public static ConstIntSymbol getFrameSize(int frame)
    {
        return frameAllocator.get(frame);
    }

    public static ObjectSymbol assign(ObjectSymbol dest, ObjectSymbol src, String comment) throws CompileException
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Assign "+(dest == null ? "null" : dest.toString())+" with "+(src == null ? "null" : src.toString())+Debug.STYLE_DEFAULT);
        if (! (dest.type.equals(src.type)))
            if (! (dest.type instanceof Scalar) || ! (src.type instanceof Scalar))
                if (src.type instanceof Aggregate)
                    throw new CompileException("aggregate value used where an integer was expected");
                else
                    throw new CompileException("incompatible types when assigning to type '"+dest.typeInfo()+"' from type '"+src.typeInfo()+"'");
        if (! dest.lvalue)
            throw new CompileException("lvalue required as left operand of assignment");
        if (src.type instanceof ArraySymbol)
        {
            if (src.frame != 0)
                IRGenerator.add("ADD", dest, "SP", src.addr, comment);
            else
                IRGenerator.add("=", dest, src.addr, comment);
        }
        else
            IRGenerator.add("=", dest, src, comment);
        return dest;
    }

    public static ObjectSymbol assignRvalue(ObjectSymbol dest, ObjectSymbol src, String comment) throws CompileException
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Assign(Rvalue) "+(dest == null ? "null" : dest.toString())+" with "+(src == null ? "null" : src.toString())+Debug.STYLE_DEFAULT);
        if (! (dest.type.equals(src.type)))
            if (! (dest.type instanceof Scalar) || ! (src.type instanceof Scalar))
                if (src.type instanceof Aggregate)
                    throw new CompileException("aggregate value used where an integer was expected");
                else
                    throw new CompileException("incompatible types when assigning to type '"+dest.typeInfo()+"' from type '"+src.typeInfo()+"'");
        if (src.type instanceof ArraySymbol)
        {
            ObjectSymbol _src = src.addr;
            if (src.frame != 0)
                IRGenerator.add("ADD", dest, "SP", _src, comment);
            else
                IRGenerator.add("=", dest, _src, comment);
        }
        else
            IRGenerator.add("=", dest, src, comment);
        return dest;
    }

    public static ObjectSymbol logic(String op, ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if (! (a.type instanceof Scalar) || ! (b.type instanceof Scalar))
            throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
        IRGenerator.add(op, c, a, b, comment);
        return c;
    }

    public static ObjectSymbol logic(String op, ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if (! (a.type instanceof Scalar) || ! (b.type instanceof Scalar))
            throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
        ObjectSymbol c = new ObjectSymbol(true, IntSymbol.instance);
        IRGenerator.add(op, c, a, b, comment);
        return c;
    }

    public static ObjectSymbol logic(String op, ObjectSymbol a, String comment) throws CompileException
    {
        if (! (a.type instanceof Scalar))
            throw new CompileException("invalid operands to unary "+op+" (have '"+a.typeInfo()+"')");
        ObjectSymbol c = new ObjectSymbol(true, IntSymbol.instance);
        if (a.type instanceof Addressable)
        {
            ObjectSymbol _a = a.type instanceof PointerSymbol ? a : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)a.type).fullDeref(), ((ArraySymbol)a.type).dim.size()), true), a.addr, comment);
            IRGenerator.add(op, c, _a, comment);
        }
        else
            IRGenerator.add(op, c, a, comment);
        return c;
    }

    public static ObjectSymbol arthimetic(String op, ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if (! (a.type instanceof Scalar) || ! (b.type instanceof Scalar))
            throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
        if ((a.type instanceof CharSymbol) && (b.type instanceof CharSymbol))
        {
            if (op.equals("*"))
                if (b instanceof ConstCharSymbol && ((ConstCharSymbol)b).val >= 1 && ((ConstCharSymbol)b).val == (((ConstCharSymbol)b).val & -((ConstCharSymbol)b).val))
                {
                    int cnt = 0;
                    for (int acc = ((ConstCharSymbol)b).val; acc > 1; acc >>= 1, ++cnt);
                    op = "<<";
                    b = new ConstCharSymbol((char)cnt);
                }
            IRGenerator.add(op, c, a, b, comment);
            if (extend)
                c.modify(CharSymbol.instance);
            return c;
        }
        if (((a.type instanceof CharSymbol) || (a.type instanceof IntSymbol)) && ((b.type instanceof CharSymbol) || (b.type instanceof IntSymbol)))
        {
            if (op.equals("*"))
                if (b instanceof ConstCharSymbol && ((ConstCharSymbol)b).val >= 1 && ((ConstCharSymbol)b).val == (((ConstCharSymbol)b).val & -((ConstCharSymbol)b).val))
                {
                    int cnt = 0;
                    for (int acc = ((ConstCharSymbol)b).val; acc > 1; acc >>= 1, ++cnt);
                    op = "<<";
                    b = new ConstCharSymbol((char)cnt);
                }
                else if (b instanceof ConstIntSymbol && ((ConstIntSymbol)b).val >= 1 && ((ConstIntSymbol)b).val == (((ConstIntSymbol)b).val & -((ConstIntSymbol)b).val))
                {
                    int cnt = 0;
                    for (int acc = ((ConstIntSymbol)b).val; acc > 1; acc >>= 1, ++cnt);
                    op = "<<";
                    b = new ConstIntSymbol(cnt);
                }
            IRGenerator.add(op, c, a, b, comment);
            if (extend)
                c.modify(IntSymbol.instance);
            return c;
        }
        if (a.type instanceof Addressable)
        {
            if ((b.type instanceof CharSymbol) || (b.type instanceof IntSymbol))
            {
                if (! op.equals("+") && ! op.equals("-"))
                    throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
                ObjectSymbol _a = a.type instanceof PointerSymbol ? a : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)a.type).fullDeref(), ((ArraySymbol)a.type).dim.size()), true), a.addr, comment);
                ObjectSymbol _b = mul(new ConstIntSymbol(((Addressable)(a.type)).get().sizeof), b, comment);
                IRGenerator.add(op, c, _a, _b, comment);
                if (extend)
                    c.modify(a.type);
                return c;
            }
            if (b.type instanceof Addressable)
            {
                if (! op.equals("-") || ! a.type.equals(b.type))
                    throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
                ObjectSymbol _a = a.type instanceof PointerSymbol ? a : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)a.type).fullDeref(), ((ArraySymbol)a.type).dim.size()), true), a.addr, comment);
                ObjectSymbol _b = b.type instanceof PointerSymbol ? b : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)b.type).fullDeref(), ((ArraySymbol)b.type).dim.size()), true), b.addr, comment);
                IRGenerator.add(op, c, _a, _b, comment);
                IRGenerator.add("/", c, c, new ConstIntSymbol(((Addressable)(a.type)).get().sizeof), comment);
                if (extend)
                    c.modify(IntSymbol.instance);
                return c;
            }
            throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
        }
        if (b.type instanceof Addressable)
        {
            if ((a.type instanceof CharSymbol) || (a.type instanceof IntSymbol))
            {
                if (! op.equals("+"))
                    throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
                ObjectSymbol _a = arthimetic("*", new ConstIntSymbol(((Addressable)(b.type)).get().sizeof), a, comment);
                ObjectSymbol _b = b.type instanceof PointerSymbol ? b : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)b.type).fullDeref(), ((ArraySymbol)b.type).dim.size()), true), b.addr, comment);
                IRGenerator.add(op, c, _a, _b, comment);
                if (extend)
                    c.modify(b.type);
                return c;
            }
        }
        throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
    }

    public static ObjectSymbol arthimetic(String op, ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if (! (a.type instanceof Scalar) || ! (b.type instanceof Scalar))
            throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
        if ((a.type instanceof CharSymbol) && (b.type instanceof CharSymbol))
        {
            ObjectSymbol c = new ObjectSymbol(true, CharSymbol.instance);
            IRGenerator.add(op, c, a, b, comment);
            return c;
        }
        if (((a.type instanceof CharSymbol) || (a.type instanceof IntSymbol)) && ((b.type instanceof CharSymbol) || (b.type instanceof IntSymbol)))
        {
            ObjectSymbol c = new ObjectSymbol(true, IntSymbol.instance);
            IRGenerator.add(op, c, a, b, comment);
            return c;
        }
        if (a.type instanceof Addressable)
        {
            if ((b.type instanceof CharSymbol) || (b.type instanceof IntSymbol))
            {
                if (! op.equals("+") && ! op.equals("-"))
                    throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
                ObjectSymbol _a = a.type instanceof PointerSymbol ? a : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)a.type).fullDeref(), ((ArraySymbol)a.type).dim.size()), true), a.addr, comment);
                ObjectSymbol _b = mul(new ConstIntSymbol(((Addressable)(a.type)).get().sizeof), b, comment);
                ObjectSymbol c = new ObjectSymbol(true, _a.type);
                IRGenerator.add(op, c, _a, _b, comment);
                return c;
            }
            if (b.type instanceof Addressable)
            {
                if (! op.equals("-") || ! a.type.equals(b.type))
                    throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
                ObjectSymbol c = new ObjectSymbol(true, IntSymbol.instance);
                ObjectSymbol _a = a.type instanceof PointerSymbol ? a : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)a.type).fullDeref(), ((ArraySymbol)a.type).dim.size()), true), a.addr, comment);
                ObjectSymbol _b = b.type instanceof PointerSymbol ? b : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)b.type).fullDeref(), ((ArraySymbol)b.type).dim.size()), true), b.addr, comment);
                IRGenerator.add(op, c, _a, _b, comment);
                IRGenerator.add("/", c, c, new ConstIntSymbol(((Addressable)(a.type)).get().sizeof), comment);
                return c;
            }
        }
        if (b.type instanceof Addressable)
        {
            if ((a.type instanceof CharSymbol) || (a.type instanceof IntSymbol))
            {
                if (! op.equals("+"))
                    throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
                ObjectSymbol _a = arthimetic("*", new ConstIntSymbol(((Addressable)(b.type)).get().sizeof), a, comment);
                ObjectSymbol _b = b.type instanceof PointerSymbol ? b : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)b.type).fullDeref(), ((ArraySymbol)b.type).dim.size()), true), b.addr, comment);
                ObjectSymbol c = new ObjectSymbol(true, _b.type);
                IRGenerator.add(op, c, _a, _b, comment);
                return c;
            }
        }
        throw new CompileException("invalid operands to binary "+op+" (have '"+a.typeInfo()+"' and '"+b.typeInfo()+"')");
    }

    public static ObjectSymbol arthimetic(String op, ObjectSymbol a, String comment) throws CompileException
    {
        if (! (a.type instanceof Scalar))
            throw new CompileException("invalid operands to unary "+op+" (have '"+a.typeInfo()+"')");
        if (op.equals("++") || op.equals("--"))
        {
            if (a.type instanceof CharSymbol)
                IRGenerator.add((op.equals("++") ? "+" : "-"), a, a, new ConstCharSymbol(1), comment);
            else if (a.type instanceof IntSymbol)
                IRGenerator.add((op.equals("++") ? "+" : "-"), a, a, new ConstIntSymbol(1), comment);
            else if (a.type instanceof PointerSymbol)
                IRGenerator.add((op.equals("++") ? "+" : "-"), a, a, new ConstCharSymbol(((Addressable)(a.type)).get().sizeof), comment);
            return a;
        }
        ObjectSymbol c = new ObjectSymbol(true, a.type);
        IRGenerator.add(op, c, a, comment);
        return c;
    }

    public static ObjectSymbol plusVoid(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        ObjectSymbol _a = a.type instanceof PointerSymbol ? a : assign(new ObjectSymbol(true, new PointerSymbol(((Addressable)a.type).fullDeref(), ((ArraySymbol)a.type).dim.size()), true), a.addr, comment);
        ObjectSymbol c = new ObjectSymbol(true, _a.type);
        IRGenerator.add("+", c, _a, b, comment);
        return c;
    }

    public static ObjectSymbol logicOr(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != '\0' || ((ConstCharSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != '\0' || ((ConstIntSymbol)b).val != 0) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != '\0' || ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val != 0) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != 0 || ((ConstCharSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != 0 || ((ConstIntSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != 0 || ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val != '\0') ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != 0 || ((ConstCharSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != 0 || ((ConstIntSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != 0 || ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val != '\0') ? 1 : 0);
        return logic("||", c, a, b, comment);
    }

    public static ObjectSymbol logicAnd(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != '\0' && ((ConstCharSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != '\0' && ((ConstIntSymbol)b).val != 0) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != '\0' && ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val != 0) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != 0 && ((ConstCharSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != 0 && ((ConstIntSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != 0 && ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val != '\0') ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != 0 && ((ConstCharSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != 0 && ((ConstIntSymbol)b).val != '\0') ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != 0 && ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val != '\0') ? 1 : 0);
        return logic("&&", c, a, b, comment);
    }

    public static ObjectSymbol equal(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val == ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val == ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val == ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val == ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val == ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val == ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val == ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val == ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val == ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        return logic("==", a, b, comment);
    }

    public static ObjectSymbol notEqual(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val != ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val != ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val != ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        return logic("!=", a, b, comment);
    }

    public static ObjectSymbol lessEqual(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val <= ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val <= ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val <= ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val <= ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val <= ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val <= ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val <= ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val <= ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val <= ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        return logic("<=", a, b, comment);
    }

    public static ObjectSymbol greaterEqual(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val >= ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val >= ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val >= ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val >= ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val >= ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val >= ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val >= ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val >= ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val >= ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        return logic(">=", a, b, comment);
    }

    public static ObjectSymbol less(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val < ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val < ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val < ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val < ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val < ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val < ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val < ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val < ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val < ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        return logic("<", a, b, comment);
    }

    public static ObjectSymbol greater(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val > ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val > ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstCharSymbol)a).val > ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val > ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val > ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)a).val > ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val > ((ConstCharSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val > ((ConstIntSymbol)b).val) ? 1 : 0);
        if ((a instanceof ConstStringSymbol) && (b instanceof ConstStringSymbol))
            return new ConstIntSymbol((((ConstIntSymbol)(((ConstStringSymbol)a).addr)).val > ((ConstIntSymbol)(((ConstStringSymbol)b).addr)).val) ? 1 : 0);
        return logic(">", a, b, comment);
    }

    public static ObjectSymbol or(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val | ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val | ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val | ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val | ((ConstIntSymbol)b).val));
        return arthimetic("|", a, b, comment);
    }

    public static ObjectSymbol or(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val | ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val | ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val | ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val | ((ConstIntSymbol)b).val));
        return arthimetic("|", c, a, b, extend, comment);
    }

    public static ObjectSymbol xor(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val ^ ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val ^ ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val ^ ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val ^ ((ConstIntSymbol)b).val));
        return arthimetic("^", a, b, comment);
    }

    public static ObjectSymbol xor(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val ^ ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val ^ ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val ^ ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val ^ ((ConstIntSymbol)b).val));
        return arthimetic("^", c, a, b, extend, comment);
    }

    public static ObjectSymbol and(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val & ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val & ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val & ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val & ((ConstIntSymbol)b).val));
        return arthimetic("&", a, b, comment);
    }

    public static ObjectSymbol and(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val & ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val & ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val & ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val & ((ConstIntSymbol)b).val));
        return arthimetic("&", c, a, b, extend, comment);
    }

    public static ObjectSymbol shiftLeft(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val << ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val << ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val << ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val << ((ConstIntSymbol)b).val));
        return arthimetic("<<", a, b, comment);
    }

    public static ObjectSymbol shiftLeft(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val << ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val << ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val << ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val << ((ConstIntSymbol)b).val));
        return arthimetic("<<", c, a, b, extend, comment);
    }

    public static ObjectSymbol shiftRight(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val >> ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val >> ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val >> ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val >> ((ConstIntSymbol)b).val));
        return arthimetic(">>", a, b, comment);
    }

    public static ObjectSymbol shiftRight(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val >> ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val >> ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val >> ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val >> ((ConstIntSymbol)b).val));
        return arthimetic(">>", c, a, b, extend, comment);
    }

    public static ConstSymbol plusConst(ConstSymbol a, ConstSymbol b, String comment)
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val + ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val + ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val + ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val + ((ConstIntSymbol)b).val));
        return null;
    }

    public static ObjectSymbol plus(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstSymbol) && (b instanceof ConstSymbol))
        {
            ObjectSymbol ret = plusConst((ConstSymbol)a, (ConstSymbol)b, comment);
            if (ret != null)
                return ret;
        }
        return arthimetic("+", a, b, comment);
    }

    public static ObjectSymbol plus(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstSymbol) && (b instanceof ConstSymbol))
        {
            ObjectSymbol ret = plusConst((ConstSymbol)a, (ConstSymbol)b, comment);
            if (ret != null)
                return ret;
        }
        return arthimetic("+", c, a, b, extend, comment);
    }

    public static ObjectSymbol minus(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val - ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val - ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val - ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val - ((ConstIntSymbol)b).val));
        return arthimetic("-", a, b, comment);
    }

    public static ObjectSymbol minus(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val - ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val - ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val - ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val - ((ConstIntSymbol)b).val));
        return arthimetic("-", c, a, b, extend, comment);
    }

    public static ObjectSymbol mul(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val * ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val * ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val * ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val * ((ConstIntSymbol)b).val));
        return arthimetic("*", a, b, comment);
    }

    public static ObjectSymbol mul(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val * ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val * ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val * ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val * ((ConstIntSymbol)b).val));
        return arthimetic("*", c, a, b, extend, comment);
    }

    public static ObjectSymbol div(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val / ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val / ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val / ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val / ((ConstIntSymbol)b).val));
        return arthimetic("/", a, b, comment);
    }

    public static ObjectSymbol div(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val / ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val / ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val / ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val / ((ConstIntSymbol)b).val));
        return arthimetic("/", c, a, b, extend, comment);
    }

    public static ObjectSymbol mod(ObjectSymbol a, ObjectSymbol b, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val % ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val % ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val % ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val % ((ConstIntSymbol)b).val));
        return arthimetic("%", a, b, comment);
    }

    public static ObjectSymbol mod(ObjectSymbol c, ObjectSymbol a, ObjectSymbol b, boolean extend, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstCharSymbol))
            return new ConstCharSymbol((char)(((ConstCharSymbol)a).val % ((ConstCharSymbol)b).val));
        if ((a instanceof ConstCharSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstCharSymbol)a).val % ((ConstIntSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstCharSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val % ((ConstCharSymbol)b).val));
        if ((a instanceof ConstIntSymbol) && (b instanceof ConstIntSymbol))
            return new ConstIntSymbol((int)(((ConstIntSymbol)a).val % ((ConstIntSymbol)b).val));
        return arthimetic("%", c, a, b, extend, comment);
    }

    public static ObjectSymbol inc(ObjectSymbol a, String comment) throws CompileException
    {
        if (! a.lvalue)
            throw new CompileException("lvalue required as unary '++' operand");
        arthimetic("++", a, comment);
        ObjectSymbol ret = new ObjectSymbol(true, a);
        IRGenerator.add("=", ret, a, comment);
        return ret;
    }

    public static ObjectSymbol dec(ObjectSymbol a, String comment) throws CompileException
    {
        if (! a.lvalue)
            throw new CompileException("lvalue required as unary '--' operand");
        arthimetic("--", a, comment);
        ObjectSymbol ret = new ObjectSymbol(true, a);
        IRGenerator.add("=", ret, a, comment);
        return ret;
    }

    public static ObjectSymbol postInc(ObjectSymbol a, String comment) throws CompileException
    {
        if (! a.lvalue)
            throw new CompileException("lvalue required as unary '++' operand");
        ObjectSymbol ret = new ObjectSymbol(true, a);
        IRGenerator.add("=", ret, a, comment);
        arthimetic("++", a, comment);
        return ret;
    }

    public static ObjectSymbol postDec(ObjectSymbol a, String comment) throws CompileException
    {
        if (! a.lvalue)
            throw new CompileException("lvalue required as unary '--' operand");
        ObjectSymbol ret = new ObjectSymbol(true, a);
        IRGenerator.add("=", ret, a, comment);
        arthimetic("--", a, comment);
        return ret;
    }

    public static ObjectSymbol posi(ObjectSymbol a, String comment) throws CompileException
    {
        if ((a instanceof ConstCharSymbol) || (a instanceof ConstIntSymbol))
            return a;
        return assign(new ObjectSymbol(true, a), a, comment);
    }

    public static ObjectSymbol neg(ObjectSymbol a, String comment) throws CompileException
    {
        if (a instanceof ConstCharSymbol)
            return new ConstCharSymbol(-((ConstCharSymbol)a).val);
        if (a instanceof ConstIntSymbol)
            return new ConstIntSymbol(-((ConstIntSymbol)a).val);
        return arthimetic("-", a, comment);
    }

    public static ObjectSymbol not(ObjectSymbol a, String comment) throws CompileException
    {
        if (a instanceof ConstCharSymbol)
            return new ConstCharSymbol(~((ConstCharSymbol)a).val);
        if (a instanceof ConstIntSymbol)
            return new ConstIntSymbol(~((ConstIntSymbol)a).val);
        return arthimetic("~", a, comment);
    }

    public static ObjectSymbol logicNot(ObjectSymbol a, String comment) throws CompileException
    {
        if (a instanceof ConstCharSymbol)
            return new ConstCharSymbol(((ConstCharSymbol)a).val == 0);
        if (a instanceof ConstIntSymbol)
            return new ConstIntSymbol(((ConstIntSymbol)a).val == 0);
        return logic("!", a, comment);
    }

    public static ObjectSymbol toBool(ObjectSymbol a, String comment) throws CompileException
    {
        if (! (a.type instanceof Scalar))
            throw new CompileException("invalid operands (have '"+a.typeInfo()+"')");
        if (a instanceof ConstCharSymbol)
            return new ConstIntSymbol(((ConstCharSymbol)a).val != 0 ? 1 : 0);
        if (a instanceof ConstIntSymbol)
            return new ConstIntSymbol(((ConstIntSymbol)a).val != 0 ? 1 : 0);
        ObjectSymbol c = new ObjectSymbol(true, IntSymbol.instance);
        IRGenerator.add("!=0", c, a, comment);
        return c;
    }
}