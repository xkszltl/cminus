import java.io.*;
import java.util.*;
import java.math.*;

public class ConstStringSymbol extends ConstSymbol
{
    public String val;

    public ConstStringSymbol()
    {
        super(new ArraySymbol(CharSymbol.instance), false);
        val = "";
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
        cover(((ConstIntSymbol)addr).val, this);
    }

    public ConstStringSymbol(String _val)
    {
        super(new ArraySymbol(CharSymbol.instance, _val.length()+1), false);
        val = "";
        for (int i = 0; i < _val.length(); ++i)
            if (_val.charAt(i) == '\\' && _val.charAt(i+1) == '\'')
            {
                ++i;
                val += '\'';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == '\"')
            {
                ++i;
                val += '\"';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == '?')
            {
                ++i;
                val += '?';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == 'a')
            {
                ++i;
                val += '\7';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == 'b')
            {
                ++i;
                val += '\10';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == 'f')
            {
                ++i;
                val += '\14';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == 'n')
            {
                ++i;
                val += '\n';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == 'r')
            {
                ++i;
                val += '\r';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == 't')
            {
                ++i;
                val += '\t';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == 'v')
            {
                ++i;
                val += '\13';
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) >= '0' && _val.charAt(i+1) < '8')
            {
                int oct = (int)(_val.charAt(++i)-'0');
                if (_val.charAt(i) == '\\' && _val.charAt(i+1) >= '0' && _val.charAt(i+1) < '8')
                {
                    oct = oct*8+(int)(_val.charAt(++i)-'0');
                    if (_val.charAt(i) == '\\' && _val.charAt(i+1) >= '0' && _val.charAt(i+1) < '8' && oct*8+(int)_val.charAt(i+1) < 256)
                        oct = oct*8+(int)(_val.charAt(++i)-'0');
                }
                val += (char)oct;
            }
            else if (_val.charAt(i) == '\\' && _val.charAt(i+1) == 'x')
            {
                int hex = 0;
                for (++i; _val.charAt(i) >= 0 && _val.charAt(i) <= 9 || _val.charAt(i) >= 'A' && _val.charAt(i) <= 'F' || _val.charAt(i) >= 'a' && _val.charAt(i) <= 'f'; ++i)
                    if (_val.charAt(i) >= 0 && _val.charAt(i) <= 9)
                        hex = hex*16+(int)(_val.charAt(i)-'0');
                    else if (_val.charAt(i) >= 'A' && _val.charAt(i) <= 'F')
                        hex = hex*16+(int)(_val.charAt(i)-'A')+10;
                    else
                        hex = hex*16+(int)(_val.charAt(i)-'a')+10;
                val += (char)hex;
            }
            else if (_val.charAt(i) == '?' && _val.charAt(i+1) == '?')
            {
                i += 2;
                switch (_val.charAt(i))
                {
                case '=':
                    val += '#';
                    break;
                case ')':
                    val += ']';
                    break;
                case '!':
                    val += '|';
                    break;
                case '(':
                    val += '[';
                    break;
                case '\'':
                    val += '^';
                    break;
                case '>':
                    val += '}';
                    break;
                case '/':
                    val += '\\';
                    break;
                case '<':
                    val += '{';
                    break;
                case '-':
                    val += '~';
                    break;
                }
            }
            else
                val += _val.charAt(i);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+" = "+toString()+Debug.STYLE_DEFAULT);
        cover(((ConstIntSymbol)addr).val, this);
    }

    public ObjectSymbol get()
    {
        return new ConstCharSymbol(val.charAt(0));
    }

    public ObjectSymbol get(ObjectSymbol index) throws CompileException
    {
        if (index instanceof ConstCharSymbol)
            return new ConstCharSymbol(val.charAt((int)(((ConstCharSymbol)index).val)));
        if (index instanceof ConstIntSymbol)
            return new ConstCharSymbol(val.charAt(((ConstIntSymbol)index).val));
        if (index.type instanceof CharSymbol)
            return new ObjectSymbol(true, CharSymbol.instance);
        if (index.type instanceof IntSymbol)
            return new ObjectSymbol(true, CharSymbol.instance);
        throw new CompileException("array subscript is not an integer");
    }

    public ObjectSymbol deref()
    {
        return new ConstCharSymbol(val.charAt(0));
    }

    public String toString()
    {
        String str = "";
        for (int i = 0; i < val.length(); ++i)
            if (val.charAt(i) == '\n')
                str += "\\n";
            else if (val.charAt(i) == '\t')
                str += "\\t";
            else if (val.charAt(i) == '\r')
                str += "\\r";
            else if (val.charAt(i) >= 32 && val.charAt(i) <= 126)
                str += val.charAt(i);
            else
            {
                String oct = "";
                for (int c = (int)val.charAt(i); c != 0; c /= 8)
                    oct = (c % 8)+oct;
                str += "\\"+oct;
            }
        return "\""+str+"\"";
    }

    public String typeInfo()
    {
        return "char *";
    }
}