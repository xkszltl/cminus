import java.io.*;
import java.util.*;
import java.math.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class ConstSymbol extends ObjectSymbol
{
    public ConstSymbol(Symbol _type)
    {
        super(true, _type, false, null);
    }

    public ConstSymbol(Symbol _type, boolean _lvalue)
    {
        super(true, _type, _lvalue, 0);
    }

    public String typeInfo()
    {
        return "incomplete const symbol";
    }
}