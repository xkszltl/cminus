#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(* ::Package:: *)

AST[name_, dest_]:=Module[{A,S={},n,i,status=0,E={},r,G,coordSize={0,0},imgSize},
A=StringSplit[StringReplace[Import[name],{"("->"( ",")"->" )"}]];
n=Length[A];
For[i=1,i<=n,
If[A[[i]]=="(",status=1];
If[A[[i]]==")",If[Length[S]>1,AppendTo[E,S[[-2]]->S[[-1]]]];S=S[[1;;-2]]];
If[A[[i]]!="("&&A[[i]]!=")"&&status==0,AppendTo[E,S[[-1]]->{i,A[[i]]}]];
If[A[[i]]!="("&&A[[i]]!=")"&&status==1,status=0;AppendTo[S,{i,A[[i]]}]];
If [Length[S]>0,r=S[[1]]];
++i];
TreePlot[E,Top,r,PlotRangePadding->.1,AspectRatio->1,PlotLabel->Style["Abstruct Syntax Tree for "<>name,96,Bold],
VertexRenderingFunction->((Do[coordSize[[i]]=Max[coordSize[[i]],#1[[i]]],{i,2}];None)&)];
imgSize=coordSize*500;
G=TreePlot[E,Top,r,VertexLabeling->True,PlotRangePadding->.1,ImageMargins->imgSize[[1]]/128,ImageSize->imgSize[[1]],
PlotLabel->Style["Abstruct Syntax Tree ("<>FileNameTake[dest]<>")",96,Bold],
VertexRenderingFunction->({RGBColor[1,1,0.6],EdgeForm[Black],
Rectangle[
#1-{Max[StringLength[ToString[#2[[2]]]]*.02+.01,.05],.15/coordSize[[2]]},
#1+{Max[StringLength[ToString[#2[[2]]]]*.02+.01,.05],.15/coordSize[[2]]},
RoundingRadius->0.015],
Black,Text[Style[#2[[2]],28,FontFamily->"Courier",FontTracking->Plain],#1]}&)];
Export[dest,G,ImageSize->imgSize[[1]]];
];
For[i=2,i<=Length[$ScriptCommandLine],AST[$ScriptCommandLine[[i]], $ScriptCommandLine[[i+1]]];i+=2];

