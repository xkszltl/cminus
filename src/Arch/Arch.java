import java.io.*;
import java.util.*;
import java.math.*;

public class Arch
{
    public static final int pointerSizeof = 4;
    public static final int intSizeof = 4;
    public static final int startAddr = 0x10000000;
    public static final int gpDefault = 0x10008000;

    public static final List<String> dataRegister = new ArrayList<String>(Arrays.asList(
        "$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9",
        "$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7", "$s8"
    ));
    public static final List<String> arthimeticRegister = new ArrayList<String>(Arrays.asList(
        "$a2", "$a3"
    ));
    public static final List<String> temporaryRegister = new ArrayList<String>(Arrays.asList(
        "$v1"
    ));
    public static final List<String> zeroRegister = new ArrayList<String>(Arrays.asList(
        "$zero"
    ));
    public static final List<String> returnAddressRegister = new ArrayList<String>(Arrays.asList(
        "$ra"
    ));
    public static final List<String> systemCallRegister = new ArrayList<String>(Arrays.asList(
        "$v0"
    ));
    public static final List<String> argumentRegister = new ArrayList<String>(Arrays.asList(
        "$a0", "$a1"
    ));
    public static final List<String> stackRegister = new ArrayList<String>(Arrays.asList(
        "$sp"
    ));
    public static final List<String> globalRegister = new ArrayList<String>(Arrays.asList(
        "$gp"
    ));
}