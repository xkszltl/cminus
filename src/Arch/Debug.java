public class Debug
{
    public static final boolean ENABLE = Panel.DEBUG_MODE;
    public static final String STYLE = "\033[31m";
    public static final String STYLE_IR = "\033[36m";
    public static final String STYLE_TR = "\033[36m";
    public static final String STYLE_INFO = "\033[32m";
    public static final String STYLE_DEFAULT = "\033[0m";
}