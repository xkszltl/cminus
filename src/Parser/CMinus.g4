grammar CMinus;

@header
{
    import java.io.*;
    import java.util.*;
    import java.math.*;
}

options
{
    language = Java;
}

program returns [Env env]
@init
{
    $env = Env.base;
}
@after
{
    ObjectSymbol entry = null;
    try
    {
        if ($env.containObj("main"))
        {
            entry = $env.getObj("main");
            IRGenerator.setEntry(((FuncSymbol)(entry.type)).label.id);
        }
    }
    catch (CompileException e)
    {
        notifyErrorListeners("cannot find program entry 'main'");
    }
}
    : program_expression[$env]
    ;

program_expression[Env env]
    : (declaration[env] | function_definition[env])+
    ;

declaration[Env env]
    : type_specifier[env] init_declarators[env, $type_specifier.type]? ';'
    ;

function_definition[Env env] returns [ObjectSymbol symbol]
@after
{
    try
    {
        ((FuncSymbol)($symbol.type)).complete(
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    catch (CompileException e)
    {
        notifyErrorListeners(e.msg);
    }
}
    : type_specifier[env] plain_declarator[env, $type_specifier.type]
    {
        try
        {
            if (env.containObj($plain_declarator.name))
            {
                if (! (env.getObj($plain_declarator.name).type instanceof FuncSymbol))
                    throw new CompileException("'"+$plain_declarator.name+"' redeclared as different kind of symbol");
                if (! ((FuncSymbol)(env.getObj($plain_declarator.name)).type).type.equals($plain_declarator.type))
                    throw new CompileException("conflicting types for '"+$plain_declarator.name+"'");
                if (((FuncSymbol)(env.getObj($plain_declarator.name)).type).env != null)
                    throw new CompileException("redefinition of '"+$plain_declarator.name+"'");
                $symbol = new ObjectSymbol(true, new FuncSymbol($plain_declarator.type, ((FuncSymbol)(env.getObj($plain_declarator.name).type)).label));
            }
            else
            {
                $symbol = new ObjectSymbol(true, new FuncSymbol($plain_declarator.type, env.getCallJump()));
                env.setObj($plain_declarator.name, $symbol);
            }
            if (($plain_declarator.type instanceof Aggregate) && ! ((Aggregate)$plain_declarator.type).defined())
                throw new CompileException("return type is an incomplete type");
            ((FuncSymbol)($symbol.type)).frame = ObjectSymbol.pushFrame((FuncSymbol)$symbol.type);
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    '(' parameters[env, (FuncSymbol)($symbol.type)]? ')'
    {
        try
        {
            ((FuncSymbol)($symbol.type)).setEnv(env,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    compound_statement[((FuncSymbol)($symbol.type)).env]
    ;

parameters[Env env, FuncSymbol func]
    : plain_declaration[env, func] (',' plain_declaration[env, func])* (',' '...'
        {
            func.setVA();
        }
    )?
    ;

declarators[Env env, Symbol bind, Symbol type]
    : declarator[env, bind, type] (',' declarator[env, bind, type])*
    ;

init_declarators[Env env, Symbol type]
    : init_declarator[env, type] (',' init_declarator[env, type])*
    ;

init_declarator[Env env, Symbol type] returns [ObjectSymbol symbol]
    : declarator[env, null, type]
    {
        $symbol = (ObjectSymbol)$declarator.symbol;
    }
    ('=' initializer[env]
        {
            try
            {
                $symbol.init($initializer.list, ((ConstIntSymbol)$symbol.addr).val,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )?
    ;

initializer[Env env] returns [Object list]
@init
{
    $list = new ArrayList<Object>();
}
    : assignment_expression[env]
    {
        $list = $assignment_expression.symbol;
    }
    | '{' exp = initializer[env]
    {
        ((List<Object>)$list).add($exp.list);
    }        
    (',' exp = initializer[env]
        {
            ((List<Object>)$list).add($exp.list);
        }
    )* '}'
    ;

type_specifier[Env env] returns [Symbol type]
@init
{
    Env declEnv = null;
}
    : 'void'
    {
        try
        {
            $type = env.getType("void");
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | 'char'
    {
        try
        {
            $type = env.getType("char");
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | 'int'
    {
        try
        {
            $type = env.getType("int");
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | 'va_list'
    {
        $type = new PointerSymbol(VoidSymbol.instance);
    }
    | 'struct'
    {
        $type = new StructSymbol();
        ((Aggregate)$type).define();
    }
    (Identifier
        {
            try
            {
                if (env.getType($Identifier.text) != null)
                {
                    $type = env.getType($Identifier.text);
                    if (! ($type instanceof StructSymbol))
                        throw new CompileException("'"+$Identifier.text+"' defined as wrong kind of tag");
                    else if (((Aggregate)$type).defined())
                        throw new CompileException("redefinition of 'struct "+$Identifier.text+"'");
                }
                env.setType($Identifier.text, $type);
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )?
    {
        declEnv = new Env(env);
    }
    '{' (curType = type_specifier[env] declarators[declEnv, $type, $curType.type]? ';')+ '}'
    {
        declEnv.clear();
    }
    | 'union'
    {
        $type = new UnionSymbol();
        ((Aggregate)$type).define();
    }
    (Identifier
        {
            try
            {
                if (env.getType($Identifier.text) != null)
                {
                    $type = env.getType($Identifier.text);
                    if (! ($type instanceof UnionSymbol))
                        throw new CompileException("'"+$Identifier.text+"' defined as wrong kind of tag");
                    else if (((Aggregate)$type) != null)
                        throw new CompileException("redefinition of 'struct "+$Identifier.text+"'");
                }
                env.setType($Identifier.text, $type);
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )?
    {
        declEnv = new Env(env);
    }
    '{' (curType = type_specifier[env] declarators[declEnv, $type, $curType.type]? ';')+ '}'
    {
        declEnv.clear();
    }
    | 'struct' Identifier
    {
        try
        {
            if (env.getType($Identifier.text) == null)
                env.setType($Identifier.text, new StructSymbol());
            $type = env.getType($Identifier.text);
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | 'union' Identifier
    {
        try
        {
            if (env.getType($Identifier.text) == null)
                env.setType($Identifier.text, new UnionSymbol());
            $type = env.getType($Identifier.text);
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ;

plain_declaration[Env env, Symbol bind] returns [Symbol symbol]
    : type_specifier[env] declarator[env, bind, $type_specifier.type]
    {
        $symbol = $declarator.symbol;
    }
    ;

declarator[Env env, Symbol bind, Symbol type] returns [Symbol symbol]
    : plain_declarator[env, type]
    ('[' dim = constant_expression[env] ']'
        {
            $symbol = new ArraySymbol($plain_declarator.type, ((ConstIntSymbol)($dim.symbol)).val);
        }
        | '[' ']'
        {
            $symbol = new ArraySymbol($plain_declarator.type);
        }
    )
    ('[' dim = constant_expression[env] ']'
        {
            ((ArraySymbol)$symbol).add(((ConstIntSymbol)($dim.symbol)).val);
        }
    )*
    {
        try
        {
            if ($plain_declarator.type instanceof VoidSymbol)
                throw new CompileException("declaration of '"+$plain_declarator.name+"' as array of voids");
            if (bind == null)
                env.setObj($plain_declarator.name, (ObjectSymbol)($symbol = new ObjectSymbol(false, $symbol)));
            else if (bind instanceof Aggregate)
                ((Aggregate)bind).set($plain_declarator.name, $symbol);
            else if (bind instanceof FuncSymbol)
                ((FuncSymbol)bind).set($plain_declarator.name, $symbol);
            else
                throw new CompileException("cannot bind to '"+bind.toString()+"'");
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | plain_declarator[env, type]
    {
        $symbol = new FuncSymbol($plain_declarator.type, env.getCallJump());
    }
    '(' parameters[env, (FuncSymbol)$symbol]? ')'
    {
        try
        {
            if (bind == null)
                env.setObj($plain_declarator.name, (ObjectSymbol)($symbol = new ObjectSymbol(true, $symbol)));
            else if (bind instanceof Aggregate)
                ((Aggregate)bind).set($plain_declarator.name, $symbol);
            else if (bind instanceof FuncSymbol)
                ((FuncSymbol)bind).set($plain_declarator.name, $symbol);
            else
                throw new CompileException("cannot bind to '"+bind.toString()+"'");
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | plain_declarator[env, type]
    {
        try
        {
            $symbol = $plain_declarator.type;
            if ($plain_declarator.type instanceof VoidSymbol)
                throw new CompileException("variable or field '"+$plain_declarator.name+"' declared void");
            if (bind == null)
                env.setObj($plain_declarator.name, (ObjectSymbol)($symbol = new ObjectSymbol(! ($symbol instanceof Aggregate), $symbol, true)));
            else if (bind instanceof Aggregate)
                ((Aggregate)bind).set($plain_declarator.name, $symbol);
            else if (bind instanceof FuncSymbol)
                ((FuncSymbol)bind).set($plain_declarator.name, $symbol);
            else
                throw new CompileException("cannot bind to '"+bind.toString()+"'");
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ;

plain_declarator[Env env, Symbol baseType] returns [String name, Symbol type]
    : Identifier
    {
        $name = $Identifier.text;
        $type = baseType;
    }
    | '*' lowerType = plain_declarator[env, baseType]
    {
        $name = $lowerType.name;
        $type = new PointerSymbol($lowerType.type);
    }
    ;

statement[Env env]
@init{
    Env compoundEnv = null;
}
    : va_start_statement[env] ';'
    | va_end_statement[env] ';'
    | (expression[env])? ';'
    | 
    {
        compoundEnv = new Env(env);
    }
    compound_statement[compoundEnv]
    {
        compoundEnv.clear();
    }
    | selection_statement[env]
    | iteration_statement[env]
    | jump_statement[env]
    ;

va_start_statement[Env env]
    : 'va_start' '(' exp0 = expression[env] ',' exp1 = expression[env] ')'
    {
        try
        {
            if (! $exp0.symbol.type.equals(new PointerSymbol(new VoidSymbol())))
                throw new CompileException("cannot convert '"+$exp0.symbol.typeInfo()+"' to 'va_list' for argument 1 to 'va_start'");
            ObjectSymbol.assign($exp0.symbol, env.getObj("..."),
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ;

va_arg_statement[Env env] returns [ObjectSymbol symbol]
    : 'va_arg' '(' expression[env] ',' type_name[env] ')'
    {
        try
        {
            if (! $expression.symbol.type.equals(new PointerSymbol(new VoidSymbol())))
                throw new CompileException("cannot convert '"+$expression.symbol.typeInfo()+"' to 'va_list' for argument 1 to 'va_arg'");
            ObjectSymbol addr = ObjectSymbol.assignRvalue(new ObjectSymbol(true, IntSymbol.instance, false), $expression.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            $symbol = new ObjectSymbol(true, $type_name.type, true, 0, addr);
            ObjectSymbol.plus($expression.symbol, $expression.symbol, new ConstIntSymbol($type_name.type.sizeof+(4-$type_name.type.sizeof % 4) % 4), true,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ;

va_end_statement[Env env]
    : 'va_end' '(' expression[env] ')'
    {
        try
        {
            if (! $expression.symbol.type.equals(new PointerSymbol(new VoidSymbol())))
                throw new CompileException("cannot convert '"+$expression.symbol.typeInfo()+"' to 'va_list' for argument 1 to 'va_end'");
            ObjectSymbol.assign($expression.symbol, new ConstIntSymbol(0),
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ;

compound_statement[Env env]
    : '{' declaration[env]* statement[env]* '}'
    ;

selection_statement[Env env]
@init
{
    JumpSymbol branchLabel = env.getBranchJump();
    JumpSymbol endLabel = env.getGotoJump();
}
    : 'if' '(' expression[env] ')'
    {
        try
        {
            IRGenerator.add("BEQZ", $expression.symbol, branchLabel.id,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            if (! ($expression.symbol.type instanceof Scalar))
                throw new CompileException("used "+$expression.symbol.typeInfo()+" type value where scalar is required");
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    statement[env]
    {
        IRGenerator.add(endLabel.id,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        IRGenerator.add(branchLabel,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    'else' statement[env]
    {
        IRGenerator.add(endLabel,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    | 'if' '(' expression[env] ')'
    {
        try
        {
            IRGenerator.add("BEQZ", $expression.symbol, branchLabel.id,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            if (! ($expression.symbol.type instanceof Scalar))
                throw new CompileException("used "+$expression.symbol.typeInfo()+" type value where scalar is required");
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    statement[env]
    {
        IRGenerator.add(branchLabel,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    ;

iteration_statement[Env env]
@init
{
    JumpSymbol breakLabel = env.getBreakJump();
    JumpSymbol continueLabel = env.getContinueJump();
    JumpSymbol bodyLabel = env.getGotoJump();
    String mainFork = IRGenerator.branch();
    String condFork;
    String stepFork;
    IRGenerator.getBind().inline = false;
}
@after
{
    try
    {
        env.removeContinueJump();
        env.removeBreakJump();
    }
    catch (CompileException e)
    {
        notifyErrorListeners(e.msg);
    }
}
    : 'while' '('
    {
        IRGenerator.add(continueLabel,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        condFork = IRGenerator.checkout();
    }
    expression[env]
    {
        try
        {
            if (! ($expression.symbol.type instanceof Scalar))
                throw new CompileException("used "+$expression.symbol.typeInfo()+" type value where scalar is required");
            IRGenerator.checkout(mainFork);
            IRGenerator.copy(condFork);
            IRGenerator.add("BEQZ", $expression.symbol, breakLabel.id,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            IRGenerator.checkout(condFork);
            IRGenerator.add("BNEZ", $expression.symbol, bodyLabel.id,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            IRGenerator.checkout(mainFork);
            IRGenerator.add(bodyLabel,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ')' statement[env]
    {
        IRGenerator.merge(condFork);
        IRGenerator.add(breakLabel,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    | 'for' '(' expression[env]? ';'
    {
        condFork = IRGenerator.checkout();
    }
    (condExp = expression[env]
        {
            try
            {
                IRGenerator.checkout(mainFork);
                IRGenerator.copy(condFork);
                if (! ($condExp.symbol.type instanceof Scalar))
                    throw new CompileException("used "+$condExp.symbol.typeInfo()+" type value where scalar is required");
                IRGenerator.add("BEQZ", $condExp.symbol, breakLabel.id,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                IRGenerator.checkout(condFork);
                IRGenerator.add("BNEZ", $condExp.symbol, bodyLabel.id,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                IRGenerator.checkout(mainFork);
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )? ';'
    {
        stepFork = IRGenerator.checkout();
    }
    expression[env]?
    {
        IRGenerator.checkout(mainFork);
    }
    ')'
    {
        IRGenerator.add(bodyLabel,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    statement[env]
    {
        IRGenerator.add(continueLabel,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        IRGenerator.merge(stepFork);
        IRGenerator.merge(condFork);
        IRGenerator.add(breakLabel,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    ;

jump_statement[Env env] returns [JumpSymbol jump]
    : 'continue' ';'
    {
        try
        {
            $jump = env.jumpToContinue(
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | 'break' ';'
    {
        try
        {
            $jump = env.jumpToBreak(
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | 'return' expression[env] ';'
    {
        try
        {
            $jump = env.jumpToReturn($expression.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | 'return' ';'
    {
        try
        {
            $jump = env.jumpToReturn(
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ;

expression[Env env] returns [ObjectSymbol symbol]
    : exp0 = assignment_expression[env]
    {
        $symbol = $exp0.symbol;
    }
    (',' exp = assignment_expression[env]
        {
            $symbol = $exp.symbol;
        }
    )*
    ;

assignment_expression[Env env] returns [ObjectSymbol symbol]
    : assignment_or_expression[env]
    {
        $symbol = $assignment_or_expression.symbol;
    }
    | assignment_xor_expression[env]
    {
        $symbol = $assignment_xor_expression.symbol;
    }
    | assignment_and_expression[env]
    {
        $symbol = $assignment_and_expression.symbol;
    }
    | assignment_shift_expression[env]
    {
        $symbol = $assignment_shift_expression.symbol;
    }
    | assignment_additive_expression[env]
    {
        $symbol = $assignment_additive_expression.symbol;
    }
    | assignment_multiplicative_expression[env]
    {
        $symbol = $assignment_multiplicative_expression.symbol;
    }
    | unary_expression[env] assignment_operator exp = assignment_expression[env]
    {
        try
        {
            switch ($assignment_operator.text)
            {
            case "=":
                $symbol = $exp.symbol;
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "*=":
                $symbol = ObjectSymbol.mul($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "/=":
                $symbol = ObjectSymbol.div($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "%=":
                $symbol = ObjectSymbol.mod($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "+=":
                $symbol = ObjectSymbol.plus($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "-=":
                $symbol = ObjectSymbol.minus($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "<<=":
                $symbol = ObjectSymbol.shiftLeft($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case ">>=":
                $symbol = ObjectSymbol.shiftRight($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "&=":
                $symbol = ObjectSymbol.and($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "^=":
                $symbol = ObjectSymbol.xor($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "|=":
                $symbol = ObjectSymbol.or($unary_expression.symbol, $exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            }
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
            $symbol = $unary_expression.symbol;
        }
    }
    | logical_or_expression[env]
    {
        $symbol = $logical_or_expression.symbol;
    }
    ;

assignment_operator
    : '='
    | '*='
    | '/='
    | '%='
    | '+='
    | '-='
    | '<<='
    | '>>='
    | '&='
    | '^='
    | '|='
    ;

assignment_or_expression[Env env] returns [ObjectSymbol symbol]
    : unary_expression[env] '=' exp0 = exclusive_or_expression[env] '|' exp1 = exclusive_or_expression[env]
    {
        try
        {
            $symbol = ObjectSymbol.or($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            if ($symbol instanceof ConstSymbol)
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
            $symbol = $unary_expression.symbol;
        }
    }
    ;

assignment_xor_expression[Env env] returns [ObjectSymbol symbol]
    : unary_expression[env] '=' exp0 = and_expression[env] '^' exp1 = and_expression[env]
    {
        try
        {
            $symbol = ObjectSymbol.or($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            if ($symbol instanceof ConstSymbol)
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
            $symbol = $unary_expression.symbol;
        }
    }
    ;

assignment_and_expression[Env env] returns [ObjectSymbol symbol]
    : unary_expression[env] '=' exp0 = equality_expression[env] '&' exp1 = equality_expression[env]
    {
        try
        {
            $symbol = ObjectSymbol.or($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            if ($symbol instanceof ConstSymbol)
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
            $symbol = $unary_expression.symbol;
        }
    }
    ;

assignment_shift_expression[Env env] returns [ObjectSymbol symbol]
    : unary_expression[env] '=' exp0 = additive_expression[env] op = ('<<' | '>>') exp1 = additive_expression[env]
    {
        try
        {
            switch ($op.text)
            {
            case "<<":
                $symbol = ObjectSymbol.shiftLeft($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case ">>":
                $symbol = ObjectSymbol.shiftRight($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            }
            if ($symbol instanceof ConstSymbol)
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
            $symbol = $unary_expression.symbol;
        }
    }
    ;

assignment_additive_expression[Env env] returns [ObjectSymbol symbol]
    : unary_expression[env] '=' exp0 = multiplicative_expression[env] op = ('+' | '-') exp1 = multiplicative_expression[env]
    {
        try
        {
            switch ($op.text)
            {
            case "+":
                $symbol = ObjectSymbol.plus($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "-":
                $symbol = ObjectSymbol.minus($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            }
            if ($symbol instanceof ConstSymbol)
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
            $symbol = $unary_expression.symbol;
        }
    }
    ;

assignment_multiplicative_expression[Env env] returns [ObjectSymbol symbol]
    : unary_expression[env] '=' exp0 = cast_expression[env] op = ('*' | '/' | '%') exp1 = cast_expression[env]
    {
        try
        {
            switch ($op.text)
            {
            case "*":
                $symbol = ObjectSymbol.mul($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "/":
                $symbol = ObjectSymbol.div($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            case "%":
                $symbol = ObjectSymbol.mod($unary_expression.symbol, $exp0.symbol, $exp1.symbol, false,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                break;
            }
            if ($symbol instanceof ConstSymbol)
                $symbol = ObjectSymbol.assign($unary_expression.symbol, $symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
            $symbol = $unary_expression.symbol;
        }
    }
    ;

constant_expression[Env env] returns [ConstSymbol symbol]
    : logical_or_expression[env]
    {
        try
        {
            if (! ($logical_or_expression.symbol instanceof ConstSymbol))
            {
                $symbol = new ConstIntSymbol();
                throw new CompileException("used '"+$logical_or_expression.symbol.typeInfo()+"' when a constant is required");
            }
            $symbol = (ConstSymbol)$logical_or_expression.symbol;
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ;

logical_or_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    JumpSymbol jump = null;
    ObjectSymbol tmp = null;
}
    : logical_and_expression[env]
    {
        try
        {
            jump = env.getBranchJump();
            ObjectSymbol boolExp = ObjectSymbol.toBool($logical_and_expression.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            $symbol = ObjectSymbol.assignRvalue(new ObjectSymbol(true, IntSymbol.instance), boolExp,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ('||'
        {
            IRGenerator.add("BNEZ", $symbol, jump.id,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        exp = logical_and_expression[env]
        {
            try
            {
                ObjectSymbol boolExp = ObjectSymbol.toBool($exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.logicOr($symbol, $symbol, boolExp,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )+
    {
        IRGenerator.add(jump,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    | logical_and_expression[env]
    {
        $symbol = $logical_and_expression.symbol;
    }
    ;

logical_and_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    JumpSymbol jump = null;
    ObjectSymbol tmp = null;
}
    : inclusive_or_expression[env]
    {
        try
        {
            jump = env.getBranchJump();
            ObjectSymbol boolExp = ObjectSymbol.toBool($inclusive_or_expression.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            $symbol = ObjectSymbol.assignRvalue(new ObjectSymbol(true, IntSymbol.instance), boolExp,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    ('&&'
        {
            IRGenerator.add("BEQZ", $symbol, jump.id,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        exp = inclusive_or_expression[env]
        {
            try
            {
                ObjectSymbol boolExp = ObjectSymbol.toBool($exp.symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                $symbol = ObjectSymbol.logicAnd($symbol, $symbol, boolExp,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )+
    {
        IRGenerator.add(jump,
            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
    }
    | inclusive_or_expression[env]
    {
        $symbol = $inclusive_or_expression.symbol;
    }
    ;

inclusive_or_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    ObjectSymbol tmp = null;
}
    : exclusive_or_expression[env]
    {
        $symbol = $exclusive_or_expression.symbol;
    }
    ('|' exp = exclusive_or_expression[env]
        {
            try
            {
                if (tmp == null)
                {
                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
                    $symbol = tmp = ObjectSymbol.or(tmp, $symbol, $exp.symbol, true,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    if (tmp instanceof ConstSymbol)
                        tmp = null;
                }
                else
                    $symbol = tmp = ObjectSymbol.or(tmp, tmp, $exp.symbol, true,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )*
    ;

exclusive_or_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    ObjectSymbol tmp = null;
}
    : and_expression[env]
    {
        $symbol = $and_expression.symbol;
    }
    ('^' exp = and_expression[env]
        {
            try
            {
                if (tmp == null)
                {
                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
                    $symbol = tmp = ObjectSymbol.xor(tmp, $symbol, $exp.symbol, true,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    if (tmp instanceof ConstSymbol)
                        tmp = null;
                }
                else
                    $symbol = tmp = ObjectSymbol.xor(tmp, tmp, $exp.symbol, true,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )*
    ;

and_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    ObjectSymbol tmp = null;
}
    : equality_expression[env]
    {
        $symbol = $equality_expression.symbol;
    }
    ('&' exp = equality_expression[env]
        {
            try
            {
                if (tmp == null)
                {
                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
                    $symbol = tmp = ObjectSymbol.and(tmp, $symbol, $exp.symbol, true,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    if (tmp instanceof ConstSymbol)
                        tmp = null;
                }
                else
                    $symbol = tmp = ObjectSymbol.and(tmp, tmp, $exp.symbol, true,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )*
    ;

equality_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    ObjectSymbol tmp = null;
}
    : relational_expression[env]
    {
        $symbol = $relational_expression.symbol;
    }
    (op = ('==' | '!=') exp = relational_expression[env]
        {
            try
            {
                switch ($op.text)
                {
                case "==":
                    $symbol = ObjectSymbol.equal($symbol, $exp.symbol,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    break;
                case "!=":
                    $symbol = ObjectSymbol.notEqual($symbol, $exp.symbol,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    break;
                }
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )*
    ;

relational_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    ObjectSymbol tmp = null;
}
    : shift_expression[env]
    {
        $symbol = $shift_expression.symbol;
    }
    (op = ('<=' | '>=' | '<' | '>') exp = shift_expression[env]
        {
            try
            {
                switch ($op.text)
                {
                case "<=":
                    $symbol = ObjectSymbol.lessEqual($symbol, $exp.symbol,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    break;
                case ">=":
                    $symbol = ObjectSymbol.greaterEqual($symbol, $exp.symbol,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    break;
                case "<":
                    $symbol = ObjectSymbol.less($symbol, $exp.symbol,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    break;
                case ">":
                    $symbol = ObjectSymbol.greater($symbol, $exp.symbol,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                    break;
                }
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )*
    ;

shift_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    ObjectSymbol tmp = null;
}
    : additive_expression[env]
    {
        $symbol = $additive_expression.symbol;
    }
    (op = ('<<' | '>>') exp = additive_expression[env]
        {
            try
            {
                if (tmp == null)
                {
                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
                    switch ($op.text)
                    {
                    case "<<":
                        $symbol = tmp = ObjectSymbol.shiftLeft(tmp, $symbol, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    case ">>":
                        $symbol = tmp = ObjectSymbol.shiftRight(tmp, $symbol, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    }
                    if (tmp instanceof ConstSymbol)
                        tmp = null;
                }
                else
                    switch ($op.text)
                    {
                    case "<<":
                        $symbol = tmp = ObjectSymbol.shiftLeft(tmp, tmp, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    case ">>":
                        $symbol = tmp = ObjectSymbol.shiftRight(tmp, tmp, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    }
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )*
    ;

additive_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    ObjectSymbol tmp = null;
}
    : multiplicative_expression[env]
    {
        $symbol = $multiplicative_expression.symbol;
    }
    (op = ('+' | '-') exp = multiplicative_expression[env]
        {
            try
            {
                if (tmp == null)
                {
                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
                    switch ($op.text)
                    {
                    case "+":
                        $symbol = tmp = ObjectSymbol.plus(tmp, $symbol, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    case "-":
                        $symbol = tmp = ObjectSymbol.minus(tmp, $symbol, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    }
                    if (tmp instanceof ConstSymbol)
                        tmp = null;
                }
                else
                    switch ($op.text)
                    {
                    case "+":
                        $symbol = tmp = ObjectSymbol.plus(tmp, tmp, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    case "-":
                        $symbol = tmp = ObjectSymbol.minus(tmp, tmp, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    }
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )*
    ;

multiplicative_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    ObjectSymbol tmp = null;
}
    : cast_expression[env]
    {
        $symbol = $cast_expression.symbol;
    }
    (op = ('*' | '/' | '%') exp = cast_expression[env]
        {
            try
            {
                if (tmp == null)
                {
                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
                    switch ($op.text)
                    {
                    case "*":
                        $symbol = tmp = ObjectSymbol.mul(tmp, $symbol, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    case "/":
                        $symbol = tmp = ObjectSymbol.div(tmp, $symbol, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    case "%":
                        $symbol = tmp = ObjectSymbol.mod(tmp, $symbol, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    }
                    if (tmp instanceof ConstSymbol)
                        tmp = null;
                }
                else
                    switch ($op.text)
                    {
                    case "*":
                        $symbol = tmp = ObjectSymbol.mul(tmp, tmp, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    case "/":
                        $symbol = tmp = ObjectSymbol.div(tmp, tmp, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    case "%":
                        $symbol = tmp = ObjectSymbol.mod(tmp, tmp, $exp.symbol, true,
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                        break;
                    }
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
    )*
    ;

cast_expression[Env env] returns [ObjectSymbol symbol]
    : '(' type_name[env] ')' exp = cast_expression[env]
    {
        try
        {
            if (! ($type_name.type instanceof Scalar))
                throw new CompileException("conversion to non-scalar type requested");
            $symbol = ObjectSymbol.assign(new ObjectSymbol(true, $type_name.type, true), $exp.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | unary_expression[env]
    {
        $symbol = $unary_expression.symbol;
    }
    ;

type_name[Env env] returns [Symbol type]
    : type_specifier[env]
    {
        $type = $type_specifier.type;
    }
    ('*'
        {
            $type = new PointerSymbol($type);
        }
    )*
    ;

unary_expression[Env env] returns [ObjectSymbol symbol]
@init
{
    List<ObjectSymbol> subscript = null;
}
    : 'sizeof' '(' type_name[env] ')'
    {
        $symbol = new ConstIntSymbol($type_name.type.sizeof);
    }
    | '++' exp = unary_expression[env]
    {
        try
        {
            $symbol = ObjectSymbol.inc($exp.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | '--' exp = unary_expression[env]
    {
        try
        {
            $symbol = ObjectSymbol.dec($exp.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | 'sizeof' exp = unary_expression[env]
    {
        $symbol = new ConstIntSymbol($exp.symbol.sizeof);
    }
    | '&' cast_expression[env]
    {
        try
        {
            $symbol = $cast_expression.symbol.ref(
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | '*' cast_expression[env]
    {
        try
        {
            $symbol = $cast_expression.symbol.deref(
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | '+' cast_expression[env]
    {
        try
        {
            $symbol = ObjectSymbol.posi($cast_expression.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | '-' cast_expression[env]
    {
        try
        {
            $symbol = ObjectSymbol.neg($cast_expression.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | '~' cast_expression[env]
    {
        $symbol = new ObjectSymbol(true, $cast_expression.symbol);
        try
        {
            $symbol = ObjectSymbol.not($cast_expression.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | '!' cast_expression[env]
    {
        $symbol = new ObjectSymbol(true, $cast_expression.symbol);
        try
        {
            $symbol = ObjectSymbol.logicNot($cast_expression.symbol,
                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | primary_expression[env]
    {
        $symbol = $primary_expression.symbol;
    }
        (('[' expression[env] ']'
            {
                if (subscript == null)
                    subscript = new ArrayList<ObjectSymbol>();
                subscript.add($expression.symbol);
            }
        )+
        {
            try
            {
                for (ObjectSymbol index : subscript)
                    $symbol = $symbol.subscript(index,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                subscript = null;
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
        | '(' arguments[env] ')'
        {
            try
            {
                if ($symbol.type.equals(env.getObj("printf").type) && $arguments.args.size() >= 1 && $arguments.args.get(0) instanceof ConstStringSymbol)
                {
                    String format = ((ConstStringSymbol)$arguments.args.get(0)).val;
                    String buffer = "";
                    for (int i = 0, j = 0; i < format.length(); ++i)
                    {
                        if (format.charAt(i) == '%')
                        {
                            ++i;
                            switch (format.charAt(i))
                            {
                            case 'd':
                                ++j;
                                if ($arguments.args.get(j) instanceof ConstCharSymbol)
                                    buffer += (int)((ConstCharSymbol)$arguments.args.get(j)).val;
                                else if ($arguments.args.get(j) instanceof ConstIntSymbol)
                                    buffer += $arguments.args.get(j).toString();
                                else
                                {
                                    if (buffer != "")
                                        IRGenerator.syscall("WRITESTR", new ConstStringSymbol(buffer),
                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                    buffer = "";
                                    IRGenerator.syscall("WRITEINT", $arguments.args.get(j),
                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                }
                                break;
                            case 'c':
                                ++j;
                                if ($arguments.args.get(j) instanceof ConstCharSymbol)
                                    buffer += ((ConstCharSymbol)$arguments.args.get(j)).val;
                                else if ($arguments.args.get(j) instanceof ConstIntSymbol)
                                    buffer += (char)((ConstIntSymbol)$arguments.args.get(j)).val;
                                else
                                {
                                    if (buffer != "")
                                        IRGenerator.syscall("WRITESTR", new ConstStringSymbol(buffer),
                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                    buffer = "";
                                    IRGenerator.syscall("WRITECHAR", $arguments.args.get(j),
                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                }
                                break;
                            case 's':
                                ++j;
                                if ($arguments.args.get(j) instanceof ConstStringSymbol)
                                    buffer += $arguments.args.get(j).toString();
                                else
                                {
                                    IRGenerator.syscall("WRITESTR", new ConstStringSymbol(buffer),
                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                    buffer = "";
                                    IRGenerator.syscall("WRITESTR", $arguments.args.get(j),
                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                }
                                break;
                            case '0':
                                ++j;
                                int len = format.charAt(++i)-'0';
                                ++i;
                                if ($arguments.args.get(j) instanceof ConstCharSymbol)
                                    buffer += java.lang.String.format("%0"+len+"d", (int)((ConstCharSymbol)$arguments.args.get(j)).val);
                                else if ($arguments.args.get(j) instanceof ConstIntSymbol)
                                    buffer += java.lang.String.format("%0"+len+"d", ((ConstIntSymbol)$arguments.args.get(j)).val);
                                else
                                {
                                    ObjectSymbol tmp = ObjectSymbol.assign(new ObjectSymbol(true, IntSymbol.instance, true), $arguments.args.get(j),
                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                    int limit = 1;
                                    for (int k = 1; k < len; ++k)
                                        limit *= 10;
                                    for (int k = 1; k < len; ++k)
                                    {
                                        JumpSymbol branchLabel = env.getBranchJump();
                                        ObjectSymbol cond = ObjectSymbol.less($arguments.args.get(j), new ConstIntSymbol(limit),
                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                        IRGenerator.add("BEQZ", cond, branchLabel.id,
                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                        IRGenerator.syscall("WRITECHAR", new ConstCharSymbol('0'),
                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                        IRGenerator.add(branchLabel,
                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                        limit /= 10;
                                    }
                                    IRGenerator.syscall("WRITEINT", $arguments.args.get(j),
                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                                }
                                break;
                            }
                        }
                        else
                            buffer += format.charAt(i);
                    }
                    if (buffer != "")
                        IRGenerator.syscall("WRITESTR", new ConstStringSymbol(buffer),
                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                }
                else
                {
                    IRGenerator.getBind().inline = false;
                    $symbol = $symbol.call($arguments.args,
                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
                }
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
        | '.' Identifier
        {
            try
            {
                $symbol = $symbol.get($Identifier.text,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
        | '->' id_arrow = Identifier
        {
            try
            {
                $symbol = $symbol.arrow($id_arrow.text,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
        | '++'
        {   
            try
            {
                $symbol = ObjectSymbol.postInc($symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
        | '--'
        {
            try
            {
                $symbol = ObjectSymbol.postDec($symbol,
                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+$start.getLine()+":"+$start.getCharPositionInLine(), $text));
            }
            catch (CompileException e)
            {
                notifyErrorListeners(e.msg);
            }
        }
        )*
    ;

arguments[Env env] returns [List<ObjectSymbol> args]
@init
{
    $args = new ArrayList<ObjectSymbol>();
}
    : (arg = assignment_expression[env]
    {
        $args.add($arg.symbol);
    }
    )? (',' arg = assignment_expression[env]
        {
            $args.add($arg.symbol);
        }
    )*
    ;

primary_expression[Env env] returns [ObjectSymbol symbol]
    : va_arg_statement[env]
    {
        $symbol = $va_arg_statement.symbol;
    }
    | Identifier
    {
        try
        {
            $symbol = env.getObj($Identifier.text);
        }
        catch (CompileException e)
        {
            notifyErrorListeners(e.msg);
        }
    }
    | IntegerConstant
    {
        $symbol = new ConstIntSymbol(Integer.parseInt($IntegerConstant.text));
    }
    | HexIntegerConstant
    {
        int val = 0;
        for (char i : $HexIntegerConstant.text.toLowerCase().substring(2).toCharArray())
            val = val*16+(Character.isDigit(i) ? (int)i-'0' : (int)i-'a'+10);
        $symbol = new ConstIntSymbol(val);
    }
    | OctIntegerConstant
    {
        int val = 0;
        for (char i : $OctIntegerConstant.text.toCharArray())
            val = val*8+(int)i-'0';
        $symbol = new ConstIntSymbol(val);
    }
    | CharacterConstant
    {
        switch ($CharacterConstant.text.substring(1, $CharacterConstant.text.length()-1))
        {
        case "\\'":
            $symbol = new ConstCharSymbol('\'');
            break;
        case "\\\"":
            $symbol = new ConstCharSymbol('\"');
            break;
        case "\\?":
            $symbol = new ConstCharSymbol('?');
            break;
        case "\\\\":
            $symbol = new ConstCharSymbol('\\');
            break;
        case "\\a":
            $symbol = new ConstCharSymbol((char)7);
            break;
        case "\\b":
            $symbol = new ConstCharSymbol((char)8);
            break;
        case "\\f":
            $symbol = new ConstCharSymbol((char)12);
            break;
        case "\\n":
            $symbol = new ConstCharSymbol('\n');
            break;
        case "\\r":
            $symbol = new ConstCharSymbol('\r');
            break;
        case "\\t":
            $symbol = new ConstCharSymbol('\t');
            break;
        case "\\v":
            $symbol = new ConstCharSymbol((char)11);
            break;
        case "??=":
            $symbol = new ConstCharSymbol('#');
            break;
        case "??)":
            $symbol = new ConstCharSymbol(']');
            break;
        case "??!":
            $symbol = new ConstCharSymbol('|');
            break;
        case "??(":
            $symbol = new ConstCharSymbol('[');
            break;
        case "??'":
            $symbol = new ConstCharSymbol('^');
            break;
        case "??>":
            $symbol = new ConstCharSymbol('}');
            break;
        case "??/":
            $symbol = new ConstCharSymbol('\\');
            break;
        case "??<":
            $symbol = new ConstCharSymbol('{');
            break;
        case "??-":
            $symbol = new ConstCharSymbol('~');
            break;
        default:
            if ($CharacterConstant.text.startsWith("'\\x"))
            {
                int val = 0;
                for (char i : $CharacterConstant.text.toLowerCase().substring(3, $CharacterConstant.text.length()-1).toCharArray())
                    val = val*16+(Character.isDigit(i) ? (int)i-'0' : (int)i-'a'+10);
                $symbol = new ConstCharSymbol((char)val);
            }
            else if ($CharacterConstant.text.startsWith("'\\"))
            {
                int val = 0;
                for (char i : $CharacterConstant.text.toLowerCase().substring(2, $CharacterConstant.text.length()-1).toCharArray())
                    val = val*8+((int)i-'0');
                $symbol = new ConstCharSymbol((char)val);
            }
            else
                $symbol = new ConstCharSymbol($CharacterConstant.text.charAt(1));
        }
    }
    | String
    {
        $symbol = new ConstStringSymbol($String.text.substring(1, $String.text.length()-1));
    }
    | '(' expression[env] ')'
    {
        $symbol = $expression.symbol;
    }
    ;

Identifier
    : ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*
    ;

HexIntegerConstant
    : '0' ('X' | 'x') ('0'..'9' | 'A'..'F' | 'a'..'f')+
    ;

IntegerConstant
    : ('1'..'9') ('0'..'9')*
    | '0'
    ;

OctIntegerConstant
    : '0' ('0'..'7')+
    ;

String
    : '\"' ('\'' | Character)* '\"'
    ;

CharacterConstant
    : '\'' Character '\''
    ;

fragment
Character
    : '\\\''
    | '\\\"'
    | '\\?'
    | '\\\\'
    | '\\a'
    | '\\b'
    | '\\f'
    | '\\n'
    | '\\r'
    | '\\t'
    | '\\v'.
    | '\\' OctDigit OctDigit OctDigit
    | '\\' OctDigit OctDigit
    | '\\' OctDigit
    | '\\x' HexDigit+
    | '??='
    | '??)'
    | '??!'
    | '??('
    | '??\''
    | '??>'
    | '??/'
    | '??<'
    | '??-'
    | ~('\n'|'\r'|'\"'|'\''|'\\')
    ;

fragment
OctDigit
    : ('0'..'7')
    ;

fragment
HexDigit
    : ('0'..'9')
    | ('A'..'F')
    | ('a'..'f')
    ;

Comment
    : ('//' ~('\n'|'\r')* '\r'? '\n' | '/*' (.)*? '*/') -> skip
    ;

PP
    : '#' ~('\n')* -> skip
    ;

WS
    : (' '|'\t'|'\n'|'\r')+ -> skip
    ;
