// Generated from CMinus.g4 by ANTLR 4.2.2

    import java.io.*;
    import java.util.*;
    import java.math.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CMinusParser}.
 */
public interface CMinusListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CMinusParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull CMinusParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull CMinusParser.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#inclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterInclusive_or_expression(@NotNull CMinusParser.Inclusive_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#inclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitInclusive_or_expression(@NotNull CMinusParser.Inclusive_or_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#assignment_xor_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_xor_expression(@NotNull CMinusParser.Assignment_xor_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#assignment_xor_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_xor_expression(@NotNull CMinusParser.Assignment_xor_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#declarator}.
	 * @param ctx the parse tree
	 */
	void enterDeclarator(@NotNull CMinusParser.DeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#declarator}.
	 * @param ctx the parse tree
	 */
	void exitDeclarator(@NotNull CMinusParser.DeclaratorContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#va_start_statement}.
	 * @param ctx the parse tree
	 */
	void enterVa_start_statement(@NotNull CMinusParser.Va_start_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#va_start_statement}.
	 * @param ctx the parse tree
	 */
	void exitVa_start_statement(@NotNull CMinusParser.Va_start_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#assignment_and_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_and_expression(@NotNull CMinusParser.Assignment_and_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#assignment_and_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_and_expression(@NotNull CMinusParser.Assignment_and_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_expression(@NotNull CMinusParser.Assignment_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_expression(@NotNull CMinusParser.Assignment_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#constant_expression}.
	 * @param ctx the parse tree
	 */
	void enterConstant_expression(@NotNull CMinusParser.Constant_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#constant_expression}.
	 * @param ctx the parse tree
	 */
	void exitConstant_expression(@NotNull CMinusParser.Constant_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#multiplicative_expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicative_expression(@NotNull CMinusParser.Multiplicative_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#multiplicative_expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicative_expression(@NotNull CMinusParser.Multiplicative_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#relational_expression}.
	 * @param ctx the parse tree
	 */
	void enterRelational_expression(@NotNull CMinusParser.Relational_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#relational_expression}.
	 * @param ctx the parse tree
	 */
	void exitRelational_expression(@NotNull CMinusParser.Relational_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void enterCompound_statement(@NotNull CMinusParser.Compound_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void exitCompound_statement(@NotNull CMinusParser.Compound_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void enterJump_statement(@NotNull CMinusParser.Jump_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void exitJump_statement(@NotNull CMinusParser.Jump_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#cast_expression}.
	 * @param ctx the parse tree
	 */
	void enterCast_expression(@NotNull CMinusParser.Cast_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#cast_expression}.
	 * @param ctx the parse tree
	 */
	void exitCast_expression(@NotNull CMinusParser.Cast_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void enterEquality_expression(@NotNull CMinusParser.Equality_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void exitEquality_expression(@NotNull CMinusParser.Equality_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#assignment_operator}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_operator(@NotNull CMinusParser.Assignment_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#assignment_operator}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_operator(@NotNull CMinusParser.Assignment_operatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary_expression(@NotNull CMinusParser.Unary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary_expression(@NotNull CMinusParser.Unary_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#assignment_shift_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_shift_expression(@NotNull CMinusParser.Assignment_shift_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#assignment_shift_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_shift_expression(@NotNull CMinusParser.Assignment_shift_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#declarators}.
	 * @param ctx the parse tree
	 */
	void enterDeclarators(@NotNull CMinusParser.DeclaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#declarators}.
	 * @param ctx the parse tree
	 */
	void exitDeclarators(@NotNull CMinusParser.DeclaratorsContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(@NotNull CMinusParser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(@NotNull CMinusParser.ParametersContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition(@NotNull CMinusParser.Function_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition(@NotNull CMinusParser.Function_definitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#va_end_statement}.
	 * @param ctx the parse tree
	 */
	void enterVa_end_statement(@NotNull CMinusParser.Va_end_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#va_end_statement}.
	 * @param ctx the parse tree
	 */
	void exitVa_end_statement(@NotNull CMinusParser.Va_end_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#assignment_multiplicative_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_multiplicative_expression(@NotNull CMinusParser.Assignment_multiplicative_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#assignment_multiplicative_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_multiplicative_expression(@NotNull CMinusParser.Assignment_multiplicative_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#program_expression}.
	 * @param ctx the parse tree
	 */
	void enterProgram_expression(@NotNull CMinusParser.Program_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#program_expression}.
	 * @param ctx the parse tree
	 */
	void exitProgram_expression(@NotNull CMinusParser.Program_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(@NotNull CMinusParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(@NotNull CMinusParser.DeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#init_declarator}.
	 * @param ctx the parse tree
	 */
	void enterInit_declarator(@NotNull CMinusParser.Init_declaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#init_declarator}.
	 * @param ctx the parse tree
	 */
	void exitInit_declarator(@NotNull CMinusParser.Init_declaratorContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#plain_declarator}.
	 * @param ctx the parse tree
	 */
	void enterPlain_declarator(@NotNull CMinusParser.Plain_declaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#plain_declarator}.
	 * @param ctx the parse tree
	 */
	void exitPlain_declarator(@NotNull CMinusParser.Plain_declaratorContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#selection_statement}.
	 * @param ctx the parse tree
	 */
	void enterSelection_statement(@NotNull CMinusParser.Selection_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#selection_statement}.
	 * @param ctx the parse tree
	 */
	void exitSelection_statement(@NotNull CMinusParser.Selection_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#init_declarators}.
	 * @param ctx the parse tree
	 */
	void enterInit_declarators(@NotNull CMinusParser.Init_declaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#init_declarators}.
	 * @param ctx the parse tree
	 */
	void exitInit_declarators(@NotNull CMinusParser.Init_declaratorsContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#exclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterExclusive_or_expression(@NotNull CMinusParser.Exclusive_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#exclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitExclusive_or_expression(@NotNull CMinusParser.Exclusive_or_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#assignment_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_or_expression(@NotNull CMinusParser.Assignment_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#assignment_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_or_expression(@NotNull CMinusParser.Assignment_or_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull CMinusParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull CMinusParser.StatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#va_arg_statement}.
	 * @param ctx the parse tree
	 */
	void enterVa_arg_statement(@NotNull CMinusParser.Va_arg_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#va_arg_statement}.
	 * @param ctx the parse tree
	 */
	void exitVa_arg_statement(@NotNull CMinusParser.Va_arg_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_and_expression(@NotNull CMinusParser.Logical_and_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_and_expression(@NotNull CMinusParser.Logical_and_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#additive_expression}.
	 * @param ctx the parse tree
	 */
	void enterAdditive_expression(@NotNull CMinusParser.Additive_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#additive_expression}.
	 * @param ctx the parse tree
	 */
	void exitAdditive_expression(@NotNull CMinusParser.Additive_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(@NotNull CMinusParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(@NotNull CMinusParser.ArgumentsContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void enterPrimary_expression(@NotNull CMinusParser.Primary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void exitPrimary_expression(@NotNull CMinusParser.Primary_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#and_expression}.
	 * @param ctx the parse tree
	 */
	void enterAnd_expression(@NotNull CMinusParser.And_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#and_expression}.
	 * @param ctx the parse tree
	 */
	void exitAnd_expression(@NotNull CMinusParser.And_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void enterShift_expression(@NotNull CMinusParser.Shift_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void exitShift_expression(@NotNull CMinusParser.Shift_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(@NotNull CMinusParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(@NotNull CMinusParser.ProgramContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_or_expression(@NotNull CMinusParser.Logical_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_or_expression(@NotNull CMinusParser.Logical_or_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void enterIteration_statement(@NotNull CMinusParser.Iteration_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void exitIteration_statement(@NotNull CMinusParser.Iteration_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(@NotNull CMinusParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(@NotNull CMinusParser.Type_nameContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#type_specifier}.
	 * @param ctx the parse tree
	 */
	void enterType_specifier(@NotNull CMinusParser.Type_specifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#type_specifier}.
	 * @param ctx the parse tree
	 */
	void exitType_specifier(@NotNull CMinusParser.Type_specifierContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#plain_declaration}.
	 * @param ctx the parse tree
	 */
	void enterPlain_declaration(@NotNull CMinusParser.Plain_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#plain_declaration}.
	 * @param ctx the parse tree
	 */
	void exitPlain_declaration(@NotNull CMinusParser.Plain_declarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#assignment_additive_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_additive_expression(@NotNull CMinusParser.Assignment_additive_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#assignment_additive_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_additive_expression(@NotNull CMinusParser.Assignment_additive_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link CMinusParser#initializer}.
	 * @param ctx the parse tree
	 */
	void enterInitializer(@NotNull CMinusParser.InitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link CMinusParser#initializer}.
	 * @param ctx the parse tree
	 */
	void exitInitializer(@NotNull CMinusParser.InitializerContext ctx);
}