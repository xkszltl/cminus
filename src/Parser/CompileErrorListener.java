import java.io.*;
import java.util.*;
import java.math.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class CompileErrorListener extends BaseErrorListener
{
    public static String fileName = "";

    public int errorCount = 0;

    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
    {
        ++errorCount;
        System.err.println("\033[1;30m"+fileName+":"+line+":"+charPositionInLine+": "+"\033[1;31m"+"error: "+"\033[1;30m"+msg+"\033[0m");
        String input;
        if (recognizer.getInputStream() instanceof TokenStream)
        {
        
            input = ((CommonTokenStream)recognizer.getInputStream()).getTokenSource().getInputStream().toString();
            System.err.println(" "+input.split("\n")[line-1]);
            for (int i = 0; i <= charPositionInLine; ++i)
                System.err.print(" ");
            int start = ((Token)offendingSymbol).getStartIndex();
            int stop = ((Token)offendingSymbol).getStopIndex();
            if (start >= 0 && stop >= 0)
            {
                System.err.print("\033[32m");
                for (int i = start; i <= stop; ++i)
                    System.err.print("^");
                System.err.print("\033[0m");
            }
            System.err.println();
        }
        else
        {
            input = recognizer.getInputStream().toString();
            System.err.println(" "+input.split("\n")[line-1]);
            for (int i = 0; i <= charPositionInLine; ++i)
                System.err.print(" ");
            System.err.println("\033[32m"+"^"+"\033[0m");
        }
    }
}