import java.io.*;
import java.util.*;
import java.math.*;

public class CompileException extends Exception
{
    public String msg;

    public CompileException()
    {
        super();
    }

    public CompileException(String _msg)
    {
        super();
        msg = _msg;
    }
}