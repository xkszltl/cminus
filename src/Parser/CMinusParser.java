// Generated from CMinus.g4 by ANTLR 4.2.2

    import java.io.*;
    import java.util.*;
    import java.math.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CMinusParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__60=1, T__59=2, T__58=3, T__57=4, T__56=5, T__55=6, T__54=7, T__53=8, 
		T__52=9, T__51=10, T__50=11, T__49=12, T__48=13, T__47=14, T__46=15, T__45=16, 
		T__44=17, T__43=18, T__42=19, T__41=20, T__40=21, T__39=22, T__38=23, 
		T__37=24, T__36=25, T__35=26, T__34=27, T__33=28, T__32=29, T__31=30, 
		T__30=31, T__29=32, T__28=33, T__27=34, T__26=35, T__25=36, T__24=37, 
		T__23=38, T__22=39, T__21=40, T__20=41, T__19=42, T__18=43, T__17=44, 
		T__16=45, T__15=46, T__14=47, T__13=48, T__12=49, T__11=50, T__10=51, 
		T__9=52, T__8=53, T__7=54, T__6=55, T__5=56, T__4=57, T__3=58, T__2=59, 
		T__1=60, T__0=61, Identifier=62, HexIntegerConstant=63, IntegerConstant=64, 
		OctIntegerConstant=65, String=66, CharacterConstant=67, Comment=68, PP=69, 
		WS=70;
	public static final String[] tokenNames = {
		"<INVALID>", "'&'", "'['", "'*'", "'<'", "'--'", "'continue'", "'!='", 
		"'<='", "'<<'", "'}'", "'char'", "'%'", "'->'", "'*='", "'union'", "')'", 
		"'='", "'|='", "'|'", "'!'", "'sizeof'", "'<<='", "']'", "'-='", "','", 
		"'-'", "'while'", "'('", "'&='", "'if'", "'int'", "'va_arg'", "'void'", 
		"'>>='", "'{'", "'...'", "'break'", "'+='", "'^='", "'else'", "'struct'", 
		"'++'", "'>>'", "'va_end'", "'^'", "'.'", "'+'", "'for'", "'return'", 
		"';'", "'&&'", "'va_list'", "'||'", "'>'", "'%='", "'/='", "'/'", "'=='", 
		"'~'", "'>='", "'va_start'", "Identifier", "HexIntegerConstant", "IntegerConstant", 
		"OctIntegerConstant", "String", "CharacterConstant", "Comment", "PP", 
		"WS"
	};
	public static final int
		RULE_program = 0, RULE_program_expression = 1, RULE_declaration = 2, RULE_function_definition = 3, 
		RULE_parameters = 4, RULE_declarators = 5, RULE_init_declarators = 6, 
		RULE_init_declarator = 7, RULE_initializer = 8, RULE_type_specifier = 9, 
		RULE_plain_declaration = 10, RULE_declarator = 11, RULE_plain_declarator = 12, 
		RULE_statement = 13, RULE_va_start_statement = 14, RULE_va_arg_statement = 15, 
		RULE_va_end_statement = 16, RULE_compound_statement = 17, RULE_selection_statement = 18, 
		RULE_iteration_statement = 19, RULE_jump_statement = 20, RULE_expression = 21, 
		RULE_assignment_expression = 22, RULE_assignment_operator = 23, RULE_assignment_or_expression = 24, 
		RULE_assignment_xor_expression = 25, RULE_assignment_and_expression = 26, 
		RULE_assignment_shift_expression = 27, RULE_assignment_additive_expression = 28, 
		RULE_assignment_multiplicative_expression = 29, RULE_constant_expression = 30, 
		RULE_logical_or_expression = 31, RULE_logical_and_expression = 32, RULE_inclusive_or_expression = 33, 
		RULE_exclusive_or_expression = 34, RULE_and_expression = 35, RULE_equality_expression = 36, 
		RULE_relational_expression = 37, RULE_shift_expression = 38, RULE_additive_expression = 39, 
		RULE_multiplicative_expression = 40, RULE_cast_expression = 41, RULE_type_name = 42, 
		RULE_unary_expression = 43, RULE_arguments = 44, RULE_primary_expression = 45;
	public static final String[] ruleNames = {
		"program", "program_expression", "declaration", "function_definition", 
		"parameters", "declarators", "init_declarators", "init_declarator", "initializer", 
		"type_specifier", "plain_declaration", "declarator", "plain_declarator", 
		"statement", "va_start_statement", "va_arg_statement", "va_end_statement", 
		"compound_statement", "selection_statement", "iteration_statement", "jump_statement", 
		"expression", "assignment_expression", "assignment_operator", "assignment_or_expression", 
		"assignment_xor_expression", "assignment_and_expression", "assignment_shift_expression", 
		"assignment_additive_expression", "assignment_multiplicative_expression", 
		"constant_expression", "logical_or_expression", "logical_and_expression", 
		"inclusive_or_expression", "exclusive_or_expression", "and_expression", 
		"equality_expression", "relational_expression", "shift_expression", "additive_expression", 
		"multiplicative_expression", "cast_expression", "type_name", "unary_expression", 
		"arguments", "primary_expression"
	};

	@Override
	public String getGrammarFileName() { return "CMinus.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public CMinusParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public Env env;
		public Program_expressionContext program_expression() {
			return getRuleContext(Program_expressionContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);

		    ((ProgramContext)_localctx).env =  Env.base;

		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92); program_expression(_localctx.env);
			}

			    ObjectSymbol entry = null;
			    try
			    {
			        if (_localctx.env.containObj("main"))
			        {
			            entry = _localctx.env.getObj("main");
			            IRGenerator.setEntry(((FuncSymbol)(entry.type)).label.id);
			        }
			    }
			    catch (CompileException e)
			    {
			        notifyErrorListeners("cannot find program entry 'main'");
			    }

		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Program_expressionContext extends ParserRuleContext {
		public Env env;
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public Function_definitionContext function_definition(int i) {
			return getRuleContext(Function_definitionContext.class,i);
		}
		public List<Function_definitionContext> function_definition() {
			return getRuleContexts(Function_definitionContext.class);
		}
		public Program_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Program_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_program_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterProgram_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitProgram_expression(this);
		}
	}

	public final Program_expressionContext program_expression(Env env) throws RecognitionException {
		Program_expressionContext _localctx = new Program_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 2, RULE_program_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(96);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(94); declaration(env);
					}
					break;

				case 2:
					{
					setState(95); function_definition(env);
					}
					break;
				}
				}
				setState(98); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 15) | (1L << 31) | (1L << 33) | (1L << 41) | (1L << 52))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public Env env;
		public Type_specifierContext type_specifier;
		public Init_declaratorsContext init_declarators() {
			return getRuleContext(Init_declaratorsContext.class,0);
		}
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public DeclarationContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitDeclaration(this);
		}
	}

	public final DeclarationContext declaration(Env env) throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState(), env);
		enterRule(_localctx, 4, RULE_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100); ((DeclarationContext)_localctx).type_specifier = type_specifier(env);
			setState(102);
			_la = _input.LA(1);
			if (_la==3 || _la==Identifier) {
				{
				setState(101); init_declarators(env, ((DeclarationContext)_localctx).type_specifier.type);
				}
			}

			setState(104); match(50);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_definitionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Type_specifierContext type_specifier;
		public Plain_declaratorContext plain_declarator;
		public Plain_declaratorContext plain_declarator() {
			return getRuleContext(Plain_declaratorContext.class,0);
		}
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public Compound_statementContext compound_statement() {
			return getRuleContext(Compound_statementContext.class,0);
		}
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public Function_definitionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Function_definitionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_function_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterFunction_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitFunction_definition(this);
		}
	}

	public final Function_definitionContext function_definition(Env env) throws RecognitionException {
		Function_definitionContext _localctx = new Function_definitionContext(_ctx, getState(), env);
		enterRule(_localctx, 6, RULE_function_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106); ((Function_definitionContext)_localctx).type_specifier = type_specifier(env);
			setState(107); ((Function_definitionContext)_localctx).plain_declarator = plain_declarator(env, ((Function_definitionContext)_localctx).type_specifier.type);

			        try
			        {
			            if (env.containObj(((Function_definitionContext)_localctx).plain_declarator.name))
			            {
			                if (! (env.getObj(((Function_definitionContext)_localctx).plain_declarator.name).type instanceof FuncSymbol))
			                    throw new CompileException("'"+((Function_definitionContext)_localctx).plain_declarator.name+"' redeclared as different kind of symbol");
			                if (! ((FuncSymbol)(env.getObj(((Function_definitionContext)_localctx).plain_declarator.name)).type).type.equals(((Function_definitionContext)_localctx).plain_declarator.type))
			                    throw new CompileException("conflicting types for '"+((Function_definitionContext)_localctx).plain_declarator.name+"'");
			                if (((FuncSymbol)(env.getObj(((Function_definitionContext)_localctx).plain_declarator.name)).type).env != null)
			                    throw new CompileException("redefinition of '"+((Function_definitionContext)_localctx).plain_declarator.name+"'");
			                ((Function_definitionContext)_localctx).symbol =  new ObjectSymbol(true, new FuncSymbol(((Function_definitionContext)_localctx).plain_declarator.type, ((FuncSymbol)(env.getObj(((Function_definitionContext)_localctx).plain_declarator.name).type)).label));
			            }
			            else
			            {
			                ((Function_definitionContext)_localctx).symbol =  new ObjectSymbol(true, new FuncSymbol(((Function_definitionContext)_localctx).plain_declarator.type, env.getCallJump()));
			                env.setObj(((Function_definitionContext)_localctx).plain_declarator.name, _localctx.symbol);
			            }
			            if ((((Function_definitionContext)_localctx).plain_declarator.type instanceof Aggregate) && ! ((Aggregate)((Function_definitionContext)_localctx).plain_declarator.type).defined())
			                throw new CompileException("return type is an incomplete type");
			            ((FuncSymbol)(_localctx.symbol.type)).frame = ObjectSymbol.pushFrame((FuncSymbol)_localctx.symbol.type);
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			        }
			    
			setState(109); match(28);
			setState(111);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 15) | (1L << 31) | (1L << 33) | (1L << 41) | (1L << 52))) != 0)) {
				{
				setState(110); parameters(env, (FuncSymbol)(_localctx.symbol.type));
				}
			}

			setState(113); match(16);

			        try
			        {
			            ((FuncSymbol)(_localctx.symbol.type)).setEnv(env,
			                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			        }
			    
			setState(115); compound_statement(((FuncSymbol)(_localctx.symbol.type)).env);
			}

			    try
			    {
			        ((FuncSymbol)(_localctx.symbol.type)).complete(
			            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			    }
			    catch (CompileException e)
			    {
			        notifyErrorListeners(e.msg);
			    }

		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersContext extends ParserRuleContext {
		public Env env;
		public FuncSymbol func;
		public Plain_declarationContext plain_declaration(int i) {
			return getRuleContext(Plain_declarationContext.class,i);
		}
		public List<Plain_declarationContext> plain_declaration() {
			return getRuleContexts(Plain_declarationContext.class);
		}
		public ParametersContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ParametersContext(ParserRuleContext parent, int invokingState, Env env, FuncSymbol func) {
			super(parent, invokingState);
			this.env = env;
			this.func = func;
		}
		@Override public int getRuleIndex() { return RULE_parameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitParameters(this);
		}
	}

	public final ParametersContext parameters(Env env,FuncSymbol func) throws RecognitionException {
		ParametersContext _localctx = new ParametersContext(_ctx, getState(), env, func);
		enterRule(_localctx, 8, RULE_parameters);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(117); plain_declaration(env, func);
			setState(122);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(118); match(25);
					setState(119); plain_declaration(env, func);
					}
					} 
				}
				setState(124);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			setState(128);
			_la = _input.LA(1);
			if (_la==25) {
				{
				setState(125); match(25);
				setState(126); match(36);

				            func.setVA();
				        
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaratorsContext extends ParserRuleContext {
		public Env env;
		public Symbol bind;
		public Symbol type;
		public DeclaratorContext declarator(int i) {
			return getRuleContext(DeclaratorContext.class,i);
		}
		public List<DeclaratorContext> declarator() {
			return getRuleContexts(DeclaratorContext.class);
		}
		public DeclaratorsContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public DeclaratorsContext(ParserRuleContext parent, int invokingState, Env env, Symbol bind, Symbol type) {
			super(parent, invokingState);
			this.env = env;
			this.bind = bind;
			this.type = type;
		}
		@Override public int getRuleIndex() { return RULE_declarators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterDeclarators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitDeclarators(this);
		}
	}

	public final DeclaratorsContext declarators(Env env,Symbol bind,Symbol type) throws RecognitionException {
		DeclaratorsContext _localctx = new DeclaratorsContext(_ctx, getState(), env, bind, type);
		enterRule(_localctx, 10, RULE_declarators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(130); declarator(env, bind, type);
			setState(135);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==25) {
				{
				{
				setState(131); match(25);
				setState(132); declarator(env, bind, type);
				}
				}
				setState(137);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_declaratorsContext extends ParserRuleContext {
		public Env env;
		public Symbol type;
		public Init_declaratorContext init_declarator(int i) {
			return getRuleContext(Init_declaratorContext.class,i);
		}
		public List<Init_declaratorContext> init_declarator() {
			return getRuleContexts(Init_declaratorContext.class);
		}
		public Init_declaratorsContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Init_declaratorsContext(ParserRuleContext parent, int invokingState, Env env, Symbol type) {
			super(parent, invokingState);
			this.env = env;
			this.type = type;
		}
		@Override public int getRuleIndex() { return RULE_init_declarators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterInit_declarators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitInit_declarators(this);
		}
	}

	public final Init_declaratorsContext init_declarators(Env env,Symbol type) throws RecognitionException {
		Init_declaratorsContext _localctx = new Init_declaratorsContext(_ctx, getState(), env, type);
		enterRule(_localctx, 12, RULE_init_declarators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(138); init_declarator(env, type);
			setState(143);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==25) {
				{
				{
				setState(139); match(25);
				setState(140); init_declarator(env, type);
				}
				}
				setState(145);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_declaratorContext extends ParserRuleContext {
		public Env env;
		public Symbol type;
		public ObjectSymbol symbol;
		public DeclaratorContext declarator;
		public InitializerContext initializer;
		public InitializerContext initializer() {
			return getRuleContext(InitializerContext.class,0);
		}
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public Init_declaratorContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Init_declaratorContext(ParserRuleContext parent, int invokingState, Env env, Symbol type) {
			super(parent, invokingState);
			this.env = env;
			this.type = type;
		}
		@Override public int getRuleIndex() { return RULE_init_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterInit_declarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitInit_declarator(this);
		}
	}

	public final Init_declaratorContext init_declarator(Env env,Symbol type) throws RecognitionException {
		Init_declaratorContext _localctx = new Init_declaratorContext(_ctx, getState(), env, type);
		enterRule(_localctx, 14, RULE_init_declarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(146); ((Init_declaratorContext)_localctx).declarator = declarator(env, null, type);

			        ((Init_declaratorContext)_localctx).symbol =  (ObjectSymbol)((Init_declaratorContext)_localctx).declarator.symbol;
			    
			setState(152);
			_la = _input.LA(1);
			if (_la==17) {
				{
				setState(148); match(17);
				setState(149); ((Init_declaratorContext)_localctx).initializer = initializer(env);

				            try
				            {
				                _localctx.symbol.init(((Init_declaratorContext)_localctx).initializer.list, ((ConstIntSymbol)_localctx.symbol.addr).val,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitializerContext extends ParserRuleContext {
		public Env env;
		public Object list;
		public Assignment_expressionContext assignment_expression;
		public InitializerContext exp;
		public List<InitializerContext> initializer() {
			return getRuleContexts(InitializerContext.class);
		}
		public Assignment_expressionContext assignment_expression() {
			return getRuleContext(Assignment_expressionContext.class,0);
		}
		public InitializerContext initializer(int i) {
			return getRuleContext(InitializerContext.class,i);
		}
		public InitializerContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public InitializerContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_initializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitInitializer(this);
		}
	}

	public final InitializerContext initializer(Env env) throws RecognitionException {
		InitializerContext _localctx = new InitializerContext(_ctx, getState(), env);
		enterRule(_localctx, 16, RULE_initializer);

		    ((InitializerContext)_localctx).list =  new ArrayList<Object>();

		int _la;
		try {
			setState(171);
			switch (_input.LA(1)) {
			case 1:
			case 3:
			case 5:
			case 20:
			case 21:
			case 26:
			case 28:
			case 32:
			case 42:
			case 47:
			case 59:
			case Identifier:
			case HexIntegerConstant:
			case IntegerConstant:
			case OctIntegerConstant:
			case String:
			case CharacterConstant:
				enterOuterAlt(_localctx, 1);
				{
				setState(154); ((InitializerContext)_localctx).assignment_expression = assignment_expression(env);

				        ((InitializerContext)_localctx).list =  ((InitializerContext)_localctx).assignment_expression.symbol;
				    
				}
				break;
			case 35:
				enterOuterAlt(_localctx, 2);
				{
				setState(157); match(35);
				setState(158); ((InitializerContext)_localctx).exp = initializer(env);

				        ((List<Object>)_localctx.list).add(((InitializerContext)_localctx).exp.list);
				    
				setState(166);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==25) {
					{
					{
					setState(160); match(25);
					setState(161); ((InitializerContext)_localctx).exp = initializer(env);

					            ((List<Object>)_localctx.list).add(((InitializerContext)_localctx).exp.list);
					        
					}
					}
					setState(168);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(169); match(10);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_specifierContext extends ParserRuleContext {
		public Env env;
		public Symbol type;
		public Token Identifier;
		public Type_specifierContext curType;
		public List<Type_specifierContext> type_specifier() {
			return getRuleContexts(Type_specifierContext.class);
		}
		public Type_specifierContext type_specifier(int i) {
			return getRuleContext(Type_specifierContext.class,i);
		}
		public List<DeclaratorsContext> declarators() {
			return getRuleContexts(DeclaratorsContext.class);
		}
		public DeclaratorsContext declarators(int i) {
			return getRuleContext(DeclaratorsContext.class,i);
		}
		public TerminalNode Identifier() { return getToken(CMinusParser.Identifier, 0); }
		public Type_specifierContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Type_specifierContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_type_specifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterType_specifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitType_specifier(this);
		}
	}

	public final Type_specifierContext type_specifier(Env env) throws RecognitionException {
		Type_specifierContext _localctx = new Type_specifierContext(_ctx, getState(), env);
		enterRule(_localctx, 18, RULE_type_specifier);

		    Env declEnv = null;

		int _la;
		try {
			setState(229);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(173); match(33);

				        try
				        {
				            ((Type_specifierContext)_localctx).type =  env.getType("void");
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(175); match(11);

				        try
				        {
				            ((Type_specifierContext)_localctx).type =  env.getType("char");
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(177); match(31);

				        try
				        {
				            ((Type_specifierContext)_localctx).type =  env.getType("int");
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(179); match(52);

				        ((Type_specifierContext)_localctx).type =  new PointerSymbol(VoidSymbol.instance);
				    
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(181); match(41);

				        ((Type_specifierContext)_localctx).type =  new StructSymbol();
				        ((Aggregate)_localctx.type).define();
				    
				setState(185);
				_la = _input.LA(1);
				if (_la==Identifier) {
					{
					setState(183); ((Type_specifierContext)_localctx).Identifier = match(Identifier);

					            try
					            {
					                if (env.getType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null)) != null)
					                {
					                    ((Type_specifierContext)_localctx).type =  env.getType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null));
					                    if (! (_localctx.type instanceof StructSymbol))
					                        throw new CompileException("'"+(((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null)+"' defined as wrong kind of tag");
					                    else if (((Aggregate)_localctx.type).defined())
					                        throw new CompileException("redefinition of 'struct "+(((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null)+"'");
					                }
					                env.setType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null), _localctx.type);
					            }
					            catch (CompileException e)
					            {
					                notifyErrorListeners(e.msg);
					            }
					        
					}
				}


				        declEnv = new Env(env);
				    
				setState(188); match(35);
				setState(195); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(189); ((Type_specifierContext)_localctx).curType = type_specifier(env);
					setState(191);
					_la = _input.LA(1);
					if (_la==3 || _la==Identifier) {
						{
						setState(190); declarators(declEnv, _localctx.type, ((Type_specifierContext)_localctx).curType.type);
						}
					}

					setState(193); match(50);
					}
					}
					setState(197); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 15) | (1L << 31) | (1L << 33) | (1L << 41) | (1L << 52))) != 0) );
				setState(199); match(10);

				        declEnv.clear();
				    
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(202); match(15);

				        ((Type_specifierContext)_localctx).type =  new UnionSymbol();
				        ((Aggregate)_localctx.type).define();
				    
				setState(206);
				_la = _input.LA(1);
				if (_la==Identifier) {
					{
					setState(204); ((Type_specifierContext)_localctx).Identifier = match(Identifier);

					            try
					            {
					                if (env.getType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null)) != null)
					                {
					                    ((Type_specifierContext)_localctx).type =  env.getType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null));
					                    if (! (_localctx.type instanceof UnionSymbol))
					                        throw new CompileException("'"+(((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null)+"' defined as wrong kind of tag");
					                    else if (((Aggregate)_localctx.type) != null)
					                        throw new CompileException("redefinition of 'struct "+(((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null)+"'");
					                }
					                env.setType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null), _localctx.type);
					            }
					            catch (CompileException e)
					            {
					                notifyErrorListeners(e.msg);
					            }
					        
					}
				}


				        declEnv = new Env(env);
				    
				setState(209); match(35);
				setState(216); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(210); ((Type_specifierContext)_localctx).curType = type_specifier(env);
					setState(212);
					_la = _input.LA(1);
					if (_la==3 || _la==Identifier) {
						{
						setState(211); declarators(declEnv, _localctx.type, ((Type_specifierContext)_localctx).curType.type);
						}
					}

					setState(214); match(50);
					}
					}
					setState(218); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 15) | (1L << 31) | (1L << 33) | (1L << 41) | (1L << 52))) != 0) );
				setState(220); match(10);

				        declEnv.clear();
				    
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(223); match(41);
				setState(224); ((Type_specifierContext)_localctx).Identifier = match(Identifier);

				        try
				        {
				            if (env.getType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null)) == null)
				                env.setType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null), new StructSymbol());
				            ((Type_specifierContext)_localctx).type =  env.getType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(226); match(15);
				setState(227); ((Type_specifierContext)_localctx).Identifier = match(Identifier);

				        try
				        {
				            if (env.getType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null)) == null)
				                env.setType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null), new UnionSymbol());
				            ((Type_specifierContext)_localctx).type =  env.getType((((Type_specifierContext)_localctx).Identifier!=null?((Type_specifierContext)_localctx).Identifier.getText():null));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Plain_declarationContext extends ParserRuleContext {
		public Env env;
		public Symbol bind;
		public Symbol symbol;
		public Type_specifierContext type_specifier;
		public DeclaratorContext declarator;
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public Plain_declarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Plain_declarationContext(ParserRuleContext parent, int invokingState, Env env, Symbol bind) {
			super(parent, invokingState);
			this.env = env;
			this.bind = bind;
		}
		@Override public int getRuleIndex() { return RULE_plain_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterPlain_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitPlain_declaration(this);
		}
	}

	public final Plain_declarationContext plain_declaration(Env env,Symbol bind) throws RecognitionException {
		Plain_declarationContext _localctx = new Plain_declarationContext(_ctx, getState(), env, bind);
		enterRule(_localctx, 20, RULE_plain_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(231); ((Plain_declarationContext)_localctx).type_specifier = type_specifier(env);
			setState(232); ((Plain_declarationContext)_localctx).declarator = declarator(env, bind, ((Plain_declarationContext)_localctx).type_specifier.type);

			        ((Plain_declarationContext)_localctx).symbol =  ((Plain_declarationContext)_localctx).declarator.symbol;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaratorContext extends ParserRuleContext {
		public Env env;
		public Symbol bind;
		public Symbol type;
		public Symbol symbol;
		public Plain_declaratorContext plain_declarator;
		public Constant_expressionContext dim;
		public Constant_expressionContext constant_expression(int i) {
			return getRuleContext(Constant_expressionContext.class,i);
		}
		public Plain_declaratorContext plain_declarator() {
			return getRuleContext(Plain_declaratorContext.class,0);
		}
		public List<Constant_expressionContext> constant_expression() {
			return getRuleContexts(Constant_expressionContext.class);
		}
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public DeclaratorContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public DeclaratorContext(ParserRuleContext parent, int invokingState, Env env, Symbol bind, Symbol type) {
			super(parent, invokingState);
			this.env = env;
			this.bind = bind;
			this.type = type;
		}
		@Override public int getRuleIndex() { return RULE_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterDeclarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitDeclarator(this);
		}
	}

	public final DeclaratorContext declarator(Env env,Symbol bind,Symbol type) throws RecognitionException {
		DeclaratorContext _localctx = new DeclaratorContext(_ctx, getState(), env, bind, type);
		enterRule(_localctx, 22, RULE_declarator);
		int _la;
		try {
			setState(270);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(235); ((DeclaratorContext)_localctx).plain_declarator = plain_declarator(env, type);
				setState(244);
				switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
				case 1:
					{
					setState(236); match(2);
					setState(237); ((DeclaratorContext)_localctx).dim = constant_expression(env);
					setState(238); match(23);

					            ((DeclaratorContext)_localctx).symbol =  new ArraySymbol(((DeclaratorContext)_localctx).plain_declarator.type, ((ConstIntSymbol)(((DeclaratorContext)_localctx).dim.symbol)).val);
					        
					}
					break;

				case 2:
					{
					setState(241); match(2);
					setState(242); match(23);

					            ((DeclaratorContext)_localctx).symbol =  new ArraySymbol(((DeclaratorContext)_localctx).plain_declarator.type);
					        
					}
					break;
				}
				setState(253);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==2) {
					{
					{
					setState(246); match(2);
					setState(247); ((DeclaratorContext)_localctx).dim = constant_expression(env);
					setState(248); match(23);

					            ((ArraySymbol)_localctx.symbol).add(((ConstIntSymbol)(((DeclaratorContext)_localctx).dim.symbol)).val);
					        
					}
					}
					setState(255);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

				        try
				        {
				            if (((DeclaratorContext)_localctx).plain_declarator.type instanceof VoidSymbol)
				                throw new CompileException("declaration of '"+((DeclaratorContext)_localctx).plain_declarator.name+"' as array of voids");
				            if (bind == null)
				                env.setObj(((DeclaratorContext)_localctx).plain_declarator.name, (ObjectSymbol)(((DeclaratorContext)_localctx).symbol =  new ObjectSymbol(false, _localctx.symbol)));
				            else if (bind instanceof Aggregate)
				                ((Aggregate)bind).set(((DeclaratorContext)_localctx).plain_declarator.name, _localctx.symbol);
				            else if (bind instanceof FuncSymbol)
				                ((FuncSymbol)bind).set(((DeclaratorContext)_localctx).plain_declarator.name, _localctx.symbol);
				            else
				                throw new CompileException("cannot bind to '"+bind.toString()+"'");
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(258); ((DeclaratorContext)_localctx).plain_declarator = plain_declarator(env, type);

				        ((DeclaratorContext)_localctx).symbol =  new FuncSymbol(((DeclaratorContext)_localctx).plain_declarator.type, env.getCallJump());
				    
				setState(260); match(28);
				setState(262);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 15) | (1L << 31) | (1L << 33) | (1L << 41) | (1L << 52))) != 0)) {
					{
					setState(261); parameters(env, (FuncSymbol)_localctx.symbol);
					}
				}

				setState(264); match(16);

				        try
				        {
				            if (bind == null)
				                env.setObj(((DeclaratorContext)_localctx).plain_declarator.name, (ObjectSymbol)(((DeclaratorContext)_localctx).symbol =  new ObjectSymbol(true, _localctx.symbol)));
				            else if (bind instanceof Aggregate)
				                ((Aggregate)bind).set(((DeclaratorContext)_localctx).plain_declarator.name, _localctx.symbol);
				            else if (bind instanceof FuncSymbol)
				                ((FuncSymbol)bind).set(((DeclaratorContext)_localctx).plain_declarator.name, _localctx.symbol);
				            else
				                throw new CompileException("cannot bind to '"+bind.toString()+"'");
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(267); ((DeclaratorContext)_localctx).plain_declarator = plain_declarator(env, type);

				        try
				        {
				            ((DeclaratorContext)_localctx).symbol =  ((DeclaratorContext)_localctx).plain_declarator.type;
				            if (((DeclaratorContext)_localctx).plain_declarator.type instanceof VoidSymbol)
				                throw new CompileException("variable or field '"+((DeclaratorContext)_localctx).plain_declarator.name+"' declared void");
				            if (bind == null)
				                env.setObj(((DeclaratorContext)_localctx).plain_declarator.name, (ObjectSymbol)(((DeclaratorContext)_localctx).symbol =  new ObjectSymbol(! (_localctx.symbol instanceof Aggregate), _localctx.symbol, true)));
				            else if (bind instanceof Aggregate)
				                ((Aggregate)bind).set(((DeclaratorContext)_localctx).plain_declarator.name, _localctx.symbol);
				            else if (bind instanceof FuncSymbol)
				                ((FuncSymbol)bind).set(((DeclaratorContext)_localctx).plain_declarator.name, _localctx.symbol);
				            else
				                throw new CompileException("cannot bind to '"+bind.toString()+"'");
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Plain_declaratorContext extends ParserRuleContext {
		public Env env;
		public Symbol baseType;
		public String name;
		public Symbol type;
		public Token Identifier;
		public Plain_declaratorContext lowerType;
		public Plain_declaratorContext plain_declarator() {
			return getRuleContext(Plain_declaratorContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(CMinusParser.Identifier, 0); }
		public Plain_declaratorContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Plain_declaratorContext(ParserRuleContext parent, int invokingState, Env env, Symbol baseType) {
			super(parent, invokingState);
			this.env = env;
			this.baseType = baseType;
		}
		@Override public int getRuleIndex() { return RULE_plain_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterPlain_declarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitPlain_declarator(this);
		}
	}

	public final Plain_declaratorContext plain_declarator(Env env,Symbol baseType) throws RecognitionException {
		Plain_declaratorContext _localctx = new Plain_declaratorContext(_ctx, getState(), env, baseType);
		enterRule(_localctx, 24, RULE_plain_declarator);
		try {
			setState(278);
			switch (_input.LA(1)) {
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(272); ((Plain_declaratorContext)_localctx).Identifier = match(Identifier);

				        ((Plain_declaratorContext)_localctx).name =  (((Plain_declaratorContext)_localctx).Identifier!=null?((Plain_declaratorContext)_localctx).Identifier.getText():null);
				        ((Plain_declaratorContext)_localctx).type =  baseType;
				    
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 2);
				{
				setState(274); match(3);
				setState(275); ((Plain_declaratorContext)_localctx).lowerType = plain_declarator(env, baseType);

				        ((Plain_declaratorContext)_localctx).name =  ((Plain_declaratorContext)_localctx).lowerType.name;
				        ((Plain_declaratorContext)_localctx).type =  new PointerSymbol(((Plain_declaratorContext)_localctx).lowerType.type);
				    
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Env env;
		public Va_start_statementContext va_start_statement() {
			return getRuleContext(Va_start_statementContext.class,0);
		}
		public Iteration_statementContext iteration_statement() {
			return getRuleContext(Iteration_statementContext.class,0);
		}
		public Selection_statementContext selection_statement() {
			return getRuleContext(Selection_statementContext.class,0);
		}
		public Compound_statementContext compound_statement() {
			return getRuleContext(Compound_statementContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Va_end_statementContext va_end_statement() {
			return getRuleContext(Va_end_statementContext.class,0);
		}
		public Jump_statementContext jump_statement() {
			return getRuleContext(Jump_statementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public StatementContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement(Env env) throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState(), env);
		enterRule(_localctx, 26, RULE_statement);

		    Env compoundEnv = null;

		int _la;
		try {
			setState(297);
			switch (_input.LA(1)) {
			case 61:
				enterOuterAlt(_localctx, 1);
				{
				setState(280); va_start_statement(env);
				setState(281); match(50);
				}
				break;
			case 44:
				enterOuterAlt(_localctx, 2);
				{
				setState(283); va_end_statement(env);
				setState(284); match(50);
				}
				break;
			case 1:
			case 3:
			case 5:
			case 20:
			case 21:
			case 26:
			case 28:
			case 32:
			case 42:
			case 47:
			case 50:
			case 59:
			case Identifier:
			case HexIntegerConstant:
			case IntegerConstant:
			case OctIntegerConstant:
			case String:
			case CharacterConstant:
				enterOuterAlt(_localctx, 3);
				{
				setState(287);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 32) | (1L << 42) | (1L << 47) | (1L << 59) | (1L << Identifier) | (1L << HexIntegerConstant))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (IntegerConstant - 64)) | (1L << (OctIntegerConstant - 64)) | (1L << (String - 64)) | (1L << (CharacterConstant - 64)))) != 0)) {
					{
					setState(286); expression(env);
					}
				}

				setState(289); match(50);
				}
				break;
			case 35:
				enterOuterAlt(_localctx, 4);
				{

				        compoundEnv = new Env(env);
				    
				setState(291); compound_statement(compoundEnv);

				        compoundEnv.clear();
				    
				}
				break;
			case 30:
				enterOuterAlt(_localctx, 5);
				{
				setState(294); selection_statement(env);
				}
				break;
			case 27:
			case 48:
				enterOuterAlt(_localctx, 6);
				{
				setState(295); iteration_statement(env);
				}
				break;
			case 6:
			case 37:
			case 49:
				enterOuterAlt(_localctx, 7);
				{
				setState(296); jump_statement(env);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Va_start_statementContext extends ParserRuleContext {
		public Env env;
		public ExpressionContext exp0;
		public ExpressionContext exp1;
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public Va_start_statementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Va_start_statementContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_va_start_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterVa_start_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitVa_start_statement(this);
		}
	}

	public final Va_start_statementContext va_start_statement(Env env) throws RecognitionException {
		Va_start_statementContext _localctx = new Va_start_statementContext(_ctx, getState(), env);
		enterRule(_localctx, 28, RULE_va_start_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(299); match(61);
			setState(300); match(28);
			setState(301); ((Va_start_statementContext)_localctx).exp0 = expression(env);
			setState(302); match(25);
			setState(303); ((Va_start_statementContext)_localctx).exp1 = expression(env);
			setState(304); match(16);

			        try
			        {
			            if (! ((Va_start_statementContext)_localctx).exp0.symbol.type.equals(new PointerSymbol(new VoidSymbol())))
			                throw new CompileException("cannot convert '"+((Va_start_statementContext)_localctx).exp0.symbol.typeInfo()+"' to 'va_list' for argument 1 to 'va_start'");
			            ObjectSymbol.assign(((Va_start_statementContext)_localctx).exp0.symbol, env.getObj("..."),
			                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Va_arg_statementContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public ExpressionContext expression;
		public Type_nameContext type_name;
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Va_arg_statementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Va_arg_statementContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_va_arg_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterVa_arg_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitVa_arg_statement(this);
		}
	}

	public final Va_arg_statementContext va_arg_statement(Env env) throws RecognitionException {
		Va_arg_statementContext _localctx = new Va_arg_statementContext(_ctx, getState(), env);
		enterRule(_localctx, 30, RULE_va_arg_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(307); match(32);
			setState(308); match(28);
			setState(309); ((Va_arg_statementContext)_localctx).expression = expression(env);
			setState(310); match(25);
			setState(311); ((Va_arg_statementContext)_localctx).type_name = type_name(env);
			setState(312); match(16);

			        try
			        {
			            if (! ((Va_arg_statementContext)_localctx).expression.symbol.type.equals(new PointerSymbol(new VoidSymbol())))
			                throw new CompileException("cannot convert '"+((Va_arg_statementContext)_localctx).expression.symbol.typeInfo()+"' to 'va_list' for argument 1 to 'va_arg'");
			            ObjectSymbol addr = ObjectSymbol.assignRvalue(new ObjectSymbol(true, IntSymbol.instance, false), ((Va_arg_statementContext)_localctx).expression.symbol,
			                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			            ((Va_arg_statementContext)_localctx).symbol =  new ObjectSymbol(true, ((Va_arg_statementContext)_localctx).type_name.type, true, 0, addr);
			            ObjectSymbol.plus(((Va_arg_statementContext)_localctx).expression.symbol, ((Va_arg_statementContext)_localctx).expression.symbol, new ConstIntSymbol(((Va_arg_statementContext)_localctx).type_name.type.sizeof+(4-((Va_arg_statementContext)_localctx).type_name.type.sizeof % 4) % 4), true,
			                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Va_end_statementContext extends ParserRuleContext {
		public Env env;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Va_end_statementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Va_end_statementContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_va_end_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterVa_end_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitVa_end_statement(this);
		}
	}

	public final Va_end_statementContext va_end_statement(Env env) throws RecognitionException {
		Va_end_statementContext _localctx = new Va_end_statementContext(_ctx, getState(), env);
		enterRule(_localctx, 32, RULE_va_end_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(315); match(44);
			setState(316); match(28);
			setState(317); ((Va_end_statementContext)_localctx).expression = expression(env);
			setState(318); match(16);

			        try
			        {
			            if (! ((Va_end_statementContext)_localctx).expression.symbol.type.equals(new PointerSymbol(new VoidSymbol())))
			                throw new CompileException("cannot convert '"+((Va_end_statementContext)_localctx).expression.symbol.typeInfo()+"' to 'va_list' for argument 1 to 'va_end'");
			            ObjectSymbol.assign(((Va_end_statementContext)_localctx).expression.symbol, new ConstIntSymbol(0),
			                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compound_statementContext extends ParserRuleContext {
		public Env env;
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Compound_statementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Compound_statementContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_compound_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterCompound_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitCompound_statement(this);
		}
	}

	public final Compound_statementContext compound_statement(Env env) throws RecognitionException {
		Compound_statementContext _localctx = new Compound_statementContext(_ctx, getState(), env);
		enterRule(_localctx, 34, RULE_compound_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(321); match(35);
			setState(325);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 15) | (1L << 31) | (1L << 33) | (1L << 41) | (1L << 52))) != 0)) {
				{
				{
				setState(322); declaration(env);
				}
				}
				setState(327);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(331);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 6) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 27) | (1L << 28) | (1L << 30) | (1L << 32) | (1L << 35) | (1L << 37) | (1L << 42) | (1L << 44) | (1L << 47) | (1L << 48) | (1L << 49) | (1L << 50) | (1L << 59) | (1L << 61) | (1L << Identifier) | (1L << HexIntegerConstant))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (IntegerConstant - 64)) | (1L << (OctIntegerConstant - 64)) | (1L << (String - 64)) | (1L << (CharacterConstant - 64)))) != 0)) {
				{
				{
				setState(328); statement(env);
				}
				}
				setState(333);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(334); match(10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Selection_statementContext extends ParserRuleContext {
		public Env env;
		public ExpressionContext expression;
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Selection_statementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Selection_statementContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_selection_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterSelection_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitSelection_statement(this);
		}
	}

	public final Selection_statementContext selection_statement(Env env) throws RecognitionException {
		Selection_statementContext _localctx = new Selection_statementContext(_ctx, getState(), env);
		enterRule(_localctx, 36, RULE_selection_statement);

		    JumpSymbol branchLabel = env.getBranchJump();
		    JumpSymbol endLabel = env.getGotoJump();

		try {
			setState(355);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(336); match(30);
				setState(337); match(28);
				setState(338); ((Selection_statementContext)_localctx).expression = expression(env);
				setState(339); match(16);

				        try
				        {
				            IRGenerator.add("BEQZ", ((Selection_statementContext)_localctx).expression.symbol, branchLabel.id,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            if (! (((Selection_statementContext)_localctx).expression.symbol.type instanceof Scalar))
				                throw new CompileException("used "+((Selection_statementContext)_localctx).expression.symbol.typeInfo()+" type value where scalar is required");
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				setState(341); statement(env);

				        IRGenerator.add(endLabel.id,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        IRGenerator.add(branchLabel,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				    
				setState(343); match(40);
				setState(344); statement(env);

				        IRGenerator.add(endLabel,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(347); match(30);
				setState(348); match(28);
				setState(349); ((Selection_statementContext)_localctx).expression = expression(env);
				setState(350); match(16);

				        try
				        {
				            IRGenerator.add("BEQZ", ((Selection_statementContext)_localctx).expression.symbol, branchLabel.id,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            if (! (((Selection_statementContext)_localctx).expression.symbol.type instanceof Scalar))
				                throw new CompileException("used "+((Selection_statementContext)_localctx).expression.symbol.typeInfo()+" type value where scalar is required");
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				setState(352); statement(env);

				        IRGenerator.add(branchLabel,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Iteration_statementContext extends ParserRuleContext {
		public Env env;
		public ExpressionContext expression;
		public ExpressionContext condExp;
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public Iteration_statementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Iteration_statementContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_iteration_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterIteration_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitIteration_statement(this);
		}
	}

	public final Iteration_statementContext iteration_statement(Env env) throws RecognitionException {
		Iteration_statementContext _localctx = new Iteration_statementContext(_ctx, getState(), env);
		enterRule(_localctx, 38, RULE_iteration_statement);

		    JumpSymbol breakLabel = env.getBreakJump();
		    JumpSymbol continueLabel = env.getContinueJump();
		    JumpSymbol bodyLabel = env.getGotoJump();
		    String mainFork = IRGenerator.branch();
		    String condFork;
		    String stepFork;
		    IRGenerator.getBind().inline = false;

		int _la;
		try {
			setState(389);
			switch (_input.LA(1)) {
			case 27:
				enterOuterAlt(_localctx, 1);
				{
				setState(357); match(27);
				setState(358); match(28);

				        IRGenerator.add(continueLabel,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        condFork = IRGenerator.checkout();
				    
				setState(360); ((Iteration_statementContext)_localctx).expression = expression(env);

				        try
				        {
				            if (! (((Iteration_statementContext)_localctx).expression.symbol.type instanceof Scalar))
				                throw new CompileException("used "+((Iteration_statementContext)_localctx).expression.symbol.typeInfo()+" type value where scalar is required");
				            IRGenerator.checkout(mainFork);
				            IRGenerator.copy(condFork);
				            IRGenerator.add("BEQZ", ((Iteration_statementContext)_localctx).expression.symbol, breakLabel.id,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            IRGenerator.checkout(condFork);
				            IRGenerator.add("BNEZ", ((Iteration_statementContext)_localctx).expression.symbol, bodyLabel.id,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            IRGenerator.checkout(mainFork);
				            IRGenerator.add(bodyLabel,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				setState(362); match(16);
				setState(363); statement(env);

				        IRGenerator.merge(condFork);
				        IRGenerator.add(breakLabel,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				    
				}
				break;
			case 48:
				enterOuterAlt(_localctx, 2);
				{
				setState(366); match(48);
				setState(367); match(28);
				setState(369);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 32) | (1L << 42) | (1L << 47) | (1L << 59) | (1L << Identifier) | (1L << HexIntegerConstant))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (IntegerConstant - 64)) | (1L << (OctIntegerConstant - 64)) | (1L << (String - 64)) | (1L << (CharacterConstant - 64)))) != 0)) {
					{
					setState(368); expression(env);
					}
				}

				setState(371); match(50);

				        condFork = IRGenerator.checkout();
				    
				setState(376);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 32) | (1L << 42) | (1L << 47) | (1L << 59) | (1L << Identifier) | (1L << HexIntegerConstant))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (IntegerConstant - 64)) | (1L << (OctIntegerConstant - 64)) | (1L << (String - 64)) | (1L << (CharacterConstant - 64)))) != 0)) {
					{
					setState(373); ((Iteration_statementContext)_localctx).condExp = expression(env);

					            try
					            {
					                IRGenerator.checkout(mainFork);
					                IRGenerator.copy(condFork);
					                if (! (((Iteration_statementContext)_localctx).condExp.symbol.type instanceof Scalar))
					                    throw new CompileException("used "+((Iteration_statementContext)_localctx).condExp.symbol.typeInfo()+" type value where scalar is required");
					                IRGenerator.add("BEQZ", ((Iteration_statementContext)_localctx).condExp.symbol, breakLabel.id,
					                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
					                IRGenerator.checkout(condFork);
					                IRGenerator.add("BNEZ", ((Iteration_statementContext)_localctx).condExp.symbol, bodyLabel.id,
					                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
					                IRGenerator.checkout(mainFork);
					            }
					            catch (CompileException e)
					            {
					                notifyErrorListeners(e.msg);
					            }
					        
					}
				}

				setState(378); match(50);

				        stepFork = IRGenerator.checkout();
				    
				setState(381);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 32) | (1L << 42) | (1L << 47) | (1L << 59) | (1L << Identifier) | (1L << HexIntegerConstant))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (IntegerConstant - 64)) | (1L << (OctIntegerConstant - 64)) | (1L << (String - 64)) | (1L << (CharacterConstant - 64)))) != 0)) {
					{
					setState(380); expression(env);
					}
				}


				        IRGenerator.checkout(mainFork);
				    
				setState(384); match(16);

				        IRGenerator.add(bodyLabel,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				    
				setState(386); statement(env);

				        IRGenerator.add(continueLabel,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        IRGenerator.merge(stepFork);
				        IRGenerator.merge(condFork);
				        IRGenerator.add(breakLabel,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				    
				}
				break;
			default:
				throw new NoViableAltException(this);
			}

			    try
			    {
			        env.removeContinueJump();
			        env.removeBreakJump();
			    }
			    catch (CompileException e)
			    {
			        notifyErrorListeners(e.msg);
			    }

		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Jump_statementContext extends ParserRuleContext {
		public Env env;
		public JumpSymbol jump;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Jump_statementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Jump_statementContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_jump_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterJump_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitJump_statement(this);
		}
	}

	public final Jump_statementContext jump_statement(Env env) throws RecognitionException {
		Jump_statementContext _localctx = new Jump_statementContext(_ctx, getState(), env);
		enterRule(_localctx, 40, RULE_jump_statement);
		try {
			setState(405);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(391); match(6);
				setState(392); match(50);

				        try
				        {
				            ((Jump_statementContext)_localctx).jump =  env.jumpToContinue(
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(394); match(37);
				setState(395); match(50);

				        try
				        {
				            ((Jump_statementContext)_localctx).jump =  env.jumpToBreak(
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(397); match(49);
				setState(398); ((Jump_statementContext)_localctx).expression = expression(env);
				setState(399); match(50);

				        try
				        {
				            ((Jump_statementContext)_localctx).jump =  env.jumpToReturn(((Jump_statementContext)_localctx).expression.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(402); match(49);
				setState(403); match(50);

				        try
				        {
				            ((Jump_statementContext)_localctx).jump =  env.jumpToReturn(
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Assignment_expressionContext exp0;
		public Assignment_expressionContext exp;
		public List<Assignment_expressionContext> assignment_expression() {
			return getRuleContexts(Assignment_expressionContext.class);
		}
		public Assignment_expressionContext assignment_expression(int i) {
			return getRuleContext(Assignment_expressionContext.class,i);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ExpressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression(Env env) throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState(), env);
		enterRule(_localctx, 42, RULE_expression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(407); ((ExpressionContext)_localctx).exp0 = assignment_expression(env);

			        ((ExpressionContext)_localctx).symbol =  ((ExpressionContext)_localctx).exp0.symbol;
			    
			setState(415);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(409); match(25);
					setState(410); ((ExpressionContext)_localctx).exp = assignment_expression(env);

					            ((ExpressionContext)_localctx).symbol =  ((ExpressionContext)_localctx).exp.symbol;
					        
					}
					} 
				}
				setState(417);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Assignment_or_expressionContext assignment_or_expression;
		public Assignment_xor_expressionContext assignment_xor_expression;
		public Assignment_and_expressionContext assignment_and_expression;
		public Assignment_shift_expressionContext assignment_shift_expression;
		public Assignment_additive_expressionContext assignment_additive_expression;
		public Assignment_multiplicative_expressionContext assignment_multiplicative_expression;
		public Unary_expressionContext unary_expression;
		public Assignment_operatorContext assignment_operator;
		public Assignment_expressionContext exp;
		public Logical_or_expressionContext logical_or_expression;
		public Assignment_additive_expressionContext assignment_additive_expression() {
			return getRuleContext(Assignment_additive_expressionContext.class,0);
		}
		public Assignment_expressionContext assignment_expression() {
			return getRuleContext(Assignment_expressionContext.class,0);
		}
		public Logical_or_expressionContext logical_or_expression() {
			return getRuleContext(Logical_or_expressionContext.class,0);
		}
		public Assignment_operatorContext assignment_operator() {
			return getRuleContext(Assignment_operatorContext.class,0);
		}
		public Assignment_and_expressionContext assignment_and_expression() {
			return getRuleContext(Assignment_and_expressionContext.class,0);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Assignment_shift_expressionContext assignment_shift_expression() {
			return getRuleContext(Assignment_shift_expressionContext.class,0);
		}
		public Assignment_or_expressionContext assignment_or_expression() {
			return getRuleContext(Assignment_or_expressionContext.class,0);
		}
		public Assignment_multiplicative_expressionContext assignment_multiplicative_expression() {
			return getRuleContext(Assignment_multiplicative_expressionContext.class,0);
		}
		public Assignment_xor_expressionContext assignment_xor_expression() {
			return getRuleContext(Assignment_xor_expressionContext.class,0);
		}
		public Assignment_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Assignment_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_assignment_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAssignment_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAssignment_expression(this);
		}
	}

	public final Assignment_expressionContext assignment_expression(Env env) throws RecognitionException {
		Assignment_expressionContext _localctx = new Assignment_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 44, RULE_assignment_expression);
		try {
			setState(444);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(418); ((Assignment_expressionContext)_localctx).assignment_or_expression = assignment_or_expression(env);

				        ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).assignment_or_expression.symbol;
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(421); ((Assignment_expressionContext)_localctx).assignment_xor_expression = assignment_xor_expression(env);

				        ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).assignment_xor_expression.symbol;
				    
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(424); ((Assignment_expressionContext)_localctx).assignment_and_expression = assignment_and_expression(env);

				        ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).assignment_and_expression.symbol;
				    
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(427); ((Assignment_expressionContext)_localctx).assignment_shift_expression = assignment_shift_expression(env);

				        ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).assignment_shift_expression.symbol;
				    
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(430); ((Assignment_expressionContext)_localctx).assignment_additive_expression = assignment_additive_expression(env);

				        ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).assignment_additive_expression.symbol;
				    
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(433); ((Assignment_expressionContext)_localctx).assignment_multiplicative_expression = assignment_multiplicative_expression(env);

				        ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).assignment_multiplicative_expression.symbol;
				    
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(436); ((Assignment_expressionContext)_localctx).unary_expression = unary_expression(env);
				setState(437); ((Assignment_expressionContext)_localctx).assignment_operator = assignment_operator();
				setState(438); ((Assignment_expressionContext)_localctx).exp = assignment_expression(env);

				        try
				        {
				            switch ((((Assignment_expressionContext)_localctx).assignment_operator!=null?_input.getText(((Assignment_expressionContext)_localctx).assignment_operator.start,((Assignment_expressionContext)_localctx).assignment_operator.stop):null))
				            {
				            case "=":
				                ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).exp.symbol;
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "*=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.mul(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "/=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.div(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "%=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.mod(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "+=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.plus(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "-=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.minus(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "<<=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.shiftLeft(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case ">>=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.shiftRight(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "&=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.and(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "^=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.xor(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            case "|=":
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.or(((Assignment_expressionContext)_localctx).unary_expression.symbol, ((Assignment_expressionContext)_localctx).exp.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                ((Assignment_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
				                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                break;
				            }
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				            ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).unary_expression.symbol;
				        }
				    
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(441); ((Assignment_expressionContext)_localctx).logical_or_expression = logical_or_expression(env);

				        ((Assignment_expressionContext)_localctx).symbol =  ((Assignment_expressionContext)_localctx).logical_or_expression.symbol;
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_operatorContext extends ParserRuleContext {
		public Assignment_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAssignment_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAssignment_operator(this);
		}
	}

	public final Assignment_operatorContext assignment_operator() throws RecognitionException {
		Assignment_operatorContext _localctx = new Assignment_operatorContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_assignment_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(446);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 14) | (1L << 17) | (1L << 18) | (1L << 22) | (1L << 24) | (1L << 29) | (1L << 34) | (1L << 38) | (1L << 39) | (1L << 55) | (1L << 56))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_or_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Unary_expressionContext unary_expression;
		public Exclusive_or_expressionContext exp0;
		public Exclusive_or_expressionContext exp1;
		public Exclusive_or_expressionContext exclusive_or_expression(int i) {
			return getRuleContext(Exclusive_or_expressionContext.class,i);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public List<Exclusive_or_expressionContext> exclusive_or_expression() {
			return getRuleContexts(Exclusive_or_expressionContext.class);
		}
		public Assignment_or_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Assignment_or_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_assignment_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAssignment_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAssignment_or_expression(this);
		}
	}

	public final Assignment_or_expressionContext assignment_or_expression(Env env) throws RecognitionException {
		Assignment_or_expressionContext _localctx = new Assignment_or_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 48, RULE_assignment_or_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(448); ((Assignment_or_expressionContext)_localctx).unary_expression = unary_expression(env);
			setState(449); match(17);
			setState(450); ((Assignment_or_expressionContext)_localctx).exp0 = exclusive_or_expression(env);
			setState(451); match(19);
			setState(452); ((Assignment_or_expressionContext)_localctx).exp1 = exclusive_or_expression(env);

			        try
			        {
			            ((Assignment_or_expressionContext)_localctx).symbol =  ObjectSymbol.or(((Assignment_or_expressionContext)_localctx).unary_expression.symbol, ((Assignment_or_expressionContext)_localctx).exp0.symbol, ((Assignment_or_expressionContext)_localctx).exp1.symbol, false,
			                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			            if (_localctx.symbol instanceof ConstSymbol)
			                ((Assignment_or_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_or_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			            ((Assignment_or_expressionContext)_localctx).symbol =  ((Assignment_or_expressionContext)_localctx).unary_expression.symbol;
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_xor_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Unary_expressionContext unary_expression;
		public And_expressionContext exp0;
		public And_expressionContext exp1;
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public And_expressionContext and_expression(int i) {
			return getRuleContext(And_expressionContext.class,i);
		}
		public List<And_expressionContext> and_expression() {
			return getRuleContexts(And_expressionContext.class);
		}
		public Assignment_xor_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Assignment_xor_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_assignment_xor_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAssignment_xor_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAssignment_xor_expression(this);
		}
	}

	public final Assignment_xor_expressionContext assignment_xor_expression(Env env) throws RecognitionException {
		Assignment_xor_expressionContext _localctx = new Assignment_xor_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 50, RULE_assignment_xor_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(455); ((Assignment_xor_expressionContext)_localctx).unary_expression = unary_expression(env);
			setState(456); match(17);
			setState(457); ((Assignment_xor_expressionContext)_localctx).exp0 = and_expression(env);
			setState(458); match(45);
			setState(459); ((Assignment_xor_expressionContext)_localctx).exp1 = and_expression(env);

			        try
			        {
			            ((Assignment_xor_expressionContext)_localctx).symbol =  ObjectSymbol.or(((Assignment_xor_expressionContext)_localctx).unary_expression.symbol, ((Assignment_xor_expressionContext)_localctx).exp0.symbol, ((Assignment_xor_expressionContext)_localctx).exp1.symbol, false,
			                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			            if (_localctx.symbol instanceof ConstSymbol)
			                ((Assignment_xor_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_xor_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			            ((Assignment_xor_expressionContext)_localctx).symbol =  ((Assignment_xor_expressionContext)_localctx).unary_expression.symbol;
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_and_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Unary_expressionContext unary_expression;
		public Equality_expressionContext exp0;
		public Equality_expressionContext exp1;
		public Equality_expressionContext equality_expression(int i) {
			return getRuleContext(Equality_expressionContext.class,i);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public List<Equality_expressionContext> equality_expression() {
			return getRuleContexts(Equality_expressionContext.class);
		}
		public Assignment_and_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Assignment_and_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_assignment_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAssignment_and_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAssignment_and_expression(this);
		}
	}

	public final Assignment_and_expressionContext assignment_and_expression(Env env) throws RecognitionException {
		Assignment_and_expressionContext _localctx = new Assignment_and_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 52, RULE_assignment_and_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(462); ((Assignment_and_expressionContext)_localctx).unary_expression = unary_expression(env);
			setState(463); match(17);
			setState(464); ((Assignment_and_expressionContext)_localctx).exp0 = equality_expression(env);
			setState(465); match(1);
			setState(466); ((Assignment_and_expressionContext)_localctx).exp1 = equality_expression(env);

			        try
			        {
			            ((Assignment_and_expressionContext)_localctx).symbol =  ObjectSymbol.or(((Assignment_and_expressionContext)_localctx).unary_expression.symbol, ((Assignment_and_expressionContext)_localctx).exp0.symbol, ((Assignment_and_expressionContext)_localctx).exp1.symbol, false,
			                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			            if (_localctx.symbol instanceof ConstSymbol)
			                ((Assignment_and_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_and_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			            ((Assignment_and_expressionContext)_localctx).symbol =  ((Assignment_and_expressionContext)_localctx).unary_expression.symbol;
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_shift_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Unary_expressionContext unary_expression;
		public Additive_expressionContext exp0;
		public Token op;
		public Additive_expressionContext exp1;
		public Additive_expressionContext additive_expression(int i) {
			return getRuleContext(Additive_expressionContext.class,i);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public List<Additive_expressionContext> additive_expression() {
			return getRuleContexts(Additive_expressionContext.class);
		}
		public Assignment_shift_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Assignment_shift_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_assignment_shift_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAssignment_shift_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAssignment_shift_expression(this);
		}
	}

	public final Assignment_shift_expressionContext assignment_shift_expression(Env env) throws RecognitionException {
		Assignment_shift_expressionContext _localctx = new Assignment_shift_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 54, RULE_assignment_shift_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(469); ((Assignment_shift_expressionContext)_localctx).unary_expression = unary_expression(env);
			setState(470); match(17);
			setState(471); ((Assignment_shift_expressionContext)_localctx).exp0 = additive_expression(env);
			setState(472);
			((Assignment_shift_expressionContext)_localctx).op = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==9 || _la==43) ) {
				((Assignment_shift_expressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
			}
			consume();
			setState(473); ((Assignment_shift_expressionContext)_localctx).exp1 = additive_expression(env);

			        try
			        {
			            switch ((((Assignment_shift_expressionContext)_localctx).op!=null?((Assignment_shift_expressionContext)_localctx).op.getText():null))
			            {
			            case "<<":
			                ((Assignment_shift_expressionContext)_localctx).symbol =  ObjectSymbol.shiftLeft(((Assignment_shift_expressionContext)_localctx).unary_expression.symbol, ((Assignment_shift_expressionContext)_localctx).exp0.symbol, ((Assignment_shift_expressionContext)_localctx).exp1.symbol, false,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			                break;
			            case ">>":
			                ((Assignment_shift_expressionContext)_localctx).symbol =  ObjectSymbol.shiftRight(((Assignment_shift_expressionContext)_localctx).unary_expression.symbol, ((Assignment_shift_expressionContext)_localctx).exp0.symbol, ((Assignment_shift_expressionContext)_localctx).exp1.symbol, false,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			                break;
			            }
			            if (_localctx.symbol instanceof ConstSymbol)
			                ((Assignment_shift_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_shift_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			            ((Assignment_shift_expressionContext)_localctx).symbol =  ((Assignment_shift_expressionContext)_localctx).unary_expression.symbol;
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_additive_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Unary_expressionContext unary_expression;
		public Multiplicative_expressionContext exp0;
		public Token op;
		public Multiplicative_expressionContext exp1;
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Multiplicative_expressionContext multiplicative_expression(int i) {
			return getRuleContext(Multiplicative_expressionContext.class,i);
		}
		public List<Multiplicative_expressionContext> multiplicative_expression() {
			return getRuleContexts(Multiplicative_expressionContext.class);
		}
		public Assignment_additive_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Assignment_additive_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_assignment_additive_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAssignment_additive_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAssignment_additive_expression(this);
		}
	}

	public final Assignment_additive_expressionContext assignment_additive_expression(Env env) throws RecognitionException {
		Assignment_additive_expressionContext _localctx = new Assignment_additive_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 56, RULE_assignment_additive_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(476); ((Assignment_additive_expressionContext)_localctx).unary_expression = unary_expression(env);
			setState(477); match(17);
			setState(478); ((Assignment_additive_expressionContext)_localctx).exp0 = multiplicative_expression(env);
			setState(479);
			((Assignment_additive_expressionContext)_localctx).op = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==26 || _la==47) ) {
				((Assignment_additive_expressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
			}
			consume();
			setState(480); ((Assignment_additive_expressionContext)_localctx).exp1 = multiplicative_expression(env);

			        try
			        {
			            switch ((((Assignment_additive_expressionContext)_localctx).op!=null?((Assignment_additive_expressionContext)_localctx).op.getText():null))
			            {
			            case "+":
			                ((Assignment_additive_expressionContext)_localctx).symbol =  ObjectSymbol.plus(((Assignment_additive_expressionContext)_localctx).unary_expression.symbol, ((Assignment_additive_expressionContext)_localctx).exp0.symbol, ((Assignment_additive_expressionContext)_localctx).exp1.symbol, false,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			                break;
			            case "-":
			                ((Assignment_additive_expressionContext)_localctx).symbol =  ObjectSymbol.minus(((Assignment_additive_expressionContext)_localctx).unary_expression.symbol, ((Assignment_additive_expressionContext)_localctx).exp0.symbol, ((Assignment_additive_expressionContext)_localctx).exp1.symbol, false,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			                break;
			            }
			            if (_localctx.symbol instanceof ConstSymbol)
			                ((Assignment_additive_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_additive_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			            ((Assignment_additive_expressionContext)_localctx).symbol =  ((Assignment_additive_expressionContext)_localctx).unary_expression.symbol;
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_multiplicative_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Unary_expressionContext unary_expression;
		public Cast_expressionContext exp0;
		public Token op;
		public Cast_expressionContext exp1;
		public List<Cast_expressionContext> cast_expression() {
			return getRuleContexts(Cast_expressionContext.class);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Cast_expressionContext cast_expression(int i) {
			return getRuleContext(Cast_expressionContext.class,i);
		}
		public Assignment_multiplicative_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Assignment_multiplicative_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_assignment_multiplicative_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAssignment_multiplicative_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAssignment_multiplicative_expression(this);
		}
	}

	public final Assignment_multiplicative_expressionContext assignment_multiplicative_expression(Env env) throws RecognitionException {
		Assignment_multiplicative_expressionContext _localctx = new Assignment_multiplicative_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 58, RULE_assignment_multiplicative_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(483); ((Assignment_multiplicative_expressionContext)_localctx).unary_expression = unary_expression(env);
			setState(484); match(17);
			setState(485); ((Assignment_multiplicative_expressionContext)_localctx).exp0 = cast_expression(env);
			setState(486);
			((Assignment_multiplicative_expressionContext)_localctx).op = _input.LT(1);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 12) | (1L << 57))) != 0)) ) {
				((Assignment_multiplicative_expressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
			}
			consume();
			setState(487); ((Assignment_multiplicative_expressionContext)_localctx).exp1 = cast_expression(env);

			        try
			        {
			            switch ((((Assignment_multiplicative_expressionContext)_localctx).op!=null?((Assignment_multiplicative_expressionContext)_localctx).op.getText():null))
			            {
			            case "*":
			                ((Assignment_multiplicative_expressionContext)_localctx).symbol =  ObjectSymbol.mul(((Assignment_multiplicative_expressionContext)_localctx).unary_expression.symbol, ((Assignment_multiplicative_expressionContext)_localctx).exp0.symbol, ((Assignment_multiplicative_expressionContext)_localctx).exp1.symbol, false,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			                break;
			            case "/":
			                ((Assignment_multiplicative_expressionContext)_localctx).symbol =  ObjectSymbol.div(((Assignment_multiplicative_expressionContext)_localctx).unary_expression.symbol, ((Assignment_multiplicative_expressionContext)_localctx).exp0.symbol, ((Assignment_multiplicative_expressionContext)_localctx).exp1.symbol, false,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			                break;
			            case "%":
			                ((Assignment_multiplicative_expressionContext)_localctx).symbol =  ObjectSymbol.mod(((Assignment_multiplicative_expressionContext)_localctx).unary_expression.symbol, ((Assignment_multiplicative_expressionContext)_localctx).exp0.symbol, ((Assignment_multiplicative_expressionContext)_localctx).exp1.symbol, false,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			                break;
			            }
			            if (_localctx.symbol instanceof ConstSymbol)
			                ((Assignment_multiplicative_expressionContext)_localctx).symbol =  ObjectSymbol.assign(((Assignment_multiplicative_expressionContext)_localctx).unary_expression.symbol, _localctx.symbol,
			                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			            ((Assignment_multiplicative_expressionContext)_localctx).symbol =  ((Assignment_multiplicative_expressionContext)_localctx).unary_expression.symbol;
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Constant_expressionContext extends ParserRuleContext {
		public Env env;
		public ConstSymbol symbol;
		public Logical_or_expressionContext logical_or_expression;
		public Logical_or_expressionContext logical_or_expression() {
			return getRuleContext(Logical_or_expressionContext.class,0);
		}
		public Constant_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Constant_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_constant_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterConstant_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitConstant_expression(this);
		}
	}

	public final Constant_expressionContext constant_expression(Env env) throws RecognitionException {
		Constant_expressionContext _localctx = new Constant_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 60, RULE_constant_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(490); ((Constant_expressionContext)_localctx).logical_or_expression = logical_or_expression(env);

			        try
			        {
			            if (! (((Constant_expressionContext)_localctx).logical_or_expression.symbol instanceof ConstSymbol))
			            {
			                ((Constant_expressionContext)_localctx).symbol =  new ConstIntSymbol();
			                throw new CompileException("used '"+((Constant_expressionContext)_localctx).logical_or_expression.symbol.typeInfo()+"' when a constant is required");
			            }
			            ((Constant_expressionContext)_localctx).symbol =  (ConstSymbol)((Constant_expressionContext)_localctx).logical_or_expression.symbol;
			        }
			        catch (CompileException e)
			        {
			            notifyErrorListeners(e.msg);
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_or_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Logical_and_expressionContext logical_and_expression;
		public Logical_and_expressionContext exp;
		public List<Logical_and_expressionContext> logical_and_expression() {
			return getRuleContexts(Logical_and_expressionContext.class);
		}
		public Logical_and_expressionContext logical_and_expression(int i) {
			return getRuleContext(Logical_and_expressionContext.class,i);
		}
		public Logical_or_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Logical_or_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_logical_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterLogical_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitLogical_or_expression(this);
		}
	}

	public final Logical_or_expressionContext logical_or_expression(Env env) throws RecognitionException {
		Logical_or_expressionContext _localctx = new Logical_or_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 62, RULE_logical_or_expression);

		    JumpSymbol jump = null;
		    ObjectSymbol tmp = null;

		int _la;
		try {
			setState(509);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(493); ((Logical_or_expressionContext)_localctx).logical_and_expression = logical_and_expression(env);

				        try
				        {
				            jump = env.getBranchJump();
				            ObjectSymbol boolExp = ObjectSymbol.toBool(((Logical_or_expressionContext)_localctx).logical_and_expression.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            ((Logical_or_expressionContext)_localctx).symbol =  ObjectSymbol.assignRvalue(new ObjectSymbol(true, IntSymbol.instance), boolExp,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				setState(500); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(495); match(53);

					            IRGenerator.add("BNEZ", _localctx.symbol, jump.id,
					                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
					        
					setState(497); ((Logical_or_expressionContext)_localctx).exp = ((Logical_or_expressionContext)_localctx).logical_and_expression = logical_and_expression(env);

					            try
					            {
					                ObjectSymbol boolExp = ObjectSymbol.toBool(((Logical_or_expressionContext)_localctx).exp.symbol,
					                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
					                ((Logical_or_expressionContext)_localctx).symbol =  ObjectSymbol.logicOr(_localctx.symbol, _localctx.symbol, boolExp,
					                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
					            }
					            catch (CompileException e)
					            {
					                notifyErrorListeners(e.msg);
					            }
					        
					}
					}
					setState(502); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==53 );

				        IRGenerator.add(jump,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(506); ((Logical_or_expressionContext)_localctx).logical_and_expression = logical_and_expression(env);

				        ((Logical_or_expressionContext)_localctx).symbol =  ((Logical_or_expressionContext)_localctx).logical_and_expression.symbol;
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_and_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Inclusive_or_expressionContext inclusive_or_expression;
		public Inclusive_or_expressionContext exp;
		public List<Inclusive_or_expressionContext> inclusive_or_expression() {
			return getRuleContexts(Inclusive_or_expressionContext.class);
		}
		public Inclusive_or_expressionContext inclusive_or_expression(int i) {
			return getRuleContext(Inclusive_or_expressionContext.class,i);
		}
		public Logical_and_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Logical_and_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_logical_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterLogical_and_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitLogical_and_expression(this);
		}
	}

	public final Logical_and_expressionContext logical_and_expression(Env env) throws RecognitionException {
		Logical_and_expressionContext _localctx = new Logical_and_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 64, RULE_logical_and_expression);

		    JumpSymbol jump = null;
		    ObjectSymbol tmp = null;

		int _la;
		try {
			setState(527);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(511); ((Logical_and_expressionContext)_localctx).inclusive_or_expression = inclusive_or_expression(env);

				        try
				        {
				            jump = env.getBranchJump();
				            ObjectSymbol boolExp = ObjectSymbol.toBool(((Logical_and_expressionContext)_localctx).inclusive_or_expression.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            ((Logical_and_expressionContext)_localctx).symbol =  ObjectSymbol.assignRvalue(new ObjectSymbol(true, IntSymbol.instance), boolExp,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				setState(518); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(513); match(51);

					            IRGenerator.add("BEQZ", _localctx.symbol, jump.id,
					                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
					        
					setState(515); ((Logical_and_expressionContext)_localctx).exp = ((Logical_and_expressionContext)_localctx).inclusive_or_expression = inclusive_or_expression(env);

					            try
					            {
					                ObjectSymbol boolExp = ObjectSymbol.toBool(((Logical_and_expressionContext)_localctx).exp.symbol,
					                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
					                ((Logical_and_expressionContext)_localctx).symbol =  ObjectSymbol.logicAnd(_localctx.symbol, _localctx.symbol, boolExp,
					                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
					            }
					            catch (CompileException e)
					            {
					                notifyErrorListeners(e.msg);
					            }
					        
					}
					}
					setState(520); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==51 );

				        IRGenerator.add(jump,
				            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(524); ((Logical_and_expressionContext)_localctx).inclusive_or_expression = inclusive_or_expression(env);

				        ((Logical_and_expressionContext)_localctx).symbol =  ((Logical_and_expressionContext)_localctx).inclusive_or_expression.symbol;
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Inclusive_or_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Exclusive_or_expressionContext exclusive_or_expression;
		public Exclusive_or_expressionContext exp;
		public Exclusive_or_expressionContext exclusive_or_expression(int i) {
			return getRuleContext(Exclusive_or_expressionContext.class,i);
		}
		public List<Exclusive_or_expressionContext> exclusive_or_expression() {
			return getRuleContexts(Exclusive_or_expressionContext.class);
		}
		public Inclusive_or_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Inclusive_or_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_inclusive_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterInclusive_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitInclusive_or_expression(this);
		}
	}

	public final Inclusive_or_expressionContext inclusive_or_expression(Env env) throws RecognitionException {
		Inclusive_or_expressionContext _localctx = new Inclusive_or_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 66, RULE_inclusive_or_expression);

		    ObjectSymbol tmp = null;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(529); ((Inclusive_or_expressionContext)_localctx).exclusive_or_expression = exclusive_or_expression(env);

			        ((Inclusive_or_expressionContext)_localctx).symbol =  ((Inclusive_or_expressionContext)_localctx).exclusive_or_expression.symbol;
			    
			setState(537);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==19) {
				{
				{
				setState(531); match(19);
				setState(532); ((Inclusive_or_expressionContext)_localctx).exp = ((Inclusive_or_expressionContext)_localctx).exclusive_or_expression = exclusive_or_expression(env);

				            try
				            {
				                if (tmp == null)
				                {
				                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
				                    ((Inclusive_or_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.or(tmp, _localctx.symbol, ((Inclusive_or_expressionContext)_localctx).exp.symbol, true,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    if (tmp instanceof ConstSymbol)
				                        tmp = null;
				                }
				                else
				                    ((Inclusive_or_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.or(tmp, tmp, ((Inclusive_or_expressionContext)_localctx).exp.symbol, true,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
				}
				setState(539);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Exclusive_or_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public And_expressionContext and_expression;
		public And_expressionContext exp;
		public And_expressionContext and_expression(int i) {
			return getRuleContext(And_expressionContext.class,i);
		}
		public List<And_expressionContext> and_expression() {
			return getRuleContexts(And_expressionContext.class);
		}
		public Exclusive_or_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Exclusive_or_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_exclusive_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterExclusive_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitExclusive_or_expression(this);
		}
	}

	public final Exclusive_or_expressionContext exclusive_or_expression(Env env) throws RecognitionException {
		Exclusive_or_expressionContext _localctx = new Exclusive_or_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 68, RULE_exclusive_or_expression);

		    ObjectSymbol tmp = null;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(540); ((Exclusive_or_expressionContext)_localctx).and_expression = and_expression(env);

			        ((Exclusive_or_expressionContext)_localctx).symbol =  ((Exclusive_or_expressionContext)_localctx).and_expression.symbol;
			    
			setState(548);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==45) {
				{
				{
				setState(542); match(45);
				setState(543); ((Exclusive_or_expressionContext)_localctx).exp = ((Exclusive_or_expressionContext)_localctx).and_expression = and_expression(env);

				            try
				            {
				                if (tmp == null)
				                {
				                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
				                    ((Exclusive_or_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.xor(tmp, _localctx.symbol, ((Exclusive_or_expressionContext)_localctx).exp.symbol, true,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    if (tmp instanceof ConstSymbol)
				                        tmp = null;
				                }
				                else
				                    ((Exclusive_or_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.xor(tmp, tmp, ((Exclusive_or_expressionContext)_localctx).exp.symbol, true,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
				}
				setState(550);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class And_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Equality_expressionContext equality_expression;
		public Equality_expressionContext exp;
		public Equality_expressionContext equality_expression(int i) {
			return getRuleContext(Equality_expressionContext.class,i);
		}
		public List<Equality_expressionContext> equality_expression() {
			return getRuleContexts(Equality_expressionContext.class);
		}
		public And_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public And_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAnd_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAnd_expression(this);
		}
	}

	public final And_expressionContext and_expression(Env env) throws RecognitionException {
		And_expressionContext _localctx = new And_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 70, RULE_and_expression);

		    ObjectSymbol tmp = null;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(551); ((And_expressionContext)_localctx).equality_expression = equality_expression(env);

			        ((And_expressionContext)_localctx).symbol =  ((And_expressionContext)_localctx).equality_expression.symbol;
			    
			setState(559);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==1) {
				{
				{
				setState(553); match(1);
				setState(554); ((And_expressionContext)_localctx).exp = ((And_expressionContext)_localctx).equality_expression = equality_expression(env);

				            try
				            {
				                if (tmp == null)
				                {
				                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
				                    ((And_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.and(tmp, _localctx.symbol, ((And_expressionContext)_localctx).exp.symbol, true,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    if (tmp instanceof ConstSymbol)
				                        tmp = null;
				                }
				                else
				                    ((And_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.and(tmp, tmp, ((And_expressionContext)_localctx).exp.symbol, true,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
				}
				setState(561);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Equality_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Relational_expressionContext relational_expression;
		public Token op;
		public Relational_expressionContext exp;
		public List<Relational_expressionContext> relational_expression() {
			return getRuleContexts(Relational_expressionContext.class);
		}
		public Relational_expressionContext relational_expression(int i) {
			return getRuleContext(Relational_expressionContext.class,i);
		}
		public Equality_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Equality_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_equality_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterEquality_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitEquality_expression(this);
		}
	}

	public final Equality_expressionContext equality_expression(Env env) throws RecognitionException {
		Equality_expressionContext _localctx = new Equality_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 72, RULE_equality_expression);

		    ObjectSymbol tmp = null;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(562); ((Equality_expressionContext)_localctx).relational_expression = relational_expression(env);

			        ((Equality_expressionContext)_localctx).symbol =  ((Equality_expressionContext)_localctx).relational_expression.symbol;
			    
			setState(570);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==7 || _la==58) {
				{
				{
				setState(564);
				((Equality_expressionContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==7 || _la==58) ) {
					((Equality_expressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				consume();
				setState(565); ((Equality_expressionContext)_localctx).exp = ((Equality_expressionContext)_localctx).relational_expression = relational_expression(env);

				            try
				            {
				                switch ((((Equality_expressionContext)_localctx).op!=null?((Equality_expressionContext)_localctx).op.getText():null))
				                {
				                case "==":
				                    ((Equality_expressionContext)_localctx).symbol =  ObjectSymbol.equal(_localctx.symbol, ((Equality_expressionContext)_localctx).exp.symbol,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    break;
				                case "!=":
				                    ((Equality_expressionContext)_localctx).symbol =  ObjectSymbol.notEqual(_localctx.symbol, ((Equality_expressionContext)_localctx).exp.symbol,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    break;
				                }
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
				}
				setState(572);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Relational_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Shift_expressionContext shift_expression;
		public Token op;
		public Shift_expressionContext exp;
		public List<Shift_expressionContext> shift_expression() {
			return getRuleContexts(Shift_expressionContext.class);
		}
		public Shift_expressionContext shift_expression(int i) {
			return getRuleContext(Shift_expressionContext.class,i);
		}
		public Relational_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Relational_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_relational_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterRelational_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitRelational_expression(this);
		}
	}

	public final Relational_expressionContext relational_expression(Env env) throws RecognitionException {
		Relational_expressionContext _localctx = new Relational_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 74, RULE_relational_expression);

		    ObjectSymbol tmp = null;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(573); ((Relational_expressionContext)_localctx).shift_expression = shift_expression(env);

			        ((Relational_expressionContext)_localctx).symbol =  ((Relational_expressionContext)_localctx).shift_expression.symbol;
			    
			setState(581);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 4) | (1L << 8) | (1L << 54) | (1L << 60))) != 0)) {
				{
				{
				setState(575);
				((Relational_expressionContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 4) | (1L << 8) | (1L << 54) | (1L << 60))) != 0)) ) {
					((Relational_expressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				consume();
				setState(576); ((Relational_expressionContext)_localctx).exp = ((Relational_expressionContext)_localctx).shift_expression = shift_expression(env);

				            try
				            {
				                switch ((((Relational_expressionContext)_localctx).op!=null?((Relational_expressionContext)_localctx).op.getText():null))
				                {
				                case "<=":
				                    ((Relational_expressionContext)_localctx).symbol =  ObjectSymbol.lessEqual(_localctx.symbol, ((Relational_expressionContext)_localctx).exp.symbol,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    break;
				                case ">=":
				                    ((Relational_expressionContext)_localctx).symbol =  ObjectSymbol.greaterEqual(_localctx.symbol, ((Relational_expressionContext)_localctx).exp.symbol,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    break;
				                case "<":
				                    ((Relational_expressionContext)_localctx).symbol =  ObjectSymbol.less(_localctx.symbol, ((Relational_expressionContext)_localctx).exp.symbol,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    break;
				                case ">":
				                    ((Relational_expressionContext)_localctx).symbol =  ObjectSymbol.greater(_localctx.symbol, ((Relational_expressionContext)_localctx).exp.symbol,
				                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                    break;
				                }
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
				}
				setState(583);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Shift_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Additive_expressionContext additive_expression;
		public Token op;
		public Additive_expressionContext exp;
		public Additive_expressionContext additive_expression(int i) {
			return getRuleContext(Additive_expressionContext.class,i);
		}
		public List<Additive_expressionContext> additive_expression() {
			return getRuleContexts(Additive_expressionContext.class);
		}
		public Shift_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Shift_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_shift_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterShift_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitShift_expression(this);
		}
	}

	public final Shift_expressionContext shift_expression(Env env) throws RecognitionException {
		Shift_expressionContext _localctx = new Shift_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 76, RULE_shift_expression);

		    ObjectSymbol tmp = null;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(584); ((Shift_expressionContext)_localctx).additive_expression = additive_expression(env);

			        ((Shift_expressionContext)_localctx).symbol =  ((Shift_expressionContext)_localctx).additive_expression.symbol;
			    
			setState(592);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==9 || _la==43) {
				{
				{
				setState(586);
				((Shift_expressionContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==9 || _la==43) ) {
					((Shift_expressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				consume();
				setState(587); ((Shift_expressionContext)_localctx).exp = ((Shift_expressionContext)_localctx).additive_expression = additive_expression(env);

				            try
				            {
				                if (tmp == null)
				                {
				                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
				                    switch ((((Shift_expressionContext)_localctx).op!=null?((Shift_expressionContext)_localctx).op.getText():null))
				                    {
				                    case "<<":
				                        ((Shift_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.shiftLeft(tmp, _localctx.symbol, ((Shift_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    case ">>":
				                        ((Shift_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.shiftRight(tmp, _localctx.symbol, ((Shift_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    }
				                    if (tmp instanceof ConstSymbol)
				                        tmp = null;
				                }
				                else
				                    switch ((((Shift_expressionContext)_localctx).op!=null?((Shift_expressionContext)_localctx).op.getText():null))
				                    {
				                    case "<<":
				                        ((Shift_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.shiftLeft(tmp, tmp, ((Shift_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    case ">>":
				                        ((Shift_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.shiftRight(tmp, tmp, ((Shift_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    }
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
				}
				setState(594);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Additive_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Multiplicative_expressionContext multiplicative_expression;
		public Token op;
		public Multiplicative_expressionContext exp;
		public Multiplicative_expressionContext multiplicative_expression(int i) {
			return getRuleContext(Multiplicative_expressionContext.class,i);
		}
		public List<Multiplicative_expressionContext> multiplicative_expression() {
			return getRuleContexts(Multiplicative_expressionContext.class);
		}
		public Additive_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Additive_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_additive_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterAdditive_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitAdditive_expression(this);
		}
	}

	public final Additive_expressionContext additive_expression(Env env) throws RecognitionException {
		Additive_expressionContext _localctx = new Additive_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 78, RULE_additive_expression);

		    ObjectSymbol tmp = null;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(595); ((Additive_expressionContext)_localctx).multiplicative_expression = multiplicative_expression(env);

			        ((Additive_expressionContext)_localctx).symbol =  ((Additive_expressionContext)_localctx).multiplicative_expression.symbol;
			    
			setState(603);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==26 || _la==47) {
				{
				{
				setState(597);
				((Additive_expressionContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==26 || _la==47) ) {
					((Additive_expressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				consume();
				setState(598); ((Additive_expressionContext)_localctx).exp = ((Additive_expressionContext)_localctx).multiplicative_expression = multiplicative_expression(env);

				            try
				            {
				                if (tmp == null)
				                {
				                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
				                    switch ((((Additive_expressionContext)_localctx).op!=null?((Additive_expressionContext)_localctx).op.getText():null))
				                    {
				                    case "+":
				                        ((Additive_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.plus(tmp, _localctx.symbol, ((Additive_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    case "-":
				                        ((Additive_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.minus(tmp, _localctx.symbol, ((Additive_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    }
				                    if (tmp instanceof ConstSymbol)
				                        tmp = null;
				                }
				                else
				                    switch ((((Additive_expressionContext)_localctx).op!=null?((Additive_expressionContext)_localctx).op.getText():null))
				                    {
				                    case "+":
				                        ((Additive_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.plus(tmp, tmp, ((Additive_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    case "-":
				                        ((Additive_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.minus(tmp, tmp, ((Additive_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    }
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
				}
				setState(605);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Multiplicative_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Cast_expressionContext cast_expression;
		public Token op;
		public Cast_expressionContext exp;
		public List<Cast_expressionContext> cast_expression() {
			return getRuleContexts(Cast_expressionContext.class);
		}
		public Cast_expressionContext cast_expression(int i) {
			return getRuleContext(Cast_expressionContext.class,i);
		}
		public Multiplicative_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Multiplicative_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_multiplicative_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterMultiplicative_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitMultiplicative_expression(this);
		}
	}

	public final Multiplicative_expressionContext multiplicative_expression(Env env) throws RecognitionException {
		Multiplicative_expressionContext _localctx = new Multiplicative_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 80, RULE_multiplicative_expression);

		    ObjectSymbol tmp = null;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(606); ((Multiplicative_expressionContext)_localctx).cast_expression = cast_expression(env);

			        ((Multiplicative_expressionContext)_localctx).symbol =  ((Multiplicative_expressionContext)_localctx).cast_expression.symbol;
			    
			setState(614);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 12) | (1L << 57))) != 0)) {
				{
				{
				setState(608);
				((Multiplicative_expressionContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 12) | (1L << 57))) != 0)) ) {
					((Multiplicative_expressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				consume();
				setState(609); ((Multiplicative_expressionContext)_localctx).exp = ((Multiplicative_expressionContext)_localctx).cast_expression = cast_expression(env);

				            try
				            {
				                if (tmp == null)
				                {
				                    tmp = new ObjectSymbol(true, IntSymbol.instance, false);
				                    switch ((((Multiplicative_expressionContext)_localctx).op!=null?((Multiplicative_expressionContext)_localctx).op.getText():null))
				                    {
				                    case "*":
				                        ((Multiplicative_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.mul(tmp, _localctx.symbol, ((Multiplicative_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    case "/":
				                        ((Multiplicative_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.div(tmp, _localctx.symbol, ((Multiplicative_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    case "%":
				                        ((Multiplicative_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.mod(tmp, _localctx.symbol, ((Multiplicative_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    }
				                    if (tmp instanceof ConstSymbol)
				                        tmp = null;
				                }
				                else
				                    switch ((((Multiplicative_expressionContext)_localctx).op!=null?((Multiplicative_expressionContext)_localctx).op.getText():null))
				                    {
				                    case "*":
				                        ((Multiplicative_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.mul(tmp, tmp, ((Multiplicative_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    case "/":
				                        ((Multiplicative_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.div(tmp, tmp, ((Multiplicative_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    case "%":
				                        ((Multiplicative_expressionContext)_localctx).symbol =  tmp = ObjectSymbol.mod(tmp, tmp, ((Multiplicative_expressionContext)_localctx).exp.symbol, true,
				                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				                        break;
				                    }
				            }
				            catch (CompileException e)
				            {
				                notifyErrorListeners(e.msg);
				            }
				        
				}
				}
				setState(616);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cast_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Type_nameContext type_name;
		public Cast_expressionContext exp;
		public Unary_expressionContext unary_expression;
		public Cast_expressionContext cast_expression() {
			return getRuleContext(Cast_expressionContext.class,0);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public Cast_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Cast_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_cast_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterCast_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitCast_expression(this);
		}
	}

	public final Cast_expressionContext cast_expression(Env env) throws RecognitionException {
		Cast_expressionContext _localctx = new Cast_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 82, RULE_cast_expression);
		try {
			setState(626);
			switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(617); match(28);
				setState(618); ((Cast_expressionContext)_localctx).type_name = type_name(env);
				setState(619); match(16);
				setState(620); ((Cast_expressionContext)_localctx).exp = cast_expression(env);

				        try
				        {
				            if (! (((Cast_expressionContext)_localctx).type_name.type instanceof Scalar))
				                throw new CompileException("conversion to non-scalar type requested");
				            ((Cast_expressionContext)_localctx).symbol =  ObjectSymbol.assign(new ObjectSymbol(true, ((Cast_expressionContext)_localctx).type_name.type, true), ((Cast_expressionContext)_localctx).exp.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(623); ((Cast_expressionContext)_localctx).unary_expression = unary_expression(env);

				        ((Cast_expressionContext)_localctx).symbol =  ((Cast_expressionContext)_localctx).unary_expression.symbol;
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public Env env;
		public Symbol type;
		public Type_specifierContext type_specifier;
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Type_nameContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterType_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitType_name(this);
		}
	}

	public final Type_nameContext type_name(Env env) throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState(), env);
		enterRule(_localctx, 84, RULE_type_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(628); ((Type_nameContext)_localctx).type_specifier = type_specifier(env);

			        ((Type_nameContext)_localctx).type =  ((Type_nameContext)_localctx).type_specifier.type;
			    
			setState(634);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==3) {
				{
				{
				setState(630); match(3);

				            ((Type_nameContext)_localctx).type =  new PointerSymbol(_localctx.type);
				        
				}
				}
				setState(636);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Type_nameContext type_name;
		public Unary_expressionContext exp;
		public Cast_expressionContext cast_expression;
		public Primary_expressionContext primary_expression;
		public ExpressionContext expression;
		public ArgumentsContext arguments;
		public Token Identifier;
		public Token id_arrow;
		public Cast_expressionContext cast_expression() {
			return getRuleContext(Cast_expressionContext.class,0);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public ArgumentsContext arguments(int i) {
			return getRuleContext(ArgumentsContext.class,i);
		}
		public TerminalNode Identifier(int i) {
			return getToken(CMinusParser.Identifier, i);
		}
		public List<TerminalNode> Identifier() { return getTokens(CMinusParser.Identifier); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public List<ArgumentsContext> arguments() {
			return getRuleContexts(ArgumentsContext.class);
		}
		public Primary_expressionContext primary_expression() {
			return getRuleContext(Primary_expressionContext.class,0);
		}
		public Unary_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Unary_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_unary_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterUnary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitUnary_expression(this);
		}
	}

	public final Unary_expressionContext unary_expression(Env env) throws RecognitionException {
		Unary_expressionContext _localctx = new Unary_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 86, RULE_unary_expression);

		    List<ObjectSymbol> subscript = null;

		int _la;
		try {
			int _alt;
			setState(712);
			switch ( getInterpreter().adaptivePredict(_input,52,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(637); match(21);
				setState(638); match(28);
				setState(639); ((Unary_expressionContext)_localctx).type_name = type_name(env);
				setState(640); match(16);

				        ((Unary_expressionContext)_localctx).symbol =  new ConstIntSymbol(((Unary_expressionContext)_localctx).type_name.type.sizeof);
				    
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(643); match(42);
				setState(644); ((Unary_expressionContext)_localctx).exp = unary_expression(env);

				        try
				        {
				            ((Unary_expressionContext)_localctx).symbol =  ObjectSymbol.inc(((Unary_expressionContext)_localctx).exp.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(647); match(5);
				setState(648); ((Unary_expressionContext)_localctx).exp = unary_expression(env);

				        try
				        {
				            ((Unary_expressionContext)_localctx).symbol =  ObjectSymbol.dec(((Unary_expressionContext)_localctx).exp.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(651); match(21);
				setState(652); ((Unary_expressionContext)_localctx).exp = unary_expression(env);

				        ((Unary_expressionContext)_localctx).symbol =  new ConstIntSymbol(((Unary_expressionContext)_localctx).exp.symbol.sizeof);
				    
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(655); match(1);
				setState(656); ((Unary_expressionContext)_localctx).cast_expression = cast_expression(env);

				        try
				        {
				            ((Unary_expressionContext)_localctx).symbol =  ((Unary_expressionContext)_localctx).cast_expression.symbol.ref(
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(659); match(3);
				setState(660); ((Unary_expressionContext)_localctx).cast_expression = cast_expression(env);

				        try
				        {
				            ((Unary_expressionContext)_localctx).symbol =  ((Unary_expressionContext)_localctx).cast_expression.symbol.deref(
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(663); match(47);
				setState(664); ((Unary_expressionContext)_localctx).cast_expression = cast_expression(env);

				        try
				        {
				            ((Unary_expressionContext)_localctx).symbol =  ObjectSymbol.posi(((Unary_expressionContext)_localctx).cast_expression.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(667); match(26);
				setState(668); ((Unary_expressionContext)_localctx).cast_expression = cast_expression(env);

				        try
				        {
				            ((Unary_expressionContext)_localctx).symbol =  ObjectSymbol.neg(((Unary_expressionContext)_localctx).cast_expression.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(671); match(59);
				setState(672); ((Unary_expressionContext)_localctx).cast_expression = cast_expression(env);

				        ((Unary_expressionContext)_localctx).symbol =  new ObjectSymbol(true, ((Unary_expressionContext)_localctx).cast_expression.symbol);
				        try
				        {
				            ((Unary_expressionContext)_localctx).symbol =  ObjectSymbol.not(((Unary_expressionContext)_localctx).cast_expression.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(675); match(20);
				setState(676); ((Unary_expressionContext)_localctx).cast_expression = cast_expression(env);

				        ((Unary_expressionContext)_localctx).symbol =  new ObjectSymbol(true, ((Unary_expressionContext)_localctx).cast_expression.symbol);
				        try
				        {
				            ((Unary_expressionContext)_localctx).symbol =  ObjectSymbol.logicNot(((Unary_expressionContext)_localctx).cast_expression.symbol,
				                java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;

			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(679); ((Unary_expressionContext)_localctx).primary_expression = primary_expression(env);

				        ((Unary_expressionContext)_localctx).symbol =  ((Unary_expressionContext)_localctx).primary_expression.symbol;
				    
				setState(709);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 5) | (1L << 13) | (1L << 28) | (1L << 42) | (1L << 46))) != 0)) {
					{
					setState(707);
					switch (_input.LA(1)) {
					case 2:
						{
						setState(686); 
						_errHandler.sync(this);
						_alt = 1;
						do {
							switch (_alt) {
							case 1:
								{
								{
								setState(681); match(2);
								setState(682); ((Unary_expressionContext)_localctx).expression = expression(env);
								setState(683); match(23);

								                if (subscript == null)
								                    subscript = new ArrayList<ObjectSymbol>();
								                subscript.add(((Unary_expressionContext)_localctx).expression.symbol);
								            
								}
								}
								break;
							default:
								throw new NoViableAltException(this);
							}
							setState(688); 
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
						} while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER );

						            try
						            {
						                for (ObjectSymbol index : subscript)
						                    ((Unary_expressionContext)_localctx).symbol =  _localctx.symbol.subscript(index,
						                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                subscript = null;
						            }
						            catch (CompileException e)
						            {
						                notifyErrorListeners(e.msg);
						            }
						        
						}
						break;
					case 28:
						{
						setState(692); match(28);
						setState(693); ((Unary_expressionContext)_localctx).arguments = arguments(env);
						setState(694); match(16);

						            try
						            {
						                if (_localctx.symbol.type.equals(env.getObj("printf").type) && ((Unary_expressionContext)_localctx).arguments.args.size() >= 1 && ((Unary_expressionContext)_localctx).arguments.args.get(0) instanceof ConstStringSymbol)
						                {
						                    String format = ((ConstStringSymbol)((Unary_expressionContext)_localctx).arguments.args.get(0)).val;
						                    String buffer = "";
						                    for (int i = 0, j = 0; i < format.length(); ++i)
						                    {
						                        if (format.charAt(i) == '%')
						                        {
						                            ++i;
						                            switch (format.charAt(i))
						                            {
						                            case 'd':
						                                ++j;
						                                if (((Unary_expressionContext)_localctx).arguments.args.get(j) instanceof ConstCharSymbol)
						                                    buffer += (int)((ConstCharSymbol)((Unary_expressionContext)_localctx).arguments.args.get(j)).val;
						                                else if (((Unary_expressionContext)_localctx).arguments.args.get(j) instanceof ConstIntSymbol)
						                                    buffer += ((Unary_expressionContext)_localctx).arguments.args.get(j).toString();
						                                else
						                                {
						                                    if (buffer != "")
						                                        IRGenerator.syscall("WRITESTR", new ConstStringSymbol(buffer),
						                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                    buffer = "";
						                                    IRGenerator.syscall("WRITEINT", ((Unary_expressionContext)_localctx).arguments.args.get(j),
						                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                }
						                                break;
						                            case 'c':
						                                ++j;
						                                if (((Unary_expressionContext)_localctx).arguments.args.get(j) instanceof ConstCharSymbol)
						                                    buffer += ((ConstCharSymbol)((Unary_expressionContext)_localctx).arguments.args.get(j)).val;
						                                else if (((Unary_expressionContext)_localctx).arguments.args.get(j) instanceof ConstIntSymbol)
						                                    buffer += (char)((ConstIntSymbol)((Unary_expressionContext)_localctx).arguments.args.get(j)).val;
						                                else
						                                {
						                                    if (buffer != "")
						                                        IRGenerator.syscall("WRITESTR", new ConstStringSymbol(buffer),
						                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                    buffer = "";
						                                    IRGenerator.syscall("WRITECHAR", ((Unary_expressionContext)_localctx).arguments.args.get(j),
						                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                }
						                                break;
						                            case 's':
						                                ++j;
						                                if (((Unary_expressionContext)_localctx).arguments.args.get(j) instanceof ConstStringSymbol)
						                                    buffer += ((Unary_expressionContext)_localctx).arguments.args.get(j).toString();
						                                else
						                                {
						                                    IRGenerator.syscall("WRITESTR", new ConstStringSymbol(buffer),
						                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                    buffer = "";
						                                    IRGenerator.syscall("WRITESTR", ((Unary_expressionContext)_localctx).arguments.args.get(j),
						                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                }
						                                break;
						                            case '0':
						                                ++j;
						                                int len = format.charAt(++i)-'0';
						                                ++i;
						                                if (((Unary_expressionContext)_localctx).arguments.args.get(j) instanceof ConstCharSymbol)
						                                    buffer += java.lang.String.format("%0"+len+"d", (int)((ConstCharSymbol)((Unary_expressionContext)_localctx).arguments.args.get(j)).val);
						                                else if (((Unary_expressionContext)_localctx).arguments.args.get(j) instanceof ConstIntSymbol)
						                                    buffer += java.lang.String.format("%0"+len+"d", ((ConstIntSymbol)((Unary_expressionContext)_localctx).arguments.args.get(j)).val);
						                                else
						                                {
						                                    ObjectSymbol tmp = ObjectSymbol.assign(new ObjectSymbol(true, IntSymbol.instance, true), ((Unary_expressionContext)_localctx).arguments.args.get(j),
						                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                    int limit = 1;
						                                    for (int k = 1; k < len; ++k)
						                                        limit *= 10;
						                                    for (int k = 1; k < len; ++k)
						                                    {
						                                        JumpSymbol branchLabel = env.getBranchJump();
						                                        ObjectSymbol cond = ObjectSymbol.less(((Unary_expressionContext)_localctx).arguments.args.get(j), new ConstIntSymbol(limit),
						                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                        IRGenerator.add("BEQZ", cond, branchLabel.id,
						                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                        IRGenerator.syscall("WRITECHAR", new ConstCharSymbol('0'),
						                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                        IRGenerator.add(branchLabel,
						                                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                        limit /= 10;
						                                    }
						                                    IRGenerator.syscall("WRITEINT", ((Unary_expressionContext)_localctx).arguments.args.get(j),
						                                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                                }
						                                break;
						                            }
						                        }
						                        else
						                            buffer += format.charAt(i);
						                    }
						                    if (buffer != "")
						                        IRGenerator.syscall("WRITESTR", new ConstStringSymbol(buffer),
						                            java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                }
						                else
						                {
						                    IRGenerator.getBind().inline = false;
						                    ((Unary_expressionContext)_localctx).symbol =  _localctx.symbol.call(((Unary_expressionContext)_localctx).arguments.args,
						                        java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						                }
						            }
						            catch (CompileException e)
						            {
						                notifyErrorListeners(e.msg);
						            }
						        
						}
						break;
					case 46:
						{
						setState(697); match(46);
						setState(698); ((Unary_expressionContext)_localctx).Identifier = match(Identifier);

						            try
						            {
						                ((Unary_expressionContext)_localctx).symbol =  _localctx.symbol.get((((Unary_expressionContext)_localctx).Identifier!=null?((Unary_expressionContext)_localctx).Identifier.getText():null),
						                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						            }
						            catch (CompileException e)
						            {
						                notifyErrorListeners(e.msg);
						            }
						        
						}
						break;
					case 13:
						{
						setState(700); match(13);
						setState(701); ((Unary_expressionContext)_localctx).id_arrow = match(Identifier);

						            try
						            {
						                ((Unary_expressionContext)_localctx).symbol =  _localctx.symbol.arrow((((Unary_expressionContext)_localctx).id_arrow!=null?((Unary_expressionContext)_localctx).id_arrow.getText():null),
						                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						            }
						            catch (CompileException e)
						            {
						                notifyErrorListeners(e.msg);
						            }
						        
						}
						break;
					case 42:
						{
						setState(703); match(42);
						   
						            try
						            {
						                ((Unary_expressionContext)_localctx).symbol =  ObjectSymbol.postInc(_localctx.symbol,
						                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						            }
						            catch (CompileException e)
						            {
						                notifyErrorListeners(e.msg);
						            }
						        
						}
						break;
					case 5:
						{
						setState(705); match(5);

						            try
						            {
						                ((Unary_expressionContext)_localctx).symbol =  ObjectSymbol.postDec(_localctx.symbol,
						                    java.lang.String.format("%-"+IRGenerator.FORMAT_POS+"s %s", (IRGenerator.COMMENT_FILENAME ? IRGenerator.fileName+":" : "line ")+_localctx.start.getLine()+":"+_localctx.start.getCharPositionInLine(), _input.getText(_localctx.start, _input.LT(-1))));
						            }
						            catch (CompileException e)
						            {
						                notifyErrorListeners(e.msg);
						            }
						        
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					setState(711);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentsContext extends ParserRuleContext {
		public Env env;
		public List<ObjectSymbol> args;
		public Assignment_expressionContext arg;
		public List<Assignment_expressionContext> assignment_expression() {
			return getRuleContexts(Assignment_expressionContext.class);
		}
		public Assignment_expressionContext assignment_expression(int i) {
			return getRuleContext(Assignment_expressionContext.class,i);
		}
		public ArgumentsContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ArgumentsContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_arguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitArguments(this);
		}
	}

	public final ArgumentsContext arguments(Env env) throws RecognitionException {
		ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState(), env);
		enterRule(_localctx, 88, RULE_arguments);

		    ((ArgumentsContext)_localctx).args =  new ArrayList<ObjectSymbol>();

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(717);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 32) | (1L << 42) | (1L << 47) | (1L << 59) | (1L << Identifier) | (1L << HexIntegerConstant))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (IntegerConstant - 64)) | (1L << (OctIntegerConstant - 64)) | (1L << (String - 64)) | (1L << (CharacterConstant - 64)))) != 0)) {
				{
				setState(714); ((ArgumentsContext)_localctx).arg = assignment_expression(env);

				        _localctx.args.add(((ArgumentsContext)_localctx).arg.symbol);
				    
				}
			}

			setState(725);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==25) {
				{
				{
				setState(719); match(25);
				setState(720); ((ArgumentsContext)_localctx).arg = assignment_expression(env);

				            _localctx.args.add(((ArgumentsContext)_localctx).arg.symbol);
				        
				}
				}
				setState(727);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Primary_expressionContext extends ParserRuleContext {
		public Env env;
		public ObjectSymbol symbol;
		public Va_arg_statementContext va_arg_statement;
		public Token Identifier;
		public Token IntegerConstant;
		public Token HexIntegerConstant;
		public Token OctIntegerConstant;
		public Token CharacterConstant;
		public Token String;
		public ExpressionContext expression;
		public TerminalNode String() { return getToken(CMinusParser.String, 0); }
		public TerminalNode CharacterConstant() { return getToken(CMinusParser.CharacterConstant, 0); }
		public Va_arg_statementContext va_arg_statement() {
			return getRuleContext(Va_arg_statementContext.class,0);
		}
		public TerminalNode IntegerConstant() { return getToken(CMinusParser.IntegerConstant, 0); }
		public TerminalNode Identifier() { return getToken(CMinusParser.Identifier, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode HexIntegerConstant() { return getToken(CMinusParser.HexIntegerConstant, 0); }
		public TerminalNode OctIntegerConstant() { return getToken(CMinusParser.OctIntegerConstant, 0); }
		public Primary_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Primary_expressionContext(ParserRuleContext parent, int invokingState, Env env) {
			super(parent, invokingState);
			this.env = env;
		}
		@Override public int getRuleIndex() { return RULE_primary_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).enterPrimary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CMinusListener ) ((CMinusListener)listener).exitPrimary_expression(this);
		}
	}

	public final Primary_expressionContext primary_expression(Env env) throws RecognitionException {
		Primary_expressionContext _localctx = new Primary_expressionContext(_ctx, getState(), env);
		enterRule(_localctx, 90, RULE_primary_expression);
		try {
			setState(748);
			switch (_input.LA(1)) {
			case 32:
				enterOuterAlt(_localctx, 1);
				{
				setState(728); ((Primary_expressionContext)_localctx).va_arg_statement = va_arg_statement(env);

				        ((Primary_expressionContext)_localctx).symbol =  ((Primary_expressionContext)_localctx).va_arg_statement.symbol;
				    
				}
				break;
			case Identifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(731); ((Primary_expressionContext)_localctx).Identifier = match(Identifier);

				        try
				        {
				            ((Primary_expressionContext)_localctx).symbol =  env.getObj((((Primary_expressionContext)_localctx).Identifier!=null?((Primary_expressionContext)_localctx).Identifier.getText():null));
				        }
				        catch (CompileException e)
				        {
				            notifyErrorListeners(e.msg);
				        }
				    
				}
				break;
			case IntegerConstant:
				enterOuterAlt(_localctx, 3);
				{
				setState(733); ((Primary_expressionContext)_localctx).IntegerConstant = match(IntegerConstant);

				        ((Primary_expressionContext)_localctx).symbol =  new ConstIntSymbol(Integer.parseInt((((Primary_expressionContext)_localctx).IntegerConstant!=null?((Primary_expressionContext)_localctx).IntegerConstant.getText():null)));
				    
				}
				break;
			case HexIntegerConstant:
				enterOuterAlt(_localctx, 4);
				{
				setState(735); ((Primary_expressionContext)_localctx).HexIntegerConstant = match(HexIntegerConstant);

				        int val = 0;
				        for (char i : (((Primary_expressionContext)_localctx).HexIntegerConstant!=null?((Primary_expressionContext)_localctx).HexIntegerConstant.getText():null).toLowerCase().substring(2).toCharArray())
				            val = val*16+(Character.isDigit(i) ? (int)i-'0' : (int)i-'a'+10);
				        ((Primary_expressionContext)_localctx).symbol =  new ConstIntSymbol(val);
				    
				}
				break;
			case OctIntegerConstant:
				enterOuterAlt(_localctx, 5);
				{
				setState(737); ((Primary_expressionContext)_localctx).OctIntegerConstant = match(OctIntegerConstant);

				        int val = 0;
				        for (char i : (((Primary_expressionContext)_localctx).OctIntegerConstant!=null?((Primary_expressionContext)_localctx).OctIntegerConstant.getText():null).toCharArray())
				            val = val*8+(int)i-'0';
				        ((Primary_expressionContext)_localctx).symbol =  new ConstIntSymbol(val);
				    
				}
				break;
			case CharacterConstant:
				enterOuterAlt(_localctx, 6);
				{
				setState(739); ((Primary_expressionContext)_localctx).CharacterConstant = match(CharacterConstant);

				        switch ((((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).substring(1, (((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).length()-1))
				        {
				        case "\\'":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('\'');
				            break;
				        case "\\\"":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('\"');
				            break;
				        case "\\?":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('?');
				            break;
				        case "\\\\":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('\\');
				            break;
				        case "\\a":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol((char)7);
				            break;
				        case "\\b":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol((char)8);
				            break;
				        case "\\f":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol((char)12);
				            break;
				        case "\\n":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('\n');
				            break;
				        case "\\r":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('\r');
				            break;
				        case "\\t":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('\t');
				            break;
				        case "\\v":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol((char)11);
				            break;
				        case "??=":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('#');
				            break;
				        case "??)":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol(']');
				            break;
				        case "??!":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('|');
				            break;
				        case "??(":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('[');
				            break;
				        case "??'":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('^');
				            break;
				        case "??>":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('}');
				            break;
				        case "??/":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('\\');
				            break;
				        case "??<":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('{');
				            break;
				        case "??-":
				            ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol('~');
				            break;
				        default:
				            if ((((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).startsWith("'\\x"))
				            {
				                int val = 0;
				                for (char i : (((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).toLowerCase().substring(3, (((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).length()-1).toCharArray())
				                    val = val*16+(Character.isDigit(i) ? (int)i-'0' : (int)i-'a'+10);
				                ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol((char)val);
				            }
				            else if ((((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).startsWith("'\\"))
				            {
				                int val = 0;
				                for (char i : (((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).toLowerCase().substring(2, (((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).length()-1).toCharArray())
				                    val = val*8+((int)i-'0');
				                ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol((char)val);
				            }
				            else
				                ((Primary_expressionContext)_localctx).symbol =  new ConstCharSymbol((((Primary_expressionContext)_localctx).CharacterConstant!=null?((Primary_expressionContext)_localctx).CharacterConstant.getText():null).charAt(1));
				        }
				    
				}
				break;
			case String:
				enterOuterAlt(_localctx, 7);
				{
				setState(741); ((Primary_expressionContext)_localctx).String = match(String);

				        ((Primary_expressionContext)_localctx).symbol =  new ConstStringSymbol((((Primary_expressionContext)_localctx).String!=null?((Primary_expressionContext)_localctx).String.getText():null).substring(1, (((Primary_expressionContext)_localctx).String!=null?((Primary_expressionContext)_localctx).String.getText():null).length()-1));
				    
				}
				break;
			case 28:
				enterOuterAlt(_localctx, 8);
				{
				setState(743); match(28);
				setState(744); ((Primary_expressionContext)_localctx).expression = expression(env);
				setState(745); match(16);

				        ((Primary_expressionContext)_localctx).symbol =  ((Primary_expressionContext)_localctx).expression.symbol;
				    
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3H\u02f1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\3\2\3\2\3\3\3\3\6\3c\n\3\r\3\16\3d\3\4\3\4\5\4"+
		"i\n\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\5\5r\n\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6"+
		"\7\6{\n\6\f\6\16\6~\13\6\3\6\3\6\3\6\5\6\u0083\n\6\3\7\3\7\3\7\7\7\u0088"+
		"\n\7\f\7\16\7\u008b\13\7\3\b\3\b\3\b\7\b\u0090\n\b\f\b\16\b\u0093\13\b"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u009b\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\7\n\u00a7\n\n\f\n\16\n\u00aa\13\n\3\n\3\n\5\n\u00ae\n\n\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00bc\n\13"+
		"\3\13\3\13\3\13\3\13\5\13\u00c2\n\13\3\13\3\13\6\13\u00c6\n\13\r\13\16"+
		"\13\u00c7\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00d1\n\13\3\13\3\13"+
		"\3\13\3\13\5\13\u00d7\n\13\3\13\3\13\6\13\u00db\n\13\r\13\16\13\u00dc"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00e8\n\13\3\f\3\f"+
		"\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00f7\n\r\3\r\3\r\3\r"+
		"\3\r\3\r\7\r\u00fe\n\r\f\r\16\r\u0101\13\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r"+
		"\u0109\n\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u0111\n\r\3\16\3\16\3\16\3\16\3"+
		"\16\3\16\5\16\u0119\n\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u0122"+
		"\n\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u012c\n\17\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\7\23\u0146\n\23\f\23\16\23\u0149"+
		"\13\23\3\23\7\23\u014c\n\23\f\23\16\23\u014f\13\23\3\23\3\23\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\5\24\u0166\n\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\5\25\u0174\n\25\3\25\3\25\3\25\3\25\3\25\5\25\u017b"+
		"\n\25\3\25\3\25\3\25\5\25\u0180\n\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25"+
		"\u0188\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\5\26\u0198\n\26\3\27\3\27\3\27\3\27\3\27\3\27\7\27\u01a0\n"+
		"\27\f\27\16\27\u01a3\13\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30"+
		"\3\30\3\30\3\30\5\30\u01bf\n\30\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36"+
		"\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3!\3!\3!\3!\3!\3!\3"+
		"!\6!\u01f7\n!\r!\16!\u01f8\3!\3!\3!\3!\3!\5!\u0200\n!\3\"\3\"\3\"\3\""+
		"\3\"\3\"\3\"\6\"\u0209\n\"\r\"\16\"\u020a\3\"\3\"\3\"\3\"\3\"\5\"\u0212"+
		"\n\"\3#\3#\3#\3#\3#\3#\7#\u021a\n#\f#\16#\u021d\13#\3$\3$\3$\3$\3$\3$"+
		"\7$\u0225\n$\f$\16$\u0228\13$\3%\3%\3%\3%\3%\3%\7%\u0230\n%\f%\16%\u0233"+
		"\13%\3&\3&\3&\3&\3&\3&\7&\u023b\n&\f&\16&\u023e\13&\3\'\3\'\3\'\3\'\3"+
		"\'\3\'\7\'\u0246\n\'\f\'\16\'\u0249\13\'\3(\3(\3(\3(\3(\3(\7(\u0251\n"+
		"(\f(\16(\u0254\13(\3)\3)\3)\3)\3)\3)\7)\u025c\n)\f)\16)\u025f\13)\3*\3"+
		"*\3*\3*\3*\3*\7*\u0267\n*\f*\16*\u026a\13*\3+\3+\3+\3+\3+\3+\3+\3+\3+"+
		"\5+\u0275\n+\3,\3,\3,\3,\7,\u027b\n,\f,\16,\u027e\13,\3-\3-\3-\3-\3-\3"+
		"-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3"+
		"-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\6-\u02b1"+
		"\n-\r-\16-\u02b2\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\7"+
		"-\u02c6\n-\f-\16-\u02c9\13-\5-\u02cb\n-\3.\3.\3.\5.\u02d0\n.\3.\3.\3."+
		"\3.\7.\u02d6\n.\f.\16.\u02d9\13.\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\3/\5/\u02ef\n/\3/\2\2\60\2\4\6\b\n\f\16\20\22\24\26"+
		"\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\\2\b\n\2\20\20\23"+
		"\24\30\30\32\32\37\37$$()9:\4\2\13\13--\4\2\34\34\61\61\5\2\5\5\16\16"+
		";;\4\2\t\t<<\6\2\6\6\n\n88>>\u0321\2^\3\2\2\2\4b\3\2\2\2\6f\3\2\2\2\b"+
		"l\3\2\2\2\nw\3\2\2\2\f\u0084\3\2\2\2\16\u008c\3\2\2\2\20\u0094\3\2\2\2"+
		"\22\u00ad\3\2\2\2\24\u00e7\3\2\2\2\26\u00e9\3\2\2\2\30\u0110\3\2\2\2\32"+
		"\u0118\3\2\2\2\34\u012b\3\2\2\2\36\u012d\3\2\2\2 \u0135\3\2\2\2\"\u013d"+
		"\3\2\2\2$\u0143\3\2\2\2&\u0165\3\2\2\2(\u0187\3\2\2\2*\u0197\3\2\2\2,"+
		"\u0199\3\2\2\2.\u01be\3\2\2\2\60\u01c0\3\2\2\2\62\u01c2\3\2\2\2\64\u01c9"+
		"\3\2\2\2\66\u01d0\3\2\2\28\u01d7\3\2\2\2:\u01de\3\2\2\2<\u01e5\3\2\2\2"+
		">\u01ec\3\2\2\2@\u01ff\3\2\2\2B\u0211\3\2\2\2D\u0213\3\2\2\2F\u021e\3"+
		"\2\2\2H\u0229\3\2\2\2J\u0234\3\2\2\2L\u023f\3\2\2\2N\u024a\3\2\2\2P\u0255"+
		"\3\2\2\2R\u0260\3\2\2\2T\u0274\3\2\2\2V\u0276\3\2\2\2X\u02ca\3\2\2\2Z"+
		"\u02cf\3\2\2\2\\\u02ee\3\2\2\2^_\5\4\3\2_\3\3\2\2\2`c\5\6\4\2ac\5\b\5"+
		"\2b`\3\2\2\2ba\3\2\2\2cd\3\2\2\2db\3\2\2\2de\3\2\2\2e\5\3\2\2\2fh\5\24"+
		"\13\2gi\5\16\b\2hg\3\2\2\2hi\3\2\2\2ij\3\2\2\2jk\7\64\2\2k\7\3\2\2\2l"+
		"m\5\24\13\2mn\5\32\16\2no\b\5\1\2oq\7\36\2\2pr\5\n\6\2qp\3\2\2\2qr\3\2"+
		"\2\2rs\3\2\2\2st\7\22\2\2tu\b\5\1\2uv\5$\23\2v\t\3\2\2\2w|\5\26\f\2xy"+
		"\7\33\2\2y{\5\26\f\2zx\3\2\2\2{~\3\2\2\2|z\3\2\2\2|}\3\2\2\2}\u0082\3"+
		"\2\2\2~|\3\2\2\2\177\u0080\7\33\2\2\u0080\u0081\7&\2\2\u0081\u0083\b\6"+
		"\1\2\u0082\177\3\2\2\2\u0082\u0083\3\2\2\2\u0083\13\3\2\2\2\u0084\u0089"+
		"\5\30\r\2\u0085\u0086\7\33\2\2\u0086\u0088\5\30\r\2\u0087\u0085\3\2\2"+
		"\2\u0088\u008b\3\2\2\2\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\r"+
		"\3\2\2\2\u008b\u0089\3\2\2\2\u008c\u0091\5\20\t\2\u008d\u008e\7\33\2\2"+
		"\u008e\u0090\5\20\t\2\u008f\u008d\3\2\2\2\u0090\u0093\3\2\2\2\u0091\u008f"+
		"\3\2\2\2\u0091\u0092\3\2\2\2\u0092\17\3\2\2\2\u0093\u0091\3\2\2\2\u0094"+
		"\u0095\5\30\r\2\u0095\u009a\b\t\1\2\u0096\u0097\7\23\2\2\u0097\u0098\5"+
		"\22\n\2\u0098\u0099\b\t\1\2\u0099\u009b\3\2\2\2\u009a\u0096\3\2\2\2\u009a"+
		"\u009b\3\2\2\2\u009b\21\3\2\2\2\u009c\u009d\5.\30\2\u009d\u009e\b\n\1"+
		"\2\u009e\u00ae\3\2\2\2\u009f\u00a0\7%\2\2\u00a0\u00a1\5\22\n\2\u00a1\u00a8"+
		"\b\n\1\2\u00a2\u00a3\7\33\2\2\u00a3\u00a4\5\22\n\2\u00a4\u00a5\b\n\1\2"+
		"\u00a5\u00a7\3\2\2\2\u00a6\u00a2\3\2\2\2\u00a7\u00aa\3\2\2\2\u00a8\u00a6"+
		"\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00ab\3\2\2\2\u00aa\u00a8\3\2\2\2\u00ab"+
		"\u00ac\7\f\2\2\u00ac\u00ae\3\2\2\2\u00ad\u009c\3\2\2\2\u00ad\u009f\3\2"+
		"\2\2\u00ae\23\3\2\2\2\u00af\u00b0\7#\2\2\u00b0\u00e8\b\13\1\2\u00b1\u00b2"+
		"\7\r\2\2\u00b2\u00e8\b\13\1\2\u00b3\u00b4\7!\2\2\u00b4\u00e8\b\13\1\2"+
		"\u00b5\u00b6\7\66\2\2\u00b6\u00e8\b\13\1\2\u00b7\u00b8\7+\2\2\u00b8\u00bb"+
		"\b\13\1\2\u00b9\u00ba\7@\2\2\u00ba\u00bc\b\13\1\2\u00bb\u00b9\3\2\2\2"+
		"\u00bb\u00bc\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00be\b\13\1\2\u00be\u00c5"+
		"\7%\2\2\u00bf\u00c1\5\24\13\2\u00c0\u00c2\5\f\7\2\u00c1\u00c0\3\2\2\2"+
		"\u00c1\u00c2\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c4\7\64\2\2\u00c4\u00c6"+
		"\3\2\2\2\u00c5\u00bf\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7"+
		"\u00c8\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9\u00ca\7\f\2\2\u00ca\u00cb\b\13"+
		"\1\2\u00cb\u00e8\3\2\2\2\u00cc\u00cd\7\21\2\2\u00cd\u00d0\b\13\1\2\u00ce"+
		"\u00cf\7@\2\2\u00cf\u00d1\b\13\1\2\u00d0\u00ce\3\2\2\2\u00d0\u00d1\3\2"+
		"\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d3\b\13\1\2\u00d3\u00da\7%\2\2\u00d4"+
		"\u00d6\5\24\13\2\u00d5\u00d7\5\f\7\2\u00d6\u00d5\3\2\2\2\u00d6\u00d7\3"+
		"\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00d9\7\64\2\2\u00d9\u00db\3\2\2\2\u00da"+
		"\u00d4\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00da\3\2\2\2\u00dc\u00dd\3\2"+
		"\2\2\u00dd\u00de\3\2\2\2\u00de\u00df\7\f\2\2\u00df\u00e0\b\13\1\2\u00e0"+
		"\u00e8\3\2\2\2\u00e1\u00e2\7+\2\2\u00e2\u00e3\7@\2\2\u00e3\u00e8\b\13"+
		"\1\2\u00e4\u00e5\7\21\2\2\u00e5\u00e6\7@\2\2\u00e6\u00e8\b\13\1\2\u00e7"+
		"\u00af\3\2\2\2\u00e7\u00b1\3\2\2\2\u00e7\u00b3\3\2\2\2\u00e7\u00b5\3\2"+
		"\2\2\u00e7\u00b7\3\2\2\2\u00e7\u00cc\3\2\2\2\u00e7\u00e1\3\2\2\2\u00e7"+
		"\u00e4\3\2\2\2\u00e8\25\3\2\2\2\u00e9\u00ea\5\24\13\2\u00ea\u00eb\5\30"+
		"\r\2\u00eb\u00ec\b\f\1\2\u00ec\27\3\2\2\2\u00ed\u00f6\5\32\16\2\u00ee"+
		"\u00ef\7\4\2\2\u00ef\u00f0\5> \2\u00f0\u00f1\7\31\2\2\u00f1\u00f2\b\r"+
		"\1\2\u00f2\u00f7\3\2\2\2\u00f3\u00f4\7\4\2\2\u00f4\u00f5\7\31\2\2\u00f5"+
		"\u00f7\b\r\1\2\u00f6\u00ee\3\2\2\2\u00f6\u00f3\3\2\2\2\u00f7\u00ff\3\2"+
		"\2\2\u00f8\u00f9\7\4\2\2\u00f9\u00fa\5> \2\u00fa\u00fb\7\31\2\2\u00fb"+
		"\u00fc\b\r\1\2\u00fc\u00fe\3\2\2\2\u00fd\u00f8\3\2\2\2\u00fe\u0101\3\2"+
		"\2\2\u00ff\u00fd\3\2\2\2\u00ff\u0100\3\2\2\2\u0100\u0102\3\2\2\2\u0101"+
		"\u00ff\3\2\2\2\u0102\u0103\b\r\1\2\u0103\u0111\3\2\2\2\u0104\u0105\5\32"+
		"\16\2\u0105\u0106\b\r\1\2\u0106\u0108\7\36\2\2\u0107\u0109\5\n\6\2\u0108"+
		"\u0107\3\2\2\2\u0108\u0109\3\2\2\2\u0109\u010a\3\2\2\2\u010a\u010b\7\22"+
		"\2\2\u010b\u010c\b\r\1\2\u010c\u0111\3\2\2\2\u010d\u010e\5\32\16\2\u010e"+
		"\u010f\b\r\1\2\u010f\u0111\3\2\2\2\u0110\u00ed\3\2\2\2\u0110\u0104\3\2"+
		"\2\2\u0110\u010d\3\2\2\2\u0111\31\3\2\2\2\u0112\u0113\7@\2\2\u0113\u0119"+
		"\b\16\1\2\u0114\u0115\7\5\2\2\u0115\u0116\5\32\16\2\u0116\u0117\b\16\1"+
		"\2\u0117\u0119\3\2\2\2\u0118\u0112\3\2\2\2\u0118\u0114\3\2\2\2\u0119\33"+
		"\3\2\2\2\u011a\u011b\5\36\20\2\u011b\u011c\7\64\2\2\u011c\u012c\3\2\2"+
		"\2\u011d\u011e\5\"\22\2\u011e\u011f\7\64\2\2\u011f\u012c\3\2\2\2\u0120"+
		"\u0122\5,\27\2\u0121\u0120\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0123\3\2"+
		"\2\2\u0123\u012c\7\64\2\2\u0124\u0125\b\17\1\2\u0125\u0126\5$\23\2\u0126"+
		"\u0127\b\17\1\2\u0127\u012c\3\2\2\2\u0128\u012c\5&\24\2\u0129\u012c\5"+
		"(\25\2\u012a\u012c\5*\26\2\u012b\u011a\3\2\2\2\u012b\u011d\3\2\2\2\u012b"+
		"\u0121\3\2\2\2\u012b\u0124\3\2\2\2\u012b\u0128\3\2\2\2\u012b\u0129\3\2"+
		"\2\2\u012b\u012a\3\2\2\2\u012c\35\3\2\2\2\u012d\u012e\7?\2\2\u012e\u012f"+
		"\7\36\2\2\u012f\u0130\5,\27\2\u0130\u0131\7\33\2\2\u0131\u0132\5,\27\2"+
		"\u0132\u0133\7\22\2\2\u0133\u0134\b\20\1\2\u0134\37\3\2\2\2\u0135\u0136"+
		"\7\"\2\2\u0136\u0137\7\36\2\2\u0137\u0138\5,\27\2\u0138\u0139\7\33\2\2"+
		"\u0139\u013a\5V,\2\u013a\u013b\7\22\2\2\u013b\u013c\b\21\1\2\u013c!\3"+
		"\2\2\2\u013d\u013e\7.\2\2\u013e\u013f\7\36\2\2\u013f\u0140\5,\27\2\u0140"+
		"\u0141\7\22\2\2\u0141\u0142\b\22\1\2\u0142#\3\2\2\2\u0143\u0147\7%\2\2"+
		"\u0144\u0146\5\6\4\2\u0145\u0144\3\2\2\2\u0146\u0149\3\2\2\2\u0147\u0145"+
		"\3\2\2\2\u0147\u0148\3\2\2\2\u0148\u014d\3\2\2\2\u0149\u0147\3\2\2\2\u014a"+
		"\u014c\5\34\17\2\u014b\u014a\3\2\2\2\u014c\u014f\3\2\2\2\u014d\u014b\3"+
		"\2\2\2\u014d\u014e\3\2\2\2\u014e\u0150\3\2\2\2\u014f\u014d\3\2\2\2\u0150"+
		"\u0151\7\f\2\2\u0151%\3\2\2\2\u0152\u0153\7 \2\2\u0153\u0154\7\36\2\2"+
		"\u0154\u0155\5,\27\2\u0155\u0156\7\22\2\2\u0156\u0157\b\24\1\2\u0157\u0158"+
		"\5\34\17\2\u0158\u0159\b\24\1\2\u0159\u015a\7*\2\2\u015a\u015b\5\34\17"+
		"\2\u015b\u015c\b\24\1\2\u015c\u0166\3\2\2\2\u015d\u015e\7 \2\2\u015e\u015f"+
		"\7\36\2\2\u015f\u0160\5,\27\2\u0160\u0161\7\22\2\2\u0161\u0162\b\24\1"+
		"\2\u0162\u0163\5\34\17\2\u0163\u0164\b\24\1\2\u0164\u0166\3\2\2\2\u0165"+
		"\u0152\3\2\2\2\u0165\u015d\3\2\2\2\u0166\'\3\2\2\2\u0167\u0168\7\35\2"+
		"\2\u0168\u0169\7\36\2\2\u0169\u016a\b\25\1\2\u016a\u016b\5,\27\2\u016b"+
		"\u016c\b\25\1\2\u016c\u016d\7\22\2\2\u016d\u016e\5\34\17\2\u016e\u016f"+
		"\b\25\1\2\u016f\u0188\3\2\2\2\u0170\u0171\7\62\2\2\u0171\u0173\7\36\2"+
		"\2\u0172\u0174\5,\27\2\u0173\u0172\3\2\2\2\u0173\u0174\3\2\2\2\u0174\u0175"+
		"\3\2\2\2\u0175\u0176\7\64\2\2\u0176\u017a\b\25\1\2\u0177\u0178\5,\27\2"+
		"\u0178\u0179\b\25\1\2\u0179\u017b\3\2\2\2\u017a\u0177\3\2\2\2\u017a\u017b"+
		"\3\2\2\2\u017b\u017c\3\2\2\2\u017c\u017d\7\64\2\2\u017d\u017f\b\25\1\2"+
		"\u017e\u0180\5,\27\2\u017f\u017e\3\2\2\2\u017f\u0180\3\2\2\2\u0180\u0181"+
		"\3\2\2\2\u0181\u0182\b\25\1\2\u0182\u0183\7\22\2\2\u0183\u0184\b\25\1"+
		"\2\u0184\u0185\5\34\17\2\u0185\u0186\b\25\1\2\u0186\u0188\3\2\2\2\u0187"+
		"\u0167\3\2\2\2\u0187\u0170\3\2\2\2\u0188)\3\2\2\2\u0189\u018a\7\b\2\2"+
		"\u018a\u018b\7\64\2\2\u018b\u0198\b\26\1\2\u018c\u018d\7\'\2\2\u018d\u018e"+
		"\7\64\2\2\u018e\u0198\b\26\1\2\u018f\u0190\7\63\2\2\u0190\u0191\5,\27"+
		"\2\u0191\u0192\7\64\2\2\u0192\u0193\b\26\1\2\u0193\u0198\3\2\2\2\u0194"+
		"\u0195\7\63\2\2\u0195\u0196\7\64\2\2\u0196\u0198\b\26\1\2\u0197\u0189"+
		"\3\2\2\2\u0197\u018c\3\2\2\2\u0197\u018f\3\2\2\2\u0197\u0194\3\2\2\2\u0198"+
		"+\3\2\2\2\u0199\u019a\5.\30\2\u019a\u01a1\b\27\1\2\u019b\u019c\7\33\2"+
		"\2\u019c\u019d\5.\30\2\u019d\u019e\b\27\1\2\u019e\u01a0\3\2\2\2\u019f"+
		"\u019b\3\2\2\2\u01a0\u01a3\3\2\2\2\u01a1\u019f\3\2\2\2\u01a1\u01a2\3\2"+
		"\2\2\u01a2-\3\2\2\2\u01a3\u01a1\3\2\2\2\u01a4\u01a5\5\62\32\2\u01a5\u01a6"+
		"\b\30\1\2\u01a6\u01bf\3\2\2\2\u01a7\u01a8\5\64\33\2\u01a8\u01a9\b\30\1"+
		"\2\u01a9\u01bf\3\2\2\2\u01aa\u01ab\5\66\34\2\u01ab\u01ac\b\30\1\2\u01ac"+
		"\u01bf\3\2\2\2\u01ad\u01ae\58\35\2\u01ae\u01af\b\30\1\2\u01af\u01bf\3"+
		"\2\2\2\u01b0\u01b1\5:\36\2\u01b1\u01b2\b\30\1\2\u01b2\u01bf\3\2\2\2\u01b3"+
		"\u01b4\5<\37\2\u01b4\u01b5\b\30\1\2\u01b5\u01bf\3\2\2\2\u01b6\u01b7\5"+
		"X-\2\u01b7\u01b8\5\60\31\2\u01b8\u01b9\5.\30\2\u01b9\u01ba\b\30\1\2\u01ba"+
		"\u01bf\3\2\2\2\u01bb\u01bc\5@!\2\u01bc\u01bd\b\30\1\2\u01bd\u01bf\3\2"+
		"\2\2\u01be\u01a4\3\2\2\2\u01be\u01a7\3\2\2\2\u01be\u01aa\3\2\2\2\u01be"+
		"\u01ad\3\2\2\2\u01be\u01b0\3\2\2\2\u01be\u01b3\3\2\2\2\u01be\u01b6\3\2"+
		"\2\2\u01be\u01bb\3\2\2\2\u01bf/\3\2\2\2\u01c0\u01c1\t\2\2\2\u01c1\61\3"+
		"\2\2\2\u01c2\u01c3\5X-\2\u01c3\u01c4\7\23\2\2\u01c4\u01c5\5F$\2\u01c5"+
		"\u01c6\7\25\2\2\u01c6\u01c7\5F$\2\u01c7\u01c8\b\32\1\2\u01c8\63\3\2\2"+
		"\2\u01c9\u01ca\5X-\2\u01ca\u01cb\7\23\2\2\u01cb\u01cc\5H%\2\u01cc\u01cd"+
		"\7/\2\2\u01cd\u01ce\5H%\2\u01ce\u01cf\b\33\1\2\u01cf\65\3\2\2\2\u01d0"+
		"\u01d1\5X-\2\u01d1\u01d2\7\23\2\2\u01d2\u01d3\5J&\2\u01d3\u01d4\7\3\2"+
		"\2\u01d4\u01d5\5J&\2\u01d5\u01d6\b\34\1\2\u01d6\67\3\2\2\2\u01d7\u01d8"+
		"\5X-\2\u01d8\u01d9\7\23\2\2\u01d9\u01da\5P)\2\u01da\u01db\t\3\2\2\u01db"+
		"\u01dc\5P)\2\u01dc\u01dd\b\35\1\2\u01dd9\3\2\2\2\u01de\u01df\5X-\2\u01df"+
		"\u01e0\7\23\2\2\u01e0\u01e1\5R*\2\u01e1\u01e2\t\4\2\2\u01e2\u01e3\5R*"+
		"\2\u01e3\u01e4\b\36\1\2\u01e4;\3\2\2\2\u01e5\u01e6\5X-\2\u01e6\u01e7\7"+
		"\23\2\2\u01e7\u01e8\5T+\2\u01e8\u01e9\t\5\2\2\u01e9\u01ea\5T+\2\u01ea"+
		"\u01eb\b\37\1\2\u01eb=\3\2\2\2\u01ec\u01ed\5@!\2\u01ed\u01ee\b \1\2\u01ee"+
		"?\3\2\2\2\u01ef\u01f0\5B\"\2\u01f0\u01f6\b!\1\2\u01f1\u01f2\7\67\2\2\u01f2"+
		"\u01f3\b!\1\2\u01f3\u01f4\5B\"\2\u01f4\u01f5\b!\1\2\u01f5\u01f7\3\2\2"+
		"\2\u01f6\u01f1\3\2\2\2\u01f7\u01f8\3\2\2\2\u01f8\u01f6\3\2\2\2\u01f8\u01f9"+
		"\3\2\2\2\u01f9\u01fa\3\2\2\2\u01fa\u01fb\b!\1\2\u01fb\u0200\3\2\2\2\u01fc"+
		"\u01fd\5B\"\2\u01fd\u01fe\b!\1\2\u01fe\u0200\3\2\2\2\u01ff\u01ef\3\2\2"+
		"\2\u01ff\u01fc\3\2\2\2\u0200A\3\2\2\2\u0201\u0202\5D#\2\u0202\u0208\b"+
		"\"\1\2\u0203\u0204\7\65\2\2\u0204\u0205\b\"\1\2\u0205\u0206\5D#\2\u0206"+
		"\u0207\b\"\1\2\u0207\u0209\3\2\2\2\u0208\u0203\3\2\2\2\u0209\u020a\3\2"+
		"\2\2\u020a\u0208\3\2\2\2\u020a\u020b\3\2\2\2\u020b\u020c\3\2\2\2\u020c"+
		"\u020d\b\"\1\2\u020d\u0212\3\2\2\2\u020e\u020f\5D#\2\u020f\u0210\b\"\1"+
		"\2\u0210\u0212\3\2\2\2\u0211\u0201\3\2\2\2\u0211\u020e\3\2\2\2\u0212C"+
		"\3\2\2\2\u0213\u0214\5F$\2\u0214\u021b\b#\1\2\u0215\u0216\7\25\2\2\u0216"+
		"\u0217\5F$\2\u0217\u0218\b#\1\2\u0218\u021a\3\2\2\2\u0219\u0215\3\2\2"+
		"\2\u021a\u021d\3\2\2\2\u021b\u0219\3\2\2\2\u021b\u021c\3\2\2\2\u021cE"+
		"\3\2\2\2\u021d\u021b\3\2\2\2\u021e\u021f\5H%\2\u021f\u0226\b$\1\2\u0220"+
		"\u0221\7/\2\2\u0221\u0222\5H%\2\u0222\u0223\b$\1\2\u0223\u0225\3\2\2\2"+
		"\u0224\u0220\3\2\2\2\u0225\u0228\3\2\2\2\u0226\u0224\3\2\2\2\u0226\u0227"+
		"\3\2\2\2\u0227G\3\2\2\2\u0228\u0226\3\2\2\2\u0229\u022a\5J&\2\u022a\u0231"+
		"\b%\1\2\u022b\u022c\7\3\2\2\u022c\u022d\5J&\2\u022d\u022e\b%\1\2\u022e"+
		"\u0230\3\2\2\2\u022f\u022b\3\2\2\2\u0230\u0233\3\2\2\2\u0231\u022f\3\2"+
		"\2\2\u0231\u0232\3\2\2\2\u0232I\3\2\2\2\u0233\u0231\3\2\2\2\u0234\u0235"+
		"\5L\'\2\u0235\u023c\b&\1\2\u0236\u0237\t\6\2\2\u0237\u0238\5L\'\2\u0238"+
		"\u0239\b&\1\2\u0239\u023b\3\2\2\2\u023a\u0236\3\2\2\2\u023b\u023e\3\2"+
		"\2\2\u023c\u023a\3\2\2\2\u023c\u023d\3\2\2\2\u023dK\3\2\2\2\u023e\u023c"+
		"\3\2\2\2\u023f\u0240\5N(\2\u0240\u0247\b\'\1\2\u0241\u0242\t\7\2\2\u0242"+
		"\u0243\5N(\2\u0243\u0244\b\'\1\2\u0244\u0246\3\2\2\2\u0245\u0241\3\2\2"+
		"\2\u0246\u0249\3\2\2\2\u0247\u0245\3\2\2\2\u0247\u0248\3\2\2\2\u0248M"+
		"\3\2\2\2\u0249\u0247\3\2\2\2\u024a\u024b\5P)\2\u024b\u0252\b(\1\2\u024c"+
		"\u024d\t\3\2\2\u024d\u024e\5P)\2\u024e\u024f\b(\1\2\u024f\u0251\3\2\2"+
		"\2\u0250\u024c\3\2\2\2\u0251\u0254\3\2\2\2\u0252\u0250\3\2\2\2\u0252\u0253"+
		"\3\2\2\2\u0253O\3\2\2\2\u0254\u0252\3\2\2\2\u0255\u0256\5R*\2\u0256\u025d"+
		"\b)\1\2\u0257\u0258\t\4\2\2\u0258\u0259\5R*\2\u0259\u025a\b)\1\2\u025a"+
		"\u025c\3\2\2\2\u025b\u0257\3\2\2\2\u025c\u025f\3\2\2\2\u025d\u025b\3\2"+
		"\2\2\u025d\u025e\3\2\2\2\u025eQ\3\2\2\2\u025f\u025d\3\2\2\2\u0260\u0261"+
		"\5T+\2\u0261\u0268\b*\1\2\u0262\u0263\t\5\2\2\u0263\u0264\5T+\2\u0264"+
		"\u0265\b*\1\2\u0265\u0267\3\2\2\2\u0266\u0262\3\2\2\2\u0267\u026a\3\2"+
		"\2\2\u0268\u0266\3\2\2\2\u0268\u0269\3\2\2\2\u0269S\3\2\2\2\u026a\u0268"+
		"\3\2\2\2\u026b\u026c\7\36\2\2\u026c\u026d\5V,\2\u026d\u026e\7\22\2\2\u026e"+
		"\u026f\5T+\2\u026f\u0270\b+\1\2\u0270\u0275\3\2\2\2\u0271\u0272\5X-\2"+
		"\u0272\u0273\b+\1\2\u0273\u0275\3\2\2\2\u0274\u026b\3\2\2\2\u0274\u0271"+
		"\3\2\2\2\u0275U\3\2\2\2\u0276\u0277\5\24\13\2\u0277\u027c\b,\1\2\u0278"+
		"\u0279\7\5\2\2\u0279\u027b\b,\1\2\u027a\u0278\3\2\2\2\u027b\u027e\3\2"+
		"\2\2\u027c\u027a\3\2\2\2\u027c\u027d\3\2\2\2\u027dW\3\2\2\2\u027e\u027c"+
		"\3\2\2\2\u027f\u0280\7\27\2\2\u0280\u0281\7\36\2\2\u0281\u0282\5V,\2\u0282"+
		"\u0283\7\22\2\2\u0283\u0284\b-\1\2\u0284\u02cb\3\2\2\2\u0285\u0286\7,"+
		"\2\2\u0286\u0287\5X-\2\u0287\u0288\b-\1\2\u0288\u02cb\3\2\2\2\u0289\u028a"+
		"\7\7\2\2\u028a\u028b\5X-\2\u028b\u028c\b-\1\2\u028c\u02cb\3\2\2\2\u028d"+
		"\u028e\7\27\2\2\u028e\u028f\5X-\2\u028f\u0290\b-\1\2\u0290\u02cb\3\2\2"+
		"\2\u0291\u0292\7\3\2\2\u0292\u0293\5T+\2\u0293\u0294\b-\1\2\u0294\u02cb"+
		"\3\2\2\2\u0295\u0296\7\5\2\2\u0296\u0297\5T+\2\u0297\u0298\b-\1\2\u0298"+
		"\u02cb\3\2\2\2\u0299\u029a\7\61\2\2\u029a\u029b\5T+\2\u029b\u029c\b-\1"+
		"\2\u029c\u02cb\3\2\2\2\u029d\u029e\7\34\2\2\u029e\u029f\5T+\2\u029f\u02a0"+
		"\b-\1\2\u02a0\u02cb\3\2\2\2\u02a1\u02a2\7=\2\2\u02a2\u02a3\5T+\2\u02a3"+
		"\u02a4\b-\1\2\u02a4\u02cb\3\2\2\2\u02a5\u02a6\7\26\2\2\u02a6\u02a7\5T"+
		"+\2\u02a7\u02a8\b-\1\2\u02a8\u02cb\3\2\2\2\u02a9\u02aa\5\\/\2\u02aa\u02c7"+
		"\b-\1\2\u02ab\u02ac\7\4\2\2\u02ac\u02ad\5,\27\2\u02ad\u02ae\7\31\2\2\u02ae"+
		"\u02af\b-\1\2\u02af\u02b1\3\2\2\2\u02b0\u02ab\3\2\2\2\u02b1\u02b2\3\2"+
		"\2\2\u02b2\u02b0\3\2\2\2\u02b2\u02b3\3\2\2\2\u02b3\u02b4\3\2\2\2\u02b4"+
		"\u02b5\b-\1\2\u02b5\u02c6\3\2\2\2\u02b6\u02b7\7\36\2\2\u02b7\u02b8\5Z"+
		".\2\u02b8\u02b9\7\22\2\2\u02b9\u02ba\b-\1\2\u02ba\u02c6\3\2\2\2\u02bb"+
		"\u02bc\7\60\2\2\u02bc\u02bd\7@\2\2\u02bd\u02c6\b-\1\2\u02be\u02bf\7\17"+
		"\2\2\u02bf\u02c0\7@\2\2\u02c0\u02c6\b-\1\2\u02c1\u02c2\7,\2\2\u02c2\u02c6"+
		"\b-\1\2\u02c3\u02c4\7\7\2\2\u02c4\u02c6\b-\1\2\u02c5\u02b0\3\2\2\2\u02c5"+
		"\u02b6\3\2\2\2\u02c5\u02bb\3\2\2\2\u02c5\u02be\3\2\2\2\u02c5\u02c1\3\2"+
		"\2\2\u02c5\u02c3\3\2\2\2\u02c6\u02c9\3\2\2\2\u02c7\u02c5\3\2\2\2\u02c7"+
		"\u02c8\3\2\2\2\u02c8\u02cb\3\2\2\2\u02c9\u02c7\3\2\2\2\u02ca\u027f\3\2"+
		"\2\2\u02ca\u0285\3\2\2\2\u02ca\u0289\3\2\2\2\u02ca\u028d\3\2\2\2\u02ca"+
		"\u0291\3\2\2\2\u02ca\u0295\3\2\2\2\u02ca\u0299\3\2\2\2\u02ca\u029d\3\2"+
		"\2\2\u02ca\u02a1\3\2\2\2\u02ca\u02a5\3\2\2\2\u02ca\u02a9\3\2\2\2\u02cb"+
		"Y\3\2\2\2\u02cc\u02cd\5.\30\2\u02cd\u02ce\b.\1\2\u02ce\u02d0\3\2\2\2\u02cf"+
		"\u02cc\3\2\2\2\u02cf\u02d0\3\2\2\2\u02d0\u02d7\3\2\2\2\u02d1\u02d2\7\33"+
		"\2\2\u02d2\u02d3\5.\30\2\u02d3\u02d4\b.\1\2\u02d4\u02d6\3\2\2\2\u02d5"+
		"\u02d1\3\2\2\2\u02d6\u02d9\3\2\2\2\u02d7\u02d5\3\2\2\2\u02d7\u02d8\3\2"+
		"\2\2\u02d8[\3\2\2\2\u02d9\u02d7\3\2\2\2\u02da\u02db\5 \21\2\u02db\u02dc"+
		"\b/\1\2\u02dc\u02ef\3\2\2\2\u02dd\u02de\7@\2\2\u02de\u02ef\b/\1\2\u02df"+
		"\u02e0\7B\2\2\u02e0\u02ef\b/\1\2\u02e1\u02e2\7A\2\2\u02e2\u02ef\b/\1\2"+
		"\u02e3\u02e4\7C\2\2\u02e4\u02ef\b/\1\2\u02e5\u02e6\7E\2\2\u02e6\u02ef"+
		"\b/\1\2\u02e7\u02e8\7D\2\2\u02e8\u02ef\b/\1\2\u02e9\u02ea\7\36\2\2\u02ea"+
		"\u02eb\5,\27\2\u02eb\u02ec\7\22\2\2\u02ec\u02ed\b/\1\2\u02ed\u02ef\3\2"+
		"\2\2\u02ee\u02da\3\2\2\2\u02ee\u02dd\3\2\2\2\u02ee\u02df\3\2\2\2\u02ee"+
		"\u02e1\3\2\2\2\u02ee\u02e3\3\2\2\2\u02ee\u02e5\3\2\2\2\u02ee\u02e7\3\2"+
		"\2\2\u02ee\u02e9\3\2\2\2\u02ef]\3\2\2\2:bdhq|\u0082\u0089\u0091\u009a"+
		"\u00a8\u00ad\u00bb\u00c1\u00c7\u00d0\u00d6\u00dc\u00e7\u00f6\u00ff\u0108"+
		"\u0110\u0118\u0121\u012b\u0147\u014d\u0165\u0173\u017a\u017f\u0187\u0197"+
		"\u01a1\u01be\u01f8\u01ff\u020a\u0211\u021b\u0226\u0231\u023c\u0247\u0252"+
		"\u025d\u0268\u0274\u027c\u02b2\u02c5\u02c7\u02ca\u02cf\u02d7\u02ee";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}