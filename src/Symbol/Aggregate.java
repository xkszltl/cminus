import java.io.*;
import java.util.*;
import java.math.*;

public interface Aggregate
{
    public void define();
    public boolean defined();
    public void set(String id, Symbol symbol) throws CompileException;
    public ObjectSymbol get(String id) throws CompileException;
    public Symbol getType(String id) throws CompileException;
    public List<String> getMembersList();
}