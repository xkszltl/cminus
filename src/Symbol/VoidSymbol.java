import java.io.*;
import java.util.*;
import java.math.*;

public class VoidSymbol extends Symbol
{
    public static VoidSymbol instance = null;

    public VoidSymbol()
    {
        sizeof = 1;
    }

    public String typeInfo()
    {
        return "void";
    }

    public boolean equals(Symbol symbol)
    {
        return (symbol instanceof VoidSymbol);
    }
}