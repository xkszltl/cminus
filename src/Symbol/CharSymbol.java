import java.io.*;
import java.util.*;
import java.math.*;

public class CharSymbol extends Symbol implements Scalar
{
    public static CharSymbol instance = null;

    CharSymbol()
    {
        sizeof = 1;
    }

    public String typeInfo()
    {
        return "char";
    }

    public boolean equals(Symbol symbol) throws CompileException
    {
        return (symbol instanceof CharSymbol);
    }
}