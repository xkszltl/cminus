import java.io.*;
import java.util.*;
import java.math.*;

public class Env
{
    private Map<String, List<Symbol>> types = new HashMap<String, List<Symbol>>();
    private Map<String, List<ObjectSymbol>> objs = new HashMap<String, List<ObjectSymbol>>();
    private Map<String, Integer> deltaTypes = new HashMap<String, Integer>();
    private Map<String, Integer> deltaObjs = new HashMap<String, Integer>();
    private static List<JumpSymbol> jump = new ArrayList<JumpSymbol>();
    private static long jumpCount = 0;

    private Env()
    {
    }

    public Env(Env prev)
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+toString()+" from "+prev.toString()+Debug.STYLE_DEFAULT);
        types = prev.types;
        objs = prev.objs;
    }

    public Env clone()
    {
        Env env = new Env();
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Clone to "+env.toString()+" from "+toString()+Debug.STYLE_DEFAULT);
        env.types = (Map<String, List<Symbol>>)(((HashMap<String, List<Symbol>>)types).clone());
        env.objs = (Map<String, List<ObjectSymbol>>)(((HashMap<String, List<ObjectSymbol>>)objs).clone());
        return env;
    }

    private Env init()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Initializing... "+toString()+Debug.STYLE_DEFAULT);
        try
        {
            setType("void", VoidSymbol.instance = new VoidSymbol());
            setType("char", CharSymbol.instance = new CharSymbol());
            setType("int", IntSymbol.instance = new IntSymbol());
            FuncSymbol func = null;
            ObjectSymbol ret = null;
            String comment;

            comment = "built-in function 'void* malloc(int)'";
            func = new FuncSymbol(new PointerSymbol(VoidSymbol.instance), getCallJump());
            func.set("size", IntSymbol.instance);
            setObj("malloc", new ObjectSymbol(true, func));
            func.frame = ObjectSymbol.pushFrame(func);
            func.setEnv(this, comment);
            ret = new ObjectSymbol(true, IntSymbol.instance);
            IRGenerator.syscall("NEW", ret, func.env.getObj("size"), comment);
            func.env.jumpToReturn(ret, comment);
            func.complete(comment);

            comment = "built-in function 'int getchar()'";
            func = new FuncSymbol(IntSymbol.instance, getCallJump());
            setObj("getchar", new ObjectSymbol(true, func));
            func.frame = ObjectSymbol.pushFrame(func);
            func.setEnv(this, comment);
            ret = new ObjectSymbol(true, CharSymbol.instance);
            IRGenerator.syscall("READ", ret, comment);
            func.env.jumpToReturn(ret, comment);
            func.complete(comment);

            comment = "built-in function 'int putchar(int)'";
            func = new FuncSymbol(IntSymbol.instance, getCallJump());
            func.set("character", IntSymbol.instance);
            setObj("putchar", new ObjectSymbol(true, func));
            func.frame = ObjectSymbol.pushFrame(func);
            func.setEnv(this, comment);
            IRGenerator.syscall("WRITECHAR", func.env.getObj("character"), comment);
            func.env.jumpToReturn(func.env.getObj("character"), comment);
            func.complete(comment);

            comment = "built-in function 'char* gets(char*)'";
            func = new FuncSymbol(new PointerSymbol(CharSymbol.instance), getCallJump());
            func.set("str", new PointerSymbol(CharSymbol.instance));
            setObj("gets", new ObjectSymbol(true, func));
            func.frame = ObjectSymbol.pushFrame(func);
            func.setEnv(this, comment);
            IRGenerator.syscall("READSTR", func.env.getObj("str"), comment);
            func.env.jumpToReturn(ret, comment);
            func.complete(comment);

            comment = "built-in function 'int puts(char*)'";
            func = new FuncSymbol(IntSymbol.instance, getCallJump());
            func.set("str", new PointerSymbol(CharSymbol.instance));
            setObj("puts", new ObjectSymbol(true, func));
            func.frame = ObjectSymbol.pushFrame(func);
            func.setEnv(this, comment);
            IRGenerator.syscall("WRITESTR", func.env.getObj("str"), comment);
            ret = new ConstStringSymbol("\n");
            IRGenerator.syscall("WRITESTR", ret, comment);
            func.env.jumpToReturn(new ConstIntSymbol(0), comment);
            func.complete(comment);

            comment = "built-in function 'int __getint()'";
            func = new FuncSymbol(IntSymbol.instance, getCallJump());
            setObj("__getint", new ObjectSymbol(true, func));
            func.frame = ObjectSymbol.pushFrame(func);
            func.setEnv(this, comment);
            ret = new ObjectSymbol(true, IntSymbol.instance, true);
            IRGenerator.syscall("READ", ret, comment);
            func.env.jumpToReturn(ret, comment);
            func.complete(comment);

            comment = "built-in function 'void __putint(int)'";
            func = new FuncSymbol(VoidSymbol.instance, getCallJump());
            func.set("integer", IntSymbol.instance);
            setObj("__putint", new ObjectSymbol(true, func));
            func.frame = ObjectSymbol.pushFrame(func);
            func.setEnv(this, comment);
            IRGenerator.syscall("WRITE", func.env.getObj("integer"), comment);
            func.complete(comment);

            comment = "built-in function 'void __putstring(char*)'";
            func = new FuncSymbol(VoidSymbol.instance, getCallJump());
            func.set("str", new PointerSymbol(CharSymbol.instance));
            setObj("__putstring", new ObjectSymbol(true, func));
            func.frame = ObjectSymbol.pushFrame(func);
            func.setEnv(this, comment);
            IRGenerator.syscall("WRITESTR", func.env.getObj("str"), comment);
            func.complete(comment);
        }
        catch (CompileException e)
        {
        }
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] "+toString()+" initialized"+Debug.STYLE_DEFAULT);
        return this;
    }

    public void setType(String id, Symbol symbol) throws CompileException
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Set "+id+"("+symbol.toString()+" -> "+symbol.typeInfo()+") in "+toString()+Debug.STYLE_DEFAULT);
        if (types.get(id) == null)
            types.put(id, new ArrayList());
        List<Symbol> list = types.get(id);
        if (deltaTypes.get(id) != null)
            if (list.get(list.size()-1).equals(symbol))
                throw new CompileException("redefinition of '"+id+"'");
            else
                throw new CompileException("redeclaration of '"+id+"'");
        list.add(symbol);
        deltaTypes.put(id, (deltaTypes.get(id) == null ? 1 : deltaTypes.get(id)+1));
    }

    public void setObj(String id, ObjectSymbol symbol) throws CompileException
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Set "+id+"("+symbol.toString()+" -> "+symbol.typeInfo()+") in "+toString()+Debug.STYLE_DEFAULT);
        if (objs.get(id) == null)
            objs.put(id, new ArrayList());
        List<ObjectSymbol> list = objs.get(id);
        if (deltaObjs.get(id) != null)
            if (list.get(list.size()-1).type.equals(symbol.type))
                throw new CompileException("redefinition of '"+id+"'");
            else
                throw new CompileException("redeclaration of '"+id+"'");
        list.add(symbol);
        deltaObjs.put(id, (deltaObjs.get(id) == null ? 1 : deltaObjs.get(id)+1));
    }

    public Symbol getType(String id) throws CompileException
    {
        List<Symbol> list = types.get(id);
        if (list == null || list.size() <= 0)
            return null;
        return list.get(list.size()-1);
    }

    public ObjectSymbol getObj(String id) throws CompileException
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Get "+id+" from "+toString()+Debug.STYLE_DEFAULT);
        List<ObjectSymbol> list = objs.get(id);
        if (list == null || list.size() <= 0)
            throw new CompileException("'"+id+"' undeclared (first use in this function)");
        return list.get(list.size()-1);
    }

    public boolean containType(String id)
    {
        if (types.get(id) == null || types.get(id).size() <= 0)
            return false;
        return true;
    }

    public boolean containObj(String id)
    {
        if (objs.get(id) == null || objs.get(id).size() <= 0)
            return false;
        return true;
    }

    public void clear()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Clear "+toString()+Debug.STYLE_DEFAULT);
        for (String id : deltaTypes.keySet())
        {
            List<Symbol> list = types.get(id);
            for (int i = 0; i < deltaTypes.get(id); ++i)
            {
                if (Debug.ENABLE)
                    System.err.println(Debug.STYLE+"["+new Date().toString()+"] Removing "+id+"("+list.get(list.size()-1).toString()+") from "+toString()+Debug.STYLE_DEFAULT);
                list.remove(list.size()-1);
            }
        }
        for (String id : deltaObjs.keySet())
        {
            List<ObjectSymbol> list = objs.get(id);
            for (int i = 0; i < deltaObjs.get(id); ++i)
            {
                if (Debug.ENABLE)
                    System.err.println(Debug.STYLE+"["+new Date().toString()+"] Removing "+id+"("+list.get(list.size()-1).toString()+") from "+toString()+Debug.STYLE_DEFAULT);
                list.remove(list.size()-1);
            }
        }
    }

    public JumpSymbol getCallJump()
    {
        ++jumpCount;
        return addJump(new CallJumpSymbol(((Long)jumpCount).toString()));
    }

    public JumpSymbol getGotoJump()
    {
        ++jumpCount;
        return addJump(new GotoJumpSymbol(((Long)jumpCount).toString()));
    }

    public JumpSymbol getBranchJump()
    {
        ++jumpCount;
        return addJump(new BranchJumpSymbol(((Long)jumpCount).toString()));
    }

    public JumpSymbol getContinueJump()
    {
        ++jumpCount;
        return addJump(new ContinueJumpSymbol(((Long)jumpCount).toString()));
    }

    public JumpSymbol getBreakJump()
    {
        ++jumpCount;
        return addJump(new BreakJumpSymbol(((Long)jumpCount).toString()));
    }

    public JumpSymbol getReturnJump(FuncSymbol func)
    {
        ++jumpCount;
        return addJump(new ReturnJumpSymbol(((Long)jumpCount).toString(), func));
    }

    public JumpSymbol addJump(JumpSymbol symbol)
    {
        jump.add(symbol);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Add "+symbol.typeInfo()+Debug.STYLE_DEFAULT);
        return symbol;
    }

    public JumpSymbol removeBreakJump() throws CompileException
    {
        if (jump == null)
            throw new CompileException("jump table is uninitialized");
        if (jump.size() == 0)
            throw new CompileException("jump table is empty");
        int index;
        for (index = jump.size()-1; index >= 0; --index)
            if (jump.get(index) instanceof BreakJumpSymbol)
                break;
        if (index < 0)
            throw new CompileException("cannot find BreakJumpSymbol to remove");
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Remove "+jump.get(index).typeInfo()+Debug.STYLE_DEFAULT);
        return jump.remove(index);
    }

    public JumpSymbol removeContinueJump() throws CompileException
    {
        if (jump == null)
            throw new CompileException("jump table is uninitialized");
        if (jump.size() == 0)
            throw new CompileException("jump table is empty");
        int index;
        for (index = jump.size()-1; index >= 0; --index)
            if (jump.get(index) instanceof ContinueJumpSymbol)
                break;
        if (index < 0)
            throw new CompileException("cannot find ContinueJumpSymbol to remove");
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Remove "+jump.get(index).typeInfo()+Debug.STYLE_DEFAULT);
        return jump.remove(index);
    }

    public JumpSymbol removeReturnJump() throws CompileException
    {
        if (jump == null)
            throw new CompileException("jump table is uninitialized");
        if (jump.size() == 0)
            throw new CompileException("jump table is empty");
        int index;
        for (index = jump.size()-1; index >= 0; --index)
            if (jump.get(index) instanceof ReturnJumpSymbol)
                break;
        if (index < 0)
            throw new CompileException("cannot find ReturnJumpSymbol to remove");
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Remove "+jump.get(index).typeInfo()+Debug.STYLE_DEFAULT);
        return jump.remove(index);
    }

    public JumpSymbol jumpToBreak(String comment) throws CompileException
    {
        for (int i = jump.size()-1; i >= 0; --i)
            if (jump.get(i) instanceof BreakJumpSymbol)
            {
                IRGenerator.add(jump.get(i).id, comment);
                return jump.get(i);
            }
        throw new CompileException("break statement not within loop or switch");
    }
    
    public JumpSymbol jumpToContinue(String comment) throws CompileException
    {
        for (int i = jump.size()-1; i >= 0; --i)
            if (jump.get(i) instanceof ContinueJumpSymbol)
            {
                IRGenerator.add(jump.get(i).id, comment);
                return jump.get(i);
            }
        throw new CompileException("continue statement not within a loop");
    }
    
    public JumpSymbol jumpToReturn(String comment) throws CompileException
    {
        for (int i = jump.size()-1; i >= 0; --i)
            if (jump.get(i) instanceof ReturnJumpSymbol)
            {
                IRGenerator.add("ADD", "SP", ObjectSymbol.getFrameSize(((ReturnJumpSymbol)jump.get(i)).func.frame), comment);
                IRGenerator.add(((ReturnJumpSymbol)(jump.get(i))).func.callBack, comment);
                return jump.get(i);
            }
        throw new CompileException("return statement not within a function");
    }
    
    public JumpSymbol jumpToReturn(ObjectSymbol symbol, String comment) throws CompileException
    {
        for (int i = jump.size()-1; i >= 0; --i)
            if (jump.get(i) instanceof ReturnJumpSymbol)
            {
                if (! (((ReturnJumpSymbol)(jump.get(i))).func.type instanceof VoidSymbol))
                    ObjectSymbol.assign(((ReturnJumpSymbol)(jump.get(i))).func.ret, symbol, comment);
                IRGenerator.add("ADD", "SP", ObjectSymbol.getFrameSize(((ReturnJumpSymbol)jump.get(i)).func.frame), comment);
                IRGenerator.add(((ReturnJumpSymbol)(jump.get(i))).func.callBack, comment);
                return jump.get(i);
            }
        throw new CompileException("return statement not within a function");
    }

    public static Env base = (new Env()).init();
}