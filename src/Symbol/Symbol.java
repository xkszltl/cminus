import java.io.*;
import java.util.*;
import java.math.*;

public class Symbol
{
    public static long idCount = 0;

    public long id;
    public int sizeof = 0;
    
    public Symbol()
    {
        if (Debug.ENABLE)
            if (! (this instanceof ObjectSymbol) && ! (this instanceof JumpSymbol))
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+toString()+Debug.STYLE_DEFAULT);
        id = ++idCount;
    }

    public String typeInfo()
    {
        return "incomplete symbol";
    }

    public boolean equals(Symbol symbol) throws CompileException
    {
        if ((this instanceof ObjectSymbol) || (symbol instanceof ObjectSymbol))
            throw new CompileException("invaild equivalence comparison between '"+toString()+"' and '"+symbol.toString()+"'"+Debug.STYLE_DEFAULT);
        return false;
    }
}