import java.io.*;
import java.util.*;
import java.math.*;

public class StructSymbol extends Symbol implements Aggregate
{
    public Map<String, Symbol> types = new HashMap<String, Symbol>();
    public Map<String, Symbol> objs = new HashMap<String, Symbol>();
    public List<String> members = null;

    public StructSymbol()
    {
    }

    public String typeInfo()
    {
        return "struct";
    }

    public void define()
    {
        members = new ArrayList<String>();
    }

    public boolean defined()
    {
        return members != null;
    }

    public void set(String id, Symbol symbol) throws CompileException
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Set "+id+"("+symbol.toString()+" -> "+symbol.typeInfo()+") in "+toString()+Debug.STYLE_DEFAULT);
        if (objs.get(id) != null)
            if (objs.get(id).equals(symbol))
                throw new CompileException("redefinition of '"+id+"'");
            else
                throw new CompileException("redeclaration of '"+id+"'");
        if (members == null)
            throw new CompileException(toString()+" has not been defined yet");
        objs.put(id, symbol);
        members.add(id);
        sizeof += (4-sizeof % 4) % 4;
        sizeof += symbol.sizeof;
    }

    public ObjectSymbol get(String id) throws CompileException
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Get "+id+" in "+toString()+Debug.STYLE_DEFAULT);
        if (members == null)
            throw new CompileException("invalid use of undefined type '"+typeInfo()+"'");
        if (getType(id) == null)
            throw new CompileException("'"+toString()+"' has no member named '"+id+"'");
        int addr = 0;
        for (String i : members)
        {
            addr += (4-addr % 4) % 4;
            if (i.equals(id))
                return new ConstIntSymbol(addr);
            else
                addr += getType(i).sizeof;
        }
        throw new CompileException("'"+toString()+"' has no member named '"+id+"'");
    }

    public Symbol getType(String id)
    {
        return objs.get(id);
    }

    public boolean equals(Symbol symbol) throws CompileException
    {
        return id == symbol.id;
    }

    public List<String> getMembersList()
    {
        return members;
    }
}