import java.io.*;
import java.util.*;
import java.math.*;

public class ArraySymbol extends Symbol implements Scalar, Addressable
{
    public List<Integer> dim = new ArrayList<Integer>();
    public Symbol type;

    public ArraySymbol(Symbol _type)
    {
        if (_type instanceof ArraySymbol)
        {
            dim.add(-1);
            dim.addAll(((ArraySymbol)_type).dim);
            type = ((ArraySymbol)_type).type;
        }
        else
        {
            dim.add(-1);
            type = _type;
        }
        sizeof = _type.sizeof;
        for (int i = dim.size()-1; i >= 0; --i)
            sizeof = dim.get(i) >= 0 ? sizeof*dim.get(i) : Arch.pointerSizeof;
    }

    public ArraySymbol(Symbol _type, int d)
    {
        if (_type instanceof ArraySymbol)
        {
            dim.add(d);
            dim.addAll(((ArraySymbol)_type).dim);
            type = ((ArraySymbol)_type).type;
        }
        else
        {
            dim.add(d);
            type = _type;
        }
        sizeof = _type.sizeof;
        for (int i = dim.size()-1; i >= 0; --i)
            sizeof = dim.get(i) >= 0 ? sizeof*dim.get(i) : Arch.pointerSizeof;
    }

    public ArraySymbol(Symbol _type, List<Integer> _dim)
    {
        if (_type instanceof ArraySymbol)
        {
            dim.addAll(_dim);
            dim.addAll(((ArraySymbol)_type).dim);
            type = ((ArraySymbol)_type).type;
        }
        else
        {
            dim.addAll(_dim);
            type = _type;
        }
        sizeof = _type.sizeof;
        for (int i = dim.size()-1; i >= 0; --i)
            sizeof = dim.get(i) >= 0 ? sizeof*dim.get(i) : Arch.pointerSizeof;
    }

    public PointerSymbol getPointer()
    {
        return new PointerSymbol(type, dim.size());
    }

    public String typeInfo()
    {
        String info = type.typeInfo();
        for (Integer i : dim)
            if (i >= 0)
                info += "["+i.toString()+"]";
            else
                info += "[]";
        return info;
    }

    public void add(int d)
    {
        dim.add(d);
        sizeof *= d;
    }

    public void add(List<Integer> _dim)
    {
        dim.addAll(_dim);
        for (int i = dim.size()-1; i >= 0; --i)
            sizeof = dim.get(i) >= 0 ? sizeof*dim.get(i) : Arch.pointerSizeof;
    }

    public Symbol get() throws CompileException
    {
        if (dim.size() == 1)
            return type;
        else
            return new ArraySymbol(type, new ArrayList<Integer>(dim.subList(1, dim.size())));
    }

    public Symbol fullDeref()
    {
        return type;
    }

    public boolean equals(Symbol symbol) throws CompileException
    {
        if (symbol instanceof Addressable)
        if (type.equals(((Addressable)symbol).fullDeref()))
        if (dim.size() == (symbol instanceof PointerSymbol ? ((PointerSymbol)symbol).level : ((ArraySymbol)symbol).dim.size()))
            return true;
        return false;
    }
}