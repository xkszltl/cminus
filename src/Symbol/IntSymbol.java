import java.io.*;
import java.util.*;
import java.math.*;

public class IntSymbol extends Symbol implements Scalar
{
    public static IntSymbol instance = null;

    IntSymbol()
    {
        sizeof = Arch.intSizeof;
    }

    public String typeInfo()
    {
        return "int";
    }

    public boolean equals(Symbol symbol) throws CompileException
    {
        return (symbol instanceof IntSymbol);
    }
}