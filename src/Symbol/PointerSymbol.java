import java.io.*;
import java.util.*;
import java.math.*;

public class PointerSymbol extends Symbol implements Scalar, Addressable
{
    public int level;
    public Symbol type;

    public PointerSymbol(Symbol _type)
    {
        super();
        sizeof = Arch.pointerSizeof;
        if (_type instanceof PointerSymbol)
        {
            type = ((PointerSymbol)_type).type;
            level = ((PointerSymbol)_type).level+1;
        }
        else
        {
            type = _type;
            level = 1;
        }
    }

    public PointerSymbol(Symbol _type, int _level)
    {
        super();
        sizeof = Arch.pointerSizeof;
        if (_type instanceof PointerSymbol)
        {
            type = ((PointerSymbol)_type).type;
            level = ((PointerSymbol)_type).level+_level;
        }
        else
        {
            type = _type;
            level = _level;
        }
    }

    public String typeInfo()
    {
        String info = type.typeInfo()+" ";
        for (int i = 0; i < level; ++i)
            info += "*";
        return info;
    }

    public void set(int _level)
    {
        level = _level;
    }

    public Symbol get() throws CompileException
    {
        if (level > 1)
            return new PointerSymbol(type, level-1);
        else
            return type;
    }

    public Symbol fullDeref()
    {
        return type;
    }

    public boolean equals(Symbol symbol) throws CompileException
    {
        if (symbol instanceof Addressable)
        if (type.equals(((Addressable)symbol).fullDeref()))
        if (level == (symbol instanceof PointerSymbol ? ((PointerSymbol)symbol).level : ((ArraySymbol)symbol).dim.size()))
            return true;
        return false;
    }
}