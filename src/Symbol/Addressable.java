public interface Addressable
{
    public Symbol get() throws CompileException;
    public Symbol fullDeref();
}