import java.io.*;
import java.util.*;
import java.math.*;

public class FuncSymbol extends Symbol implements Addressable
{
    Env env = null;
    public Map<String, Symbol> objs = new HashMap<String, Symbol>();
    public List<String> args = new ArrayList<String>();
    public Symbol type;
    public ObjectSymbol va = null;
    public JumpSymbol label;
    public ObjectSymbol ret = null;
    public ObjectSymbol callBack = null;
    public int frame;
    public boolean inline = false;
    public String forkName = null;
    public List<Instruction> list = new ArrayList<Instruction>();

    public FuncSymbol(Symbol _type, JumpSymbol _label)
    {
        super();
        sizeof = Arch.pointerSizeof;
        type = _type;
        label = _label;
    }

    public String typeInfo()
    {
        String info = type.typeInfo()+" (";
        if (args.size() > 0)
        {
            info += objs.get(args.get(0)).typeInfo();
            for (int i = 1; i < args.size(); info += ", "+objs.get(args.get(i++)).typeInfo());
        }
        if (va != null)
            info += ", ...";
        info += ")";
        return info;
    }

    public Env setEnv(Env _env, String comment) throws CompileException
    {
        env = _env.clone();
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Set function environment "+env.toString()+" -> "+toString()+Debug.STYLE_DEFAULT);
        IRGenerator.add(label, comment);
        env.getReturnJump(this);
        if (! (type instanceof VoidSymbol))
            ret = new ObjectSymbol(false, type, true, 0, new ConstIntSymbol(-8));
        callBack = new ObjectSymbol(false, IntSymbol.instance, false, new ConstIntSymbol(-4));
        IRGenerator.add("STORE", "RA", callBack, comment);
        IRGenerator.add("SUB", "SP", ObjectSymbol.getFrameSize(frame), comment);
        for (String i : args)
            if (objs.get(i) instanceof ArraySymbol)
                env.setObj(i, new ObjectSymbol(false, objs.get(i), false, 0, new ObjectSymbol(true, IntSymbol.instance, false, new ConstIntSymbol(0))));
            else
                env.setObj(i, new ObjectSymbol(true, objs.get(i), true, new ConstIntSymbol(0)));
        if (va != null)
            env.setObj("...", va);
        inline = Optimizer.AUTO_INLINE;
        return env;
    }

    public void complete(String comment) throws CompileException
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Complete function definition "+label.id+Debug.STYLE_DEFAULT);
        for (String i : args)
            if (getType(i) instanceof ArraySymbol)
            {
                env.getObj(i).addr.addr = (new ObjectSymbol(! inline, ((ArraySymbol)getType(i)).getPointer())).addr;
                env.getObj(i).addr.restrict &= ! inline;
            }
            else
            {
                ((ConstIntSymbol)(env.getObj(i).addr)).val = ((ConstIntSymbol)((new ObjectSymbol(! inline, getType(i), true)).addr)).val;
                env.getObj(i).restrict &= ! inline;
            }
        if (va != null)
        {
            va.addr = (new ObjectSymbol(! inline, new PointerSymbol(VoidSymbol.instance))).addr;
            va.restrict &= ! inline;
        }
        if (! (type instanceof VoidSymbol))
        {
            ret.addr = new ObjectSymbol(false, IntSymbol.instance);
            ret.restrict &= ! inline;
        }
        new ObjectSymbol(false, IntSymbol.instance);
        env.jumpToReturn(comment);
        env.removeReturnJump();
        ObjectSymbol.popFrame();
        env.clear();
        if (inline)
        {
            if (Debug.ENABLE)
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Found an inline function with label "+label.id+Debug.STYLE_DEFAULT);
            String mainFork = IRGenerator.branch();
            forkName = IRGenerator.checkout();
            for (Instruction inst : list)
                IRGenerator.add(inst);
            String tmpFork = IRGenerator.checkout();
            IRGenerator.copy(forkName);
            IRGenerator.checkout(mainFork);
            IRGenerator.remove(tmpFork);
        }
    }

    public ObjectSymbol call(List<ObjectSymbol> _args, String comment) throws CompileException
    {
        if (_args.size() < args.size())
            throw new CompileException("too few arguments to function '"+typeInfo()+"'");
        if (_args.size() > args.size() && va == null)
            throw new CompileException("too many arguments to function '"+typeInfo()+"'");
        ObjectSymbol symbol = null;
        int addr = 0;
        if (type instanceof VoidSymbol)
            addr -= 4;
        else
        {
            symbol = new ObjectSymbol(! inline, type);
            IRGenerator.add("ADD", new ObjectSymbol(! inline, IntSymbol.instance, true, new ConstIntSymbol(-8)), "SP", (ConstSymbol)symbol.addr, comment);
            addr -= 8;
        }
        if (va != null)
        {
            addr -= 4;
            int vaSize = 0;
            for (int i = _args.size()-1; i >= args.size(); --i)
            {
                if (_args.get(i).type instanceof ArraySymbol)
                    vaSize += Arch.pointerSizeof;
                else
                    vaSize += _args.get(i).sizeof;
                vaSize += (4-vaSize % 4) % 4;
            }
            ObjectSymbol vaPtr = new ObjectSymbol(! inline, va, true, new ConstIntSymbol(-12));
            if (_args.size() == args.size())
                ObjectSymbol.assign(vaPtr, new ConstIntSymbol(0), comment);
            else
            {
                IRGenerator.syscall("NEW", vaPtr, new ConstIntSymbol(vaSize), comment);
                ObjectSymbol offset = vaPtr;
                if (_args.size()-args.size() > 1)
                {
                    offset = new ObjectSymbol(! inline, IntSymbol.instance, true, 0);
                    ObjectSymbol.assign(offset, vaPtr, comment);
                }
                for (int i = args.size(); i < _args.size(); ++i)
                {
                    ObjectSymbol arg = _args.get(i);
                    if (arg.type instanceof ArraySymbol)
                    {
                        arg = new ObjectSymbol(! inline, ((ArraySymbol)arg.type).getPointer(), true, 0, offset);
                        if (_args.get(i).frame == 0)
                            ObjectSymbol.assign(arg, _args.get(i).addr, comment);
                        else
                        {
                            ObjectSymbol globalAddr = new ObjectSymbol(! inline, IntSymbol.instance);
                            IRGenerator.add("ADD", globalAddr, "SP", _args.get(i).addr, comment);
                            ObjectSymbol.assign(arg, globalAddr, comment);
                        }
                    }
                    else
                    {
                        arg = new ObjectSymbol(! inline, arg, true, 0, offset);
                        ObjectSymbol.assign(arg, _args.get(i), comment);
                    }
                    if (i+1 < _args.size())
                        ObjectSymbol.assign(offset, ObjectSymbol.plus(offset, new ConstIntSymbol(arg.sizeof+(4-arg.sizeof % 4) % 4), comment), comment);
                }
            }
        }
        for (int i = args.size()-1; i >= 0; --i)
        {
            if (getType(args.get(i)) instanceof ArraySymbol)
                addr -= Arch.pointerSizeof;
            else
                addr -= getType(args.get(i)).sizeof;
            addr -= (addr % 4+4) % 4;
            try
            {
                if (getType(args.get(i)) instanceof ArraySymbol)
                {
                    if (_args.get(i).frame == 0)
                        ObjectSymbol.assign(new ObjectSymbol(! inline, ((ArraySymbol)getType(args.get(i))).getPointer(), true, new ConstIntSymbol(addr)), _args.get(i).addr, comment);
                    else
                    {
                        ObjectSymbol globalAddr = new ObjectSymbol(! inline, IntSymbol.instance);
                        IRGenerator.add("ADD", globalAddr, "SP", _args.get(i).addr, comment);
                        ObjectSymbol.assign(new ObjectSymbol(! inline, ((ArraySymbol)getType(args.get(i))).getPointer(), true, new ConstIntSymbol(addr)), globalAddr, comment);
                    }
                }
                else
                    ObjectSymbol.assign(new ObjectSymbol(! inline, getType(args.get(i)), true, new ConstIntSymbol(addr)), _args.get(i), comment);
            }
            catch (CompileException e)
            {
                throw new CompileException("cannot convert '"+_args.get(i).typeInfo()+"' to '"+objs.get(args.get(i)).typeInfo()+"' for argument '"+(i+1)+"' to '"+typeInfo()+"'. "+e.msg);
            }
        }
        if (inline)
            IRGenerator.copyInline(forkName, frame, new LabelInstruction(env.getGotoJump().id, comment), new LabelInstruction(env.getGotoJump().id, comment));
        else
            IRGenerator.add(label.id, comment);
        return symbol;
    }

    public void setVA()
    {
        va = new ObjectSymbol(true, new PointerSymbol(VoidSymbol.instance), true, new ConstIntSymbol(0));
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Set various arguments list of "+toString()+Debug.STYLE_DEFAULT);
    }

    public void set(String id, Symbol symbol) throws CompileException
    {
        if (objs.get(id) != null)
            throw new CompileException("redefinition of parameter '"+id+"'");
        objs.put(id, symbol);
        args.add(id);
        if (env != null)
            env.setObj(id, new ObjectSymbol(true, symbol, true));
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Set "+id+"("+symbol.toString()+") in "+toString()+Debug.STYLE_DEFAULT);
    }

    public int get(String id) throws CompileException
    {
        if (getType(id) == null)
            throw new CompileException("'"+toString()+"' has no member named '"+id+"'");
        int addr = 0;
        for (String i : args)
            if (i.equals(id))
                break;
            else
                addr += getType(i).sizeof;
        return addr;
    }

    public Symbol getType(String id)
    {
        return objs.get(id);
    }

    public Symbol get() throws CompileException
    {
        return this;
    }

    public Symbol fullDeref()
    {
        return this;
    }

    public boolean equals(Symbol symbol) throws CompileException
    {
        return id == symbol.id;
    }
}