import java.io.*;
import java.util.*;
import java.math.*;

public class Translator
{
    public static final boolean COMMENT_ENABLE = Panel.ASM_COMMENT;
    public static final String COMMENT_SYMBOL = "#";

    public static final String FORMAT_PREFIX = "            ";
    public static final int FORMAT_INST = 58;
    public static final int FORMAT_ADDR = 11;
    public static final int FORMAT_CMD = 7;
    public static final int FORMAT_LABEL = 11;
    public static final int FORMAT_POS = 12;
    public static final int FORMAT_COMMENT = 128;
    public static final char FORMAT_CUT = '=';
    public static final int FORMAT_DATA_LENGTH = 8;
    public static final String FORMAT_STR_COMMENT = "%-"+FORMAT_INST+"s %s";
    public static final String FORMAT_STR_LABEL = "%-"+FORMAT_LABEL+"s ";
    public static final String FORMAT_STR_0 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s";
    public static final String FORMAT_STR_1 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s %-"+FORMAT_ADDR+"s";
    public static final String FORMAT_STR_2 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s";
    public static final String FORMAT_STR_3 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s";
    public static final String FORMAT_STR_4 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s";

    private int tmpLabelCount = 0;
    private List<Instruction> list;
    private List<String> asm = new ArrayList<String>();

    private String regZero = Arch.zeroRegister.get(0);
    private String regTemp = Arch.temporaryRegister.get(0);
    private String regRA = Arch.returnAddressRegister.get(0);
    private String regSP = Arch.stackRegister.get(0);
    private String regGP = Arch.globalRegister.get(0);
    private String regSC = Arch.systemCallRegister.get(0);
    private List<String> regOp = Arch.arthimeticRegister;
    private List<String> regArg = Arch.argumentRegister;

    private Map<ObjectSymbol, Integer> alloc = null;

    public Translator(List<Instruction> _list)
    {
        super();
        list = _list;
        header();
        scan();
        optimize();
    }

    private boolean add(String comment)
    {
        if (COMMENT_ENABLE)
            return asm.add(COMMENT_SYMBOL+" "+(comment.length() <= FORMAT_INST+FORMAT_COMMENT+1 ? comment : comment.substring(0, FORMAT_INST+FORMAT_COMMENT-2)+"..."));
        return false;
    }

    private boolean add(String format, String cmd, String comment)
    {
        if (COMMENT_ENABLE)
            return asm.add(String.format(FORMAT_STR_COMMENT, String.format(format, cmd),
                (comment.equals("") ? "" : COMMENT_SYMBOL+" "+(comment.length() <= FORMAT_COMMENT ? comment : comment.substring(0, FORMAT_COMMENT-3)+"..."))));
        return asm.add(String.format(format, cmd));
    }

    private boolean add(String format, String cmd, String arg0, String comment)
    {
        if (COMMENT_ENABLE)
            return asm.add(String.format(FORMAT_STR_COMMENT, String.format(format, cmd, arg0),
                (comment.equals("") ? "" : COMMENT_SYMBOL+" "+(comment.length() <= FORMAT_COMMENT ? comment : comment.substring(0, FORMAT_COMMENT-3)+"..."))));
        return asm.add(String.format(format, cmd, arg0));
    }

    private boolean add(String format, String cmd, String arg0, String arg1, String comment)
    {
        if (COMMENT_ENABLE)
            return asm.add(String.format(FORMAT_STR_COMMENT, String.format(format, cmd, arg0, arg1),
                (comment.equals("") ? "" : COMMENT_SYMBOL+" "+(comment.length() <= FORMAT_COMMENT ? comment : comment.substring(0, FORMAT_COMMENT-3)+"..."))));
        return asm.add(String.format(format, cmd, arg0, arg1));
    }

    private boolean add(String format, String cmd, String arg0, String arg1, String arg2, String comment)
    {
        if (COMMENT_ENABLE)
            return asm.add(String.format(FORMAT_STR_COMMENT, String.format(format, cmd, arg0, arg1, arg2),
                (comment.equals("") ? "" : COMMENT_SYMBOL+" "+(comment.length() <= FORMAT_COMMENT ? comment : comment.substring(0, FORMAT_COMMENT-3)+"..."))));
        return asm.add(String.format(format, cmd, arg0, arg1, arg2));
    }

    private boolean add(String format, String cmd, String arg0, String arg1, String arg2, String arg3, String comment)
    {
        if (COMMENT_ENABLE)
            return asm.add(String.format(FORMAT_STR_COMMENT, String.format(format, cmd, arg0, arg1, arg2, arg3),
                (comment.equals("") ? "" : COMMENT_SYMBOL+" "+(comment.length() <= FORMAT_COMMENT ? comment : comment.substring(0, FORMAT_COMMENT-3)+"..."))));
        return asm.add(String.format(format, cmd, arg0, arg1, arg2, arg3));
    }

    private void add(String cmd, List<String> args, String comment)
    {
        for (int i = 0; i < args.size(); i += FORMAT_DATA_LENGTH)
        {
            String data = "";
            for (int j = i; j < i+FORMAT_DATA_LENGTH && j < args.size(); ++j)
                data += (j == i ? "" : ", ")+args.get(j);
            asm.add(String.format(FORMAT_STR_1, cmd, data));
        }
    }

    private void header()
    {
        add(FORMAT_STR_1, ".data", ((Integer)(Arch.startAddr)).toString(), "");
        List<String> data = new ArrayList<String>();
        for (ObjectSymbol.DataBlock i : ObjectSymbol.staticData)
            data.add(((Integer)(i.val)).toString());
        add(".word", data, "");
        add(FORMAT_STR_0, ".text", "");
        add(FORMAT_STR_1, ".align", "2", "");
    }

    private void scan()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_TR+"["+new Date().toString()+"] Scanning instructions"+Debug.STYLE_DEFAULT);
        for (Instruction inst : list)
        {
            alloc = inst.alloc;
            if (Debug.ENABLE)
                System.err.println(Debug.STYLE_TR+"["+new Date().toString()+"] Translating instruction "+inst.toString()+Debug.STYLE_DEFAULT);
            String comment = inst.comment;
            if (inst instanceof BinaryInstruction)
            {
                String op = ((BinaryInstruction)inst).op;
                ObjectSymbol destObj = ((BinaryInstruction)inst).dest;
                ObjectSymbol srcObj0 = ((BinaryInstruction)inst).src0;
                ObjectSymbol srcObj1 = ((BinaryInstruction)inst).src1;
                String dest = ((BinaryInstruction)inst).destReg;
                String src0 = ((BinaryInstruction)inst).srcReg0;
                String src1 = ((BinaryInstruction)inst).srcReg1;
                dest = (dest != null ? dest : regOp.get(0));
                switch (op)
                {
                case "OR":
                    src0 = (src0 != null ? src0 : fetch(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : fetch(regOp.get(1), srcObj1, comment));
                    if (src0.indexOf("$") >= 0 && src1.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "or", dest+",", src0+",", src1, comment);
                    else if (src0.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "ori", dest+",", src0+",", src1, comment);
                    else
                        add(FORMAT_STR_3, "ori", dest+",", src1+",", src0, comment);
                    break;
                case "AND":
                    src0 = (src0 != null ? src0 : fetch(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : fetch(regOp.get(1), srcObj1, comment));
                    if (src0.indexOf("$") >= 0 && src1.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "and", dest+",", src0+",", src1, comment);
                    else if (src0.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "andi", dest+",", src0+",", src1, comment);
                    else
                        add(FORMAT_STR_3, "andi", dest+",", src1+",", src0, comment);
                    break;
                case "XOR":
                    src0 = (src0 != null ? src0 : fetch(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : fetch(regOp.get(1), srcObj1, comment));
                    if (src0.indexOf("$") >= 0 && src1.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "xor", dest+",", src0+",", src1, comment);
                    else if (src0.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "xori", dest+",", src0+",", src1, comment);
                    else
                        add(FORMAT_STR_3, "xori", dest+",", src1+",", src0, comment);
                    break;
                case "EQ":
                case "NEQ":
                    src0 = (src0 != null ? src0 : fetchUnsigned(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : fetchUnsigned(regOp.get(1), srcObj1, comment));
                    if (src0.indexOf("$") >= 0 && src1.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "xor", dest+",", src0+",", src1, comment);
                    else if (src0.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "xori", dest+",", src0+",", src1, comment);
                    else
                        add(FORMAT_STR_3, "xori", dest+",", src1+",", src0, comment);
                    add(FORMAT_STR_3, "sltu", dest+",", regZero+",", dest, comment);
                    if (op.equals("EQ"))
                        add(FORMAT_STR_3, "xori", dest+",", dest+",", "1", comment);
                    break;
                case "LEQ":
                case "GT":
                    src0 = (src0 != null ? src0 : fetch(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : load(regOp.get(1), srcObj1, comment));
                    add(FORMAT_STR_3, (src0.indexOf("$") >= 0 ? "slt" : "slti"), dest+",", src1+",", src0, comment);
                    if (op.equals("LEQ"))
                        add(FORMAT_STR_3, "xori", dest+",", dest+",", "1", comment);
                    break;
                case "GEQ":
                case "LT":
                    src0 = (src0 != null ? src0 : fetch(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : load(regOp.get(1), srcObj1, comment));
                    add(FORMAT_STR_3, (src1.indexOf("$") >= 0 ? "slt" : "slti"), dest+",", src0+",", src1, comment);
                    if (op.equals("GEQ"))
                        add(FORMAT_STR_3, "xori", dest+",", dest+",", "1", comment);
                    break;
                case "SL":
                case "SR":
                    src0 = (src0 != null ? src0 : load(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : fetch(regOp.get(1), srcObj1, comment));
                    if (op.equals("SL"))
                        add(FORMAT_STR_3, (src1.indexOf("$") >= 0 ? "sllv" : "sll"), dest+",", src0+",", src1, comment);
                    else
                        add(FORMAT_STR_3, (src1.indexOf("$") >= 0 ? "srav" : "sra"), dest+",", src0+",", src1, comment);
                    break;
                case "ADD":
                    src0 = (src0 != null ? src0 : fetch(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : fetch(regOp.get(1), srcObj1, comment));
                    if (src0.indexOf("$") >= 0 && src1.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "addu", dest+",", src0+",", src1, comment);
                    else if (src0.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "addiu", dest+",", src0+",", src1, comment);
                    else if (src1.indexOf("$") >= 0)
                        add(FORMAT_STR_3, "addiu", dest+",", src1+",", src0, comment);
                    break;
                case "SUB":
                    src0 = (src0 != null ? src0 : load(regOp.get(0), srcObj0, comment));
                    if (srcObj1 instanceof ConstCharSymbol)
                        add(FORMAT_STR_3, "addiu", dest+",", src0+",", ((Integer)(-((ConstCharSymbol)srcObj1).val)).toString(), comment);
                    else if ((srcObj1 instanceof ConstIntSymbol) && signedCheck(-((ConstIntSymbol)srcObj1).val, 16))
                        add(FORMAT_STR_3, "addiu", dest+",", src0+",", ((Integer)(-((ConstIntSymbol)srcObj1).val)).toString(), comment);
                    else
                    {
                        src1 = (src1 != null ? src1 : load(regOp.get(1), srcObj1, comment));
                        add(FORMAT_STR_3, "subu", dest+",", src0+",", src1, comment);
                    }
                    break;
                case "MUL":
                    src0 = (src0 != null ? src0 : load(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : load(regOp.get(1), srcObj1, comment));
                    add(FORMAT_STR_3, "mul", dest+",", src0+",", src1, comment);
                    break;
                case "DIV":
                case "MOD":
                    src0 = (src0 != null ? src0 : load(regOp.get(0), srcObj0, comment));
                    src1 = (src1 != null ? src1 : load(regOp.get(1), srcObj1, comment));
                    add(FORMAT_STR_2, "div", src0+",", src1, comment);
                    add(FORMAT_STR_1, (op.equals("DIV") ? "mflo" : "mfhi"), dest, comment);
                    break;
                }
                if (destObj.spill)
                    store(dest, destObj, comment);
            }
            else if (inst instanceof BranchInstruction)
            {
                String op = ((BranchInstruction)inst).op;
                ObjectSymbol condObj = ((BranchInstruction)inst).cond;
                String label = ((BranchInstruction)inst).label;
                String cond = ((BranchInstruction)inst).condReg;
                cond = (cond != null ? cond : load(regOp.get(0), condObj, comment));
                switch (op)
                {
                case "BEQZ":
                    add(FORMAT_STR_3, "beq", cond+",", regZero+",", label, comment);
                    break;
                case "BNEZ":
                    add(FORMAT_STR_3, "bne", cond+",", regZero+",", label, comment);
                    break;
                }
            }
            else if (inst instanceof JumpInstruction)
            {
                String label = ((JumpInstruction)inst).label;
                ObjectSymbol addrObj = ((JumpInstruction)inst).addr;
                if (label != null)
                    add(FORMAT_STR_1, "jal", label, comment);
                else
                {
                    String addr = ((JumpInstruction)inst).addrReg;
                    addr = (addr != null ? addr : fetch(regOp.get(0), addrObj, comment));
                    add(FORMAT_STR_1, (addr.indexOf("$") >= 0 ? "jalr" : "jal"), addr, comment);
                }
            }
            else if (inst instanceof LabelInstruction)
            {
                String label = ((LabelInstruction)inst).label;
                add(FORMAT_STR_LABEL, label+":", comment);
            }
            else if (inst instanceof RegisterInstruction)
            {
                String op = ((RegisterInstruction)inst).op;
                ObjectSymbol destObj = ((RegisterInstruction)inst).dest;
                String reg = ((RegisterInstruction)inst).reg;
                ObjectSymbol srcObj = ((RegisterInstruction)inst).src;
                String dest = ((RegisterInstruction)inst).destReg;
                String src = ((RegisterInstruction)inst).srcReg;
                switch (reg)
                {
                case "RA":
                    reg = regRA;
                    break;
                case "SP":
                    reg = regSP;
                    break;
                }
                switch (op)
                {
                case "LOAD":
                {
                    load(reg, srcObj, comment);
                    break;
                }
                case "STORE":
                    store(reg, srcObj, comment);
                    break;
                case "ADD":
                    dest = (destObj == null ? reg : (dest != null ? dest : regOp.get(0)));
                    src = (src != null ? src : fetch(regOp.get(0), srcObj, comment));
                    add(FORMAT_STR_3, (src.indexOf("$") >= 0 ? "addu" : "addiu"), dest+",", reg+",", src, comment);
                    if (destObj != null && destObj.spill)
                        store(dest, destObj, comment);
                    break;
                case "SUB":
                    dest = (destObj == null ? reg : (dest != null ? dest : regOp.get(0)));
                    if (srcObj instanceof ConstCharSymbol)
                        add(FORMAT_STR_3, "addiu", dest+",", reg+",", ((Integer)(-((ConstCharSymbol)srcObj).val)).toString(), comment);
                    else if ((srcObj instanceof ConstIntSymbol) && signedCheck(-((ConstIntSymbol)srcObj).val, 16))
                        add(FORMAT_STR_3, "addiu", dest+",", reg+",", ((Integer)(-((ConstIntSymbol)srcObj).val)).toString(), comment);
                    else
                    {
                        src = (src != null ? src : load(regOp.get(0), srcObj, comment));
                        add(FORMAT_STR_3, "subu", dest+",", reg+",", src, comment);
                    }
                    if (destObj != null && destObj.spill)
                        store(dest, destObj, comment);
                    break;
                }
            }
            else if (inst instanceof SystemCallInstruction)
            {
                String op = ((SystemCallInstruction)inst).op;
                ObjectSymbol argObj0 = ((SystemCallInstruction)inst).arg0;
                ObjectSymbol argObj1 = ((SystemCallInstruction)inst).arg1;
                String arg0 = ((SystemCallInstruction)inst).argReg0;
                String arg1 = ((SystemCallInstruction)inst).argReg1;
                switch (op)
                {
                case "READ":
                case "READCHAR":
                case "READINT":
                    if (op.equals("READ"))
                        load(regSC, new ConstIntSymbol(argObj0.type instanceof CharSymbol ? 12 : 5), comment);
                    else if (op.equals("READCHAR"))
                        load(regSC, new ConstIntSymbol(12), comment);
                    else
                        load(regSC, new ConstIntSymbol(5), comment);
                    if (arg0 == null)
                        load(regArg.get(0), argObj0, comment);
                    else
                        add(FORMAT_STR_3, "addu", regArg.get(0), regZero, arg0, comment);
                    add(FORMAT_STR_0, "syscall", comment);
                    if (argObj0.spill)
                        store(regSC, argObj0, comment);
                    else
                        add(FORMAT_STR_3, "addu", arg0, regZero, regSC, comment);
                    break;
                case "READSTR":
                    load(regSC, new ConstIntSymbol(8), comment);
                    if (arg0 == null)
                        load(regArg.get(0), argObj0, comment);
                    else
                        add(FORMAT_STR_3, "addu", regArg.get(0), regZero, arg0, comment);
                    if (arg1 == null)
                        load(regArg.get(1), argObj1, comment);
                    else
                        add(FORMAT_STR_3, "addu", regArg.get(1), regZero, arg1, comment);
                    add(FORMAT_STR_0, "syscall", comment);
                    break;
                case "WRITE":
                case "WRITECHAR":
                case "WRITEINT":
                case "WRITESTR":
                    if (op.equals("WRITE"))
                        load(regSC, new ConstIntSymbol(argObj0.type instanceof CharSymbol ? 11 : 1), comment);
                    else if (op.equals("WRITECHAR"))
                        load(regSC, new ConstIntSymbol(11), comment);
                    else if (op.equals("WRITEINT"))
                        load(regSC, new ConstIntSymbol(1), comment);
                    else
                        load(regSC, new ConstIntSymbol(4), comment);
                    if (arg0 == null)
                        load(regArg.get(0), argObj0, comment);
                    else
                        add(FORMAT_STR_3, "addu", regArg.get(0), regZero, arg0, comment);
                    add(FORMAT_STR_0, "syscall", comment);
                    break;
                case "NEW":
                    load(regSC, new ConstIntSymbol(9), comment);
                    if (arg1 == null)
                        load(regArg.get(0), argObj1, comment);
                    else
                        add(FORMAT_STR_3, "addu", regArg.get(0), regZero, arg1, comment);
                    add(FORMAT_STR_0, "syscall", comment);
                    if (arg0 == null)
                        store(regSC, argObj0, comment);
                    else
                        add(FORMAT_STR_3, "addu", arg0, regZero, regSC, comment);
                    break;
                case "EXIT":
                    load(regSC, new ConstIntSymbol(10), comment);
                    add(FORMAT_STR_0, "syscall", comment);
                    break;
                }
            }
            else if (inst instanceof UnaryInstruction)
            {
                String op = ((UnaryInstruction)inst).op;
                ObjectSymbol destObj = ((UnaryInstruction)inst).dest;
                ObjectSymbol srcObj = ((UnaryInstruction)inst).src;
                String dest = ((UnaryInstruction)inst).destReg;
                String src = ((UnaryInstruction)inst).srcReg;
                dest = (dest != null ? dest : regOp.get(0));
                if (op.equals("MOV"))
                {
                    if (srcObj.type instanceof CharSymbol || srcObj.type instanceof IntSymbol || srcObj.type instanceof PointerSymbol)
                    {
                        src = (src != null ? src : load(regOp.get(0), srcObj, comment));
                        if (destObj.spill)
                            store(src, destObj, comment);
                        else
                            add(FORMAT_STR_3, "addu", dest+",", regZero+",", src, comment);
                    }
                    else
                        memcpy(regOp.get(0), regOp.get(1), destObj, srcObj, comment);
                }
                else
                {
                    src = (src != null ? src : load(regOp.get(0), srcObj, comment));
                    switch (op)
                    {
                    case "NEG":
                        add(FORMAT_STR_3, "subu", dest+",", regZero+",", src, comment);
                        break;
                    case "NOT":
                        add(FORMAT_STR_2, "not", dest+",", src, comment);
                        break;
                    case "EQZ":
                    case "NEQZ":
                        add(FORMAT_STR_3, "sltu", dest+",", regZero, src, comment);
                        if (op.equals("EQZ"))
                            add(FORMAT_STR_3, "xori", dest+",", dest, "1", comment);
                        break;
                    }
                    if (destObj.spill)
                        store(dest, destObj, comment);
                }
            }
            else
                add(inst.comment);
        }
    }

    private void optimize()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_TR+"["+new Date().toString()+"] Optimizing assembly code"+Debug.STYLE_DEFAULT);
        for (int i = 0; i+1 < asm.size(); ++i)
        {
            String cmd0 = asm.get(i).split(COMMENT_SYMBOL)[0].trim();
            String cmd1 = asm.get(i+1).split(COMMENT_SYMBOL)[0].trim();
            if (cmd0.startsWith("sw") && cmd1.startsWith("lw") && cmd0.substring(1).equals(cmd1.substring(1)))
            {
                if (Debug.ENABLE)
                    System.err.println(Debug.STYLE_TR+"["+new Date().toString()+"] Delete "+asm.get(i+1)+Debug.STYLE_DEFAULT);
                asm.remove(i+1);
            }
        }
    }

    private String fetch(String reg, ObjectSymbol symbol, String comment)
    {
        if ((symbol instanceof ConstCharSymbol) || (symbol instanceof ConstIntSymbol) && signedCheck(((ConstIntSymbol)symbol).val, 16))
            return symbol.toString();
        return load(reg, symbol, comment);
    }

    private String fetchUnsigned(String reg, ObjectSymbol symbol, String comment)
    {
        if ((symbol instanceof ConstCharSymbol) || (symbol instanceof ConstIntSymbol) && unsignedCheck(((ConstIntSymbol)symbol).val, 16))
            return symbol.toString();
        return load(reg, symbol, comment);
    }

    private String load(String reg, ObjectSymbol symbol, String comment)
    {
        if (symbol instanceof ConstCharSymbol)
            add(FORMAT_STR_3, "addiu", reg+",", regZero+",", symbol.toString(), comment);
        else if (symbol instanceof ConstIntSymbol)
        {
            if (signedCheck(((ConstIntSymbol)symbol).val, 16))
                add(FORMAT_STR_3, "addiu", reg+",", regZero+",", symbol.toString(), comment);
            else
                add(FORMAT_STR_2, "li", reg+",", symbol.toString(), comment);
        }
        else if (symbol instanceof ConstStringSymbol)
            add(FORMAT_STR_2, "li", reg+",", getAddr(regTemp, symbol, comment), comment);
        else
            add(FORMAT_STR_2, (symbol.sizeof == 1 ? "lbu" : "lw"), reg+",", getAddr(regTemp, symbol, comment), comment);
        return reg;
    }

    private String store(String reg, ObjectSymbol symbol, String comment)
    {
        String addr = getAddr(regTemp, symbol, comment);
        if (symbol.type instanceof CharSymbol)
            add(FORMAT_STR_2, "sb", reg+",", addr, comment);
        else if (symbol.type instanceof IntSymbol)
            add(FORMAT_STR_2, "sw", reg+",", addr, comment);
        else if (symbol.type instanceof Addressable)
            add(FORMAT_STR_2, "sw", reg+",", addr, comment);
        return reg;
    }

    private void memcpy(String destAddr, String srcAddr, ObjectSymbol dest, ObjectSymbol src, String comment)
    {
        int size = Math.min(dest.sizeof, src.sizeof);
        if (size == 0)
            return;
        destAddr = getAddrReg(destAddr, dest, comment);
        srcAddr = getAddrReg(srcAddr, src, comment);
        for (int i = 0; i < size; i += 4)
        {
            add(FORMAT_STR_2, "lw", regTemp+",", i+"("+srcAddr+")", comment);
            add(FORMAT_STR_2, "sw", regTemp+",", i+"("+destAddr+")", comment);
        }
    }

    private String getAddrReg(String reg, ObjectSymbol symbol, String comment)
    {
        if (alloc.containsKey(symbol.addr))
            return Arch.dataRegister.get(alloc.get(symbol.addr));
        if (symbol.addr instanceof ConstIntSymbol)
        {
            if (symbol.frame == 0)
            {
                if (signedCheck(((ConstIntSymbol)(symbol.addr)).val-Arch.gpDefault, 16))
                    add(FORMAT_STR_3, "addiu", reg+",", regGP+",", ((Integer)(((ConstIntSymbol)(symbol.addr)).val-Arch.gpDefault)).toString(), comment);
                else
                    add(FORMAT_STR_2, "li", reg+",", symbol.addr.toString(), comment);
                return reg;
            }
            if (signedCheck(((ConstIntSymbol)(symbol.addr)).val, 16))
            {
                add(FORMAT_STR_3, "addiu", reg+",", regSP+",", symbol.addr.toString(), comment);
                return reg;
            }
            
            add(FORMAT_STR_2, "li", reg+",", ((ConstIntSymbol)(symbol.addr)).toString(), comment);
            if (symbol.frame != 0)
                add(FORMAT_STR_3, "addu", reg+",", reg+",", regSP, comment);
            return reg;
        }
        add(FORMAT_STR_2, "lw", reg+",", getAddr(reg, symbol.addr, comment), comment);
        return reg;
    }

    private String getAddr(String reg, ObjectSymbol symbol, String comment)
    {
        if (alloc.containsKey(symbol.addr))
            return "("+Arch.dataRegister.get(alloc.get(symbol.addr))+")";
        if (symbol.addr instanceof ConstIntSymbol)
        {
            if (symbol.frame == 0)
                if (signedCheck(((ConstIntSymbol)(symbol.addr)).val-Arch.gpDefault, 16))
                    return ((Integer)(((ConstIntSymbol)(symbol.addr)).val-Arch.gpDefault)).toString()+"("+regGP+")";
                else
                    return symbol.addr.toString();
            if (signedCheck(((ConstIntSymbol)(symbol.addr)).val, 16))
                return symbol.addr.toString()+(symbol.frame == 0 ? "" : "("+regSP+")");
            add(FORMAT_STR_2, "li", reg+",", ((ConstIntSymbol)(symbol.addr)).toString(), comment);
            if (symbol.frame != 0)
                add(FORMAT_STR_3, "addu", reg+",", reg+",", regSP, comment);
            return "("+reg+")";
        }
        add(FORMAT_STR_2, "lw", reg+",", getAddr(reg, symbol.addr, comment), comment);
        return "("+reg+")";
    }

    private boolean signedCheck(int val, int width)
    {
        return val >= -(1 << (width-1)) && val < (1 << (width-1));
    }

    private boolean unsignedCheck(int val, int width)
    {
        return val >=0 && val < (1 << width);
    }

    public String toString()
    {
        String code = "";
        for (String line : asm)
            code += line+"\n";
        return code;
    }

    public String getLabel()
    {
        ++tmpLabelCount;
        return "tmp_"+tmpLabelCount;
    }
}