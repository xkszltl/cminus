import java.io.*;
import java.util.*;
import java.math.*;

public class Block
{
    public class Vertex
    {
        public Instruction inst = null;
        public List<Edge> prev = new ArrayList<Edge>();
        public List<Edge> next = new ArrayList<Edge>();

        public Vertex(Instruction _inst)
        {
            inst = _inst;
            vertexSet.put(inst, this);
        }
    }

    public class Edge
    {
        public Vertex u;
        public Vertex v;

        public Edge(Vertex _u, Vertex _v)
        {
            u = _u;
            v = _v;
            u.next.add(this);
            v.prev.add(this);
        }
    }

    public List<Instruction> list = new ArrayList<Instruction>();
    public String label;
    public Instruction jump = null;
    public Set<String> inDeg = new HashSet<String>();
    public Set<String> outDeg = new HashSet<String>();
    public String prev = null;
    public String next = null;
    public Set<ObjectSymbol> def = new HashSet<ObjectSymbol>();
    public Set<ObjectSymbol> use = new HashSet<ObjectSymbol>();
    public Set<ObjectSymbol> all = new HashSet<ObjectSymbol>();
    public Set<ObjectSymbol> liveIn = new HashSet<ObjectSymbol>();
    public Set<ObjectSymbol> liveOut = new HashSet<ObjectSymbol>();
    public Map<ObjectSymbol, Integer> alloc = new HashMap<ObjectSymbol, Integer>();
    public List<ObjectSymbol> load = new ArrayList<ObjectSymbol>();
    public List<ObjectSymbol> store = new ArrayList<ObjectSymbol>();
    private Map<Instruction, Vertex> vertexSet = new HashMap<Instruction, Vertex>();

    public Block(String _label)
    {
        super();
        label = _label;
    }

    public Block(String _label, Block block)
    {
        super();
        label = _label;
        block.outDeg.add(label);
    }

    public void add(Instruction inst)
    {
        list.add(inst);
        if(inst instanceof JumpInstruction || inst instanceof BranchInstruction)
            jump = inst;
    }

    public void addDef(ObjectSymbol obj)
    {
        if (obj.addr != null && ! use.contains(obj))
        {
            def.add(obj);
            all.add(obj);
        }
    }

    public void addUse(ObjectSymbol obj)
    {
        if (obj.addr != null && ! def.contains(obj))
        {
            use.add(obj);
            all.add(obj);
        }
    }

    public Edge connect(Instruction u, Instruction v)
    {
        if (u == null || v == null)
            return null;
        if (! vertexSet.containsKey(u))
            vertexSet.put(u, new Vertex(u));
        if (! vertexSet.containsKey(v))
            vertexSet.put(v, new Vertex(v));
        return new Edge(vertexSet.get(u), vertexSet.get(v));
    }

    public boolean uselessMoveElimination()
    {
        String mainFork = IRGenerator.branch();
        String optimizingFork = IRGenerator.checkout();
        Map<ObjectSymbol, ObjectSymbol> map = new HashMap<ObjectSymbol, ObjectSymbol>();
        Set<ObjectSymbol> addressing = new HashSet<ObjectSymbol>();
        for (Instruction inst : list)
            if (inst instanceof BinaryInstruction)
            {
                for (ObjectSymbol obj = ((BinaryInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                    addressing.add(obj);
                for (ObjectSymbol obj = ((BinaryInstruction)inst).src0.addr; obj != null; obj = obj.addr)
                    addressing.add(obj);
                for (ObjectSymbol obj = ((BinaryInstruction)inst).src1.addr; obj != null; obj = obj.addr)
                    addressing.add(obj);
            }
            else if (inst instanceof BranchInstruction)
            {
                for (ObjectSymbol obj = ((BranchInstruction)inst).cond.addr; obj != null; obj = obj.addr)
                    addressing.add(obj);
            }
            else if (inst instanceof JumpInstruction)
            {
                if (((JumpInstruction)inst).addr != null)
                    for (ObjectSymbol obj = ((JumpInstruction)inst).addr.addr; obj != null; obj = obj.addr)
                        addressing.add(obj);
            }
            else if (inst instanceof RegisterInstruction)
            {
                if (((RegisterInstruction)inst).dest != null)
                    for (ObjectSymbol obj = ((RegisterInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                        addressing.add(obj);
                for (ObjectSymbol obj = ((RegisterInstruction)inst).src.addr; obj != null; obj = obj.addr)
                    addressing.add(obj);
            }
            else if (inst instanceof SystemCallInstruction)
            {
                if (((SystemCallInstruction)inst).arg0 != null)
                    for (ObjectSymbol obj = ((SystemCallInstruction)inst).arg0.addr; obj != null; obj = obj.addr)
                        addressing.add(obj);
                if (((SystemCallInstruction)inst).arg1 != null)
                    for (ObjectSymbol obj = ((SystemCallInstruction)inst).arg1.addr; obj != null; obj = obj.addr)
                        addressing.add(obj);
            }
            else if (inst instanceof UnaryInstruction)
            {
                for (ObjectSymbol obj = ((UnaryInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                    addressing.add(obj);
                for (ObjectSymbol obj = ((UnaryInstruction)inst).src.addr; obj != null; obj = obj.addr)
                    addressing.add(obj);
            }
        boolean changed = false;
        for (int i = 0; i < list.size(); ++i)
        {
            Instruction inst = list.get(i);
            if (inst instanceof BinaryInstruction)
            {
                if (map.containsKey(((BinaryInstruction)inst).src0))
                    ((BinaryInstruction)inst).src0 = map.get(((BinaryInstruction)inst).src0);
                if (map.containsKey(((BinaryInstruction)inst).src1))
                    ((BinaryInstruction)inst).src1 = map.get(((BinaryInstruction)inst).src1);
                map.remove(((BinaryInstruction)inst).dest);
                IRGenerator.add(inst);
            }
            else if (inst instanceof BranchInstruction)
            {
                if (map.containsKey(((BranchInstruction)inst).cond))
                    ((BranchInstruction)inst).cond = map.get(((BranchInstruction)inst).cond);
                IRGenerator.add(inst);
            }
            else if (inst instanceof JumpInstruction)
            {
                if (((JumpInstruction)inst).addr != null && map.containsKey(((JumpInstruction)inst).addr))
                    ((JumpInstruction)inst).addr = map.get(((JumpInstruction)inst).addr);
                IRGenerator.add(inst);
            }
            else if (inst instanceof LabelInstruction)
                IRGenerator.add(inst);
            else if (inst instanceof RegisterInstruction)
            {
                switch (((RegisterInstruction)inst).op)
                {
                case "LOAD":
                    map.remove(((RegisterInstruction)inst).src);
                    break;
                case "STORE":
                    if (map.containsKey(((RegisterInstruction)inst).src))
                        ((RegisterInstruction)inst).src = map.get(((RegisterInstruction)inst).src);
                    break;
                case "ADD":
                case "SUB":
                    if (map.containsKey(((RegisterInstruction)inst).src))
                        ((RegisterInstruction)inst).src = map.get(((RegisterInstruction)inst).src);
                    break;
                }
                if (((RegisterInstruction)inst).dest != null)
                    map.remove(((RegisterInstruction)inst).dest);
                IRGenerator.add(inst);
            }
            else if (inst instanceof SystemCallInstruction)
            {
                switch (((SystemCallInstruction)inst).op)
                {
                case "NEW":
                    if (map.containsKey(((SystemCallInstruction)inst).arg1))
                        ((SystemCallInstruction)inst).arg1 = map.get(((SystemCallInstruction)inst).arg1);
                case "READ":
                case "READCHAR":
                case "READINT":
                    map.remove(((SystemCallInstruction)inst).arg0);
                    break;
                case "WRITE":
                case "WRITECHAR":
                case "WRITEINT":
                case "WRITESTR":
                    if (map.containsKey(((SystemCallInstruction)inst).arg0))
                        ((SystemCallInstruction)inst).arg0 = map.get(((SystemCallInstruction)inst).arg0);
                case "READSTR":
                    if (map.containsKey(((SystemCallInstruction)inst).arg1))
                        ((SystemCallInstruction)inst).arg1 = map.get(((SystemCallInstruction)inst).arg1);
                    break;
                }
                IRGenerator.add(inst);
            }
            else if (inst instanceof UnaryInstruction)
            {
                if (((UnaryInstruction)inst).op.equals("MOV") && ! liveOut.contains(((UnaryInstruction)inst).dest) && ! addressing.contains(((UnaryInstruction)inst).dest))
                {
                    ObjectSymbol src = map.containsKey(((UnaryInstruction)inst).src) ? map.get(((UnaryInstruction)inst).src) : ((UnaryInstruction)inst).src;
                    if (src.restrict)
                    {
                        boolean modify = false;
                        for (int j = i; j < list.size() && ! modify; ++j)
                        {
                            Instruction modifyInst = list.get(j);
                            if (modifyInst instanceof BinaryInstruction)
                                modify = ((BinaryInstruction)modifyInst).dest.equals(src);
                            else if (modifyInst instanceof RegisterInstruction)
                            {
                                switch (((RegisterInstruction)modifyInst).op)
                                {
                                case "LOAD":
                                    modify = ((RegisterInstruction)modifyInst).src.equals(src);
                                    break;
                                case "ADD":
                                case "SUB":
                                    if (((RegisterInstruction)modifyInst).dest != null)
                                        modify = ((RegisterInstruction)modifyInst).dest.equals(src);
                                    break;
                                }
                            }
                            else if (modifyInst instanceof SystemCallInstruction)
                            {
                                switch (((SystemCallInstruction)modifyInst).op)
                                {
                                case "READ":
                                case "READCHAR":
                                case "READINT":
                                case "NEW":
                                    modify = ((SystemCallInstruction)modifyInst).arg0.equals(src);
                                    break;
                                }
                            }
                            else if (modifyInst instanceof UnaryInstruction)
                                modify = ((UnaryInstruction)modifyInst).dest.equals(src);
                        }
                        if (! modify)
                        {
                            if (Debug.ENABLE)
                                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Delete "+inst+Debug.STYLE_DEFAULT);
                            map.put(((UnaryInstruction)inst).dest, src);
                            changed = true;
                        }
                        else
                            {
                                map.remove(((UnaryInstruction)inst).dest);
                                IRGenerator.add(inst);
                            }
                    }
                    else
                    {
                        map.remove(((UnaryInstruction)inst).dest);
                        IRGenerator.add(inst);
                    }
                }
                else
                {
                    if (map.containsKey(((UnaryInstruction)inst).src))
                        ((UnaryInstruction)inst).src = map.get(((UnaryInstruction)inst).src);
                    map.remove(((UnaryInstruction)inst).dest);
                    IRGenerator.add(inst);
                }
            }
        }
        list = IRGenerator.getInstructions(optimizingFork);
        IRGenerator.checkout(mainFork);
        IRGenerator.remove(optimizingFork);
        return changed;
    }

    public void deadCodeElimination()
    {
        if (jump instanceof JumpInstruction && ((JumpInstruction)jump).addr != null)
            return;
        if (label.startsWith("call_"))
            return;
        Map<ObjectSymbol, Instruction> lastDef = new HashMap<ObjectSymbol, Instruction>();
        for (Instruction inst : list)
        {
            Vertex v = new Vertex(inst);
            if (inst instanceof BinaryInstruction)
            {
                for (ObjectSymbol obj = ((BinaryInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                    connect(lastDef.get(obj), inst);
                for (ObjectSymbol obj = ((BinaryInstruction)inst).src0; obj != null; obj = obj.addr)
                    connect(lastDef.get(obj), inst);
                for (ObjectSymbol obj = ((BinaryInstruction)inst).src1; obj != null; obj = obj.addr)
                    connect(lastDef.get(obj), inst);
                lastDef.put(((BinaryInstruction)inst).dest, inst);
            }
            else if (inst instanceof BranchInstruction)
            {
                for (ObjectSymbol obj = ((BranchInstruction)inst).cond; obj != null; obj = obj.addr)
                    connect(lastDef.get(obj), inst);
            }
            else if (inst instanceof JumpInstruction)
            {
                for (ObjectSymbol obj = ((JumpInstruction)inst).addr; obj != null; obj = obj.addr)
                    connect(lastDef.get(obj), inst);
            }
            else if (inst instanceof RegisterInstruction)
            {
                for (ObjectSymbol obj = ((RegisterInstruction)inst).src; obj != null; obj = obj.addr)
                    connect(lastDef.get(obj), inst);
                switch (((RegisterInstruction)inst).op)
                {
                case "ADD":
                case "SUB":
                    if (((RegisterInstruction)inst).dest != null)
                    {
                        for (ObjectSymbol obj = ((RegisterInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                            connect(lastDef.get(obj), inst);
                        lastDef.put(((RegisterInstruction)inst).dest, inst);
                    }
                    break;
                }
            }
            else if (inst instanceof SystemCallInstruction)
            {
                switch (((SystemCallInstruction)inst).op)
                {
                case "READ":
                case "READCHAR":
                case "READINT":
                    lastDef.put(((SystemCallInstruction)inst).arg0, inst);
                    for (ObjectSymbol obj = ((SystemCallInstruction)inst).arg0.addr; obj != null; obj = obj.addr)
                        connect(lastDef.get(obj), inst);
                    break;
                case "NEW":
                case "WRITESTR":
                    for (ObjectSymbol obj = ((SystemCallInstruction)inst).arg1; obj != null; obj = obj.addr)
                        connect(lastDef.get(obj), inst);
                case "READSTR":
                case "WRITE":
                case "WRITECHAR":
                case "WRITEINT":
                    for (ObjectSymbol obj = ((SystemCallInstruction)inst).arg0; obj != null; obj = obj.addr)
                        connect(lastDef.get(obj), inst);
                    break;
                case "EXIT":
                    break;
                }
            }
            else if (inst instanceof UnaryInstruction)
            {
                for (ObjectSymbol obj = ((UnaryInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                    connect(lastDef.get(obj), inst);
                for (ObjectSymbol obj = ((UnaryInstruction)inst).src; obj != null; obj = obj.addr)
                    connect(lastDef.get(obj), inst);
                lastDef.put(((UnaryInstruction)inst).dest, inst);
            }
        }

        Set<Instruction> liveCode = new HashSet<Instruction>();
        Deque<Vertex> queue = new ArrayDeque<Vertex>();
        for (ObjectSymbol obj : liveOut)
            if (lastDef.containsKey(obj) && ! liveCode.contains(lastDef.get(obj)))
            {
                queue.add(vertexSet.get(lastDef.get(obj)));
                liveCode.add(lastDef.get(obj));
            }
        for (Instruction inst : list)
            if (! (inst instanceof BinaryInstruction) && ! (inst instanceof UnaryInstruction) && ! liveCode.contains(inst))
            {
                queue.add(vertexSet.get(inst));
                liveCode.add(inst);
            }
        while (queue.size() > 0)
            for (Edge edge : queue.poll().prev)
                if (! liveCode.contains(edge.u.inst))
                {
                    queue.add(vertexSet.get(edge.u.inst));
                    liveCode.add(edge.u.inst);
                }
        if (Debug.ENABLE)
            for (Instruction inst : list)
                if (! liveCode.contains(inst))
                    System.err.println(Debug.STYLE+"["+new Date().toString()+"] Eliminate dead code in "+label+": "+inst.toString()+Debug.STYLE_DEFAULT);
        list.retainAll(liveCode);
    }
}