import java.io.*;
import java.util.*;
import java.math.*;

public class NetworkFlow
{
    private List<Vertex> vertexSet = new ArrayList<Vertex>();
    private List<Edge> edgeSet = new ArrayList<Edge>();
    private Vertex source;
    private Vertex sink;
    private Set<Vertex> useful = new HashSet<Vertex>();
    public int length = 0;
    public int capacity = 0;
    public int maxFlow = 0;

    public class Vertex
    {
        public int id;
        public List<Edge> prev = new ArrayList<Edge>();
        public List<Edge> next = new ArrayList<Edge>();

        public long dist;
        public Edge path;
        public boolean flag;

        public Vertex()
        {
            id = vertexSet.size();
            vertexSet.add(this);
        }
    }

    public class Edge
    {
        public Vertex u;
        public Vertex v;
        public int capacity;
        public int cost;
        public Edge inv;

        public Edge(int _u, int _v, int _capacity, int _cost)
        {
            u = vertexSet.get(_u);
            v = vertexSet.get(_v);
            capacity = _capacity;
            cost = _cost;
            u.next.add(this);
            v.prev.add(this);
        }
    }

    public NetworkFlow(int _length, int _capacity)
    {
        length = _length;
        capacity = _capacity;
        for (int i = 0; i <= length; ++i)
            new Vertex();
        source = new Vertex();
        sink = vertexSet.get(length);
        useful.add(source);
        useful.add(sink);
    }

    public Edge add(int l, int r)
    {
        Edge edge = addEdge(l, r, 1, -1);
        edgeSet.add(edge);
        useful.add(vertexSet.get(l));
        useful.add(vertexSet.get(r));
        return edge;
    }

    public Set<Edge> solve()
    {
        for (int i = 0, j = source.id; i <= length; ++i)
            if (useful.contains(vertexSet.get(i)))
            {
                addEdge(j, i, capacity, 0);
                j = i;
            }
        for (; spfa(); augment());
        Set<Edge> full = new HashSet<Edge>();
        for (Edge i : edgeSet)
            if (i.capacity == 0)
                full.add(i);
        return full;
    }

    private Edge addEdge(int _u, int _v, int _capacity, int _cost)
    {
        Edge edge = new Edge(_u, _v, _capacity, _cost);
        edge.inv = new Edge(_v, _u, 0, -_cost);
        edge.inv.inv = edge;
        return edge;
    }

    private boolean spfa()
    {
        Deque<Vertex> queue = new ArrayDeque<Vertex>();
        for (Vertex i : vertexSet)
        {
            i.dist = Long.MAX_VALUE;
            i.path = null;
            i.flag = false;
        }
        source.dist = 0;
        queue.add(source);
        for (Vertex t; (t = queue.poll()) != null;)
        {
            t.flag = false;
            for (Edge i : t.next)
                if (i.capacity > 0 && t.dist+i.cost < i.v.dist)
                {
                    i.v.path = i;
                    i.v.dist = t.dist+i.cost;
                    if (! i.v.flag)
                    {
                        i.v.flag = true;
                        queue.add(i.v);
                    }
                }
        }
        return sink.path != null;
    }

    private void augment()
    {
        int minFlow = Integer.MAX_VALUE;
        for (Vertex t = sink; t != source; t = t.path.u)
            minFlow = Math.min(minFlow, t.path.capacity);
        maxFlow += minFlow;
        for (Vertex t = sink; t != source; t = t.path.u)
        {
            t.path.capacity -= minFlow;
            t.path.inv.capacity += minFlow;
        }
    }
}