import java.io.*;
import java.util.*;
import java.math.*;

public class Optimizer
{
    public static final boolean AUTO_INLINE                     = Panel.AUTO_INLINE;
    public static final boolean USELESS_JUMP_ELIMINATION        = Panel.USELESS_JUMP_ELIMINATION;
    public static final boolean CONSTANT_PROPAGATION            = Panel.CONSTANT_PROPAGATION;
    public static final boolean USELESS_MOVE_ELIMINATION        = Panel.USELESS_MOVE_ELIMINATION;
    public static final boolean UNREACHABLE_CODE_ELIMINATION    = Panel.UNREACHABLE_CODE_ELIMINATION;
    public static final boolean DEAD_CODE_ELIMINATION           = Panel.DEAD_CODE_ELIMINATION;
    public static final boolean REGISTER_ALLOCATION             = Panel.REGISTER_ALLOCATION;
    public static final boolean LIVENESS_ANALYSIS               = REGISTER_ALLOCATION | USELESS_MOVE_ELIMINATION;

    public class RegisterRequest implements Comparable<RegisterRequest>
    {
        public int id;
        public ObjectSymbol obj;
        public int request;

        public RegisterRequest(int _id, ObjectSymbol _obj, int _request)
        {
            super();
            id = _id;
            obj = _obj;
            request = _request;
        }

        public int compareTo(RegisterRequest b)
        {
            if (id != b.id)
                return id < b.id ? -1 : 1;
            if (request != b.request)
                return request > b.request ? -1 : 1;
            return 0;
        }
    }

    Map<String, Block> blockTable = new HashMap<String, Block>();
    List<Block> blockList = new ArrayList<Block>();
    Map<Block, Set<Integer>> requiredReg = new HashMap<Block, Set<Integer>>();

    public Optimizer()
    {
        if (CONSTANT_PROPAGATION)
            constantPropagation();
        controlFlowAnalysis();
        if (UNREACHABLE_CODE_ELIMINATION)
            unreachableCodeElimintaion();
        constantCalculation();
        if (LIVENESS_ANALYSIS)
            livenessAnalysis();
        if (USELESS_MOVE_ELIMINATION)
            uselessMoveElimintaion();
        if (DEAD_CODE_ELIMINATION)
            deadCodeElimination();
        if (UNREACHABLE_CODE_ELIMINATION)
            unreachableCodeElimintaion();
        if (REGISTER_ALLOCATION)
            registerAllocation();
        generate();
    }

    private void constantPropagation()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Constant propagation"+Debug.STYLE_DEFAULT);
        Set<ObjectSymbol> constant = new HashSet<ObjectSymbol>();
        for (ObjectSymbol obj : ObjectSymbol.frameSymbolList.get(0))
            if (! (obj instanceof ConstSymbol) && obj.restrict && (obj.addr instanceof ConstIntSymbol) && (obj.type instanceof Scalar))
                constant.add(obj);
        for (Instruction inst : IRGenerator.getInstructions("master"))
            if (inst instanceof BinaryInstruction)
                constant.remove(((BinaryInstruction)inst).dest);
            else if (inst instanceof RegisterInstruction)
            {
                switch (((RegisterInstruction)inst).op)
                {
                case "LOAD":
                    constant.remove(((RegisterInstruction)inst).src);
                    break;
                case "ADD":
                case "SUB":
                    if (((RegisterInstruction)inst).dest != null)
                        constant.remove(((RegisterInstruction)inst).dest);
                    break;
                }
            }
            else if (inst instanceof SystemCallInstruction)
            {
                switch (((SystemCallInstruction)inst).op)
                {
                case "READ":
                case "READCHAR":
                case "READINT":
                case "NEW":
                    constant.remove(((SystemCallInstruction)inst).arg0);
                    break;
                }
            }
            else if (inst instanceof UnaryInstruction)
                constant.remove(((UnaryInstruction)inst).dest);
        for (ObjectSymbol obj : constant)
            if (Debug.ENABLE)
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Found constant "+obj+" = "+obj.read()+Debug.STYLE_DEFAULT);
        for (Instruction inst : IRGenerator.getInstructions("master"))
            if (inst instanceof BinaryInstruction)
            {
                if (constant.contains(((BinaryInstruction)inst).dest))
                    ((BinaryInstruction)inst).dest = ((BinaryInstruction)inst).dest.read();
                else
                    for (ObjectSymbol obj = ((BinaryInstruction)inst).dest; obj.addr != null; obj = obj.addr)
                        if (constant.contains(obj.addr))
                            obj.addr = obj.addr.read();
                if (constant.contains(((BinaryInstruction)inst).src0))
                    ((BinaryInstruction)inst).src0 = ((BinaryInstruction)inst).src0.read();
                else
                    for (ObjectSymbol obj = ((BinaryInstruction)inst).src0; obj.addr != null; obj = obj.addr)
                        if (constant.contains(obj.addr))
                            obj.addr = obj.addr.read();
                if (constant.contains(((BinaryInstruction)inst).src1))
                    ((BinaryInstruction)inst).src1 = ((BinaryInstruction)inst).src1.read();
                else
                    for (ObjectSymbol obj = ((BinaryInstruction)inst).src1; obj.addr != null; obj = obj.addr)
                        if (constant.contains(obj.addr))
                            obj.addr = obj.addr.read();
            }
            else if (inst instanceof UnaryInstruction)
            {
                if (constant.contains(((UnaryInstruction)inst).dest))
                    ((UnaryInstruction)inst).dest = ((UnaryInstruction)inst).dest.read();
                else
                    for (ObjectSymbol obj = ((UnaryInstruction)inst).dest; obj.addr != null; obj = obj.addr)
                        if (constant.contains(obj.addr))
                            obj.addr = obj.addr.read();
                if (constant.contains(((UnaryInstruction)inst).src))
                    ((UnaryInstruction)inst).src = ((UnaryInstruction)inst).src.read();
                else
                    for (ObjectSymbol obj = ((UnaryInstruction)inst).src; obj.addr != null; obj = obj.addr)
                        if (constant.contains(obj.addr))
                            obj.addr = obj.addr.read();
            }
    }

    private void constantCalculation()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Recalculate constant expression"+Debug.STYLE_DEFAULT);
        try
        {
            for (Block block : blockList)
            {
                String calcFork = IRGenerator.checkout();
                for (Instruction inst : block.list)
                {
                    ObjectSymbol ret = null;
                    if (inst instanceof BinaryInstruction)
                    {
                        if ((((BinaryInstruction)inst).src0 instanceof ConstCharSymbol || ((BinaryInstruction)inst).src0 instanceof ConstIntSymbol) &&
                            (((BinaryInstruction)inst).src1 instanceof ConstCharSymbol || ((BinaryInstruction)inst).src1 instanceof ConstIntSymbol))
                        {
                            if (Debug.ENABLE)
                                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Recalculating "+inst+Debug.STYLE_DEFAULT);
                            switch (((BinaryInstruction)inst).op)
                            {
                            case "EQ":
                                ret = ObjectSymbol.equal(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "NEQ":
                                ret = ObjectSymbol.notEqual(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "LEQ":
                                ret = ObjectSymbol.lessEqual(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "GEQ":
                                ret = ObjectSymbol.greaterEqual(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "LT":
                                ret = ObjectSymbol.less(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "GT":
                                ret = ObjectSymbol.greater(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "XOR":
                                ret = ObjectSymbol.xor(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "AND":
                                ret = ObjectSymbol.and(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "SL":
                                ret = ObjectSymbol.shiftLeft(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "SR":
                                ret = ObjectSymbol.shiftRight(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "ADD":
                                ret = ObjectSymbol.plus(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "SUB":
                                ret = ObjectSymbol.minus(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "MUL":
                                ret = ObjectSymbol.mul(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "DIV":
                                ret = ObjectSymbol.div(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            case "MOD":
                                ret = ObjectSymbol.mod(((BinaryInstruction)inst).src0, ((BinaryInstruction)inst).src1, inst.comment);
                                break;
                            }
                            if (ret != null)
                                IRGenerator.add("=", ((BinaryInstruction)inst).dest, ret, inst.comment);
                            else
                                IRGenerator.add(inst);
                        }
                        else
                            IRGenerator.add(inst);
                    }
                    else if (inst instanceof UnaryInstruction)
                    {
                        if (((UnaryInstruction)inst).src instanceof ConstCharSymbol || ((UnaryInstruction)inst).src instanceof ConstIntSymbol)
                        {
                            if (! ((UnaryInstruction)inst).op.equals("MOV"))
                                if (Debug.ENABLE)
                                    System.err.println(Debug.STYLE+"["+new Date().toString()+"] Recalculating "+inst+Debug.STYLE_DEFAULT);
                            switch (((UnaryInstruction)inst).op)
                            {
                            case "NEG":
                                ret = ObjectSymbol.neg(((UnaryInstruction)inst).src, inst.comment);
                                break;
                            case "NOT":
                                ret = ObjectSymbol.not(((UnaryInstruction)inst).src, inst.comment);
                                break;
                            case "EQZ":
                                ret = ObjectSymbol.logicNot(((UnaryInstruction)inst).src, inst.comment);
                                break;
                            case "NEQZ":
                                ret = ObjectSymbol.toBool(((UnaryInstruction)inst).src, inst.comment);
                                break;
                            }
                            if (ret != null)
                                IRGenerator.add("=", ((UnaryInstruction)inst).dest, ret, inst.comment);
                            else
                                IRGenerator.add(inst);
                        }
                        else
                            IRGenerator.add(inst);
                    }
                    else
                        IRGenerator.add(inst);
                }
                block.list = IRGenerator.getInstructions();
                IRGenerator.checkout("master");
                IRGenerator.remove(calcFork);
            }
        }
        catch (CompileException e)
        {
            System.err.println(e.msg);
        }
    }

    private void controlFlowAnalysis()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Control flow analysis"+Debug.STYLE_DEFAULT);
        int blockLabel = 0;

        LabelInstruction begin = null;
        Instruction end = null;
        Block block = null;

        for (Instruction inst : IRGenerator.getInstructions("master"))
            if (inst instanceof LabelInstruction)
            {
                if (end == null)
                {
                    begin = null;
                    end = new JumpInstruction(((LabelInstruction)inst).label, "");
                    if (block != null)
                        block.add(end);
                }
                begin = (LabelInstruction)inst;
                Block prev = block;
                if (block == null)
                    block = new Block(begin.label);
                else if (end instanceof JumpInstruction)
                {
                    if (((JumpInstruction)end).label == null)
                        block = new Block(begin.label);
                    else if (((JumpInstruction)end).label.equals(begin.label))
                        block = new Block(begin.label, block);
                    else if (((JumpInstruction)end).label.startsWith("call_"))
                        block = new Block(begin.label, block);
                    else
                        block = new Block(begin.label);
                }
                else
                    block = new Block(begin.label, block);
                blockTable.put(begin.label, block);
                blockList.add(block);
                if (prev != null && prev.jump instanceof BranchInstruction)
                {
                    prev.next = block.label;
                    block.prev = prev.label;
                }
                end = null;
                block.add(inst);
            }
            else if (inst instanceof RegisterInstruction && ((RegisterInstruction)inst).reg.equals("SP") && ((RegisterInstruction)inst).dest == null)
            {
                if (((RegisterInstruction)inst).op.equals("SUB"))
                {
                    block.add(inst);
                    ++blockLabel;
                    begin = new LabelInstruction("block_"+(Integer)blockLabel, "");
                    if (! (block.jump instanceof JumpInstruction) && ! (block.jump instanceof BranchInstruction))
                        block.add(new JumpInstruction(begin.label, ""));
                    block = new Block(begin.label, block);
                    blockTable.put(begin.label, block);
                    blockList.add(block);
                    end = null;
                    block.add(begin);
                }
                else if (((RegisterInstruction)inst).op.equals("ADD"))
                {
                    if (end == null)
                    {
                        ++blockLabel;
                        begin = new LabelInstruction("block_"+(Integer)blockLabel, "");
                        if (! (block.jump instanceof JumpInstruction) && ! (block.jump instanceof BranchInstruction))
                            block.add(new JumpInstruction(begin.label, ""));
                        block = new Block(begin.label, block);
                        blockTable.put(begin.label, block);
                        blockList.add(block);
                        end = null;
                        block.add(begin);
                    }
                    else
                    {
                        ++blockLabel;
                        begin = new LabelInstruction("block_"+(Integer)blockLabel, "");
                        Block prev = block;
                        if (block == null)
                            block = new Block(begin.label);
                        else if (end instanceof JumpInstruction)
                        {
                            if (((JumpInstruction)end).label == null)
                                block = new Block(begin.label);
                            else if (((JumpInstruction)end).label.equals(begin.label))
                                block = new Block(begin.label, block);
                            else if (((JumpInstruction)end).label.startsWith("call_"))
                                block = new Block(begin.label, block);
                            else
                                block = new Block(begin.label);
                        }
                        else
                            block = new Block(begin.label, block);
                        blockTable.put(begin.label, block);
                        blockList.add(block);
                        if (prev != null && prev.jump instanceof BranchInstruction)
                        {
                            prev.next = block.label;
                            block.prev = prev.label;
                        }
                        end = null;
                        block.add(begin);
                    }
                    block.add(inst);
                }
            }
            else if (! inst.getClass().equals(Instruction.class))
            {
                if (begin == null)
                {
                    ++blockLabel;
                    begin = new LabelInstruction("block_"+(Integer)blockLabel, "");
                    Block prev = block;
                    if (block == null)
                        block = new Block(begin.label);
                    else if (end instanceof JumpInstruction)
                    {
                        if (((JumpInstruction)end).label == null)
                            block = new Block(begin.label);
                        else if (((JumpInstruction)end).label.equals(begin.label))
                            block = new Block(begin.label, block);
                        else if (((JumpInstruction)end).label.startsWith("call_"))
                            block = new Block(begin.label, block);
                        else
                            block = new Block(begin.label);
                    }
                    else
                        block = new Block(begin.label, block);
                    blockTable.put(begin.label, block);
                    blockList.add(block);
                    if (prev != null && prev.jump instanceof BranchInstruction)
                    {
                        prev.next = block.label;
                        block.prev = prev.label;
                    }
                    end = null;
                    block.add(begin);
                }
                block.add(inst);
                if (inst instanceof JumpInstruction)
                {
                    if (((JumpInstruction)inst).label != null && ! ((JumpInstruction)inst).label.startsWith("call_"))
                        block.outDeg.add(((JumpInstruction)inst).label);
                    begin = null;
                    end = inst;
                }
                else if (inst instanceof BranchInstruction)
                {
                    block.outDeg.add(((BranchInstruction)inst).label);
                    begin = null;
                    end = inst;
                }
            }

        for (String i : blockTable.keySet())
            for (String j : blockTable.get(i).outDeg)
            {
                blockTable.get(j).inDeg.add(i);
                if (Debug.ENABLE)
                    System.err.println(Debug.STYLE+"["+new Date().toString()+"]     "+i+" -> "+j+Debug.STYLE_DEFAULT);
            }
    }

    private void unreachableCodeElimintaion()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Unreachable code elimination"+Debug.STYLE_DEFAULT);
        Set<Block> visited = new HashSet<Block>();
        Deque<Block> queue = new ArrayDeque<Block>();
        for (Block block : blockList)
            if (block.label.equals(IRGenerator.getEntry()))
            {
                queue.add(block);
                visited.add(block);
                break;
            }
        for (Block block; (block = queue.poll()) != null;)
        {
            for (String i : block.outDeg)
                if (! visited.contains(blockTable.get(i)))
                {
                    queue.add(blockTable.get(i));
                    visited.add(blockTable.get(i));
                }
            if (block.jump != null && block.jump instanceof JumpInstruction)
                if (((JumpInstruction)block.jump).label != null && ((JumpInstruction)block.jump).label.startsWith("call_"))
                    if (! visited.contains(blockTable.get(((JumpInstruction)block.jump).label)))
                    {
                        queue.add(blockTable.get(((JumpInstruction)block.jump).label));
                        visited.add(blockTable.get(((JumpInstruction)block.jump).label));
                    }
        }
        blockList.retainAll(visited);
    }

    private void livenessAnalysis()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Liveness analysis"+Debug.STYLE_DEFAULT);
        Deque<Block> queue = new ArrayDeque<Block>();
        for (Block block : blockList)
        {
            queue.add(block);
            for (Instruction inst : block.list)
            {
                if (inst instanceof BinaryInstruction)
                {
                    block.addUse(((BinaryInstruction)inst).src0);
                    block.addUse(((BinaryInstruction)inst).src1);
                    for (ObjectSymbol obj = ((BinaryInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                        block.addUse(obj);
                    for (ObjectSymbol obj = ((BinaryInstruction)inst).src0.addr; obj != null; obj = obj.addr)
                        block.addUse(obj);
                    for (ObjectSymbol obj = ((BinaryInstruction)inst).src1.addr; obj != null; obj = obj.addr)
                        block.addUse(obj);
                    block.addDef(((BinaryInstruction)inst).dest);
                }
                else if (inst instanceof BranchInstruction)
                {
                    block.addUse(((BranchInstruction)inst).cond);
                    for (ObjectSymbol obj = ((BranchInstruction)inst).cond.addr; obj != null; obj = obj.addr)
                        block.addUse(obj);
                }
                else if (inst instanceof RegisterInstruction)
                {
                    switch (((RegisterInstruction)inst).op)
                    {
                    case "LOAD":
                    case "STORE":
                        for (ObjectSymbol obj = ((RegisterInstruction)inst).src.addr; obj != null; obj = obj.addr)
                            block.addUse(obj);
                        block.addDef(((RegisterInstruction)inst).src);
                        break;
                    case "ADD":
                    case "SUB":
                        block.addUse(((RegisterInstruction)inst).src);
                        for (ObjectSymbol obj = ((RegisterInstruction)inst).src.addr; obj != null; obj = obj.addr)
                            block.addUse(obj);
                        if (((RegisterInstruction)inst).dest != null)
                        {
                            for (ObjectSymbol obj = ((RegisterInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                                block.addUse(obj);
                            block.addDef(((RegisterInstruction)inst).dest);
                        }
                        break;
                    }
                }
                else if (inst instanceof SystemCallInstruction)
                {
                    switch (((SystemCallInstruction)inst).op)
                    {
                    case "READ":
                    case "READCHAR":
                    case "READINT":
                        block.addDef(((SystemCallInstruction)inst).arg0);
                        break;
                    case "READSTR":
                        block.addUse(((SystemCallInstruction)inst).arg0);
                        block.addUse(((SystemCallInstruction)inst).arg1);
                        for (ObjectSymbol obj = ((SystemCallInstruction)inst).arg1.addr; obj != null; obj = obj.addr)
                            block.addUse(obj);
                    case "WRITE":
                    case "WRITECHAR":
                    case "WRITEINT":
                    case "WRITESTR":
                        block.addUse(((SystemCallInstruction)inst).arg0);
                        break;
                    case "NEW":
                        block.addUse(((SystemCallInstruction)inst).arg1);
                        for (ObjectSymbol obj = ((SystemCallInstruction)inst).arg1.addr; obj != null; obj = obj.addr)
                            block.addUse(obj);
                        block.addDef(((SystemCallInstruction)inst).arg0);
                        break;
                    case "EXIT":
                        break;
                    }
                    for (ObjectSymbol obj = ((SystemCallInstruction)inst).arg0.addr; obj != null; obj = obj.addr)
                        block.addUse(obj);
                }
                else if (inst instanceof UnaryInstruction)
                {
                    block.addUse(((UnaryInstruction)inst).src);
                    for (ObjectSymbol obj = ((UnaryInstruction)inst).dest.addr; obj != null; obj = obj.addr)
                        block.addUse(obj);
                    for (ObjectSymbol obj = ((UnaryInstruction)inst).src.addr; obj != null; obj = obj.addr)
                        block.addUse(obj);
                    block.addDef(((UnaryInstruction)inst).dest);
                }
            }
            block.liveIn.addAll(block.use);
            for (ObjectSymbol obj : block.all)
                if (! obj.restrict || obj.frame == 0 || obj.addr instanceof ConstIntSymbol && ((ConstIntSymbol)obj.addr).val < 0)
                    for (; obj != null; obj = obj.addr)
                        block.liveOut.add(obj);
        }
        for (Block block; (block = queue.poll()) != null;)
        {
            Set<ObjectSymbol> through = new HashSet<ObjectSymbol>();
            through.addAll(block.liveOut);
            through.removeAll(block.def);
            if (block.liveIn.addAll(through))
                queue.add(block);
            for (String i : block.inDeg)
                if (blockTable.get(i).liveOut.addAll(block.liveIn))
                    queue.add(blockTable.get(i));
        }
        if (Debug.ENABLE)
            for (Block block : blockList)
            {
                System.err.print(Debug.STYLE+"["+new Date().toString()+"] "+block.label+" ->");
                for (String j : block.outDeg)
                    System.err.print(" "+j);
                System.err.println(Debug.STYLE_DEFAULT);
                System.err.print(Debug.STYLE+"["+new Date().toString()+"]     Live-in:");
                for (ObjectSymbol j : block.liveIn)
                    if (! (j instanceof ConstSymbol))
                        System.err.print(" "+IRGenerator.getAddr(j)+"|#"+j.frame);
                System.err.println(Debug.STYLE_DEFAULT);
                System.err.print(Debug.STYLE+"["+new Date().toString()+"]     Def:");
                for (ObjectSymbol j : block.def)
                    if (! (j instanceof ConstSymbol))
                        System.err.print(" "+IRGenerator.getAddr(j)+"|#"+j.frame);
                System.err.println(Debug.STYLE_DEFAULT);
                System.err.print(Debug.STYLE+"["+new Date().toString()+"]     Use:");
                for (ObjectSymbol j : block.use)
                    if (! (j instanceof ConstSymbol))
                        System.err.print(" "+IRGenerator.getAddr(j)+"|#"+j.frame);
                System.err.println(Debug.STYLE_DEFAULT);
                
                System.err.print(Debug.STYLE+"["+new Date().toString()+"]     Live-out:");
                for (ObjectSymbol j : block.liveOut)
                    if (! (j instanceof ConstSymbol))
                        System.err.print(" "+IRGenerator.getAddr(j)+"|#"+j.frame);
                System.err.println(Debug.STYLE_DEFAULT);
            }
    }

    private void uselessMoveElimintaion()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Useless move elimination"+Debug.STYLE_DEFAULT);
        for (boolean changed = true; changed;)
        {
            changed = false;
            for (Block block : blockList)
                for (; block.uselessMoveElimination(); changed = true);
            IRGenerator.checkout("master");
            IRGenerator.clear();
            if (changed)
            {
                constantCalculation();
                if (Debug.ENABLE)
                    System.err.println(Debug.STYLE+"["+new Date().toString()+"] Elimination success."+Debug.STYLE_DEFAULT);
            }
            else
                if (Debug.ENABLE)
                    System.err.println(Debug.STYLE+"["+new Date().toString()+"] Nothing can be eliminated."+Debug.STYLE_DEFAULT);
        }
    }

    private void deadCodeElimination()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Dead code elimination"+Debug.STYLE_DEFAULT);
        for (Block block : blockList)
            block.deadCodeElimination();
    }

    private void registerAllocation()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Register allocation"+Debug.STYLE_DEFAULT);
        Set<Block> visited = new HashSet<Block>();
        for (Block block : blockList)
        {
            if (visited.contains(block))
                continue;

            Set<ObjectSymbol> objs = new HashSet<ObjectSymbol>();
            Set<Block> blocks = new HashSet<Block>();
            Deque<Block> queue = new ArrayDeque<Block>();
            visited.add(block);
            queue.add(block);
            for (Block t; (t = queue.poll()) != null; blocks.add(t))
            {
                for (ObjectSymbol obj : t.all)
                    if (obj.frame != 0)
                        objs.add(obj);
                for (String i : t.inDeg)
                    if (! visited.contains(blockTable.get(i)))
                    {
                        visited.add(blockTable.get(i));
                        queue.add(blockTable.get(i));
                    }
                for (String i : t.outDeg)
                    if (! visited.contains(blockTable.get(i)))
                    {
                        visited.add(blockTable.get(i));
                        queue.add(blockTable.get(i));
                    }
            }

            TreeSet<Integer> pool = new TreeSet<Integer>();
            for (int j = 0; j < Arch.dataRegister.size(); pool.add(j++));
            NetworkFlow graph = new NetworkFlow(blockList.size(), Arch.dataRegister.size());
            Map<ObjectSymbol, NetworkFlow.Edge> edge = new HashMap<ObjectSymbol, NetworkFlow.Edge>();
            List<RegisterRequest> request = new ArrayList<RegisterRequest>();
            for (ObjectSymbol obj : objs)
                if (obj.restrict)
                {
                    int first = Integer.MAX_VALUE;
                    int last = Integer.MIN_VALUE;
                    for (int j = 0; j < blockList.size(); ++j)
                        if (blocks.contains(blockList.get(j)) && blockList.get(j).all.contains(obj))
                        {
                            first = Math.min(first, j);
                            last = Math.max(last, j);
                            request.add(new RegisterRequest(j, obj, 0));
                        }
                    if (first <= last)
                    {
                        edge.put(obj, graph.add(first, last+1));
                        request.add(new RegisterRequest(first, obj, 1));
                        request.add(new RegisterRequest(last, obj, -1));
                    }
                }
            Collections.sort(request);
            Set<NetworkFlow.Edge> full = graph.solve();
            if (Debug.ENABLE)
                System.err.println(Debug.STYLE+"["+new Date().toString()+"] Use "+full.size()+" register variable(s)"+Debug.STYLE_DEFAULT);

            Map<ObjectSymbol, Integer> alloc = new HashMap<ObjectSymbol, Integer>();
            Set<Integer> used = new HashSet<Integer>();
            int prev = 0;
            for (RegisterRequest j : request)
            {
                ObjectSymbol obj = j.obj;
                switch (j.request)
                {
                case 1:
                    for (; prev < j.id; ++prev)
                        if (blocks.contains(blockList.get(prev)))
                            blockList.get(prev).alloc.putAll(alloc);
                    if (full.contains(edge.get(obj)))
                    {
                        obj.spill = false;
                        blockList.get(j.id).load.add(obj);
                        alloc.put(obj, pool.first());
                        used.add(pool.first());
                        pool.remove(pool.first());
                    }
                    break;
                case 0:
                    for (; prev <= j.id; ++prev)
                        if (blocks.contains(blockList.get(prev)))
                            blockList.get(prev).alloc.putAll(alloc);
                    break;
                case -1:
                    if (alloc.containsKey(obj))
                    {
                        blockList.get(j.id).store.add(obj);
                        pool.add(alloc.get(obj));
                        alloc.remove(obj);
                    }
                    break;
                }
            }
            for (; prev < blockList.size(); ++prev)
                if (blocks.contains(blockList.get(prev)))
                    blockList.get(prev).alloc.putAll(alloc);
            for (Block i : blocks)
                requiredReg.put(i, used);
        }
    }

    private void generate()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Generate optimized IR code"+Debug.STYLE_DEFAULT);
        int regLabel = 0;
        IRGenerator.checkout("register");
        IRGenerator.clear();
        IRGenerator.checkout("optimize");
        IRGenerator.clear();
        for (int i = 0; i < blockList.size(); ++i)
        {
            Block block = blockList.get(i);
            Block next = (i+1 < blockList.size() ? blockList.get(i+1) : null);
            for (int j = 0; j+1 < block.list.size(); ++j)
                IRGenerator.add(block.list.get(j).allocate(block.alloc));

            if (block.jump instanceof JumpInstruction)
            {
                JumpInstruction jump = (JumpInstruction)block.jump;
                if (jump.label == null)
                    IRGenerator.add(block.jump.allocate(block.alloc));
                else if (jump.label.startsWith("call_"))
                {
                    Set<ObjectSymbol> store = new HashSet<ObjectSymbol>();
                    store.addAll(block.alloc.keySet());
                    Set<ObjectSymbol> load = new HashSet<ObjectSymbol>();
                    if (next != null)
                        load.addAll(next.alloc.keySet());

                    for (ObjectSymbol j : store)
                        IRGenerator.add((new RegisterInstruction("STORE", Arch.dataRegister.get(block.alloc.get(j)), j, "Store(Function call) "+j.toString())).allocate(block.alloc));
                    IRGenerator.add(jump.allocate(block.alloc));
                    for (ObjectSymbol j : load)
                        IRGenerator.add(new RegisterInstruction("LOAD", Arch.dataRegister.get(next.alloc.get(j)), j, "Load(Recovery) "+j.toString()));
                }
                else
                {
                    Block target = blockTable.get(jump.label);
                    Set<ObjectSymbol> store = new HashSet<ObjectSymbol>();
                    store.addAll(block.alloc.keySet());
                    store.retainAll(block.liveOut);
                    store.removeAll(target.alloc.keySet());
                    Set<ObjectSymbol> load = new HashSet<ObjectSymbol>();
                    load.addAll(target.alloc.keySet());
                    load.retainAll(target.liveIn);
                    load.removeAll(block.alloc.keySet());

                    for (ObjectSymbol j : store)
                        IRGenerator.add((new RegisterInstruction("STORE", Arch.dataRegister.get(block.alloc.get(j)), j, "Store(Jump) "+j.toString())).allocate(block.alloc));
                    for (ObjectSymbol j : load)
                        IRGenerator.add((new RegisterInstruction("LOAD", Arch.dataRegister.get(target.alloc.get(j)), j, "Load(Jump) "+j.toString())).allocate(block.alloc));
                    if (USELESS_JUMP_ELIMINATION && next == null || ! next.label.equals(jump.label))
                        IRGenerator.add(block.jump.allocate(block.alloc));
                }
            }
            else
            {
                Set<ObjectSymbol> store = null;
                Set<ObjectSymbol> load = null;
                BranchInstruction jump = (BranchInstruction)block.jump;
                LabelInstruction begin = new LabelInstruction("reg_"+(Integer)(++regLabel), "");

                Block target = blockTable.get(jump.label);
                store = new HashSet<ObjectSymbol>();
                store.addAll(block.alloc.keySet());
                store.retainAll(block.liveOut);
                store.removeAll(target.alloc.keySet());
                load = new HashSet<ObjectSymbol>();
                load.addAll(target.alloc.keySet());
                load.retainAll(target.liveIn);
                load.removeAll(block.alloc.keySet());
                if (store.size() > 0 || load.size() > 0)
                {
                    IRGenerator.checkout("register");
                    IRGenerator.add(begin.allocate(block.alloc));
                    for (ObjectSymbol j : store)
                        IRGenerator.add((new RegisterInstruction("STORE", Arch.dataRegister.get(block.alloc.get(j)), j, "Store(Branch) "+j.toString())).allocate(block.alloc));
                    for (ObjectSymbol j : load)
                        IRGenerator.add((new RegisterInstruction("LOAD", Arch.dataRegister.get(target.alloc.get(j)), j, "Load(Branch) "+j.toString())).allocate(block.alloc));
                    IRGenerator.add((new JumpInstruction(jump.label, "")).allocate(target.alloc));
                    IRGenerator.checkout("optimize");
                    IRGenerator.add((new BranchInstruction(jump.op, jump.cond, begin.label, jump.comment)).allocate(block.alloc));
                }
                else
                    IRGenerator.add(jump.allocate(block.alloc));

                store = new HashSet<ObjectSymbol>();
                load = new HashSet<ObjectSymbol>();
                store.addAll(block.alloc.keySet());
                store.retainAll(block.liveOut);
                if (next != null)
                    store.removeAll(next.alloc.keySet());
                for (ObjectSymbol j : store)
                    IRGenerator.add((new RegisterInstruction("STORE", Arch.dataRegister.get(block.alloc.get(j)), j, "Store(Branch) "+j.toString())).allocate(block.alloc));
                if (next != null)
                {
                    load.addAll(next.alloc.keySet());
                    load.removeAll(block.alloc.keySet());
                    load.retainAll(next.liveIn);
                    for (ObjectSymbol j : load)
                        IRGenerator.add((new RegisterInstruction("LOAD", Arch.dataRegister.get(next.alloc.get(j)), j, "Load(Branch) "+j.toString())).allocate(block.alloc));
                }
            }
        }
        IRGenerator.merge("register");
        IRGenerator.checkout("master");
        IRGenerator.clear();
        IRGenerator.merge("optimize");
    }

    private List<Block> reorder(Map<String, Block> table)
    {
        List<Block> buffer = new ArrayList<Block>();
        Set<String> visited = new HashSet<String>();
        for (String i : table.keySet())
            if (! visited.contains(i))
                for (Block next, block = table.get(i); block != null; block = next)
                {
                    visited.add(block.label);
                    buffer.add(block);
                    if (block.next == null)
                    {
                        next = null;
                        for (String j : block.outDeg)
                            if (! visited.contains(j) && (table.get(j).prev == null || table.get(j).prev.equals(block.label)))
                            {
                                next = table.get(j);
                                break;
                            }
                    }
                    else
                        next = table.get(block.next);
                }
        return buffer;
    }
}