public class Panel
{
    public static final boolean DEBUG_MODE                      = false;
    public static final boolean ASM_COMMENT                     = true;
    public static final boolean AUTO_INLINE                     = true;
    public static final boolean USELESS_LABEL_ELIMINATION       = true;
    public static final boolean USELESS_JUMP_ELIMINATION        = true;
    public static final boolean CONSTANT_PROPAGATION            = true;
    public static final boolean USELESS_MOVE_ELIMINATION        = true;
    public static final boolean UNREACHABLE_CODE_ELIMINATION    = true;
    public static final boolean DEAD_CODE_ELIMINATION           = true;
    public static final boolean REGISTER_ALLOCATION             = true;
}