import java.io.*;
import java.util.*;
import java.math.*;

public class ReturnJumpSymbol extends JumpSymbol
{
    public FuncSymbol func = null;

    public ReturnJumpSymbol(String _id, FuncSymbol _func)
    {
        super("ret_"+_id);
        func = _func;
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+"("+id+") for '"+func.typeInfo()+"'"+Debug.STYLE_DEFAULT);
    }

    public String typeInfo()
    {
        return "label(return) "+"\'"+id+" ("+func.typeInfo()+")\'";
    }
}
