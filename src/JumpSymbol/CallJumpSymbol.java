import java.io.*;
import java.util.*;
import java.math.*;

public class CallJumpSymbol extends JumpSymbol
{
    public CallJumpSymbol(String _id)
    {
        super("call_"+_id);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+"("+id+")"+Debug.STYLE_DEFAULT);
    }

    public String typeInfo()
    {
        return "label(call) \'"+id+"\'";
    }
}
