import java.io.*;
import java.util.*;
import java.math.*;

public class JumpSymbol extends Symbol
{
    String id;

    public JumpSymbol(String _id)
    {
        super();
        id = _id;
    }

    public String typeInfo()
    {
        return "incomplete jump label";
    }
}