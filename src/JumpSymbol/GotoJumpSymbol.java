import java.io.*;
import java.util.*;
import java.math.*;

public class GotoJumpSymbol extends JumpSymbol
{
    public GotoJumpSymbol(String _id)
    {
        super("goto_"+_id);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+"("+id+")"+Debug.STYLE_DEFAULT);
    }

    public String typeInfo()
    {
        return "label(goto) \'"+id+"\'";
    }
}
