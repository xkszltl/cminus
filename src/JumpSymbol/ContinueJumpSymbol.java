import java.io.*;
import java.util.*;
import java.math.*;

public class ContinueJumpSymbol extends JumpSymbol
{
    public ContinueJumpSymbol(String _id)
    {
        super("ctn_"+_id);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+"("+id+")"+Debug.STYLE_DEFAULT);
    }

    public String typeInfo()
    {
        return "label(continue) \'"+id+"\'";
    }
}
