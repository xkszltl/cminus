import java.io.*;
import java.util.*;
import java.math.*;

public class BreakJumpSymbol extends JumpSymbol
{
    public BreakJumpSymbol(String _id)
    {
        super("brk_"+_id);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+"("+id+")"+Debug.STYLE_DEFAULT);
    }

    public String typeInfo()
    {
        return "label(break) \'"+id+"\'";
    }
}
