import java.io.*;
import java.util.*;
import java.math.*;

public class BranchJumpSymbol extends JumpSymbol
{
    public BranchJumpSymbol(String _id)
    {
        super("brch_"+_id);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE+"["+new Date().toString()+"] Construct "+getClass().toString()+"("+id+")"+Debug.STYLE_DEFAULT);
    }

    public String typeInfo()
    {
        return "label(branch) \'"+id+"\'";
    }
}
