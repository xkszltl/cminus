import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.math.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class CMinus
{
    private static void usageInfo() throws Exception
    {
        System.out.println("Usage: c-- [options] file");
        System.out.println("Options:");
        System.out.println("  -i <file>             Place the intermediate representation code into <file>");
        System.out.println("  -o <file>             Place the output into <file>");
        System.out.println("  -S                    Compile only; do not assemble");
        System.out.println("  -I <path>             Specify the path for searching the included files");
        System.out.println("  --help                Display this information");
        System.out.println("  -v, --version         Display compiler version information");
        System.out.println();
        System.out.println("For bug reporting instructions, please contact: <xkszltl@gmail.com>");
        System.out.println();
    }

    private static void versionInfo() throws Exception
    {
        System.out.println("c-- (CMinus) 20140601");
        System.out.println("Copyright (C) 2014 Tongliang Liao");
        System.out.println();
        System.out.println("This is free software.    There is NO warranty; not even");
        System.out.println("for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.");
        System.out.println();
    }

    private static Map<String, Object> cmdArg(String[] cmd) throws Exception
    {
        Map<String, Object> args = new HashMap<String, Object>();
        int error = 0;

        for (int i = 0; i < cmd.length; ++i)
        {
            switch (cmd[i])
            {
            case "-v":
                if (i+1 < cmd.length && cmd[i+1].equals("--help"))
                {
                    args.put("help", cmd[i++]);
                    break;
                }
            case "--version":
                args.put("version", true);
                break;
            case "--help":
                args.put("help", "");
                break;
            case "-i":
                args.put("IR", cmd[++i]);
                break;
            case "-o":
                args.put("target", cmd[++i]);
                break;
            case "-S":
                args.put("asm", true);
                break;
            case "-tree":
                args.put("tree", cmd[++i]);
                break;
            case "-Tree":
                args.put("Tree", cmd[++i]);
                break;
            default:
                if (cmd[i].startsWith("-I"))
                {
                    if (args.get("include") == null)
                        args.put("include", new ArrayList<String>());
                    ((List<String>)args.get("include")).add(cmd[i].substring(2));
                    break;
                }
                File srcFile = new File(cmd[i]);
                if (! srcFile.exists())
                {
                    System.out.println("c--: error: No such file or directory: "+cmd[i]);
                    error |= 1;
                }
                else
                {
                    if (args.get("file") == null)
                        args.put("file", new ArrayList<File>());
                    ((List<File>)args.get("file")).add(srcFile);
                }
            }
        }

        if (args.get("target") == null)
            if (args.get("asm") == null)
                args.put("target", "a.out");
            else
            {
                String fileName = ((List<File>)args.get("file")).get(0).toString();
                args.put("target", fileName.substring(0, fileName.lastIndexOf("."))+".s");
            }

        if (args.get("help") != null)
        {
            args.clear();
            args.put("help", true);
        }

        if ((args.get("file") == null || ((List<File>)args.get("file")).size() == 0) && args.get("help") == null && args.get("version") == null)
        {
            System.out.println("c--: error: no input files");
            error |= 1;
        }

        if (error > 0)
            throw new Exception();

        return args;
    }

    public static File loadLib(List<String> libPath, String libName) throws Exception
    {
        for (String path : libPath)
        {
            File lib = new File(path+"/"+libName);
            if (lib.exists())
                return lib;
        }
        System.out.println("c--: error: "+libName+": No such file or directory");
        return null;
    }

    public static void main(String[] args) throws Exception
    {
        try
        {
            Map<String, Object> cmd = new HashMap<String, Object>();
            cmd = cmdArg(args);
            if (cmd.get("file") != null)
            {
                ((List<File>)cmd.get("file")).add(0, loadLib((List<String>)cmd.get("include"), "stdio.c"));
                CompileErrorListener errorListener = new CompileErrorListener();
                for (File src : (List<File>)cmd.get("file"))
                {
                    if (src == null)
                        System.exit(1);
                    if (Debug.ENABLE)
                        System.err.println(Debug.STYLE_INFO+"["+new Date().toString()+"] Compiling "+src+Debug.STYLE_DEFAULT);
                    errorListener.fileName = src.toString();
                    IRGenerator.fileName = src.getName();
                    ANTLRInputStream input = new ANTLRFileStream(src.toString());
                    CMinusLexer lexer = new CMinusLexer(input);
                    lexer.removeErrorListeners();
                    lexer.addErrorListener(errorListener);
                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    CMinusParser parser = new CMinusParser(tokens);
                    parser.removeErrorListeners();
                    parser.addErrorListener(errorListener);
                    parser.program();
                }
                if (errorListener.errorCount > 0)
                    System.exit(1);

                Optimizer optimizer = new Optimizer();

                IRGenerator.link();

                IRGenerator.checkout("master");
                Translator translator = new Translator(IRGenerator.getInstructions("master"));

                if (cmd.get("IR") != null)
                {
                    PrintWriter fileOut = new PrintWriter(new BufferedWriter(new FileWriter((String)cmd.get("IR"))));
                    fileOut.println(IRGenerator.getCode());
                    fileOut.close();
                }

                if (cmd.get("asm") != null)
                {
                    PrintWriter asmOut = new PrintWriter(new BufferedWriter(new FileWriter((String)cmd.get("target"))));
                    asmOut.println(translator.toString());
                    asmOut.close();
                }
            }

            if (cmd.get("help") != null)
                usageInfo();

            if (cmd.get("version") != null)
                versionInfo();
        }
        catch (Exception e)
        {
            //throw e;
            System.exit(1);
        }
        System.exit(0);
    }
}