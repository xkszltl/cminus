import java.io.*;
import java.util.*;
import java.math.*;

public class UnaryInstruction extends Instruction
{
    public ObjectSymbol dest, src;
    public String op;
    public int vDest = 0, vSrc = 0;
    public String destReg = null, srcReg = null;

    public UnaryInstruction(String _op, ObjectSymbol _dest, ObjectSymbol _src, String _comment)
    {
        super(_comment);
        op = _op;
        dest = _dest;
        src = _src;
    }

    public Instruction allocate(Map<ObjectSymbol, Integer> _alloc)
    {
        alloc = _alloc;
        destReg = (alloc.containsKey(dest) ? Arch.dataRegister.get(alloc.get(dest)) : null);
        srcReg = (alloc.containsKey(src) ? Arch.dataRegister.get(alloc.get(src)) : null);
        return this;
    }

    public String toString()
    {
        if (op.equals("MOV"))
            return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_2,
                op+"("+Math.min(dest.sizeof, src.sizeof)+")", IRGenerator.getAddr(dest)+(destReg != null ? "|"+destReg : "")+",", IRGenerator.getAddr(src)+(srcReg != null ? "|"+srcReg : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
        return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_2,
            op, IRGenerator.getAddr(dest)+(destReg != null ? "|"+destReg : "")+",", IRGenerator.getAddr(src)+(srcReg != null ? "|"+srcReg : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
    }

    public UnaryInstruction clone()
    {
        return new UnaryInstruction(op, dest, src, comment);
    }
}