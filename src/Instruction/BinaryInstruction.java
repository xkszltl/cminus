import java.io.*;
import java.util.*;
import java.math.*;

public class BinaryInstruction extends Instruction
{
    public ObjectSymbol dest, src0, src1;
    public String op;
    public int vDest = 0, vSrc0 = 0, vSrc1 = 0;
    public String destReg = null, srcReg0 = null, srcReg1 = null;

    public BinaryInstruction(String _op, ObjectSymbol _dest, ObjectSymbol _src0, ObjectSymbol _src1, String _comment)
    {
        super(_comment);
        op = _op;
        dest = _dest;
        src0 = _src0;
        src1 = _src1;
    }

    public Instruction allocate(Map<ObjectSymbol, Integer> _alloc)
    {
        alloc = _alloc;
        destReg = (alloc.containsKey(dest) ? Arch.dataRegister.get(alloc.get(dest)) : null);
        srcReg0 = (alloc.containsKey(src0) ? Arch.dataRegister.get(alloc.get(src0)) : null);
        srcReg1 = (alloc.containsKey(src1) ? Arch.dataRegister.get(alloc.get(src1)) : null);
        return this;
    }

    public String toString()
    {
        return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_3,
            op, IRGenerator.getAddr(dest)+(destReg != null ? "|"+destReg : "")+",", IRGenerator.getAddr(src0)+(srcReg0 != null ? "|"+srcReg0 : "")+",", IRGenerator.getAddr(src1)+(srcReg1 != null ? "|"+srcReg1 : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
    }

    public BinaryInstruction clone()
    {
        return new BinaryInstruction(op, dest, src0, src1, comment);
    }
}