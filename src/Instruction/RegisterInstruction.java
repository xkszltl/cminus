import java.io.*;
import java.util.*;
import java.math.*;

public class RegisterInstruction extends Instruction
{
    public String op;
    public ObjectSymbol dest;
    public String reg;
    public ObjectSymbol src;
    public int vDest = 0, vSrc = 0;
    public String destReg = null, srcReg = null;

    public RegisterInstruction(String _op, String _reg, ObjectSymbol _src, String _comment)
    {
        super(_comment);
        op = _op;
        dest = null;
        reg = _reg;
        src = _src;
    }

    public RegisterInstruction(String _op, ObjectSymbol _dest, String _reg, ObjectSymbol _src, String _comment)
    {
        super(_comment);
        dest = _dest;
        op = _op;
        reg = _reg;
        src = _src;
    }

    public Instruction allocate(Map<ObjectSymbol, Integer> _alloc)
    {
        alloc = _alloc;
        switch (op)
        {
        case "LOAD":
        case "STORE":
            srcReg = (alloc.containsKey(src) ? Arch.dataRegister.get(alloc.get(src)) : null);
            break;
        case "ADD":
        case "SUB":
            destReg = (alloc.containsKey(dest) ? Arch.dataRegister.get(alloc.get(dest)) : null);
            srcReg = (alloc.containsKey(src) ? Arch.dataRegister.get(alloc.get(src)) : null);
            break;
        }
        return this;
    }

    public String toString()
    {
        if (op.equals("LOAD") || op.equals("STORE"))
            return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_2,
                op+"("+src.sizeof+")", "|"+reg+",", IRGenerator.getAddr(src)+(srcReg != null ? "|"+srcReg : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
        if (dest == null)
            return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_2,
                op, "|"+reg+",", IRGenerator.getAddr(src)+(srcReg != null ? "|"+srcReg : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
        return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_3,
            op, IRGenerator.getAddr(dest)+(destReg != null ? "|"+destReg : "")+",", "|"+reg+",", IRGenerator.getAddr(src)+(srcReg != null ? "|"+srcReg : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
    }

    public RegisterInstruction clone()
    {
        return new RegisterInstruction(op, dest, reg, src, comment);
    }
}