import java.io.*;
import java.util.*;
import java.math.*;

public class JumpInstruction extends Instruction
{
    public String label = null;
    public ObjectSymbol addr = null;
    public String addrReg = null;

    public JumpInstruction(String _label, String _comment)
    {
        super(_comment);
        label = _label;
    }

    public JumpInstruction(ObjectSymbol _addr, String _comment)
    {
        super(_comment);
        addr = _addr;
    }

    public Instruction allocate(Map<ObjectSymbol, Integer> _alloc)
    {
        alloc = _alloc;
        if (addr != null)
            addrReg = (alloc.containsKey(addr) ? Arch.dataRegister.get(alloc.get(addr)) : null);
        return this;
    }

    public String toString()
    {
        return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_PREFIX+"%-"+IRGenerator.FORMAT_CMD+"s %-"+IRGenerator.FORMAT_LABEL+"s",
            "JUMP", (label == null ? IRGenerator.getAddr(addr)+(addrReg != null ? "|"+addrReg : "") : label)), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
    }

    public JumpInstruction clone()
    {
        if (label != null)
            return new JumpInstruction(label, comment);
        return new JumpInstruction(addr, comment);
    }
}