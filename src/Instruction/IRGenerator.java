import java.io.*;
import java.util.*;
import java.math.*;

public class IRGenerator
{
    public static final boolean USELESS_LABEL_ELIMINATION = Panel.USELESS_LABEL_ELIMINATION;

    public static final String FORMAT_PREFIX = "            ";
    public static final int FORMAT_INST = 58;
    public static final int FORMAT_ADDR = 11;
    public static final int FORMAT_CMD = 10;
    public static final int FORMAT_LABEL = 11;
    public static final int FORMAT_POS = 12;
    public static final int FORMAT_COMMENT = 128;
    public static final char FORMAT_CUT = '=';
    public static final String FORMAT_STR_COMMENT = "%-"+FORMAT_INST+"s %s";
    public static final String FORMAT_STR_LABEL = "%-"+FORMAT_LABEL+"s ";
    public static final String FORMAT_STR_0 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s";
    public static final String FORMAT_STR_1 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s %-"+FORMAT_ADDR+"s";
    public static final String FORMAT_STR_2 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s";
    public static final String FORMAT_STR_3 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s";
    public static final String FORMAT_STR_4 = FORMAT_PREFIX+"%-"+FORMAT_CMD+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s %-"+FORMAT_ADDR+"s";
    public static final boolean COMMENT_FILENAME = false;

    public static String fileName = "";
    public static List<FuncSymbol> bind = new ArrayList<FuncSymbol>();

    public static Map<String, String> cmdBinary = new HashMap<String, String>();
    static
    {
        cmdBinary.put("||", "OR");
        cmdBinary.put("&&", "AND");
        cmdBinary.put("|", "OR");
        cmdBinary.put("^", "XOR");
        cmdBinary.put("&", "AND");
        cmdBinary.put("==", "EQ");
        cmdBinary.put("!=", "NEQ");
        cmdBinary.put("<=", "LEQ");
        cmdBinary.put(">=", "GEQ");
        cmdBinary.put("<", "LT");
        cmdBinary.put(">", "GT");
        cmdBinary.put("<<", "SL");
        cmdBinary.put(">>", "SR");
        cmdBinary.put("+", "ADD");
        cmdBinary.put("-", "SUB");
        cmdBinary.put("*", "MUL");
        cmdBinary.put("/", "DIV");
        cmdBinary.put("%", "MOD");
    }

    public static Map<String, String> cmdUnary = new HashMap<String, String>();
    static
    {
        cmdUnary.put("-", "NEG");
        cmdUnary.put("~", "NOT");
        cmdUnary.put("!", "EQZ");
        cmdUnary.put("=", "MOV");
        cmdUnary.put("==0", "EQZ");
        cmdUnary.put("!=0", "NEQZ");
    }

    private static Map<String, List<Instruction>> fork = new HashMap<String, List<Instruction>>();
    private static String currentFork = "master";
    static
    {
        fork.put("master", new ArrayList<Instruction>());
    }

    private static String entryLabel = null;

    public static void setEntry(String label)
    {
        entryLabel = label;
    }

    public static String getEntry()
    {
        if (entryLabel == null)
        {
            System.err.println("Undefined symbols for architecture mips32:");
            System.err.println("  \"main\", referenced from:");
            System.err.println("     implicit entry/start for main executable");
        }
        return entryLabel;
    }

    public static void align()
    {
        for (ConstIntSymbol i : ObjectSymbol.frameAllocator)
            if (i.val % 4 != 0)
                i.val += 4-i.val % 4;
    }

    public static void link()
    {
        List<Instruction> branch = fork.get(currentFork);
        align();
        String comment = "program entry";
        branch.add(0, new SystemCallInstruction("EXIT", comment));
        branch.add(0, new JumpInstruction(getEntry(), comment));
        branch.add(0, new UnaryInstruction("MOV", new ObjectSymbol(true, IntSymbol.instance, true, 1, new ConstIntSymbol(-8)), (ConstSymbol)(new ObjectSymbol(false, IntSymbol.instance, false, 0)).addr, comment));
        branch.add(0, new LabelInstruction("main", comment));
        Set<String> labels = new HashSet<String>();
        labels.add("main");
        for (Instruction i : branch)
            if (i instanceof JumpInstruction)
                labels.add(((JumpInstruction)i).label);
            else if (i instanceof BranchInstruction)
                labels.add(((BranchInstruction)i).label);
        if (USELESS_LABEL_ELIMINATION)
            for (int i = 0; i < branch.size(); ++i)
                if (branch.get(i) instanceof LabelInstruction && ! labels.contains(((LabelInstruction)branch.get(i)).label))
                    branch.remove(i--);
    }

    public static String branch()
    {
        return currentFork;
    }

    public static String checkout()
    {
        for (currentFork = "random_"+Math.random(); fork.containsKey(currentFork); currentFork = "random_"+Math.random());
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Checkout IR fork '"+currentFork+"'"+Debug.STYLE_DEFAULT);
        fork.put(currentFork, new ArrayList<Instruction>());
        return currentFork;
    }

    public static String checkout(String name)
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Checkout IR fork '"+name+"'"+Debug.STYLE_DEFAULT);
        if (! fork.containsKey(name))
            fork.put(name, new ArrayList<Instruction>());
        currentFork = name;
        return currentFork;
    }

    public static void merge(String name)
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Merge IR fork '"+name+"' to '"+currentFork+"'"+Debug.STYLE_DEFAULT);
        if (fork.get(name) == null)
            return;
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.addAll(fork.get(name));
        if (fork.get(currentFork).size() == 0)
            fork.put(currentFork, fork.get(name));
        else
        {
            fork.get(currentFork).addAll(fork.get(name));
            fork.get(name).clear();
        }
        fork.remove(name);
    }

    public static void copy(String name)
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Copy IR fork '"+name+"' to '"+currentFork+"'"+Debug.STYLE_DEFAULT);
        if (! fork.containsKey(name))
            return;
        List<Instruction> list = new ArrayList<Instruction>();
        for (Instruction inst : fork.get(name))
            list.add(inst.clone());
        Map<String, String> copyLabel = new HashMap<String, String>();
        Map<ObjectSymbol, ObjectSymbol> copyObj = new HashMap<ObjectSymbol, ObjectSymbol>();
        for (Instruction inst : list)
            if (inst instanceof LabelInstruction)
            {
                copyLabel.put(((LabelInstruction)inst).label, ((LabelInstruction)inst).label+"_copy");
                ((LabelInstruction)inst).label += "_copy";
            }
            else if (inst instanceof BinaryInstruction)
            {
                ObjectSymbol obj = ((BinaryInstruction)inst).dest;
                for (; ! (obj.addr instanceof ConstIntSymbol); obj = obj.addr);
                if (((ConstIntSymbol)obj.addr).val < 0)
                    copyObj.put(obj, new ObjectSymbol(obj.restrict, obj.type, obj.lvalue, obj.frame, obj.addr));
            }
            else if (inst instanceof RegisterInstruction)
            {
                ObjectSymbol obj = ((RegisterInstruction)inst).dest;
                if (obj != null)
                {
                    for (; ! (obj.addr instanceof ConstIntSymbol); obj = obj.addr);
                    if (((ConstIntSymbol)obj.addr).val < 0)
                        copyObj.put(obj, new ObjectSymbol(obj.restrict, obj.type, obj.lvalue, obj.frame, obj.addr));
                }
            }
            else if (inst instanceof UnaryInstruction)
            {
                ObjectSymbol obj = ((UnaryInstruction)inst).dest;
                for (; ! (obj.addr instanceof ConstIntSymbol); obj = obj.addr);
                if (((ConstIntSymbol)obj.addr).val < 0)
                    copyObj.put(obj, new ObjectSymbol(obj.restrict, obj.type, obj.lvalue, obj.frame, obj.addr));
            }
        for (Instruction inst : list)
            if (inst instanceof JumpInstruction && ((JumpInstruction)inst).label != null && copyLabel.containsKey(((JumpInstruction)inst).label))
                ((JumpInstruction)inst).label = copyLabel.get(((JumpInstruction)inst).label);
            else if (inst instanceof BranchInstruction && copyLabel.containsKey(((BranchInstruction)inst).label))
                ((BranchInstruction)inst).label = copyLabel.get(((BranchInstruction)inst).label);
            else if (inst instanceof BinaryInstruction)
            {
                if (copyObj.containsKey(((BinaryInstruction)inst).dest))
                    ((BinaryInstruction)inst).dest = copyObj.get(((BinaryInstruction)inst).dest);
                else
                {
                    for (ObjectSymbol obj = ((BinaryInstruction)inst).dest; ! (obj.addr instanceof ConstIntSymbol); obj = obj.addr)
                        if (copyObj.containsKey(obj.addr))
                        {
                            obj.addr = copyObj.get(obj.addr);
                            break;
                        }
                }
            }
            else if (inst instanceof RegisterInstruction && ((RegisterInstruction)inst).dest != null)
            {
                if (copyObj.containsKey(((RegisterInstruction)inst).dest))
                    ((RegisterInstruction)inst).dest = copyObj.get(((RegisterInstruction)inst).dest);
                else
                {
                    for (ObjectSymbol obj = ((RegisterInstruction)inst).dest; ! (obj.addr instanceof ConstIntSymbol); obj = obj.addr)
                        if (copyObj.containsKey(obj.addr))
                        {
                            obj.addr = copyObj.get(obj.addr);
                            break;
                        }
                }
            }
            else if (inst instanceof UnaryInstruction)
            {
                if (copyObj.containsKey(((UnaryInstruction)inst).dest))
                    ((UnaryInstruction)inst).dest = copyObj.get(((UnaryInstruction)inst).dest);
                else
                {
                    for (ObjectSymbol obj = ((UnaryInstruction)inst).dest; ! (obj.addr instanceof ConstIntSymbol); obj = obj.addr)
                        if (copyObj.containsKey(obj.addr))
                        {
                            obj.addr = copyObj.get(obj.addr);
                            break;
                        }
                }
            }
        fork.get(currentFork).addAll(fork.get(name));
        fork.put(name, list);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.addAll(fork.get(name));
    }

    public static void copyInline(String name, int frame, LabelInstruction entry, LabelInstruction exit)
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Copy(inline) IR fork '"+name+"' to '"+currentFork+"'"+Debug.STYLE_DEFAULT);
        String mainFork = branch();
        String inlineFork = checkout();
        copy(name);
        List<Instruction> list = getInstructions(inlineFork);
        list.remove(0);
        list.remove(0);
        list.add(0, entry);
        for (Instruction inst : list)
            if (inst instanceof JumpInstruction && ((JumpInstruction)inst).addr != null)
            {
                ((JumpInstruction)inst).addr = null;
                ((JumpInstruction)inst).label = exit.label;
            }
        add(exit);
        copyObjs(list, frame);
        checkout(mainFork);
        merge(inlineFork);
    }

    private static ObjectSymbol copyObj(ObjectSymbol obj, Map<ObjectSymbol, ObjectSymbol> map)
    {
        if (map.containsKey(obj))
            return map.get(obj);
        if (obj instanceof ConstSymbol)
            return obj;
        copyObj(obj.addr, map);
        if (! map.containsKey(obj.addr) && obj.frame == 0)
            return obj;
        map.put(obj, new ObjectSymbol(obj.restrict, obj.type, obj.lvalue, obj.frame, map.containsKey(obj.addr) ? map.get(obj.addr) : obj.addr));
        return map.get(obj);
    }

    private static void copyObjs(List<Instruction> list, int frame)
    {
        Map<ObjectSymbol, ObjectSymbol> map = new HashMap<ObjectSymbol, ObjectSymbol>();
        for (Instruction inst : list)
            if (inst instanceof BinaryInstruction)
            {
                if (! (((BinaryInstruction)inst).dest instanceof ConstSymbol) && map.containsKey(((BinaryInstruction)inst).dest))
                    ((BinaryInstruction)inst).dest = copyObj(((BinaryInstruction)inst).dest, map);
                if (! (((BinaryInstruction)inst).src0 instanceof ConstSymbol) && map.containsKey(((BinaryInstruction)inst).src0))
                    ((BinaryInstruction)inst).src0 = copyObj(((BinaryInstruction)inst).src0, map);
                if (! (((BinaryInstruction)inst).src1 instanceof ConstSymbol) && map.containsKey(((BinaryInstruction)inst).src1))
                    ((BinaryInstruction)inst).src1 = copyObj(((BinaryInstruction)inst).src1, map);
            }
            else if (inst instanceof BranchInstruction)
            {
                if (! (((BranchInstruction)inst).cond instanceof ConstSymbol) && map.containsKey(((BranchInstruction)inst).cond))
                    ((BranchInstruction)inst).cond = copyObj(((BranchInstruction)inst).cond, map);
            }
            else if (inst instanceof RegisterInstruction)
            {
                if ((((RegisterInstruction)inst).op.equals("ADD") || ((RegisterInstruction)inst).op.equals("SUB")) && ((RegisterInstruction)inst).dest != null)
                    if (! (((RegisterInstruction)inst).dest instanceof ConstSymbol) && map.containsKey(((RegisterInstruction)inst).dest))
                        ((RegisterInstruction)inst).dest = copyObj(((RegisterInstruction)inst).dest, map);
                if (! (((RegisterInstruction)inst).src instanceof ConstSymbol) && map.containsKey(((RegisterInstruction)inst).src))
                    ((RegisterInstruction)inst).src = copyObj(((RegisterInstruction)inst).src, map);
            }
            else if (inst instanceof SystemCallInstruction)
            {
                if (! (((SystemCallInstruction)inst).arg0 instanceof ConstSymbol) && map.containsKey(((SystemCallInstruction)inst).arg0))
                    ((SystemCallInstruction)inst).arg0 = copyObj(((SystemCallInstruction)inst).arg0, map);
                if (((SystemCallInstruction)inst).op.equals("READSTR") || ((SystemCallInstruction)inst).op.equals("NEW"))
                    if (! (((SystemCallInstruction)inst).arg1 instanceof ConstSymbol) && map.containsKey(((SystemCallInstruction)inst).arg1))
                        ((SystemCallInstruction)inst).arg1 = copyObj(((SystemCallInstruction)inst).arg1, map);
            }
            else if (inst instanceof UnaryInstruction)
            {
                if (! (((UnaryInstruction)inst).dest instanceof ConstSymbol) && map.containsKey(((UnaryInstruction)inst).dest))
                    ((UnaryInstruction)inst).dest = copyObj(((UnaryInstruction)inst).dest, map);
                if (! (((UnaryInstruction)inst).src instanceof ConstSymbol) && map.containsKey(((UnaryInstruction)inst).src))
                    ((UnaryInstruction)inst).src = copyObj(((UnaryInstruction)inst).src, map);
            }
    }

    public static void clear()
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Clear IR fork '"+currentFork+"'"+Debug.STYLE_DEFAULT);
        fork.put(currentFork, new ArrayList<Instruction>());
    }

    public static void remove(String name)
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Remove IR fork '"+name+"'"+Debug.STYLE_DEFAULT);
        fork.remove(name);
    }

    public static void addBind(FuncSymbol func)
    {
        bind.add(func);
    }

    public static void removeBind()
    {
        bind.remove(bind.size()-1);
    }

    public static FuncSymbol getBind()
    {
        return bind.get(bind.size()-1);
    }

    public static FuncSymbol getBind(int index)
    {
        return bind.get(index);
    }

    public static Instruction add(Instruction inst)
    {
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add "+inst.getClass()+": "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(String op, String reg, ObjectSymbol src, String comment)
    {
        Instruction inst = new RegisterInstruction(op, reg, src, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add RegisterInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(String op, ObjectSymbol dest, String reg, ObjectSymbol src, String comment)
    {
        Instruction inst = new RegisterInstruction(op, dest, reg, src, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add RegisterInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(String op, ObjectSymbol dest, ObjectSymbol src0, ObjectSymbol src1, String comment)
    {
        Instruction inst = new BinaryInstruction(cmdBinary.get(op), dest, src0, src1, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add BinaryInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(String op, ObjectSymbol dest, ObjectSymbol src, String comment)
    {
        Instruction inst = new UnaryInstruction(cmdUnary.get(op), dest, src, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add UnaryInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(JumpSymbol symbol, String comment)
    {
        Instruction inst = new LabelInstruction(symbol.id, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add LabelInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(String label, String comment)
    {
        Instruction inst = new JumpInstruction(label, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add JumpInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(ObjectSymbol label, String comment)
    {
        Instruction inst = new JumpInstruction(label, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add JumpInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(String op, ObjectSymbol cond, String label, String comment)
    {
        Instruction inst = new BranchInstruction(op, cond, label, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add BranchInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction syscall(String op, ObjectSymbol arg, String comment)
    {
        if (arg instanceof ConstStringSymbol || arg.type instanceof ArraySymbol)
            arg = arg.addr;
        Instruction inst = new SystemCallInstruction(op, arg, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add SystemCallInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction syscall(String op, ObjectSymbol arg0, ObjectSymbol arg1, String comment)
    {
        if (arg0 instanceof ConstStringSymbol || arg0.type instanceof ArraySymbol)
            arg0 = arg0.addr;
        if (arg1 instanceof ConstStringSymbol || arg1.type instanceof ArraySymbol)
            arg1 = arg1.addr;
        Instruction inst = new SystemCallInstruction(op, arg0, arg1, comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add SystemCallInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static Instruction add(String comment)
    {
        Instruction inst = new Instruction(comment);
        if (Debug.ENABLE)
            System.err.println(Debug.STYLE_IR+"["+new Date().toString()+"] Add CommentInstruction: "+inst.toString()+Debug.STYLE_DEFAULT);
        fork.get(currentFork).add(inst);
        if (currentFork.equals("master") && bind.size() > 0)
            bind.get(bind.size()-1).list.add(inst);
        return inst;
    }

    public static List<Instruction> getInstructions()
    {
        return fork.get(currentFork);
    }

    public static List<Instruction> getInstructions(String branch)
    {
        return fork.get(branch);
    }

    public static String getCode()
    {
        String code = "";
        for (Instruction i : getInstructions())
        {
            try
            {
                code += i.toString()+"\n";
            }
            catch (Exception e)
            {
                System.err.println(code);
                throw e;
            }
        }
        return code;
    }

    public static String getCode(String name)
    {
        String code = "";
        for (Instruction i : getInstructions(name))
        {
            try
            {
                code += i.toString()+"\n";
            }
            catch (Exception e)
            {
                System.err.println(code);
                throw e;
            }
        }
        return code;
    }

    public static String getAddr(ObjectSymbol symbol)
    {
        if ((symbol instanceof ConstCharSymbol) || (symbol instanceof ConstIntSymbol))
            return symbol.toString();
        if (symbol.addr instanceof ConstIntSymbol)
            return (symbol.frame == 0 ? "&" : "$")+symbol.addr.toString();
        return "&"+getAddr(symbol.addr);
    }
}