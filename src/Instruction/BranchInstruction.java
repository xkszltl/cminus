import java.io.*;
import java.util.*;
import java.math.*;

public class BranchInstruction extends Instruction
{
    public String op;
    public ObjectSymbol cond;
    public String label;
    public String condReg = null;

    public BranchInstruction(String _op, ObjectSymbol _cond, String _label, String _comment)
    {
        super(_comment);
        op = _op;
        cond = _cond;
        label = _label;
    }

    public Instruction allocate(Map<ObjectSymbol, Integer> _alloc)
    {
        alloc = _alloc;
        condReg = (alloc.containsKey(cond) ? Arch.dataRegister.get(alloc.get(cond)) : null);
        return this;
    }

    public String toString()
    {
        return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_2,
            "BEQZ", IRGenerator.getAddr(cond)+(condReg != null ? "|"+condReg : "")+",", label), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
    }

    public BranchInstruction clone()
    {
        return new BranchInstruction(op, cond, label, comment);
    }
}