import java.io.*;
import java.util.*;
import java.math.*;

public class Instruction
{
    public String comment = "";
    public Map<ObjectSymbol, Integer> alloc = new HashMap<ObjectSymbol, Integer>();

    public Instruction(String _comment)
    {
        comment = _comment;
    }

    public Instruction allocate(Map<ObjectSymbol, Integer> _alloc)
    {
        alloc = _alloc;
        return this;
    }

    public String toString()
    {
        return "; "+(comment.length() <= IRGenerator.FORMAT_INST+IRGenerator.FORMAT_COMMENT+1 ? comment : comment.substring(0, IRGenerator.FORMAT_INST+IRGenerator.FORMAT_COMMENT-2)+"...");
    }

    public Instruction clone()
    {
        return new Instruction(comment);
    }
}