import java.io.*;
import java.util.*;
import java.math.*;

public class SystemCallInstruction extends Instruction
{
    public String op;
    public ObjectSymbol arg0;
    public ObjectSymbol arg1;
    public int vArg0 = 0, vArg1 = 0;
    public String argReg0 = null, argReg1 = null;

    public SystemCallInstruction(String _op, String _comment)
    {
        super(_comment);
        op = _op;
        arg0 = null;
        arg1 = null;
    }

    public SystemCallInstruction(String _op, ObjectSymbol _arg, String _comment)
    {
        super(_comment);
        op = _op;
        arg0 = _arg;
        arg1 = new ConstIntSymbol(0x7FFFFFFF);
    }

    public SystemCallInstruction(String _op, ObjectSymbol _arg0, ObjectSymbol _arg1, String _comment)
    {
        super(_comment);
        op = _op;
        arg0 = _arg0;
        arg1 = _arg1;
    }

    public Instruction allocate(Map<ObjectSymbol, Integer> _alloc)
    {
        alloc = _alloc;
        argReg0 = (alloc.containsKey(arg0) ? Arch.dataRegister.get(alloc.get(arg0)) : null);
        argReg1 = (alloc.containsKey(arg1) ? Arch.dataRegister.get(alloc.get(arg1)) : null);
        return this;
    }

    public String toString()
    {
        switch (op)
        {
        case "READ":
        case "READINT":
        case "READCHAR":
            return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_1,
                op, IRGenerator.getAddr(arg0)+(argReg0 != null ? "|"+argReg0 : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
        case "READSTR":
            return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_2,
                op, IRGenerator.getAddr(arg0)+(argReg0 != null ? "|"+argReg0 : "")+",", IRGenerator.getAddr(arg1)+(argReg1 != null ? "|"+argReg1 : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
        case "WRITE":
        case "WRITEINT":
        case "WRITECHAR":
        case "WRITESTR":
            return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_1,
                op, IRGenerator.getAddr(arg0)+(argReg0 != null ? "|"+argReg0 : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
        case "NEW":
            return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_2,
                op, IRGenerator.getAddr(arg0)+(argReg0 != null ? "|"+argReg0 : "")+",", IRGenerator.getAddr(arg1)+(argReg1 != null ? "|"+argReg1 : "")), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
        case "EXIT":
            return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format(IRGenerator.FORMAT_STR_0,
                op), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
        }
        return op;
    }

    public SystemCallInstruction clone()
    {
        return new SystemCallInstruction(op, arg0, arg1, comment);
    }
}