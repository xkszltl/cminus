import java.io.*;
import java.util.*;
import java.math.*;

public class LabelInstruction extends Instruction
{
    public String label;

    public LabelInstruction(String _label, String _comment)
    {
        super(_comment);
        label = _label;
    }

    public String toString()
    {
        return String.format(IRGenerator.FORMAT_STR_COMMENT, String.format("%-"+IRGenerator.FORMAT_LABEL+"s ",
            label+":"), (comment.equals("") ? "" : "; "+(comment.length() <= IRGenerator.FORMAT_COMMENT ? comment : comment.substring(0, IRGenerator.FORMAT_COMMENT-3)+"...")));
    }

    public LabelInstruction clone()
    {
        return new LabelInstruction(label, comment);
    }
}